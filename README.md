# dce-mri-qtools.m

Dynamic contrast enhanced MRI quantitative toolbox is a Matlab toolbox aimed to develop and validate quantitative workflows for DCE MRI. 

It provides tools for semi-automatic arterial input function (AIF) selection, signal delay correction, advanced DCE signal-to-concentration equations, partial-volume corrected AIF calculation, and pharmacokinetic modelling. These steps can be validated with provided MR signal simulations and a digital reference object creation tool. The method is described in the following paper:
- B. Bourassa-Moreau, R. Lebel, G. Gilbert, D. Mathieu, M. Lepage, "Robust arterial input function surrogate measurement from the superior sagittal sinus complex signal for fast dynamic contrast-enhanced MRI in the brain". Magn Reson Med. First published: 15 July 2021. https://doi.org/10.1002/mrm.28922
A previous commit was used in the following conference proceedings:
- B. Bourassa-Moreau, R. Lebel, G. Gilbert, D. Mathieu, M. Lepage, "Partial Volume Correction of the Arterial Input Function with Surrounding Tissue Signal for Dynamic Contrast Enhanced MRI in the Brain". Proc. Intl. Soc. Mag Reson. Med. 0010 (2020).

## Description of source files

Matlab source functions folder structure:

| **Description** | **src subfolder** |
| --- | --- |
| Region of interest and semi-automatic AIF selection | `roi` |
| Signal to concentration equations | `Fsignal2conc` |
| Tracer delay correction | `delay` |
| Partial-volume effect correction | `pve` |
| Arterial input function models and fitting | `aif` |
| Pharmacokinetic models and fitting | `pkm` |
| Digital reference object creation | `dro` |
| Concentration to signal equations | `Fconc2signal` |
| Signal noise models | `noise` |

The semi-automated AIF selection tool is based on the files "roi/inclusioncriteria*.m"  modified from:
- Peruzzo Denis, Bertoldo Alessandra, Zanderigo Francesca and Cobelli Claudio, “Automatic selection of arterial input function on dynamic contrast-enhanced MR images”, Computer methods and programs in biomedicine, 104:e148-e157 (2011).

## Getting started

Run startup.m to add project paths to Matlab. Installation can be verified by running the test suite: 
```
testResults = dcemriqtoolsruntests;
```

### Example

This code was developed by @b3nbm and distributed in association with the work presented in the research article "Robust arterial input function surrogate measurement from the superior sagittal sinus complex signal for fast dynamic contrast-enhanced MRI in the brain" published in Magnetic Resonance in Medicine. Example scripts for the digital reference object analysis presented in this work are available under:
- `scripts\MRM_28922_RobustCpxAif\vofBiasPaperInflow.m`: This script reproduce the simulation figures of the research article with latest commit.
- `scripts\ISMRM2020_AIF_PVE\droAifPvePresentation.m`: This script reproduce the figures of the oral prensentation with the digital reference object modified to better match patient data (Cpeak = 5 mM). When it is run for the first time, it requires the user input to define the venous and arterial rows over the DRO image when using the commit of 2020-07-23 (fcb1d1dcdd5bdff950bbd4e8f92323b31372df2b). 
- `scripts\ISMRM2020_AIF_PVE\aifPveDroAnalysisScipt.m`: This old script will enable to reproduce the Figures 2 and 3 of the abstract when using the commit of 2019-11-22 (5d119b06424bad621ffe220ae57a4983bd6234e8). 

The partial test suite (tests subfolder) also provides unit tests that shows the usage of most functions. The processing steps of the partial volume corrected AIF are shown below:

![aifPvePipeline](aifPvePipeline.png)

## Usage

This tool is distributed to help reproducibility in quantitative DCE research. No medical decision should be made using it.

## BUGS

Please report any bugs to <benoit.bourassa.moreau@usherbrooke.ca>