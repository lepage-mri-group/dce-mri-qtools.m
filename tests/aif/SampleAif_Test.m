classdef SampleAif_Test < matlab.unittest.TestCase
    
    properties
        Time
        RedTimeSample
        SampledAifOrig
        ModelAifOrig
        ModelAif
        VectorAif
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.Time = (0:1:300)/60; % min
            aifModel.FHandle = @aifhorsfield;
            aifModel.pValues = [0/60,10.06, 0.33, 0.37,16.02, 1.17, 0.11,5.26,0.032,0.129,3.0214];
            aifModel.minXIdx = 1;
            
            testCase.SampledAifOrig = aifModel.FHandle(testCase.Time,aifModel.pValues);
            testCase.ModelAifOrig = sampleaif(testCase.Time,aifModel,testCase.Time);
            
            testCase.RedTimeSample = (-0.5:5:200)/60;
            testCase.VectorAif = sampleaif(testCase.Time,testCase.SampledAifOrig,testCase.RedTimeSample);
            testCase.ModelAif = sampleaif(testCase.Time,aifModel,testCase.RedTimeSample);
        end
    end
    
    methods (Test)
        function testSamplingInputs(testCase)
            meanSquaredError = sum((testCase.VectorAif-testCase.ModelAif).^2)/sum(testCase.ModelAif);
            maxError = 1e-4;
            testCase.verifyLessThan(meanSquaredError,maxError)
        end
        function testModelSampling(testCase)
            testCase.verifyEqual(testCase.ModelAifOrig,testCase.SampledAifOrig)
        end
    end
end