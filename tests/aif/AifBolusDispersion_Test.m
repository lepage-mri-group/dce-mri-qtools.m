classdef AifBolusDispersion_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        AifDefault
        DelayedAif
        RefMild
        RefSev
        DispMdlMild
        DispMdlSev
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            vtfTime = (0:0.1:6); % sec
            testCase.Time = (0:0.5:300)/60; % min
            testCase.InitTime = 1; % min
            testCase.AifDefault = aifhorsfield(testCase.Time,testCase.InitTime);
            
            % Reference data from the QIBA DRO source code (JSim model - ToftsKermode_two_parameter_20120103.proj).(https://sites.duke.edu/dblab/qibacontent/).
            fig2WillatsMild = [0.041881992 0.0073287090;0.133574198 0.0087045530;0.202239733 0.012003124;0.204158622 0.388182182;0.205693734 0.689125429;0.207213494 0.906773529;0.20784289 0.976636451;0.209101681 1.116362294;0.21748339 1.434798136;0.228137063 1.703534744;0.31046509 1.658859164;0.323313972 1.62868271;0.348643311 1.576389139;0.375170037 1.497902729;0.389216306 1.441533437;0.402065189 1.411356984;0.415282499 1.373121196;0.428131382 1.342944743;0.441348691 1.304708954;0.454197574 1.274532501;0.467691203 1.230252212;0.480447979 1.202090592;0.506145745 1.141737686;0.520468335 1.079323893;0.533133004 1.053177107;0.546810847 1.00486715;0.55938341 0.980735198;0.584896962 0.924411959;0.618853627 0.85065753;0.65250327 0.783619214;0.697957919 0.709239227;0.743535378 0.632172795;0.796143647 0.56856929;0.844683871 0.510323324;0.868815823 0.484222592;0.916527086 0.44411013;0.940659038 0.418009398;1.000206011 0.369883654;1.083148082 0.311775848;1.118563103 0.289750837;1.189270336 0.248387259;1.457147282 0.159114772;1.619347157 0.123492511;1.75796772 0.10188198;1.815626506 0.095060328;1.942503466 0.079448245;2.034518046 0.073772170;2.126486572 0.069103513;2.287673274 0.055644423;2.379549693 0.052990599;2.47138006 0.051344192;2.632382548 0.041914770;2.724120807 0.042283197;2.815997227 0.039629373;2.90778154 0.038990383;2.9995198 0.039358810;3.056856212 0.039589076;3.183364746 0.032036328;3.3439067 0.032681075;3.412710395 0.032957395;3.550317784 0.033510035;3.642056044 0.033878462;3.733794304 0.034246889;3.791130716 0.034477155;3.917270823 0.034983742;3.986074518 0.035260062;4.100747343 0.035720596;4.169551037 0.035996916;4.307388693 0.031512471;4.395442686 0.028843296;4.491003373 0.029227074;4.559807068 0.029503394;4.674479892 0.029963928;4.743283587 0.030240248;4.857956412 0.030700781;4.926760106 0.030977101;5.041432931 0.031437635;5.121703908 0.031760008;5.247844015 0.032266595;5.31664771 0.032542915;5.500124229 0.033279768;5.637731619 0.033832408;5.706535314 0.034108729;5.821208138 0.034569262;5.912946398 0.034937689;5.981842199 0.033199175];
            testCase.RefMild.mvtt = 0.607; % s
            testCase.RefMild.vvtt = 0.632; % s^2
            testCase.RefMild.vtf = interp1(fig2WillatsMild(:,1),fig2WillatsMild(:,2),vtfTime,'linear',0);
            
            fig2WillatsSevere = [0.042987253 0.0039106310;0.134755168 0.0050317980;0.226523084 0.0061529650;0.318302546 0.0082844540;0.410116647 0.013446904;0.501907655 0.016588713;0.593698664 0.019730522;0.685570498 0.029944576;0.777511611 0.046220555;0.869452724 0.062496533;0.961416931 0.080793154;1.053461963 0.106162019;1.145576274 0.137592809;1.237655945 0.165992637;1.318246826 0.192694741;1.398922382 0.226805864;1.479621031 0.262937628;1.560246552 0.292670695;1.640887468 0.323750855;1.721501442 0.352473601;1.813581113 0.380873429;1.905637692 0.407252615;1.997613445 0.426559556;2.089554558 0.442835535;2.181449485 0.45507023;2.27325204 0.45922236;2.365008409 0.459333207;2.456718592 0.45540277;2.548336402 0.443389768;2.63993112 0.429356124;2.731502744 0.413301838;2.823062822 0.396237232;2.914565168 0.374121022;3.006067513 0.352004812;3.097558312 0.328878281;3.189014471 0.302720788;3.280493723 0.278583937;3.371961429 0.253436764;3.463440681 0.229299913;3.55496612 0.209204344;3.646480012 0.188098455;3.738016997 0.169013207;3.829553982 0.14992796;3.921125606 0.133873674;4.012697231 0.117819389;4.104291948 0.103785745;4.195909759 0.091772742;4.287573755 0.083801023;4.379249298 0.076839624;4.470890202 0.066847263;4.562519559 0.055844581;4.654206649 0.049893504;4.745916832 0.045963067;4.837592375 0.039001668;4.929291011 0.034060911;5.02101274 0.031140796;5.112757563 0.030241322;5.204456199 0.025300564;5.296177928 0.022380449;5.387934297 0.022491295;5.479690666 0.022602142;5.571447035 0.022712989;5.663203404 0.022823835;5.75493668 0.020914040;5.846623769 0.014962962;5.938380138 0.015073809;5.995681683 0.011101805];
            testCase.RefSev.mvtt = 4.572; % s
            testCase.RefSev.vvtt = 0.853; % s^2
            testCase.RefSev.vtf = interp1(fig2WillatsSevere(:,1),fig2WillatsSevere(:,2),vtfTime,'linear',0);
            
            
            testCase.DispMdlMild.sigma = 1.0; % s
            testCase.DispMdlMild.mu = -1.0; % s
            testCase.DispMdlMild.t0 = 0.2; % s
            testCase.DispMdlMild.time = vtfTime; % s
            [~,testCase.DispMdlMild] = aifbolusdispersion(testCase.Time,testCase.AifDefault,testCase.DispMdlMild);
            
            testCase.DispMdlSev.sigma = 0.2; % s
            testCase.DispMdlSev.mu = 1.5; % s
            testCase.DispMdlSev.t0 = -2; % Different than the -1.1 s from Willats.
            testCase.DispMdlSev.time = vtfTime; % s
            [testCase.DelayedAif,testCase.DispMdlSev] = aifbolusdispersion(testCase.Time,testCase.AifDefault,testCase.DispMdlSev);
        end
    end
    
    methods (Test)
        function testReferenceVttCurves(testCase)
            maxNormSumOfSquares = 0.01;
            normSumOfSquares = sum((testCase.DispMdlMild.vtf-testCase.RefMild.vtf).^2)/length(testCase.RefMild.vtf);
            testCase.verifyLessThan(normSumOfSquares,maxNormSumOfSquares)
            normSumOfSquares = sum((testCase.DispMdlSev.vtf-testCase.RefSev.vtf).^2)/length(testCase.RefSev.vtf);
            testCase.verifyLessThan(normSumOfSquares,maxNormSumOfSquares)
        end
        function testReferenceMvtt(testCase)
            mvttPrecision = 0.001;
            testCase.verifyLessThan(abs(testCase.DispMdlMild.mvtt-testCase.DispMdlMild.mvtt),mvttPrecision)
            testCase.verifyLessThan(abs(testCase.DispMdlSev.mvtt-testCase.RefSev.mvtt),mvttPrecision)
        end
        function testReferenceVvtt(testCase)
            mvttPrecision = 0.001;
            testCase.verifyLessThan(abs(testCase.DispMdlMild.vvtt-testCase.DispMdlMild.vvtt),mvttPrecision)
            testCase.verifyLessThan(abs(testCase.DispMdlSev.vvtt-testCase.RefSev.vvtt),mvttPrecision)
        end
        function testDelayedAif(testCase)
            [~,defaultIdx] = max(testCase.AifDefault);
            [~,delayedIdx] = max(testCase.DelayedAif);
            delayedDiff = (testCase.Time(delayedIdx) - testCase.Time(defaultIdx))*60;
            expectedDelay = testCase.DispMdlSev.t0 + testCase.DispMdlSev.mvtt;
            diffDelayErrorThresh = 0.05;
            diffDelayError = abs(delayedDiff - expectedDelay)/expectedDelay;
            testCase.verifyLessThan(diffDelayError,diffDelayErrorThresh);
        end
    end
end