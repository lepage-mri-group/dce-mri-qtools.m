classdef AifRect_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        ConcDefault
    end
    
    properties (TestParameter)
        % Call the function with the same default parameters but sequential
        % input types
        rectDefaultParameters = {31/60, ... % t0, min
            struct('t0',31/60,'tf',37/60,'At',0.2),...
            [31/60,37/60,0.2],...
            [31/60,37/60,0.2]};
        % [t0,min],[tf,min],[A,mM min]
        varParameters = {[],[],[],[true true false]};
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.Time = (0:2:300)/60; % [min]
            testCase.InitTime = testCase.rectDefaultParameters{1}; % min
            
            testCase.ConcDefault = aifrect(testCase.Time,testCase.InitTime);
        end
    end
    
    methods (Test, ParameterCombination='sequential')
        function testAifInputType(testCase,rectDefaultParameters,varParameters)
            if isempty(varParameters)
                concAifTest = aifrect(testCase.Time,rectDefaultParameters);
            else
                varParam = rectDefaultParameters(varParameters);
                fixParam = rectDefaultParameters(~varParameters);
                
                concAifTest = aifrect(testCase.Time,varParam,fixParam,varParameters);
            end
            testCase.verifyEqual(testCase.ConcDefault,concAifTest);
        end
    end
    
    methods (Test)
        function testDefaultAif(testCase)
            aifInteger = trapz(testCase.Time,testCase.ConcDefault);
            atDiff = abs(aifInteger-testCase.rectDefaultParameters{2}.At);
            maxAtDiff = 1e-10;
            testCase.verifyLessThan(atDiff,maxAtDiff)
        end
    end
end