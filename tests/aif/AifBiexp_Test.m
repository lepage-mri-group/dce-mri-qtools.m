classdef AifBiexp_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        ConcReference
        ConcDefault
    end
    
    properties (TestParameter)
        % Call the function with the same default parameters but sequential
        % input types
        biexpDefaultParameters = {80.5/60, ... % t0, min
            struct('t0',80.5/60,'tf',128/60,'A',0.7857,'wd',140/60,'we',62/60,'td',26/60,'te',25),...
            [80.5/60,128/60,0.7857,140/60,62/60,26/60,25],...
            [80.5/60,128/60,0.7857,140/60,62/60,26/60,25]};
        % [t0,min],[tf,min],[A,-],[wd,min],[we,min],[td,min],[te,min]
        varParameters = {[],[],[],[false true false true true true true]};
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.Time = (0:5:720)/60; % min
            testCase.InitTime = testCase.biexpDefaultParameters{1}; % min
            
            % Reference data from Fig. 4 of (Pellerin et al., 2007))
            fig4Pellerin = [0.200897731 0.00333789700;1.307083066 0.00558395900;1.327028371 0.0701873110;1.408026938 0.212527965;1.49029942 0.360718236;1.567051609 0.483560170;1.645502349 0.614201593;1.692002952 0.692154878;1.732870486 0.785790327;1.805376295 0.889133542;1.883402397 1.017825092;1.950812552 1.097769843;2.023318362 1.201113058;2.083900212 1.281043945;2.133619868 1.329089617;2.14968001 1.290821034;2.181386482 1.238160626;2.260579224 1.142616899;2.30852423 1.070499365;2.338498052 1.041223556;2.421512536 0.963228677;2.508773398 0.904732518;2.596883537 0.850136102;2.73721426 0.786152753;2.932011577 0.727665032;3.208748549 0.669343686;3.503301503 0.635423996;3.867177345 0.607736434;4.382315731 0.586447375;5.278799328 0.559841251;6.149356164 0.541304310;7.482504636 0.513554358;8.598187206 0.491454221;10.47621358 0.456688792;12.06849101 0.427434533];
            testCase.ConcReference = interp1(fig4Pellerin(:,1),fig4Pellerin(:,2),testCase.Time,'linear',0);
            testCase.ConcDefault = aifbiexp(testCase.Time,testCase.InitTime);
        end
    end
    
    methods (Test, ParameterCombination='sequential')
        function testAifInputType(testCase,biexpDefaultParameters,varParameters)
            if isempty(varParameters)
                concAifTest = aifbiexp(testCase.Time,biexpDefaultParameters);
            else
                varParam = biexpDefaultParameters(varParameters);
                fixParam = biexpDefaultParameters(~varParameters);
                
                concAifTest = aifbiexp(testCase.Time,varParam,fixParam,varParameters);
            end
            testCase.verifyEqual(testCase.ConcDefault,concAifTest);
        end
    end
    
    methods (Test)
        function testReferenceAif(testCase)
            normSumOfSquares = sum((testCase.ConcDefault-testCase.ConcReference).^2)/length(testCase.ConcDefault);
            maxNormSumOfSquares = 0.01;
            testCase.verifyLessThan(normSumOfSquares,maxNormSumOfSquares)
        end
    end
end