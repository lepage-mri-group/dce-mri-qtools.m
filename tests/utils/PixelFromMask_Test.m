classdef PixelFromMask_Test < matlab.unittest.TestCase
    
    properties
        TrueVoxelList
        TestVoxelList
        TestMask
    end
    
    methods (TestMethodSetup)
        function runDefineTestValues(testCase)
            % Direct method: 3D mask from voxel coordinates
            rawVoxelList = [1 1 1; 1 11 1; 1 2 3; 10 15 30; 17 23 57; 49 53 1];
            nVoxels = size(rawVoxelList,1);
            nDimLength = 64;
            testCase.TestMask = false(nDimLength,nDimLength,nDimLength);
            for iVox = 1:nVoxels
                testCase.TestMask(rawVoxelList(iVox,1),rawVoxelList(iVox,2),rawVoxelList(iVox,3)) = true;
            end
            
            % Voxel list order must match pixelfrommask order. Currently
            % only sorted for 3rd dim.
            [~,sortIdx] = sort(rawVoxelList(:,3));
            testCase.TrueVoxelList = rawVoxelList(sortIdx,:);
            
            % Direct method: Voxel coordinates from 3D mask
            testCase.TestVoxelList = pixelfrommask(testCase.TestMask);
        end
    end
    
    methods (Test)
        function testEqualLists(testCase)
            testCase.verifyEqual(testCase.TrueVoxelList,testCase.TestVoxelList)
        end
    end
end