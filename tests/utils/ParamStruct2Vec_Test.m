classdef ParamStruct2Vec_Test < matlab.unittest.TestCase
    
    properties
        TestVector
        TestStructureList
        TestStructure
        ComputedVector
    end
    
    methods (TestMethodSetup)
        function runDefineTestValues(testCase)
            testCase.TestVector = [pi 10.1 -15.5 1+1j];
            testCase.TestStructureList = {'Rad','Vector(1)','Vector(2)','Complex'};
            testCase.TestStructure.Rad = testCase.TestVector(1);
            testCase.TestStructure.Vector(1) = testCase.TestVector(2);
            testCase.TestStructure.Vector(2) = testCase.TestVector(3);
            testCase.TestStructure.Complex = testCase.TestVector(4);
            testCase.ComputedVector = paramstruct2vec(testCase.TestStructure,testCase.TestStructureList);
        end
    end
    
    methods (Test)
        function testVectors(testCase)
            testCase.verifyEqual(testCase.ComputedVector,testCase.TestVector)
        end
    end
end