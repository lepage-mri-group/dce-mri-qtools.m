classdef Dyn3dConversion_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        ConcT
        ParamDefaultStruct
        ParamDefaultVec
        nX = 32;
        nY = 16;
        nZ = 8;
        nE = 4;
        Mask
        iYMask
        iZMask
        iEMask
    end
    
    properties (TestParameter)
        slicedMat = {[1,32;1,1;1,1;1,1],...
            [1,32;1,16;1,1;1,1],...
            [1,32;1,16;1,8;1,1],...
            [1,32;1,16;1,8;1,4],...
            [1,32;1,1;1,1;1,1],...
            [1,32;1,16;1,1;1,1],...
            [1,32;1,16;1,8;1,1],...
            [1,32;1,16;1,8;1,4]};
        permuteDim = {[1 5 2 3 4],[1 2 5 3 4],[1 2 3 5 4],[1 2 3 4 5],...
            [5 1 2 3 4],[5 1 2 3 4],[5 1 2 3 4],[5 1 2 3 4]};
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
%             testCase.nX = 32;
%             testCase.nY = 16;
%             testCase.nZ = 8;
%             testCase.nE = 4;
            testCase.Time = (0:5:720)/60; % min
            testCase.InitTime = 80.5/60; % min
            
            concAif = aifbiexp(testCase.Time,testCase.InitTime);
            testCase.Mask = false(testCase.nX,testCase.nY,testCase.nZ,testCase.nE);
            testCase.iYMask = 7;
            testCase.iZMask = 3;
            testCase.iEMask = 3;
            testCase.Mask(1:2:32,1:3:16,[1 3 5],[1 3]) = true;

            
            nT = length(testCase.Time);
            testCase.ConcT = zeros(testCase.nX,testCase.nY,testCase.nZ,testCase.nE,nT);
            for iX = 1:testCase.nX
                for iY = 1:testCase.nY
                    for iZ = 1:testCase.nZ
                        for iE = 1:testCase.nE
                            testCase.ConcT(iX,iY,iZ,iE,:) = concAif + (iX-1)*testCase.nY*testCase.nZ*testCase.nE + (iY-1)*testCase.nZ*testCase.nE+iZ*testCase.nE+iE;
                        end
                    end
                end
            end
        end
    end
    
    methods (Test, ParameterCombination='sequential')
        function testMask(testCase,slicedMat,permuteDim)
            nT = length(testCase.Time);
            currentConc = permute(testCase.ConcT(slicedMat(1,1):slicedMat(1,2),slicedMat(2,1):slicedMat(2,2),slicedMat(3,1):slicedMat(3,2),slicedMat(4,1):slicedMat(4,2),:),permuteDim);
            currentMask = squeeze(permute(testCase.Mask(slicedMat(1,1):slicedMat(1,2),slicedMat(2,1):slicedMat(2,2),slicedMat(3,1):slicedMat(3,2),slicedMat(4,1):slicedMat(4,2)),permuteDim));
            
            [dynMat, nIdxT] = dyn3dconversion(currentConc,currentMask);
            [dynImg] = mat2dyn3d(dynMat,nIdxT,currentMask);
            nTotData = numel(dynMat);
            nTotTheo = sum(currentMask(:))*nT;
            testCase.verifyEqual(nTotTheo,nTotData);
            repDynMat = ones(1,5);
            repDynMat(nIdxT) = nT;
            if nIdxT == 1
                dynMask = repmat(permute(currentMask,[6 1 2 3 4 5]),repDynMat);
            else
                dynMask = repmat(currentMask,repDynMat);
            end
            testCase.verifyEqual(currentConc(dynMask),dynImg(dynMask));
        end
    end
    methods (Test)
        function testPixelMask(testCase)
            % Check pixel list for 3D mask case
            currentSlicedMat = [1,32;1,16;1,8;1,1];
            currentPermuteDim = [1 2 3 5 4];
            nT = length(testCase.Time);
            currentConc = permute(testCase.ConcT(currentSlicedMat(1,1):currentSlicedMat(1,2),currentSlicedMat(2,1):currentSlicedMat(2,2),currentSlicedMat(3,1):currentSlicedMat(3,2),currentSlicedMat(4,1):currentSlicedMat(4,2),:),currentPermuteDim);
            currentMask = squeeze(permute(testCase.Mask(currentSlicedMat(1,1):currentSlicedMat(1,2),currentSlicedMat(2,1):currentSlicedMat(2,2),currentSlicedMat(3,1):currentSlicedMat(3,2),currentSlicedMat(4,1):currentSlicedMat(4,2)),currentPermuteDim));
            nPixelMask = sum(currentMask(:));
            pixelList = zeros(nPixelMask+1,3);
            pixelList(1:nPixelMask,:) = pixelfrommask(currentMask);
            pixelList(nPixelMask+1,:) = pixelList(1,:);
            [dynMat, nIdxT] = dyn3dconversion(currentConc,currentMask,pixelList);
            
            [dynImg] = mat2dyn3d(dynMat(1:nPixelMask,:),nIdxT,currentMask);
            nTotData = numel(dynMat);
            nTotTheo = (nPixelMask+1)*nT;
            testCase.verifyEqual(nTotTheo,nTotData);
            repDynMat = ones(1,5);
            repDynMat(nIdxT) = nT;
            if nIdxT == 1
                dynMask = repmat(permute(currentMask,[6 1 2 3 4 5]),repDynMat);
            else
                dynMask = repmat(currentMask,repDynMat);
            end
            testCase.verifyEqual(currentConc(dynMask),dynImg(dynMask));
            testCase.verifyEqual(dynMat(1,:),dynMat(end,:));
        end
    end
end