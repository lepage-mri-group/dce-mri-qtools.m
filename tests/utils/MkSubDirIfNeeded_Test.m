classdef MkSubDirIfNeeded_Test < matlab.unittest.TestCase
    
    properties
        TestFuncPath
        TestDirName
        TestDirPath
    end
    
    methods (TestMethodSetup)
        function runDefineTestValues(testCase)
            testCase.TestFuncPath = fileparts(mfilename('fullpath'));
            testCase.TestDirName = 'mkDirTestDir';
            testCase.TestDirPath = fullfile(testCase.TestFuncPath,testCase.TestDirName);
        end
    end
    
    methods (TestMethodTeardown)
        function removeCreatedDir(testCase)
            rmdir(testCase.TestDirPath);
        end
    end
    
    methods (Test)
        function testEqualLists(testCase)
            fullDirPath = mksubdirifneeded(testCase.TestFuncPath,testCase.TestDirName);
            % Twice to make sure no warnings if directory exists.
            fullDirPath = mksubdirifneeded(testCase.TestFuncPath,testCase.TestDirName);
            
            testCase.verifyEqual(testCase.TestDirPath,fullDirPath);
        end
    end
end