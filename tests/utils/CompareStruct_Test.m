classdef CompareStruct_Test < matlab.unittest.TestCase
    
    properties
        DefaultStruct
        CorrectedStruct
        MissingFieldList
    end
    
    methods (TestMethodSetup)
        function runCompareStruct(testCase)
            checkStruct.a = 1;
            checkStruct.b = 2;
            checkStruct.c.a = 1;
            checkStruct.c.f.a = 2*pi;
            
            testCase.DefaultStruct = checkStruct;
            testCase.DefaultStruct.First = 'Hello';
            testCase.DefaultStruct.c.Second = 3;
            testCase.DefaultStruct.c.f.Third = pi;
            
            [testCase.CorrectedStruct, testCase.MissingFieldList]= comparestruct(checkStruct,testCase.DefaultStruct,false);
        end
    end
    
    methods (Test)
        function testAssignedFirstField(testCase)
            defaultStruct =  testCase.DefaultStruct;
            correctedStruct =  testCase.CorrectedStruct;
            
            testCase.verifyEqual(correctedStruct.First,defaultStruct.First)
        end
        function testAssignedSecondField(testCase)
            defaultStruct =  testCase.DefaultStruct;
            correctedStruct =  testCase.CorrectedStruct;
            
            testCase.verifyEqual(correctedStruct.c.Second,defaultStruct.c.Second)
        end
        function testAssignedThirdField(testCase)
            defaultStruct =  testCase.DefaultStruct;
            correctedStruct =  testCase.CorrectedStruct;
            
            testCase.verifyEqual(correctedStruct.c.f.Third,defaultStruct.c.f.Third)
        end
        function testFieldList(testCase)
            missingFieldList = testCase.MissingFieldList;
            trueMissingFieldList = {'.c.f.Third','.c.Second','.First'};
            
            testCase.verifyEqual(missingFieldList{1},trueMissingFieldList{1})
            testCase.verifyEqual(missingFieldList{2},trueMissingFieldList{2})
            testCase.verifyEqual(missingFieldList{3},trueMissingFieldList{3})
        end
    end
end