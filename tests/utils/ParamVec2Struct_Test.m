classdef ParamVec2Struct_Test < matlab.unittest.TestCase
    
    properties
        TestVector
        TestStructureList
        TestStructure
        ComputedStructure
    end
    
    methods (TestMethodSetup)
        function runDefineTestValues(testCase)
            testCase.TestVector = [pi 10.1 -15.5 1+1j];
            testCase.TestStructureList = {'Rad','Vector(1)','Vector(2)','Complex'};
            testCase.TestStructure.Rad = testCase.TestVector(1);
            testCase.TestStructure.Vector(1) = testCase.TestVector(2);
            testCase.TestStructure.Vector(2) = testCase.TestVector(3);
            testCase.TestStructure.Complex = testCase.TestVector(4);
            testCase.ComputedStructure = paramvec2struct(testCase.TestVector,testCase.TestStructureList);
        end
    end
    
    methods (Test)
        function testFirstInput(testCase)
            testCase.verifyEqual(testCase.TestStructure.Rad,testCase.ComputedStructure.Rad)
        end
        function testVector(testCase)
            testCase.verifyEqual(testCase.TestStructure.Vector,testCase.ComputedStructure.Vector);
        end
        function testComplex(testCase)
            testCase.verifyEqual(testCase.TestStructure.Complex,testCase.ComputedStructure.Complex);
        end
    end
end