classdef ManageFlexibleInput_Test < matlab.unittest.TestCase
    
    properties
        TestVector
        TestOrder
        TestStructureList
        VararginInitTime
        VararginStructure
        VararginVector
        VararginVectorFixed
    end
    
    methods (TestMethodSetup)
        function runDefineTestValues(testCase)
            testCase.TestStructureList = {'Rad','Vector(1)','Vector(2)','Complex'};
            testCase.TestVector = [pi 10.1 -15.5 1+1j];
            testCase.TestOrder = [true, false, false true];
            
            structure.Rad = testCase.TestVector(1);
            structure.Vector(1) = testCase.TestVector(2);
            structure.Vector(2) = testCase.TestVector(3);
            structure.Complex = testCase.TestVector(4);
            
            variableVector = testCase.TestVector(testCase.TestOrder);
            fixedVector = testCase.TestVector(~testCase.TestOrder);
            
            testCase.VararginInitTime = {10/60};
            testCase.VararginStructure = {structure};
            testCase.VararginVector = {testCase.TestVector};
            testCase.VararginVectorFixed = {variableVector, fixedVector, testCase.TestOrder};
        end
    end
    
    methods (Test)
        function testInputTime(testCase)
            paramVector = manageflexibleinput(testCase.VararginInitTime,testCase.TestStructureList);
            
            testCase.verifyEmpty(paramVector)
        end
        function testInputVector(testCase)
            paramVector = manageflexibleinput(testCase.VararginVector,testCase.TestStructureList);
            
            testCase.verifyEqual(paramVector,testCase.TestVector)
        end
        function testInputStructure(testCase)
            paramVector = manageflexibleinput(testCase.VararginStructure,testCase.TestStructureList);
            
            testCase.verifyEqual(paramVector,testCase.TestVector)
        end
        function testInputVariableFixed(testCase)
            paramVector = manageflexibleinput(testCase.VararginVectorFixed,testCase.TestStructureList);
            
            testCase.verifyEqual(paramVector,testCase.TestVector)
        end
    end
end