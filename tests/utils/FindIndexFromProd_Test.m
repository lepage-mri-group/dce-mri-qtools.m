classdef FindIndexFromProd_Test < matlab.unittest.TestCase
    
    methods (Test)
        function testAssignedFirstField(testCase)
            individualLength = [2, 3, 4,5];
            nIdx = length(individualLength);
            nRep = prod(individualLength);
            
            % Direct method
            iRep=0;
            expectedValues = NaN(nRep,nIdx);
            for h=1:individualLength(4)
                for i=1:individualLength(3)
                    for j=1:individualLength(2)
                        for k=1:individualLength(1)
                            iRep=iRep+1;
                            expectedValues(iRep,:) =[k,j,i,h];
                        end
                    end
                end
            end
            
            % Inverse method
            individualIndex = NaN(nRep,nIdx);
            for iRep = 1:nRep
                individualIndex(iRep,:) = findindexfromprod(iRep,individualLength);
            end
            
            testCase.verifyEqual(individualIndex,expectedValues)
        end
    end
end