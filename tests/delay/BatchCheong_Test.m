classdef BatchCheong_Test < matlab.unittest.TestCase
    
    properties
        ShiftStep
        Time
        TimeDiff
        NPre
        InitTime
        ConcReference
        DelayedTime
        ConcDelayed
        ConcBiexp
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.ShiftStep = 0.5/60;
            testCase.TimeDiff = 2/60;
            testCase.Time = 0:testCase.TimeDiff:(300/60); % min
            testCase.InitTime = 1; % min
            testCase.DelayedTime = (45:5:75)'/60;
            testCase.NPre = find(testCase.Time<min(testCase.DelayedTime),1,'last');
            
            hosrfieldParamRef = struct('t0',testCase.InitTime,'A',[10.06, 0.33, 0.37],'m',[16.02, 1.17, 0.11],'alpha',5.26,'beta',0.032,'tau',0.129,'kn',1,'k',3.0214);
            testCase.ConcReference = aifhorsfield(testCase.Time,hosrfieldParamRef);
            
            nDelay = length(testCase.DelayedTime);
            nT = length(testCase.Time);
            testCase.ConcDelayed = NaN(nDelay,nT);
            for iDelay = 1:nDelay
                hosrfieldParam = struct('t0',testCase.DelayedTime(iDelay),'A',[10.06, 0.33, 0.37],'m',[16.02, 1.17, 0.11],'alpha',5.26,'beta',0.032,'tau',0.129,'kn',1,'k',3.0214);
                testCase.ConcDelayed(iDelay,:) = aifhorsfield(testCase.Time',hosrfieldParam);
            end
            
            testCase.ConcBiexp = NaN(nDelay,nT);
            for iDelay = 1:nDelay
                testCase.ConcBiexp(iDelay,:) = aifschabel(testCase.Time',testCase.DelayedTime(iDelay));
            end
        end
    end
    
    methods (Test)
        function testReferenceAif(testCase)
            [bat, ~] = batcheong(testCase.ConcDelayed,testCase.Time,testCase.NPre,testCase.ShiftStep);
            smallerThanTimeRes = abs(bat-testCase.DelayedTime);
            constantDelayError = std(bat-testCase.DelayedTime);
            maxStd = 1e-10;
            testCase.verifyLessThan(constantDelayError,maxStd);
            testCase.verifyLessThan(smallerThanTimeRes,1.0005*testCase.TimeDiff);
        end
        function testBiexpAif(testCase)
            [bat, ~] = batcheong(testCase.ConcBiexp,testCase.Time,testCase.NPre,testCase.ShiftStep);
            smallerThanTimeRes = abs(bat-testCase.DelayedTime);
            constantDelayError = std(bat-testCase.DelayedTime);
            maxStd = 2*testCase.ShiftStep;
            testCase.verifyLessThan(constantDelayError,maxStd);
            testCase.verifyLessThan(smallerThanTimeRes,1.0005*testCase.TimeDiff);
        end
    end
end