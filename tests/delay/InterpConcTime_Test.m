classdef InterpConcTime_Test < matlab.unittest.TestCase
    
    properties
        ShiftStep
        Time
        TimeDiff
        NPre
        InitTime
        Aif
        ConcReference
        DelayedTime
        ConcDelayed
        IdxGap
        TimeGap
        ConcGapDelayed
        RefBat
        Bat
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.ShiftStep = 0.5/60;
            testCase.TimeDiff = 2/60;
            testCase.Time = 0:testCase.TimeDiff:(600/60); % min
            testCase.InitTime = 1; % min
            testCase.DelayedTime = (45:5:75)'/60;
            testCase.NPre = find(testCase.Time<min(testCase.DelayedTime),1,'last');
            
            testCase.Aif.param.FHandle = @aifhorsfield;
            hosrfieldParamRef = struct('t0',testCase.InitTime,'A',[10.06, 0.33, 0.37],'m',[16.02, 1.17, 0.11],'alpha',5.26,'beta',0.032,'tau',0.129,'kn',1,'k',3.0214);
            horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
            testCase.Aif.pValues = manageflexibleinput({hosrfieldParamRef},horsfieldStructFieldOrder);
            testCase.Aif.minXIdx = 1;
            testCase.ConcReference = testCase.Aif.param.FHandle(testCase.Time,hosrfieldParamRef);
            
            nDelay = length(testCase.DelayedTime);
            nT = length(testCase.Time);
            
            testCase.ConcDelayed = NaN(nDelay,nT);
            for iDelay = 1:nDelay
                hosrfieldParam = struct('t0',testCase.DelayedTime(iDelay),'A',[10.06, 0.33, 0.37],'m',[16.02, 1.17, 0.11],'alpha',5.26,'beta',0.032,'tau',0.129,'kn',1,'k',3.0214);
                testCase.ConcDelayed(iDelay,:) = testCase.Aif.param.FHandle(testCase.Time',hosrfieldParam);
            end
            [testCase.RefBat, ~] = batcheong(testCase.ConcReference,testCase.Time,testCase.NPre,testCase.ShiftStep);
            [testCase.Bat, ~] = batcheong(testCase.ConcDelayed,testCase.Time,testCase.NPre,testCase.ShiftStep);
            
            testCase.IdxGap = [1:121,151:nT];
            testCase.TimeGap = testCase.Time(testCase.IdxGap);
            testCase.ConcGapDelayed = testCase.ConcDelayed(:,testCase.IdxGap);
        end
    end
    
    methods (Test)
        function testShiftTissues(testCase)
            shiftTissueCurve = true;
            [timeAll,shiftedConc,aifConc,weights] = interpconctime(testCase.Time,testCase.Aif,testCase.ConcDelayed,shiftTissueCurve,testCase.Bat-testCase.RefBat);
            [nP, nT] = size(shiftedConc);
            shiftedConcError = NaN(nP,1);
            for iP=1:nP
                shiftedConcError(iP) = sum((shiftedConc(iP,:) - testCase.ConcReference).^2);
            end
            
            maxStd = 1e-10;
            testCase.verifyLessThan(shiftedConcError,maxStd);
        end
        
        function testShiftAif(testCase)
            shiftArterialCurve = false;
            [timeAll,shiftedConc,aifConc,weights] = interpconctime(testCase.Time,testCase.Aif,testCase.ConcDelayed,shiftArterialCurve,testCase.Bat-testCase.RefBat);
            [nP, nT] = size(shiftedConc);
            shiftedConcError = NaN(nP,1);
            for iP=1:nP
                shiftedConcError(iP) = sum((aifConc(iP,:) - testCase.ConcDelayed(iP,:)).^2);
            end
            
            maxStd = 1e-10;
            testCase.verifyLessThan(shiftedConcError,maxStd);
        end
        
        
        function testShiftGapTissues(testCase)
            shiftTissueCurve = true;
            [timeAll,shiftedConc,aifConc,weights] = interpconctime(testCase.TimeGap,testCase.Aif,testCase.ConcGapDelayed,shiftTissueCurve,testCase.Bat-testCase.RefBat);
            [nP, nT] = size(shiftedConc);
            shiftedConcError = NaN(nP,1);
            for iP=1:nP
                shiftedConcError(iP) = sum((shiftedConc(iP,:) - testCase.ConcReference).^2);
            end
            
            maxStd = 1e-10;
            testCase.verifyLessThan(shiftedConcError,maxStd);
        end
    end
end