classdef ConcMRISPGRCpx_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        ConcT
        TissueParam
        BloodParam
        ImgParam
        Signal
        Signal0
        SignalPhase
        SignalBlood
        SignalBlood0
        SignalPhaseBlood
        SimonisSimul
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.Time = (0:5:720)/60; % min
            testCase.InitTime = 80.5/60; % min
            preInjIdx = find(testCase.Time<testCase.InitTime,1,'last');

            testCase.ConcT = 5*aifbiexp(testCase.Time,testCase.InitTime);
            testCase.TissueParam.tracer.r1 = 4.46; % (s-1mM-1) Gd-DO3A-butrol (Gadovist) @ 3T, Human blood, 3T, Shen 2015
            testCase.TissueParam.tracer.r2s = 87; % (s-1mM-1) @ 3T simulations for Gd-DTPA in white and gray matter by Kjolby et al. 2006, but might deviates under 10 ms.
            testCase.TissueParam.tracer.xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
            testCase.TissueParam.tracer.nBaseline = preInjIdx;
            testCase.TissueParam.tracer.framesBaseline = [1 preInjIdx];
            
            testCase.TissueParam.T10 = 1e3; % ms
            testCase.TissueParam.R10 = 1/testCase.TissueParam.T10; % 1/ms
            testCase.TissueParam.T20s = 200; % ms
            testCase.TissueParam.R20s = 1/testCase.TissueParam.T20s; % 1/ms
            testCase.TissueParam.S0 = 1e5;
            
            testCase.ImgParam.tr = 15; % ms
            testCase.ImgParam.te = 3; % ms
            testCase.ImgParam.fa = 20/180*pi; % radians
            testCase.ImgParam.B0 = 3; % T
            testCase.ImgParam.T20s = testCase.TissueParam.T20s; % ms

            % Blood quadratic relaxivity
            testCase.BloodParam = testCase.TissueParam;
            testCase.BloodParam.tracer.r1 = 4.46; % (s-1mM-1) Gd-DO3A-butrol (Gadovist) @ 3T, Human blood, 3T, Shen 2015
            testCase.BloodParam.tracer.enableQuadR2 = true;
            testCase.BloodParam.tracer.r2s = 0.4929; % (s mM)^-1 @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
            testCase.BloodParam.tracer.r22s = 2.6159; % [ (s mM�)^-1] @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
            testCase.BloodParam.tracer.nBaseline = preInjIdx;
            
            [testCase.Signal, ~] = signalmrispgr(testCase.ConcT,preInjIdx,testCase.TissueParam,testCase.ImgParam);
            [testCase.SignalPhase, ~] = signalmrispgrphase(testCase.ConcT,[],testCase.TissueParam,testCase.ImgParam);
            testCase.Signal0 = mean(testCase.Signal(1:preInjIdx));
            
            [testCase.SignalBlood, ~] = signalmrispgr(testCase.ConcT,preInjIdx,testCase.BloodParam,testCase.ImgParam);
            testCase.SignalBlood0 = mean(testCase.SignalBlood(1:preInjIdx));
            testCase.SignalPhaseBlood = testCase.SignalPhase;
            
            
            % Reproduce SImonis simulations
            testCase.SimonisSimul.Time = (0:1:72)/60; % min
            nT = length(testCase.SimonisSimul.Time);
            testCase.SimonisSimul.InitTime = 10/60; % min
            preInjIdx = find(testCase.SimonisSimul.Time<testCase.SimonisSimul.InitTime,1,'last');
            
            testCase.SimonisSimul.ConcT = [zeros(1,preInjIdx-1) linspace(0,8.5,nT-preInjIdx+1)];
            
            testCase.SimonisSimul.noiseParam.nNoise = 10;
            testCase.SimonisSimul.noiseParam.seed = 700;
            testCase.SimonisSimul.noiseParam.repeatedPattern = false;
            testCase.SimonisSimul.noiseParam.FHandle = @noisemri;
            testCase.SimonisSimul.noiseParam.snr = 13;
            
            testCase.SimonisSimul.TissueParam.tracer.r1 = 3.6; % (s-1mM-1)
            testCase.SimonisSimul.TissueParam.tracer.r2s = 6.3; % (s-1mM-1)
            testCase.SimonisSimul.TissueParam.tracer.xiM = 3.20e-7; % (1/mM) 
            testCase.SimonisSimul.TissueParam.tracer.nBaseline = preInjIdx;
            testCase.SimonisSimul.TissueParam.tracer.framesBaseline = [1 preInjIdx];
            
            testCase.SimonisSimul.TissueParam.T10 = 1932; % ms
            testCase.SimonisSimul.TissueParam.R10 = 1/testCase.SimonisSimul.TissueParam.T10; % 1/ms
            testCase.SimonisSimul.TissueParam.T20s = 275; % ms
            testCase.SimonisSimul.TissueParam.R20s = 1/testCase.SimonisSimul.TissueParam.T20s; % 1/ms
            testCase.SimonisSimul.TissueParam.S0 = 1e5;
            
            testCase.SimonisSimul.ImgParam.tr = 4; % ms
            testCase.SimonisSimul.ImgParam.te = 1.7; % ms
            testCase.SimonisSimul.ImgParam.fa = 8/180*pi; % radians
            testCase.SimonisSimul.ImgParam.B0 = 3; % T
            testCase.SimonisSimul.ImgParam.T20s = testCase.SimonisSimul.TissueParam.T20s; % ms

            [testCase.SimonisSimul.Signal, ~] = signalmrispgr(testCase.SimonisSimul.ConcT,preInjIdx,testCase.SimonisSimul.TissueParam,testCase.SimonisSimul.ImgParam);
            [testCase.SimonisSimul.SignalPhase, ~] = signalmrispgrphase(testCase.SimonisSimul.ConcT,[],testCase.SimonisSimul.TissueParam,testCase.SimonisSimul.ImgParam);
            
            testCase.SimonisSimul.noiseParam.standardDeviation = ...
                testCase.SimonisSimul.TissueParam.S0/testCase.SimonisSimul.noiseParam.snr;
            [noiseMag, noisePhase] = noisemri(testCase.SimonisSimul.Signal.*exp(1j*testCase.SimonisSimul.SignalPhase),testCase.SimonisSimul.noiseParam);
            testCase.SimonisSimul.NSignal = permute(noiseMag,[3 4 1 2]);
            testCase.SimonisSimul.NSignalPhase = permute(noisePhase,[3 4 1 2]);
            testCase.SimonisSimul.Signal0 = mean(testCase.SimonisSimul.Signal(1:preInjIdx));
        end
    end
    
    methods (Test)
        function testSimonis(testCase)
            % With noisy signal
            % Similar to Simonis Fig. 4
            % This test results in smaller errors than Simonis
%             mask = true(testCase.SimonisSimul.noiseParam.nNoise,testCase.SimonisSimul.noiseParam.nNoise);
%             b1Map = ones(testCase.SimonisSimul.noiseParam.nNoise,testCase.SimonisSimul.noiseParam.nNoise);
%             t1Map = testCase.SimonisSimul.TissueParam.T10*ones(testCase.SimonisSimul.noiseParam.nNoise,testCase.SimonisSimul.noiseParam.nNoise);
%             [conc, outliers, faFit] = concmrispgrcpx(testCase.SimonisSimul.NSignal,testCase.SimonisSimul.NSignalPhase,testCase.SimonisSimul.TissueParam.tracer,t1Map,testCase.SimonisSimul.ImgParam,mask,b1Map,testCase.SimonisSimul.Time);
%             
%             meanConc = permute(mean(mean(conc,1),2),[4,1,2,3]);
%             stdConc = permute(std(std(conc,[],1),[],2),[4,1,2,3]);
%             figure; 
%             plot(testCase.SimonisSimul.Time,testCase.SimonisSimul.ConcT); hold on;
%             plot(testCase.SimonisSimul.Time,meanConc);
%             plot(testCase.SimonisSimul.Time,meanConc+2*stdConc,'b--');
%             plot(testCase.SimonisSimul.Time,meanConc-2*stdConc,'b--');
%             hold off;
%             legend('True Conc','Mean','95% interval');
            
            mask = true;
            b1Map = 1;
            [conc, outliers, faFit] = concmrispgrcpx(testCase.SimonisSimul.Signal,testCase.SimonisSimul.SignalPhase,testCase.SimonisSimul.TissueParam.tracer,testCase.SimonisSimul.TissueParam.T10,testCase.SimonisSimul.ImgParam,mask,b1Map,[],testCase.SimonisSimul.Time);
%             figure; plot(testCase.SimonisSimul.Time,testCase.SimonisSimul.ConcT,testCase.SimonisSimul.Time,conc);

            limExpectedConcError = 1e-10;
            limExpectedFaError = 1e-10; % rad
            concError = sum((conc - testCase.SimonisSimul.ConcT).^2)/sum((testCase.SimonisSimul.ConcT).^2);
            testCase.verifyLessThan(concError,limExpectedConcError)
            testCase.verifyTrue(~any(outliers));
            testCase.verifyLessThan(abs(faFit-testCase.SimonisSimul.ImgParam.fa),limExpectedFaError);
        end
        function testTissue(testCase)
            mask = true;
            b1Map = 1;
            [conc, outliers, faFit] = concmrispgrcpx(testCase.Signal,testCase.SignalPhase,testCase.TissueParam.tracer,testCase.TissueParam.T10,testCase.ImgParam,mask,b1Map,[],testCase.Time);
%             figure; plot(testCase.Time,testCase.ConcT,testCase.Time,conc);

            limExpectedConcError = 1e-10;
            limExpectedFaError = 1e-10; % rad
            concError = sum((conc - testCase.ConcT).^2)/sum((testCase.ConcT).^2);
            testCase.verifyLessThan(concError,limExpectedConcError)
            testCase.verifyTrue(~any(outliers));
            testCase.verifyLessThan(abs(faFit-testCase.ImgParam.fa),limExpectedFaError);
        end
        function testBlood(testCase)
            mask = true;
            b1Map = 1;
            [conc, outliers, faFit] = concmrispgrcpx(testCase.SignalBlood,testCase.SignalPhaseBlood,testCase.BloodParam.tracer,testCase.BloodParam.T10,testCase.ImgParam,mask,b1Map,[],testCase.Time);
%             figure; plot(testCase.Time,testCase.ConcT,testCase.Time,conc);

            limExpectedConcError = 1e-10;
            limExpectedFaError = 1e-10; % rad
            concError = sum((conc - testCase.ConcT).^2)/sum((testCase.ConcT).^2);
            testCase.verifyLessThan(concError,limExpectedConcError)
            testCase.verifyTrue(~any(outliers));
            testCase.verifyLessThan(abs(faFit-testCase.ImgParam.fa),limExpectedFaError);
        end
    end
end