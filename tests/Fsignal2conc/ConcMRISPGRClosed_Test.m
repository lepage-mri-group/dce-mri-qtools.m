classdef ConcMRISPGRClosed_Test < matlab.unittest.TestCase
    
    properties
        Time
        InitTime
        ConcT
        TissueParam
        ImgParam
        Signal
        Signal0
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            testCase.Time = (0:5:720)/60; % min
            testCase.InitTime = 80.5/60; % min
            preInjIdx = find(testCase.Time<testCase.InitTime,1,'last');

            testCase.ConcT = aifbiexp(testCase.Time,testCase.InitTime);
            testCase.TissueParam.tracer.r1 = 4.46; % (s-1mM-1) Gd-DO3A-butrol (Gadovist) @ 3T, Human blood, 3T, Shen 2015
            testCase.TissueParam.tracer.r2s = 0; %87; % (s-1mM-1) @ 3T simulations for Gd-DTPA in white and gray matter by Kjolby et al. 2006, but might deviates under 10 ms.
            testCase.TissueParam.tracer.xiM = 0; %3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
            
            testCase.TissueParam.T10 = 1e3; % ms
            testCase.TissueParam.R10 = 1/testCase.TissueParam.T10; % 1/ms
            testCase.TissueParam.T20s = 200; % ms
            testCase.TissueParam.R20s = 1/testCase.TissueParam.T20s; % 1/ms
            testCase.TissueParam.S0 = 1e5;
            
            testCase.ImgParam.tr = 15; % ms
            testCase.ImgParam.te = 1; % ms
            testCase.ImgParam.fa = 20/180*pi; % radians

            % Blood quadratic relaxivity
%             testCase.TissueParam.tracer.enableQuadR2 = true;
%             testCase.TissueParam.tracer.r2 = 0.4929;
%             testCase.TissueParam.tracer.r22 = 2.6159; % ([(s mM)^-1 (s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006

            [testCase.Signal, ~] = signalmrispgr(testCase.ConcT,preInjIdx,testCase.TissueParam,testCase.ImgParam);
            testCase.Signal0 = mean(testCase.Signal(1:preInjIdx));
        end
    end
    
    methods (Test)
        function testT(testCase)
            mask = true;
            b1Map = 1;
            [conc, outliers] = concmrispgrclosed(testCase.Signal,testCase.Signal0,testCase.TissueParam.tracer,testCase.TissueParam.T10,testCase.ImgParam,mask,b1Map);
%             figure; plot(testCase.Time,testCase.ConcT,testCase.Time,conc);

            limExpectedError = 1e-10;
            concError = sum((conc - testCase.ConcT).^2)/sum((testCase.ConcT).^2);
            testCase.verifyLessThan(concError,limExpectedError)
            testCase.verifyTrue(~any(outliers));
        end
    end
end