classdef CreateDROTest < matlab.unittest.TestCase
    % Integration test compares Tofts DRO with the noise free QIBA_v6 DRO.
    properties
        Time
        VisualCheck
        S0
        NNoise
        PkModel
        DRO
        ParamMap
        QIBA_v6
    end
    methods (TestMethodSetup)
        function createToftsDro(testCase)
            testCase.VisualCheck = 1; % Set to 1 to visually compare the DROs concentration curves
            
            % Replicate parameters of QIBA_v6 Tofts DRO
            % Based on Barboriak Lab info (https://sites.duke.edu/dblab/qibacontent/)            
            % AIF model
            testCase.Time = (0:0.5:660)/60;
            aifModel.FHandle = @aifqiba;
            aifModel.Params.t0 = 60.25/60;
            aifModel.Params.A = 5.30488; % mM
            structFieldOrder = {'t0','A'};
            aifModel.pValues = paramstruct2vec(aifModel.Params,structFieldOrder);
            aifModel.units = 'mM';
            % PKModel
            testCase.PkModel.hematocrit = 0.45;
            testCase.PkModel.brainDensity = 1; % g/mL
            testCase.PkModel.repeatedNoise = false;
            % Default phantom parameters
            testCase.PkModel.FHandle = @pkmetofts;
            testCase.PkModel.Ktrans = 1/(testCase.PkModel.brainDensity/100)*[0.01 0.02 0.05 0.1 0.2 0.35]; % QIBA_v6. in mL/(100g min)
            testCase.PkModel.Ve = 1/(testCase.PkModel.brainDensity/100)*[0.01 0.05 0.1 0.2 0.5]; % QIBA_v6. in mL/100g
            testCase.PkModel.Vp = 0;
            testCase.PkModel.paramNames = {'Ktrans','Ve','Vp'};
            % MRI signal
            imgParams.FHandle = @signalmrispgr;
            imgParams.tissueParam.S0 = 5e4; % QIBA_v6, Equilibrium magnetization. a.u
            imgParams.tissueParam.R10 = 1/1000; % QIBA_v6, 1/ms (about the white matter T10)
            imgParams.tissueParam.tracer.r1 = 4.5; % QIBA_v6, 1/(mM*s)
            imgParams.bloodParam.S0 = 5e4; % QIBA_v6, Equilibrium magnetization. a.u
            imgParams.bloodParam.R10 = 1/1440;  % QIBA_v6, 1/ms 
            imgParams.bloodParam.tracer.r1 = imgParams.tissueParam.tracer.r1; % QIBA_v6, 1/(mM*s)
            % MRI sequence
            imgParams.seqParam.te = 0; % Neglects T2* effects ms
            imgParams.seqParam.nAcq = length(imgParams.seqParam.te);
            imgParams.seqParam.tr = 5; % ms
            imgParams.seqParam.fa = 30/180*pi; % rad
            imgParams.noiseParam.nNoise = 10;
            % imgParams.noiseParam.seed = 700;
            imgParams.noiseParam.repeatedPattern = false;
            imgParams.noiseParam.FHandle = @noisemri;
            imgParams.noiseParam.snr = inf; % No Noise added to QIBA_v6
            imgParams.noiseParam.standardDeviation = ...
                imgParams.tissueParam.S0/imgParams.noiseParam.snr;
            % Compute DRO
            [ACQdata, tissueSignal, tissueConc, paramMap, aifModel,modelOutParams] = drocreate(testCase.Time,aifModel,testCase.PkModel,imgParams);
            % Add AIF patch at the last row
            rowWidth = imgParams.noiseParam.nNoise;
            [ACQdataAIF, signalData, concData] = droaifpatch(testCase.Time,aifModel,imgParams,ACQdata, tissueSignal, tissueConc,rowWidth,[]);
            testCase.S0 = imgParams.tissueParam.S0;
            testCase.NNoise = imgParams.noiseParam.nNoise;
            testCase.DRO = ACQdataAIF;
            testCase.ParamMap = paramMap;
        end
        function loadQibaV4Dro(testCase)
            qiba_v6Folder = fullfile(fileparts(which('CreateDROTest')),'testData');
            S = which('load_nii');
            if isempty(S)
                % The complete test requires the load_nii function from Tools for NIfTI and ANALYZE image by Jimmy Shen
                load(fullfile(qiba_v6Folder,'test_qibaV6AcqData'));
            else
                % Load and prepare QIBA_v6 reference phantom
                qibaV6 = load_nii(fullfile(qiba_v6Folder,'dro_ref_qibaV4.nii'));
                % Correct QIBA dimensions
                qibaV6AcqData = flip(flip(permute(double(qibaV6.img),[4 3 2 1]),3),4);
            end
            nT = size(qibaV6AcqData,1);
            % Remove the timer
            droRange = 11:80;
            testCase.QIBA_v6 = qibaV6AcqData(:,:,droRange,:);
            % Rescale for the equilibrium magnetization stated in QIBA_V6
            % parameter sheet
            initQiba = double(qibaV6AcqData(1,:,droRange,:));
            for iT=1:nT
                currentQiba = double(qibaV6AcqData(iT,:,droRange,:));
                testCase.QIBA_v6(iT,:,:,:) = currentQiba./initQiba*testCase.S0;
            end
        end
    end
    methods (Test)
        function compareaif(testCase)
            aifRows = 61:70;
            aifQiba = mean(mean(testCase.QIBA_v6(:,:,aifRows,:),3),4);
            aifDro = mean(mean(testCase.DRO(:,:,aifRows,:),3),4);
            normRootMeanSquaredAif = sqrt(sum((aifQiba-aifDro).^2))/sum(aifQiba);
            if testCase.VisualCheck == 1;
                figure; plot(testCase.Time,aifQiba,testCase.Time,aifDro);
            end
            maxNormSumOfSquares = 1e-4;
            testCase.verifyLessThan(normRootMeanSquaredAif,maxNormSumOfSquares);
        end
        function compareconc(testCase)
            tissueRows = 1:60;
            tConcQiba = mean(mean(testCase.QIBA_v6(:,:,tissueRows,:),3),4);
            tConcDRO = mean(mean(testCase.DRO(:,:,tissueRows,:),3),4);
            normRootMeanSquaredTConc = sqrt(sum((tConcQiba-tConcDRO).^2))/sum(tConcQiba);
            if testCase.VisualCheck == 1;
                figure; plot(testCase.Time,tConcQiba,testCase.Time,tConcDRO);
                
                noiseDimAcq = [3 4];
                [meanAcq, ~] = droparamsnoise(testCase.DRO(:,:,tissueRows,:), testCase.NNoise, noiseDimAcq);
                [tConcQiba, ~] = droparamsnoise(testCase.QIBA_v6(:,:,tissueRows,:), testCase.NNoise, noiseDimAcq);
                listNames = {'DRO','QIBAv6'};
                figHandle = drodisplaydynamic(testCase.Time,{meanAcq tConcQiba},testCase.ParamMap, testCase.PkModel,listNames);
            end
            maxNormSumOfSquares = 1e-4;
            testCase.verifyLessThan(normRootMeanSquaredTConc,maxNormSumOfSquares);
        end
    end
end