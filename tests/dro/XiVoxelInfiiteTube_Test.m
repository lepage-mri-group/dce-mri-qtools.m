classdef XiVoxelInfiiteTube_Test < matlab.unittest.TestCase
    % Integration test compares Tofts DRO with the noise free QIBA_v6 DRO.
    properties
        BfGoal
        PhaseVesselXi
        BFXi
        BFXiParallel
        BFXiPerp
        PhaseSpgr
    end
    methods (TestMethodSetup)
        function createXiTube(testCase)
            
            conc = 5; % mM
            gamma = 2.675e8; % rad/sT
            molarSusceptibility = 3.209e-7; % (L/mol) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
            B0 = 3; % Tesla
            
            % DCE sequence parameters
            voxelRes = 2.3; % mm
            echoTime = 1.85; % ms
            nVox = 128;
            resWithinVoxel = voxelRes/nVox; % mm
            
            degreeNormalisation = 1/pi*180;
            % Vessel geometry
            testCase.BfGoal = 0.2;
            nBloodFraction = 3;
            angOfVesselTheta = 30/degreeNormalisation;
            angOfVesselPhi = 45/degreeNormalisation;
            
            approxVesselRadius = sqrt(testCase.BfGoal*(voxelRes)^2/pi); % mm
            vesselRadius = approxVesselRadius;
            dPhase = (conc*molarSusceptibility)*gamma*B0/2*(echoTime/1e3); % radians
            
            displayOffset = 2*[0 0 round(nVox/2 - vesselRadius/resWithinVoxel)];
            nSimDim = nVox + displayOffset;
            simVoxRes = voxelRes/nVox*nSimDim;
            isVoxel = false(nSimDim);
            isVoxel((1:nVox)+displayOffset(1),(1:nVox)+displayOffset(2),(1:nVox)+displayOffset(3)) = true;
            
            vesselParam.radius = vesselRadius;
            vesselParam.theta = angOfVesselTheta;
            vesselParam.phi = angOfVesselPhi;
            [voxelPhaseOffset,isVessel,isTissue,testCase.BFXi,unitVector] = xivoxelinfinitetube(dPhase,vesselParam,isVoxel,simVoxRes,nSimDim);
            testCase.PhaseVesselXi = mean(voxelPhaseOffset(isVessel));
            
            
            vesselParamParallel.radius = vesselRadius;
            vesselParamParallel.theta = 0;
            vesselParamParallel.phi = angOfVesselPhi;
            [~,isVessel,~,testCase.BFXiPerp,~] = xivoxelinfinitetube(dPhase,vesselParamParallel,isVoxel,simVoxRes,nSimDim);
            vesselParamPerp.radius = vesselRadius;
            vesselParamPerp.theta = 0;
            vesselParamPerp.phi = angOfVesselPhi;
            [~,~,~,testCase.BFXiParallel,~] = xivoxelinfinitetube(dPhase,vesselParamPerp,isVoxel,simVoxRes,nSimDim);
            
            imgParam.B0 = B0;
            imgParam.te = echoTime;
            tissueParam.theta = angOfVesselTheta;
            tissueParam.tracer.xiM = molarSusceptibility;
            
            [testCase.PhaseSpgr, ~] = signalmrispgrphase(conc,[],tissueParam,imgParam);
        end
    end
    methods (Test)
        function bloodfraction(testCase)
            bfError = abs(testCase.BFXi-testCase.BfGoal);
            bfErrorParallel = abs(testCase.BFXiParallel-testCase.BfGoal);
            bfErrorPerp = abs(testCase.BFXiPerp-testCase.BfGoal);
            
            minBfError = 0.05;
            testCase.verifyLessThan(bfError,minBfError);
            testCase.verifyLessThan(bfErrorParallel,minBfError);
            testCase.verifyLessThan(bfErrorPerp,minBfError);
        end
        function comparephase(testCase)
            phaseError = abs(testCase.PhaseVesselXi-testCase.PhaseSpgr); % rad
            minError = 1e-10;
            testCase.verifyLessThan(phaseError,minError);
            
        end
    end
end