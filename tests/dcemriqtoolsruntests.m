function projectTestSuiteResult = dcemriqtoolsruntests(specificTests)
% DCEMRIQTOOLSRUNTESTS Run all the test suite of the dce-mri-qtools 
% project.
% projectTestSuiteResult = DCEMRIQTOOLSRUNTESTS() runs all tests.
%
% projectTestSuiteResult = DCEMRIQTOOLSRUNTESTS(subFolder) runs tests of a
% specific subfolder.
%
% projectTestSuiteResult = DCEMRIQTOOLSRUNTESTS(functionName) runs test of 
% a specific toolbox function.
%
% See also: DCEMRIQTOOLSRUNFUNCTEST
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-11-26

import matlab.unittest.TestSuite;
testFolderPaths = fileparts(mfilename('fullpath'));

%% Find the test to run
if ~exist('specificTests','var')
    % Entire test suite
    projectTestSuiteClass = TestSuite.fromFolder(testFolderPaths,'IncludingSubfolders',true);
else
    % Search for a test subfolder
    runSubFolderTests = 0;
    testSubDir = dir(testFolderPaths);
    nDir = length(testSubDir);
    for iDir = 1:nDir
        if strcmp(testSubDir(iDir).name,specificTests)
            runSubFolderTests = iDir;
        end
    end
    
    if runSubFolderTests>0
        % Subfolder tests
        subTestFolderPaths = fullfile(testFolderPaths,testSubDir(runSubFolderTests).name);
        projectTestSuiteClass = TestSuite.fromFolder(subTestFolderPaths,'IncludingSubfolders',true);
    else
        % Search for a specific function test
        [funcPath, fFoundName, ~] = fileparts(which(specificTests));
        sourceFolder = 'src';
        testsFolder = 'tests';
        
        fSearchTestName = [fFoundName, '_Test.m'];
        
        sourceIdx = strfind(funcPath,sourceFolder);
        if isempty(sourceIdx)
            error('The function path is outside of project functions.');
        end
        
        testsPath =fullfile(funcPath(1:(sourceIdx-1)), testsFolder,funcPath((sourceIdx+length(sourceFolder):end)));
        testsFuncDir = dir(testsPath);
        nTestsFunc = length(testsFuncDir);
        foundTestFuncIdx = 0;
        for iFunc=1:nTestsFunc
            if strcmpi(fSearchTestName,testsFuncDir(iFunc).name)
                foundTestFuncIdx = iFunc;
            end
        end
        
        if foundTestFuncIdx~=0
            % Test a specific function
            testFuncName = testsFuncDir(foundTestFuncIdx).name;
            projectTestSuiteClass = TestSuite.fromFile(fullfile(testsPath,testFuncName));
        else
            error('Specified tests cannot be associated to a subdirectory or function.');
        end
    end
end
%% Run the test(s)
projectTestSuiteResult = run(projectTestSuiteClass);
end