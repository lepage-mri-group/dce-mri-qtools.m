classdef Pkm2CMARTest < matlab.unittest.TestCase
    properties
        TimeCst
        TimeVar
        NShift
        TimeShift
        AIFCst
        AIFVar
        AIFShift
        ParamVec
        ParamVarVec
        ParamFixVec
        Order
        TimeRange
        ParamStruct
        PatientParam
        ConcVar
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            seqParams.frameDurations = [ones(1,12)*10, ones(1,2)*30, ones(1,5)*60, ones(1,3)*240, ones(1,8)*300]; % seconds
            testCase.TimeVar = (cumsum(seqParams.frameDurations) - seqParams.frameDurations/2)/60;  % min
            testCase.NShift = 5;
            testCase.TimeShift = testCase.TimeVar+testCase.TimeVar(testCase.NShift);  % min
            testCase.TimeCst = linspace(testCase.TimeVar(1),testCase.TimeVar(end),256);
            biexpStructFieldOrder = {'t0','tf','A','wd','we','td','te'};
            biexpParamStruct.t0 = 80.5/60;
            biexpParamStruct.tf = 128/60; % tf, min
            biexpParamStruct.A = 0.7857; % A, mM
            biexpParamStruct.wd = 140/60;%0.7823; % wd, min
            biexpParamStruct.we = 62/60; % we, min
            biexpParamStruct.td = 26/60; % td, min
            biexpParamStruct.te = 25; % te, min
            testCase.AIFCst.pValues = paramstruct2vec(biexpParamStruct,biexpStructFieldOrder);
            testCase.AIFCst.FHandle = @aifbiexp;
            testCase.AIFVar = testCase.AIFCst;
            testCase.AIFShift = testCase.AIFCst;
            
            testCase.AIFCst.AIFFit = testCase.AIFCst.FHandle(testCase.TimeCst,testCase.AIFCst.pValues);
            
            testCase.AIFShift.AIFFit = testCase.AIFShift.FHandle(testCase.TimeShift,testCase.AIFShift.pValues);
            
            testCase.ParamStruct.k(1) = 0.20; % (1/min)
            testCase.ParamStruct.k(2) = 0.3; % (1/min)
            testCase.ParamStruct.k(3) = 0.003; % (1/min)
            testCase.ParamStruct.k(4) = 0.05; % (1/min)
            testCase.ParamStruct.Vb = 0.03; % (-)
            testCase.ParamVec = [testCase.ParamStruct.k, testCase.ParamStruct.Vb];
            
            testCase.Order = [true true true true false];
            testCase.ParamVarVec = testCase.ParamVec(testCase.Order);
            testCase.ParamFixVec = testCase.ParamVec(~testCase.Order);
            testCase.TimeRange = [5 20];
            testCase.PatientParam.brainDensity = 1; % g/mL
            testCase.ConcVar = pkm2cmar(testCase.TimeVar,testCase.AIFVar,testCase.PatientParam,[],testCase.ParamStruct);
        end
    end
    
    methods (Test)
        function testCompareWithObsolete(testCase)
            optionalExtFunc = @mar_calculateconcentration;
            infoExtFunc = functions(optionalExtFunc);
            if isempty(infoExtFunc.file)
                disp('Obsolete PKM function is not in path. Subtest ''testCompareWithObsolete'' is skipped.');
            else
                obsoleteConc = mar_calculateconcentration(testCase.TimeVar', testCase.ParamVec, testCase.AIFVar.pValues, 'k'); % kBq/mL
                testCase.verifyEqual(testCase.ConcVar',obsoleteConc)
            end
        end
        function testVarVsCstTime(testCase)
            concCst = pkm2cmar(testCase.TimeCst,testCase.AIFCst,testCase.PatientParam,[],testCase.ParamStruct);
            concCstAtVar = interp1(testCase.TimeCst,concCst,testCase.TimeVar);
            nT = length(testCase.TimeVar);
            rmsError = sqrt(sum((concCstAtVar-testCase.ConcVar).^2))/nT;
            limError = 0.001;
            testCase.verifyLessThan(rmsError,limError)
        end
        function testShiftTime(testCase)
            concShift = pkm2cmar(testCase.TimeShift,testCase.AIFShift,testCase.PatientParam,[],testCase.ParamStruct);
            concShiftAtVar = interp1(testCase.TimeShift,concShift,testCase.TimeVar);
            nT = length(testCase.TimeVar((testCase.NShift+1):end));
            rmsError = sqrt(sum((concShiftAtVar((testCase.NShift+1):end)-testCase.ConcVar((testCase.NShift+1):end)).^2))/nT;
            limError = 0.001;
            testCase.verifyLessThan(rmsError,limError)
        end
        function testVectorInput(testCase)
            concVec = pkm2cmar(testCase.TimeVar,testCase.AIFVar,testCase.PatientParam,[],testCase.ParamVec);
            testCase.verifyEqual(concVec,testCase.ConcVar)
        end
        function testVariableFixedInput(testCase)
            concVarFix = pkm2cmar(testCase.TimeVar,testCase.AIFVar,testCase.PatientParam,[],testCase.ParamVarVec,testCase.ParamFixVec,testCase.Order);
            testCase.verifyEqual(concVarFix,testCase.ConcVar)
        end
        function testTimeRange(testCase)
            concRange = pkm2cmar(testCase.TimeVar,testCase.AIFVar,testCase.PatientParam,[],testCase.ParamVarVec,testCase.ParamFixVec,testCase.Order,testCase.TimeRange);
            startTimeIdx = find(testCase.TimeVar>=testCase.TimeRange(1),1,'first');
            stopTimeIdx = find(testCase.TimeVar<=testCase.TimeRange(2),1,'last');
            testCase.verifyEqual(concRange(startTimeIdx:stopTimeIdx),testCase.ConcVar(startTimeIdx:stopTimeIdx))
        end
    end
end