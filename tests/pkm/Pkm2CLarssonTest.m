classdef Pkm2CLarssonTest < matlab.unittest.TestCase
    properties
        TimeCst
        TimeVar
        NShift
        TimeShift
        AIFCst
        AIFVar
        AIFShift
        AifDirac
        ParamVec
        ParamVarVec
        ParamFixVec
        Order
        TimeRange
        ParamStruct
        PatientParam
        ConcCst
    end
    
    methods (TestMethodSetup)
        function runDefineTime(testCase)
            seqParams.frameDurations = [ones(1,6)*30, ones(1,90)*2, ones(1,12)*10, ones(1,2)*30]; % seconds
            testCase.TimeVar = (cumsum(seqParams.frameDurations)-seqParams.frameDurations(1))/60;  % min
            testCase.NShift = 20;
            testCase.TimeCst = linspace(testCase.TimeVar(1),testCase.TimeVar(end),256);
            testCase.TimeShift = testCase.TimeCst+testCase.TimeCst(testCase.NShift);  % min
            
            horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
            horsParamStruct.t0 = 190/60;
            horsParamStruct.A = [0.37, 0.33, 10.06]; % mM
            horsParamStruct.m = [0.11, 1.17, 16.02];% 1/min
            horsParamStruct.alpha = 5.26; % -
            horsParamStruct.beta = 0.032; % min
            horsParamStruct.tau = 0.129; % min
            horsParamStruct.kN = 1; % (-)
            horsParamStruct.k = 28.8314*(horsParamStruct.beta*(horsParamStruct.alpha+1)+horsParamStruct.tau);
            testCase.AIFCst.pValues = paramstruct2vec(horsParamStruct,horsfieldStructFieldOrder);
            testCase.AIFCst.FHandle = @aifhorsfield;
            testCase.AIFVar = testCase.AIFCst;
            testCase.AIFShift = testCase.AIFCst;
            
            testCase.AIFCst.AIFFit = testCase.AIFCst.FHandle(testCase.TimeCst,testCase.AIFCst.pValues);
            
            testCase.AIFShift.AIFFit = testCase.AIFShift.FHandle(testCase.TimeShift,testCase.AIFShift.pValues);
            
            testCase.PatientParam.hematocrit = 0.38; % (-)
            testCase.PatientParam.brainDensity = 1; % (g/mL)
            testCase.ParamStruct.F = 130; % mL/(100g min)
            testCase.ParamStruct.Vb = 24.3; % mL/100g
            testCase.ParamStruct.Ki = 19.1; % mL/(100g min)
            testCase.ParamStruct.Ve = 1/(testCase.PatientParam.brainDensity/100)*0.1; % mL/100g
            
            larssonStructFieldOrder = {'F','Ki','Vb','Ve'};
            testCase.ParamVec = paramstruct2vec(testCase.ParamStruct,larssonStructFieldOrder);
            
            testCase.Order = [false true true true];
            testCase.ParamVarVec = testCase.ParamVec(testCase.Order);
            testCase.ParamFixVec = testCase.ParamVec(~testCase.Order);
            testCase.TimeRange = [4 7];
            
            testCase.ConcCst = pkm2clarsson(testCase.TimeCst,testCase.AIFCst,testCase.PatientParam,testCase.ParamStruct);
            
            % Simple Dirac AIF for validations
            aifDose = 10; % mM
            dt = testCase.TimeCst(2)-testCase.TimeCst(1);
            testCase.AifDirac.nT0 = 10;
            testCase.AifDirac.t0 = testCase.TimeCst(testCase.AifDirac.nT0) - dt/2;
            testCase.AifDirac.tf = testCase.TimeCst(testCase.AifDirac.nT0) + dt/2;
            testCase.AifDirac.At = aifDose*dt;
            testCase.AifDirac.FHandle = @aifrect;
            testCase.AifDirac.AIFFit = testCase.AifDirac.FHandle(testCase.TimeCst,testCase.AifDirac);
            
        end
    end
    
    methods (Test)
        function testKi0Parameters(testCase)
            limExpectedError = 1e-10;
            
            % Test the simple case when Ki = 0;
            Ki0ParamVec = testCase.ParamVec;
            Ki0ParamVec(2) = 0;
            % Larsson model simplifies to: conc = F*conv(aif,exp(-F/Vb t));
            ki0Conc = pkm2clarsson(testCase.TimeCst,testCase.AifDirac.AIFFit,testCase.PatientParam,Ki0ParamVec);
            
            % Check peak value
            expectedPeakConc = testCase.AifDirac.At * ...
                (testCase.ParamStruct.F*(testCase.PatientParam.brainDensity/100)); % mM
            peakError = abs(ki0Conc(testCase.AifDirac.nT0) - expectedPeakConc);
            testCase.verifyLessThan(peakError,limExpectedError)
            % Test mono exponential behavior
            logConc = log(ki0Conc);
            logLinFitConc = polyfit(testCase.TimeCst(testCase.AifDirac.nT0:end),logConc(testCase.AifDirac.nT0:end),1);
            expConcCoeff = -logLinFitConc(1);
            expectedExpConcCoeff = (testCase.ParamStruct.F*(testCase.PatientParam.brainDensity/100))/...
                (testCase.ParamStruct.Vb*(testCase.PatientParam.brainDensity/100));
            expCoeffError = abs(expConcCoeff - expectedExpConcCoeff);
            testCase.verifyLessThan(expCoeffError,limExpectedError)
        end
        function testVbInfParameters(testCase)
            limExpectedError = 1e-10;
            
            % Test the simple case when Vb -> infinity;
            VbInfParamVec = testCase.ParamVec;
            VbInfParamVec(3) = inf;
            % Larsson model simplifies to: conc = F*conv(aif,1);
            vbInfConc = pkm2clarsson(testCase.TimeCst,testCase.AifDirac.AIFFit,testCase.PatientParam,VbInfParamVec);

            % Check average constant conc value
            expectedCstConc = testCase.AifDirac.At * ...
                (testCase.ParamStruct.F*(testCase.PatientParam.brainDensity/100)); % mM
            peakError = abs(mean(vbInfConc(testCase.AifDirac.nT0:end)) - expectedCstConc);
            testCase.verifyLessThan(peakError,limExpectedError)
        end
        function testFullDirac(testCase)
            limExpectedError = 1e-10;
            % Test the full parameters with dirac AIF
            diracConc = pkm2clarsson(testCase.TimeCst,testCase.AifDirac.AIFFit,testCase.PatientParam,testCase.ParamStruct);

            % Compute expected value
            F = testCase.ParamVec(1)*(testCase.PatientParam.brainDensity/100); % (1/min)
            Ki = testCase.ParamVec(2)*(testCase.PatientParam.brainDensity/100); % (1/min)
            Vb = testCase.ParamVec(3)*(testCase.PatientParam.brainDensity/100); % (-)
            Ve = testCase.ParamVec(4)*(testCase.PatientParam.brainDensity/100); % (-)
            alpha = (F+Ki)/Vb;
            theta = Ki*(1-testCase.PatientParam.hematocrit)/Ve;
            
            constSquareRoot = sqrt(theta^2 + alpha^2 - 2*theta*alpha + ...
                4*Ki*(1-testCase.PatientParam.hematocrit)*Ki/(Vb*Ve));
            
            a = (theta + alpha + constSquareRoot)/2;
            b = (theta + alpha - constSquareRoot)/2;
            
            irfTimeFromZero = testCase.TimeCst - testCase.TimeCst(testCase.AifDirac.nT0);
            biexpIrf = ((a - theta - Ki/Vb)*exp(-a*irfTimeFromZero) + ...
                (-b + theta + Ki/Vb)*exp(-b*irfTimeFromZero)) / (a - b);
            biexpIrf(1:(testCase.AifDirac.nT0-1)) = 0;
            
            expectedConc = biexpIrf*testCase.AifDirac.At * ...
                (testCase.ParamStruct.F*(testCase.PatientParam.brainDensity/100));
            concError = sum((diracConc - expectedConc).^2)/sum((diracConc).^2);
            testCase.verifyLessThan(concError,limExpectedError)
        end
        function testCompareWithObsolete(testCase)
            optionalExtFunc = @bbm_2cmx_calculateconcentration;
            infoExtFunc = functions(optionalExtFunc);
            if isempty(infoExtFunc.file)
                disp('Obsolete PKM function is not in path. Subtest ''testCompareWithObsolete'' is skipped.');
            else
                nT = length(testCase.TimeVar);
                P = [testCase.TimeCst(1), testCase.TimeCst(end) testCase.PatientParam.hematocrit, testCase.PatientParam.brainDensity];
                obsoleteConc = bbm_2cmx_calculateconcentration(testCase.TimeCst', testCase.AIFCst.AIFFit', testCase.ParamVec, P, [],true(1,4)); % kBq/mL
                
                rmsError = sqrt(sum((obsoleteConc-testCase.ConcCst').^2))/nT;
                limError = 0.001;
                testCase.verifyLessThan(rmsError,limError)
            end
        end
        function testVarVsCstTime(testCase)
            concVar = pkm2clarsson(testCase.TimeVar,testCase.AIFVar,testCase.PatientParam,testCase.ParamStruct);
            concVarAtCst = interp1(testCase.TimeVar,concVar,testCase.TimeCst);
            nT = length(testCase.TimeCst);
            rmsError = sqrt(sum((concVarAtCst-testCase.ConcCst).^2))/nT;
            limError = 0.001;
            testCase.verifyLessThan(rmsError,limError)
        end
        function testShiftTime(testCase)
            concShift = pkm2clarsson(testCase.TimeShift,testCase.AIFShift,testCase.PatientParam,testCase.ParamStruct);
            concShiftAtCst = interp1(testCase.TimeShift,concShift,testCase.TimeCst);
            nT = length(testCase.TimeCst((testCase.NShift+1):end));
            rmsError = sqrt(sum((concShiftAtCst((testCase.NShift+1):end)-testCase.ConcCst((testCase.NShift+1):end)).^2))/nT;
            limError = 0.001;
            testCase.verifyLessThan(rmsError,limError)
        end
        function testVectorInput(testCase)
            concVec = pkm2clarsson(testCase.TimeCst,testCase.AIFCst,testCase.PatientParam,testCase.ParamVec);
            testCase.verifyEqual(concVec,testCase.ConcCst)
        end
        function testVariableFixedInput(testCase)
            concCstFix = pkm2clarsson(testCase.TimeCst,testCase.AIFCst,testCase.PatientParam,testCase.ParamVarVec,testCase.ParamFixVec,testCase.Order);
            testCase.verifyEqual(concCstFix,testCase.ConcCst)
        end
        function testTimeRange(testCase)
            concRange = pkm2clarsson(testCase.TimeCst,testCase.AIFCst,testCase.PatientParam,testCase.ParamVarVec,testCase.ParamFixVec,testCase.Order,testCase.TimeRange);
            startTimeIdx = find(testCase.TimeVar>=testCase.TimeRange(1),1,'first');
            stopTimeIdx = find(testCase.TimeVar<=testCase.TimeRange(2),1,'last');
            testCase.verifyEqual(concRange(startTimeIdx:stopTimeIdx),testCase.ConcCst(startTimeIdx:stopTimeIdx))
        end
    end
end