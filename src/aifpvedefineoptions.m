function aifOptions = aifpvedefineoptions(time,framesPre,pveEndIdx,nZ,aifPveType,clusterType,vofOptions)
% AIFPVEDEFINEOPTIONS Set the chosen default options for the VOF 
% calculation, AIF PVE correction, and AIF fitting
%  
%   aifOptions = AIFPVEDEFINEOPTIONS(time,framesPre,pveEndIdx,nZ,aifPveType,clusterType,vofType,gmMask)
%
% See also: GLOBALAIF, GLOBALVOF, AIFPVESIGNAL, AIFPVECONC
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-08-09

%% Define options
if ~exist('aifPveType','var') || isempty(aifPveType)
    aifPveType = 's-pvc-closed-form'; % Default PVE type
end
if ~exist('clusterType','var') || isempty(clusterType)
    clusterType = 'bf_n60'; % Default cluster type
end
dt = time(2) - time(1);

% Typically first pass duration is 30 seconds, recirculation by 45 seconds
firstPassRecircDelay = 45/60; % min
minRecircPoints = 5;
recircEndIdx = find(time>(time(framesPre(2))+firstPassRecircDelay),1,'first')-1;
recircEndIdx = max(recircEndIdx,framesPre(2)+minRecircPoints);

% Refine range with average AIF to VOF delay for brain
vofAverageDelay = 7/60; % min
vofStartIdx = find(time>(time(framesPre(2))+vofAverageDelay),1,'first');
vofStartIdx = max(vofStartIdx,framesPre(2));

aifPveRange = [framesPre(2) pveEndIdx];
aifClusterRange = [framesPre(2) recircEndIdx];

%% VOF options
aifOptions.vof = vofOptions;

%% AIF options
aifOptions.aif.fit = aifOptions.vof.fit;
aifOptions.aif.cluster = aifOptions.vof.cluster;
aifOptions.aif.cluster.aif.iaucRangeIdx = aifClusterRange;

aifOptions.aif.conc.type = aifPveType;
switch lower(aifOptions.aif.conc.type)
    case 's-pvc-open-form'
        aifOptions.aif.pve.bloodFractionGap = 0.15; % Fited blood fractions are limited to Closed-form fractions � this gap
        aifOptions.aif.pve.bloodFractions = 1:-0.01:0.10; % Open form calculation is too slow
        aifOptions.aif.cluster.aif.selectionType = 'BVF'; % cluster by blood volume fraction
    case 's-pvc-closed-form';
        aifOptions.aif.pve.bloodFractions = 1:-0.001:0.10;
        aifOptions.aif.cluster.aif.selectionType = 'BVF'; % cluster by blood volume fraction
    case 'c-pvc'
        aifOptions.aif.cluster.aif.selectionType = 'IAUC'; % cluster by area under the curve
    case 'basic'
        aifOptions.aif.cluster.aif.selectionType = 'IAUC'; % cluster by area under the curve
    case 'skip-calc'
        aifOptions.aif.cluster.aif.selectionType = 'BVF'; % cluster by blood volume fraction
end
aifOptions.aif.pve.tissCorrType = 'dynamic';
aifOptions.aif.pve.rangeVofIAUC =  aifPveRange;
aifOptions.aif.pve.reducedAifTime = vofAverageDelay; % To account for the ~7 s delay between AIF and VOF in the AUC
aifOptions.aif.pve.open2ndSol.maxFractionDiff = 1.3; % 2. 2nd solution cannot be over venous concentration by more than this ratio
aifOptions.aif.pve.open2ndSol.maxConcGapFraction = 0.6; % 3. Gaps between 1st and 2nd solution cannot be bigger than this fration of the maximum venous concentration
aifOptions.aif.pve.nMaxOutliers = 5; % Tolerate maximum 5 outliers in solutions
aifOptions.aif.pve.intRatioThresh = 0.15; % Only keep AIF to VOF conc integer ratio within �15%
aifOptions.aif.pve.delayCorr = 'none';

switch lower(clusterType)
    case 'bf'
        aifOptions.aif.pve.nMaxVoxels = inf; % Only keep the N voxels with highest blood fraction
        aifOptions.aif.cluster.aif.selectionType = 'BVF'; % cluster by blood volume fraction
    case 'bf_n60'
        aifOptions.aif.pve.nMaxVoxels = 60; % Only keep the N voxels with highest blood fraction
        aifOptions.aif.cluster.aif.selectionType = 'BVF'; % cluster by blood volume fraction
    case 'iauc'
        aifOptions.aif.pve.nMaxVoxels = inf; % Only keep the N voxels with highest blood fraction
        aifOptions.aif.cluster.aif.selectionType = 'IAUC'; % cluster by blood volume fraction
    case 'iauc_n60'
        aifOptions.aif.pve.nMaxVoxels = 60; % Only keep the N voxels with highest blood fraction
        aifOptions.aif.cluster.aif.selectionType = 'IAUC'; % cluster by blood volume fraction
end

aifOptions.aif.reg.time = time;
aifOptions.aif.reg.aif.pReg = 0.40;
aifOptions.aif.reg.display = false;
aifOptions.aif.reg.aif.regRange = aifClusterRange;

aifOptions.aif.cluster.aif.nVoxelMax= 15; % maximum number of voxel in final AIF cluster
aifOptions.aif.cluster.aif.nVoxelMin= 7; % minimum number of voxel in final AIF cluster
aifOptions.aif.cluster.aif.diffPicco= 0.1; % Cluster selection criterion: Threshold between peak AUC or TTP criterion
end