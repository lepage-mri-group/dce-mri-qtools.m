function [artPve,venCpx] = globalaif(...
    time,conc,magDyn,phaseDyn,T1Map,B1Map,...
    bloodParamTracer,seqParam,framesPre,...
    venous,arterial,tissues,venCpx,aifOptions)
% GLOBALAIF Single function call to compute the arterial input function 
% (AIF) globally. Computation steps are:
%   - (opt) VOF
%   - Arterial contrast agent (CA) concentration calculation
%       > Various CA concentration models are available. 
%   - (TODO: Voxelwise delay correction)
%   - Arterial voxel clustering
%   - AIF Horsfield model fitting
%
%   [artPve,venCpx] = globalaif(time,[],magDyn,phaseDyn,T1Map,B1Map,bloodParamTracer,seqParam,framesPre,venous,arterial,tissues,[],aifOptions)
%       Computes the closed form AIF corrected for the partial volume
%       effect on signal level.
%
%   [artPve,venCpx] = globalaif(time,[],magDyn,phaseDyn,T1Map,B1Map,bloodParamTracer,seqParam,framesPre,venous,arterial,tissues,venCpx,aifOptions)
%       VOF calculation is skipped and replaced by the provided venCpx structure.
%
% See also: GLOBALVOF, AIFPVECONC, AIFPVESIGNAL,FITAIF, AIFPVEDEFINEOPTIONS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-09-16

nT = length(time);
[nX,nY,nZ] = size(arterial.mask);
if isfield(aifOptions.aif.pve,'delayCorr')
    delayCorr = 'none';
else
    delayCorr = 'none';
end

%% 1. Compute VOF
bloodParamTracer.framesBaseline = framesPre;
if exist('venCpx','var') && ~isempty(venCpx)
    % VOF is an input
else
    switch lower(aifOptions.aif.conc.type)
        case 'basic'
            % Not needed for non PVC models
            venCpx = [];
        otherwise
            % Compute VOF
            [venCpx.fit,venCpx.cluster, venCpx.all, venCpx.conc] = ...
                globalvof(time,magDyn,phaseDyn,T1Map,B1Map,...
                bloodParamTracer,seqParam,venous.mask,aifOptions.vof,conc);
    end
end

%% 2. Arterial conc calculation
switch lower(aifOptions.aif.conc.type)
    case 's-pvc-open-form'
        if ~isfield(arterial,'T1blood') || isempty(arterial.T1blood) % Defaults to DCE T1 blood
            arterial.T1bloodDce = mean(T1Map(venCpx.cluster.mask));
            arterial.T1blood = arterial.T1bloodDce;
            artPve.T1blood = arterial.T1blood;
        else
            artPve.T1blood = arterial.T1blood;
        end
        % Find initial PVE factor
        bloodTracerParamClosed = bloodParamTracer;
        bloodTracerParamClosed.enableBloodR2 = false;
        [~, aifOptions.aif.pve.initBloodFrac, ~,~] = aifpvesignal(...
            time,venCpx.cluster.mean,magDyn,phaseDyn,artPve.T1blood,B1Map,...
            bloodTracerParamClosed,seqParam,framesPre,...
            arterial.mask,arterial.pxList,tissues.mask, tissues.pxList,...
            aifOptions.aif.pve);
        bloodTracerParamOpen = bloodParamTracer;
        bloodTracerParamOpen.enableBloodR2 = true;
        [artPve.conc, artPve.factor, artPve.outlier,artPve.intRatio,artPve.open2ndSol] = aifpvesignal(...
            time,venCpx.cluster.mean,magDyn,phaseDyn,artPve.T1blood,B1Map,...
            bloodTracerParamOpen,seqParam,framesPre,...
            arterial.mask,arterial.pxList,tissues.mask, tissues.pxList,...
            aifOptions.aif.pve);
    case 's-pvc-closed-form';
        if ~isfield(arterial,'T1blood') || isempty(arterial.T1blood)  % Defaults to DCE T1 blood
            arterial.T1bloodDce = mean(T1Map(venCpx.cluster.mask));
            arterial.T1blood = arterial.T1bloodDce;
            artPve.T1blood = arterial.T1blood;
        else
            artPve.T1blood = arterial.T1blood;
        end
        bloodTracerParamClosed = bloodParamTracer;
        bloodTracerParamClosed.enableBloodR2 = false;
        [artPve.conc, artPve.factor, artPve.outlier,artPve.intRatio] = aifpvesignal(...
            time,venCpx.cluster.mean,magDyn,phaseDyn,artPve.T1blood,B1Map,...
            bloodTracerParamClosed,seqParam,framesPre,...
            arterial.mask,arterial.pxList,tissues.mask, tissues.pxList,...
            aifOptions.aif.pve);
    case 'c-pvc'
        [artPve.conc,artPve.factor, artPve.outlier,artPve.intRatio] = aifpveconc(...
            time,venCpx.cluster.mean,conc,...
            arterial.mask,aifOptions.aif.pve);
    case 'basic'
        artPve.conc = conc;
        artPve.factor = ones(nX,nY,nZ);
        artPve.intRatio = ones(nX,nY,nZ);
        artPve.outlier = false(nX,nY,nZ,nT);
    case 'skip-calc'
        if isstruct(conc)
            artPve = conc;
            if ~isfield(artPve,'conc') || ~isfield(artPve,'factor') || ~isfield(artPve,'intRatio') || ~isfield(artPve,'outlier') 
                error('Fields are missing to skip concentration calculation.')
            end
        else
            artPve.conc = conc;
            artPve.factor = ones(nX,nY,nZ);
            artPve.intRatio = ones(nX,nY,nZ);
            artPve.outlier = false(nX,nY,nZ,nT);
        end
end

%% Mask based on PVE correction
% Minimum voxels to garantee at least 1 cluster
minVoxelsToKeep = ceil(aifOptions.aif.cluster.aif.nVoxelMax/aifOptions.aif.reg.aif.pReg);
maskPvePre = (artPve.intRatio > (1-aifOptions.aif.pve.intRatioThresh)) & ...
    (artPve.intRatio < (1+aifOptions.aif.pve.intRatioThresh)) & ...
    (arterial.mask); %  & ~artPve.outlier

if sum(maskPvePre(:)) < minVoxelsToKeep
    [sortedIntRatio, intRatioOrder] = sort(abs((artPve.intRatio(arterial.mask) - 1)));
    if length(sortedIntRatio) >= minVoxelsToKeep
        voxelsKept = minVoxelsToKeep;
        avgIntRatioDeviation = mean(sortedIntRatio(1:minVoxelsToKeep));
        keptIntRatioList = false(size(intRatioOrder));
        keptIntRatioList(intRatioOrder(1:minVoxelsToKeep)) = true;
        artPve.maskPve = false(nX,nY,nZ);
        artPve.maskPve(arterial.mask) = keptIntRatioList;
        artPve.maskPveReg = artPve.maskPve; % Not enough voxels to apply the irregular criteria
    elseif sum(maskPvePre(:)) == 0
        voxelsKept = sum(arterial.mask(:));
        artPve.maskPve = arterial.mask;
        artPve.maskPveReg = arterial.mask;
        avgIntRatioDeviation = mean(sortedIntRatio);
    else
        voxelsKept = sum(maskPvePre);
        artPve.maskPve = maskPvePre;
        artPve.maskPveReg = maskPvePre;
        avgIntRatioDeviation = mean(abs((artPve.intRatio(maskPvePre) - 1)));
    end
    warning('Could not find enough voxels for int ratio threshold of %f %%. %d voxel kept with a IAUC ratio of %f %%. Skipping irregularity exclusion criterion.',100*aifOptions.aif.pve.intRatioThresh,voxelsKept,100*avgIntRatioDeviation);
else
    if sum((artPve.factor(maskPvePre))>0)>aifOptions.aif.pve.nMaxVoxels
        [sortedFactors, factorIdx] = sort(artPve.factor(maskPvePre));
        nMaskPts = length(factorIdx);
        pveMaskList = false(nMaskPts,1);
        pveMaskList(factorIdx(end:-1:(end-aifOptions.aif.pve.nMaxVoxels+1))) = true;
        artPve.maskPve =  false(nX,nY,nZ);
        artPve.maskPve(maskPvePre) = pveMaskList;
    else
        artPve.maskPve = maskPvePre;
    end
    % Irregular time curve exclusion
    [excludedRegMask, ~] = inclusioncriteriareg(artPve.conc,artPve.maskPve,aifOptions.aif.reg);
    artPve.maskPveReg = (~excludedRegMask) & artPve.maskPve;
end

%% AIF delay correction
artPve.maskPveDelay = artPve.maskPveReg;
switch delayCorr
    case 'none'
        
    case 'cheong'
        maxDelay = 12/60;
        minDelay = 2/60;
        defaultDelay = 2/60;
        [vofBat, output] = batcheong(venCpx.cluster.mean, time,framesPre(2),'constrained');
        [aifVoxBat, output] = batcheong(artPve.all.concList, time,framesPre(2),'constrained',artPve.all.excludeConc);
        
        aifVoxBatCorr = aifVoxBat;
        aifVoxBatCorr((vofBat-aifVoxBat)>maxDelay) = defaultDelay; % Wrong AIF delay calculation
        excludeVenousVoxels = ((vofBat-aifVoxBat)<minDelay);
        artPve.maskPveDelay(excludeVenousVoxels) = false;
    case ''
end
% Current implementation of delay correction is not optimal. Corrected 
% voxel enhancement curves still looks delayed. Dispersion might also
% affect these curves.

% artConcList = dyn3dconversion(artPve.conc,artPve.maskPve);
% caRef = mean(artConcList,1);
% arterialShiftMat = measconctimeshift(time,artPve.conc,framesPre(2),artPve.maskPve,caRef,venCpx.fit.conc,caRef);
% [cstTime, ~, shiftedArtConc, ~] = interpconcshiftandgap(time,caRef,artPve.conc,[time(1) time(end)],artPve.maskPve,-arterialShiftMat,0);

%% Average and cluster
aifOptions.aif.cluster.aif.pveFactorMap = artPve.factor.*artPve.intRatio; % Cluster selection criterion: Threshold between peak AUC or TTP criterion

[artPve.cluster, artPve.all] = aifconccomparemasks(...
    artPve.conc,artPve.maskPveDelay,artPve.maskPveDelay,aifOptions.aif.cluster,artPve.outlier);

artPve.cluster.statsPve.intRatio = mean(artPve.intRatio(artPve.cluster.mask));
artPve.cluster.statsPve.factor = mean(artPve.factor(artPve.cluster.mask));
artPve.all.statsPve.intRatio = mean(artPve.intRatio(artPve.all.mask));
artPve.all.statsPve.factor = mean(artPve.factor(artPve.all.mask));

%% Fit Horsfield AIF

batShiftStep = 0.5/60;
[bat, batOutput] = batcheong(artPve.cluster.mean(artPve.cluster.meanNotSat),time(artPve.cluster.meanNotSat),framesPre(2),batShiftStep);
batIdx = find(time>bat,1,'first')-1;
[aifFHandle,lb,ub,x0, fitOptions] = initfitaifhorsfield(time(artPve.cluster.meanNotSat),artPve.cluster.mean(artPve.cluster.meanNotSat), batIdx,batOutput.peakIdx,aifOptions.aif.fit);
% use2ndDeriv = true;
% [~, rangeAif, ~] = fitinitialenhancement(time,artPve.cluster.mean,framesPre(2),[],[], use2ndDeriv);
% [aifFHandle,lb,ub,x0, fitOptions] = initfitaifhorsfield(time,artPve.cluster.mean, rangeAif(2),rangeAif(5),aifOptions.aif.fit);

fixedAmplitude = true(1,11);fixedAmplitude(11) = false;
artPve.fit.param.FHandle = aifFHandle;
artPve.fit.param.x0 = x0;
artPve.fit.param.lb = lb;
artPve.fit.param.ub = ub;
artPve.fit.param.fitOptions = fitOptions;
artPve.fit.param.fixedAmplitude = fixedAmplitude;
[artPve.fit.pValues, artPve.fit.resnorm,artPve.fit.minXIdx,artPve.fit.conc] = ...
    fitaif(time,artPve.cluster.mean,aifFHandle,x0,lb,ub,fitOptions,fixedAmplitude,artPve.cluster.meanNotSat);

end