function [conc, outliers,conc2] = concmrispgropen(signal,signal0,tracerParam,T10,imgParam,mask,b1Map)
% CONCMRISPGROPEN Computes contrast agent concentration from the MRI 
% RF-spoiled Gradient-recalled echo steady-state signal with the full signal
% equation including T2*w effects. Two solutions are possible. This is
% based on the work of (Schabel and Parker, Phys. in Med. and Biol., 2008)
% but extended to allow quadratic blood transverse relaxivity.
%
%   signal = CONCMRISPGROPEN(conc,preInjIdx,tracerParam,tissueParam,imgParam)
%   Concentrations dynamic curves are computed from the tissue signal given
%   tracer parameters (relaxivity (r1,r2s)), tissue parameters (initial
%   relaxation rate (R10, R20) and initial signal (S0)) and sequence
%   parameters (te, tr, fa).
%
% See also CONCMRISPGRSIMPLE, CONCMRISPGRCLOSED, CONCMRISPGCPX
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

%% Input checks
% Units validation
if isfield(tracerParam,'r1')
    r1 = tracerParam.r1;
else
    warning('Missing T1 relaxivity, DR1 data wont be scaled to concentration.');
    r1 = 1000; % 1/(mM s)
end
if isfield(tracerParam,'r2s')
    r2s = tracerParam.r2s;
else
    warning('Missing T2 relaxivity, conc wont be corrected for T2*. Consider closed form equation instead.');
    r2s = 0; % 1/(mM s)  % T2* relaxation becomes independant of concentration
end
if isfield(tracerParam,'r22s') && isfield(tracerParam,'enableQuadR2') && tracerParam.enableQuadR2  
    r22s = tracerParam.r22s;
else
    r22s = []; % 1/(mM s)
end
te = imgParam.te;
tr = imgParam.tr;
fa = imgParam.fa;

meanT10 = mean(T10(mask));
[fa,te,tr,t10Scaling,~,r1, r2s, r22s,~] = checksignalconcunits(fa, te, tr, meanT10, [], r1, r2s,r22s, []);
R10 = 1./(T10*t10Scaling);

% Size conversion
if exist('b1Map','var') && ~isempty(b1Map)
    [listSignal, nIdxT, listSignal0, listR10,listB1Map] = dyn3dconversion(signal,mask,[],signal0,R10,b1Map);
else
    [listSignal, nIdxT, listSignal0, listR10] = dyn3dconversion(signal,mask,[],signal0,R10);
    listB1Map = 1;
end

%% Concentration calculation
% Preparation
r1ms = r1/1000; % from 1/(mM s) to 1/(mM ms)
r2sms = r2s/1000; % from 1/(mM s) to 1/(mM ms)
r22sms = r22s/1000; % from 1/(mM� s) to 1/(mM� ms)
fa = fa*listB1Map;

[nPx,nT] = size(listSignal);
listOutliers = false(nPx,nT);
listConc1st = zeros(nPx,nT);
listConc2st = zeros(nPx,nT);

normSignalRatio = zeros(nPx,nT); 
for iT=1:nT
    normSignalRatio(:,iT) = (listSignal(:,iT)-listSignal0)./listSignal0;
end

options = optimset('TolX',1e-6,'Display','off'); % fsolve options
% Find theoretical maximum signal ratio (Peak of Fig. 2 Schabel and Parker, 2008)
concAtMaxSignal = findconcatmaxsignal(tr,te,fa,listR10,r1ms,r2sms,r22sms,options);
defaultConcAtMax = 5;
concAtMaxSignal((concAtMaxSignal)>10) = defaultConcAtMax;
concAtMaxSignal((concAtMaxSignal)<0) = defaultConcAtMax;
concAtMaxSignal(isnan(concAtMaxSignal)) = defaultConcAtMax;
concAtMaxSignal(~isreal(concAtMaxSignal)) = defaultConcAtMax;
maxSignalRatio = relsignalenhancementdiff(concAtMaxSignal,0,tr,te,fa,listR10,r1ms,r2sms,r22sms);
c0 = 10*concAtMaxSignal;
% Find starting value for first solution
listMask = true(nPx,1);
[initConc1st, ~] = concmrispgrclosed(listSignal,listSignal0,tracerParam,1./listR10,imgParam,listMask,listB1Map);

if nT*nPx > 2000
    if exist('parfor_progress','file')==2
        parfor_progress(nPx);
        displayProgress = true;
    else
        warning('parfor_progress was not found in matlab path. Paralle processing open-form concentration calculation progress won''t be shown.')
        displayProgress = false;
    end
   parfor iPx=1:nPx
        for iT=1:nT
            fun = @(C) relsignalenhancementdiff(C,normSignalRatio(iPx,iT),tr,te,fa(iPx),listR10(iPx),r1ms,r2sms,r22sms);
            [listConc1st(iPx,iT),listConc2st(iPx,iT),listOutliers(iPx,iT)] = fitopenform(fun,normSignalRatio(iPx,iT),maxSignalRatio(iPx),initConc1st(iPx,iT),c0(iPx),concAtMaxSignal(iPx),options);
        end
        if displayProgress
        parfor_progress;
        end
   end
   if displayProgress
       parfor_progress(0);
   end
else
    hWait = waitbar(0,'Open-form signal to concentration');
    for iPx=1:nPx
        for iT=1:nT
            fun = @(C) relsignalenhancementdiff(C,normSignalRatio(iPx,iT),tr,te,fa(iPx),listR10(iPx),r1ms,r2sms,r22sms);
            [listConc1st(iPx,iT),listConc2st(iPx,iT),listOutliers(iPx,iT)] = fitopenform(fun,normSignalRatio(iPx,iT),maxSignalRatio(iPx),initConc1st(iPx,iT),c0(iPx),concAtMaxSignal(iPx),options);
        end
        if mod(iPx,10)==0
            waitbar(iPx/nPx,hWait);
        end
    end
    close(hWait);
end

%% Output size conversion
[conc, outliers, conc2] = mat2dyn3d(listConc1st, nIdxT,mask,listOutliers,listConc2st);
end

function [conc1st,conc2st,isOutlier] = fitopenform(fun,normSignalRatio,maxSignalRatio,initConc1st,c0,concAtMaxSignal,fSolveOptions)
if (normSignalRatio>=0)&&(normSignalRatio<=maxSignalRatio)
    % Two solutions
    conc1st = fsolve(fun,initConc1st,fSolveOptions);
    conc2st = fsolve(fun,c0,fSolveOptions);
    isOutlier = false;
elseif (normSignalRatio>=-1)&&(normSignalRatio<0)
    % Noise might cause xi < 0 when C = 0.
    % Currently this section only handles these noise
    % cases. Might be smart to include SNR in
    % this calculation.
    conc1st = 0;
    conc2st = fsolve(fun,c0,fSolveOptions);
    isOutlier = false;
elseif (normSignalRatio>maxSignalRatio) % (xi(iPx,iT)<-1)||(xi(iPx,iT)>xiMax(iPx))
    % warning('Signal ratio = %f > %f (MAX). Concentration is fixed at max conc for current enhancement value with coordinates (iPx,iT) = (%d,%d,%d,%d).',xi(iPx,iT), xiMax(iPx), iPx,iT);
    conc1st = concAtMaxSignal;
    conc2st = concAtMaxSignal;
    isOutlier = true;
elseif (normSignalRatio<-1)
    % warning('Signal ratio = %f < -1 (MIN). Concentration is fixed at 0 for current enhancement value with coordinates (iPx,iT) = (%d,%d,%d,%d).',xi(iPx,iT), iPx,iT);
    conc1st = 0;
    conc2st = conc1st;
    isOutlier = true;
end
end

function diffXi = relsignalenhancementdiff(C,xi,tr,te,fa,R10,r1,r2s,r22s)
% Based on eq. 4 of Schabel & Parker 2008. 
% xi = (S(t)-S(t0))/S(t0)
E10 = exp(-tr*R10);
E1 = E10.*exp(-tr*r1*C);
if exist('r22s','var') && ~isempty(r22s) % Quadratic T2* relaxivity (blood)
    DE2 = exp(-te*(r2s*C + r22s*C.^2)); % DE2 = E2/E20
else % Linear T2* relaxivity (tissues)
    DE2 = exp(-te*r2s*C);
end
diffXi = (DE2.*((E1-1).*(E10.*cos(fa)-1))./((E10-1).*(E1.*cos(fa)-1))-1) - xi;

end

function cmax = findconcatmaxsignal(tr,te,fa,R10,r1,r2s,r22s,options)
% Based on eq. 21 of Schabel & Parker 2008. 
phi = r1*tr + r2s*te;
psi = r1*tr - r2s*te;

r1max = 1/tr*log(1/(phi-psi)*(phi-psi*cos(fa)+sqrt((cos(fa)-1).*(psi^2*cos(fa)-phi^2))));

cmax = 1/r1*(r1max-R10);

if  exist('r22s','var') && ~isempty(r22s) % Quadratic T2* relaxivity (blood)
    cmax0 = cmax;
    % Iterative cmax determination
    % Use the linear calculation as x0
    nPx = length(R10);
    for iPx=1:nPx
        fun = @(currentCmax) maxsignalconcquadr2diff(currentCmax, tr,te,fa(iPx),R10(iPx),r1,r2s,r22s);
        cmax(iPx) = fsolve(fun,cmax0(iPx),options);
    end
end
end

function diffCMax = maxsignalconcquadr2diff(cmax, tr,te,fa,R10,r1,r2s,r22s)
% Extension of eq. 21 of Schabel & Parker 2008. to allow blood quadratic relaxivity 
phi = r1*tr + (r2s + 2*r22s*cmax)*te;
psi = r1*tr - (r2s + 2*r22s*cmax)*te;
r1max = 1/tr*log(1/(phi-psi)*(phi-psi*cos(fa)+sqrt((cos(fa)-1).*(psi^2*cos(fa)-phi^2))));

diffCMax = cmax - 1/r1*(r1max-R10);
end