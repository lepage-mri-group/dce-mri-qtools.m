function [conc, outliers,faFit,fittedDensity,fittedInitPhase] = concmrispgrcpx(signalMag,signalPhase,tracerParam,T10,imgParam,mask,b1Map,sssB0Ang,time)
% CONCMRISPGCPX Computes MRI contrast agent concentration from the 
% RF-spoiled Gradient-recalled echo steady-state signal. It fits signal 
% magnitude and phase for vascular input function (AIF or VOF)
% for vessels parallel with the main magnetic field. Ex : SSS or ICA
% Based on the method described in Simonis et al. MRM 76:1236-1245 (2016)
%
% Requires the full contrast agent calibration curves for r1, r2, and molar
% susceptibility. The method was adapted to enable quadratic r2* relaxivity
% as measured in whole blood.
%
%   signal = CONCMRISPGCPX(conc,preInjIdx,tracerParam,tissueParam,imgParam)
%   Concentrations dynamic curves are computed from the tissue signal given
%   tracer parameters (relaxivity (r1,r2s)), tissue parameters (initial
%   relaxation rate (R10, R20) and initial signal (S0)) and sequence
%   parameters (te, tr, fa).
%
% See also CONCMRISPGRSIMPLE, CONCMRISPGRCLOSED, CONCMRISPGROPEN
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

%% Input checks
% Units validation
debug = false;
if isfield(tracerParam,'r1')
    r1 = tracerParam.r1;
else
    r1 = 1;
end
if isfield(tracerParam,'r2s')
    r2s = tracerParam.r2s;
else
    r2s = 0; % T2* relaxation becomes independant of concentration
end
if isfield(tracerParam,'r22s') && isfield(tracerParam,'enableQuadR2') && tracerParam.enableQuadR2  
    r22s = tracerParam.r22s;
else
    r22s = []; % 1/(mM s)
end
if isfield(tracerParam,'xiM')   
    xiM = tracerParam.xiM;
else
    xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
end
if isfield(tracerParam,'framesBaseline')
    framesBaseline = tracerParam.framesBaseline;
else
    framesBaseline = [1 30];
end

te = imgParam.te;
if length(te)>1
    te = te(1);
    warning('Multi echo sequence detected. CPX conc calculation only uses first echo by default.');
end
tr = imgParam.tr;
fa = imgParam.fa;
h1GyromagRatio = 2.675e8; % rad/(T s)
if isfield(imgParam,'B0')
    larmorRad = h1GyromagRatio*imgParam.B0;
else
    defaultField = 3; % T
    larmorRad = h1GyromagRatio*defaultField; % (rad/s) @3T
end
if isfield(imgParam,'T20s')
    T20s = imgParam.T20s;
else % Default T20 from Simonis
    T20s = 275/2; % ms (T20star ~ Half blood T20) Stanisz et al. 2005
end
meanT10 = mean(T10(mask));
[fa,te,tr,t10Scaling,~,r1, r2s, ~,~] = checksignalconcunits(fa, te, tr, meanT10, [], r1, r2s,[], []);
T10 = (T10*t10Scaling);
% Size conversion
if (exist('sssB0Ang','var') && ~isempty(sssB0Ang)) && (exist('b1Map','var') && ~isempty(b1Map))
    [listSignalMag, nIdxT, listSignalPhase, listT10,listB1Map,listSssB0Ang] = dyn3dconversion(signalMag,mask,[],signalPhase,T10,b1Map,sssB0Ang);
elseif exist('sssB0Ang','var') && ~isempty(sssB0Ang)
    [listSignalMag, nIdxT, listSignalPhase, listT10,listSssB0Ang] = dyn3dconversion(signalMag,mask,[],signalPhase,T10,sssB0Ang);
    listB1Map = 1;
elseif exist('b1Map','var') && ~isempty(b1Map)
    [listSignalMag, nIdxT, listSignalPhase, listT10,listB1Map] = dyn3dconversion(signalMag,mask,[],signalPhase,T10,b1Map);
    [nPx,nT] = size(listSignalMag);
    listSssB0Ang = zeros(nPx,1);
else
    [listSignalMag, nIdxT, listSignalPhase, listT10] = dyn3dconversion(signalMag,mask,[],signalPhase,T10);
    listB1Map = 1;
    [nPx,nT] = size(listSignalMag);
    listSssB0Ang = zeros(nPx,1);
end
[nPx,nT] = size(listSignalMag);
listT20 = T20s*ones(nPx,1);

%% Concentration calculation
% Preparation
r1ms = r1/1000; % from 1/(mM s) to 1/(mM ms)
r2sms = r2s/1000; % from 1/(mM s) to 1/(mM ms)
r22sms = r22s/1000; % from 1/(mM� s) to 1/(mM� ms)


% Find the baseline magnitude and phase
meanSCpx0 = mean(listSignalMag(:,framesBaseline(1):framesBaseline(2)).*exp(-1j*listSignalPhase(:,framesBaseline(1):framesBaseline(2))),2);
meanS0 = abs(meanSCpx0);
meanTheta0 = -angle(meanSCpx0);
% Initial phase is manually set to the average phase value
% This was added (Not in the Simonis method) because the noise in the initial phase data causes a bad
% fit of C_baseline
for iPx=1:nPx
    listSignalPhase(iPx,framesBaseline(1):framesBaseline(2)) = meanTheta0(iPx);
end
% Compute complex signal
measCpxS = NaN(nPx,2*nT);
measCpxS(:,1:nT) = listSignalMag.*cos(listSignalPhase);
measCpxS(:,nT+1:2*nT) = -listSignalMag.*sin(listSignalPhase);

% Initial values
maxSssB0 = 45/180*pi; % allows up to a quarter of maximum phase sensitivity and avoids the zero at 54.7 degrees
fitSssB0 = false; % This parameter is too noisy to be fitted
[W, x0, lb, ub,order,nOffsetConc] = preparecpxfit(time,framesBaseline(2),fa,fitSssB0);

fa = fa*listB1Map;
fittedDensity = NaN(nPx,1);
fittedInitPhase = NaN(nPx,1);
fittedFa = NaN(nPx,1);
fittedSssBoAng = NaN(nPx,1);
fittedConc = NaN(nPx,nT);
listOutliers = false(nPx,nT);

options = optimset('TolX',1e-10,'Display','off'); % fzero options

hWait = waitbar(0,'Complex-form signal to concentration');
for iPx=1:nPx
    x0(1) = findrho0(meanS0(iPx),fa(iPx),tr,listT10(iPx),te,listT20(iPx)); % S0
    x0(2) = meanTheta0(iPx); % theta0
    if isnan(listSssB0Ang(iPx))|| listSssB0Ang(iPx)<0
        x0(4) = 0;        
    elseif listSssB0Ang(iPx)>maxSssB0
        x0(4) = maxSssB0;        
    else
        x0(4) = listSssB0Ang(iPx); % vessel to B0 angulation
    end
    fixedX = x0(~order);
    
    ub(1) = 100*x0(1);
    ub(4) = min(maxSssB0,x0(4)+10/180*pi);
    lb(4) = max(0,x0(4)-10/180*pi);
    % Full fitting
    cpxFun = @(varX)cpxsignaldiff(varX,fixedX,order,measCpxS(iPx,:)',W,tr,te,listT10(iPx),r1ms,listT20(iPx),r2sms,r22sms,larmorRad,xiM);
    varX = lsqnonlin(cpxFun,x0(order),lb(order),ub(order),options); % 
    
    % Assign fixed and var values.
    x = NaN(length(order),1);
    x(order) = varX;
    x(~order) = fixedX;
    
    
    if (x(3) == lb(3)) || (x(3) == ub(3)) % FA reaches bounds
        listOutliers(iPx,:) = true;
    else
        listOutliers(iPx,:) = (x((nOffsetConc+1):end) == ub((nOffsetConc+1):end)); % Conc reaches upper bound
    end
    % Sub fitting
%     cpxFun = @(x)cpxsignaleqinit(x,x0(1),x0(2),measCpxS(iPx,:)',W,tr,te,listT10(iPx),r1ms,listT20,r2ms,r22ms,w0,xiM);
%     x = lsqnonlin(cpxFun,x0(3:end),lb(3:end),ub(3:end)); % 
    
    fittedDensity(iPx) = x(1);
    fittedInitPhase(iPx) = x(2);
    fittedFa(iPx) = x(3);
    fittedSssBoAng(iPx) = x(4);
    fittedConc(iPx,:) = x((nOffsetConc+1):end)';
    if debug
        fittedS = cpxsignaleq(x(4:end),x(1),x(2),x(3),tr,te,listT10(iPx),r1ms,listT20(iPx),r2sms,r22sms,larmorRad,xiM,listSssB0Ang(iPx));
        
        figure;
        set(gcf,'Position',[154         414        1260         420]);
        subplot(1,3,1);
        plot(measCpxS(iPx,1:end/2)',measCpxS(iPx,(end/2+1):end)','xb');
        hold on;
        plot(meanS0(iPx)*cos(meanTheta0(iPx)),-meanS0(iPx)*sin(meanTheta0(iPx)),'xr');
        plot(real(fittedS),imag(fittedS),'.m');
        hold off;
        xlabel('Real (a.u.)');
        ylabel('Imaginary (a.u.)');
        legend('Input','Initial value','Weighted fit');
        subplot(1,3,2);
        plot(measCpxS(iPx,:)','x');hold on;
        plot([real(fittedS);imag(fittedS)]);
        xlabel('Time point (-)');
        ylabel('Signal (a.u.)');
        legend('Input','Weighted fit');
        subplot(1,3,3);
        plot(x(4:end)');
        xlabel('Time point (-)');
        ylabel('Concentration (mM)');
    end
    if mod(iPx,10)==0
        waitbar(iPx/nPx,hWait);
    end
end
close(hWait);

%% Output size conversion
[conc, outliers, faFit] = mat2dyn3d(fittedConc, nIdxT,mask,listOutliers,fittedFa);
end

function [W, x0, lb, ub,order,nOffsetConc] = preparecpxfit(time,nLastBaseline,fa,fitSssB0)
nT = length(time);
% Fitting weight
% Simonis did not describe is exact weighting procedure.
% He only lowered the weight of the tail of the AIF, described as points
% betwen the 30th and 120th dynamic @ 2.4s/dyn = 72-288 s

% Find the time frame corresponding to 72 s post injection
nExcludedSimonis = (120-30);
% Reduced weight is set at 1/200 in the tail of the AIF
approxSimonisWeight = 0.01;
tIncluded = 72;
nIncluded = find((time - time(nLastBaseline))*60>tIncluded,1,'first');
if isempty(nIncluded) % Include all points
        nIncluded = nT-nLastBaseline;
else
    nIncluded = nIncluded - nLastBaseline;
end
nExcluded = nT-nIncluded-nLastBaseline;
scaledWeightExcluded = approxSimonisWeight*nExcludedSimonis/nExcluded;
weightVect = repmat([1*ones(1,nLastBaseline) ones(1,nIncluded) scaledWeightExcluded*ones(1,nExcluded)],[1, 2]);
W = diag(weightVect);

nOffsetConc = 4;
% Initial values
x0 = NaN(nT+nOffsetConc,1);
x0(3) = fa; % fa nom
x0((nOffsetConc+1):(nOffsetConc+nLastBaseline)) = 0; % mM
x0((nOffsetConc+nLastBaseline+1):end) = 1; % mM

% Fix baseline concentration to zero
order = true(nT+nOffsetConc,1);
if fitSssB0
    order(4) = true;
else
    order(4) = false;
end
order((nOffsetConc+1):(nOffsetConc+nLastBaseline)) = false;

% Lower bound
lb = NaN(nT+nOffsetConc,1);
lb(1) = 0;
lb(2) = -pi;
lb(3) = 0.5*fa;%2/180*pi; % fa nom
lb((nOffsetConc+1):(nOffsetConc+nLastBaseline)) = -0.05; % mM
lb((nOffsetConc+nLastBaseline+1):end) = -0.1; % mM

% Higher bound
ub = NaN(nT+nOffsetConc,1);
ub(2) = pi;
ub(3) = 1.5*fa;%60/180*pi; % fa nom
ub((nOffsetConc+1):(nOffsetConc+nLastBaseline)) = 10; % mM
ub((nOffsetConc+nLastBaseline+1):end) = 10; % mM

end

function SdiffVector = cpxsignaldiff(varX,fixedX,order,Smeas,W,tr,te,T10,r1,T20,r2,r22,larmorRad,xiM)
% x(1) = rho_eff = density
% x(2) = theta_0 initial phase
% x(3) = alpha = flip angle
% x(4) = vessel to B0 angle
% x(5:end) = c = concentration time points
nOffsetConc = 4;

% Assign fixed and var values.
x = NaN(length(order),1);
x(order) = varX;
x(~order) = fixedX;

% Compute signal
SMag = cpxsignaleq(x((nOffsetConc+1):end),x(1),x(2),x(3),tr,te,T10,r1,T20,r2,r22,larmorRad,xiM,x(4));

% Real and imaginary parts splitted
SdiffVector = W*([real(SMag);imag(SMag)] - Smeas);
end

% function SdiffVector = cpxsignaleqinit(x,s0,theta0,Smeas,W,tr,te,T10,r1,T20,r2,w0,xiM)
% % Compute signal
% SMag = cpxsignaleq(x(2:end),s0,theta0,x(1),tr,te,T10,r1,T20,r2,w0,xiM);
% 
% % Real and imaginary parts splitted
% SdiffVector = W*([real(SMag);imag(SMag)] - Smeas);
% end

function SMag = cpxsignaleq(C,S0,theta0,fa,tr,te,T10,r1,T20,r2,r22,larmorRad,xiM,sssB0Ang)
% Concentration changes
nT = length(C);
onesNt = ones(nT,1);
R1C = (onesNt/T10+r1*C);
if exist('r22','var') && ~isempty(r22) % Quadratic T2* relaxivity (blood)
    R2C = (onesNt/T20+r2*C+r22*C.^2);
else
    R2C = (onesNt/T20+r2*C);
end
thetaC = (te/1e3)*larmorRad*xiM*C*(cos(sssB0Ang)^2-1/3)/2;

E1 = exp(-tr*R1C);
E2 = exp(-te*R2C);

% Cpx magn diff
SMag = S0 ...
    *(sin(fa)*(onesNt-E1))./(onesNt-cos(fa)*E1)...
    .*E2...
    .*exp(-1j*(thetaC+theta0)); 
end

function rho0 = findrho0(S,fa,tr,T10,te,T20)

rho0 = S*(1-cos(fa)*exp(-tr/T10))/(sin(fa)*(1-exp(-tr/T10)))/exp(-te/T20);

end