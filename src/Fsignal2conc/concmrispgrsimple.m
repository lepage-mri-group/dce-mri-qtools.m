function [conc, outliers] = concmrispgrsimple(signal,signal0,tracerParam,T10,imgParam,mask)
% CONCMRISPGRSIMPLE Computes contrast agent concentration from MRI 
% RF-spoiled Gradient-recalled echo steady-state signal with the linear 
% approximation of signal equation. Was compared to the non-linear model in 
% Heilmann et al., Inv. Radiol., 2006.
% Requires a tissue/sequence specific calibration factor instead or 
% relaxivity. Usually taken from a vial in the FOV. 
% If initial relaxation rate (1/T10) and TR are provided, relaxivity can be
% used. Only valid for (TR * r1 * C) << 1
% 
%   signal = CONCMRISPGRSIMPLE(conc,preInjIdx,tracerParam,tissueParam,imgParam)
%   Concentrations dynamic curves are computed from the tissue signal given
%   tracer parameters (relaxivity (r1,r2s)), tissue parameters (initial
%   relaxation rate (R10, R20) and initial signal (S0)) and sequence
%   parameters (te, tr, fa).
%
% See also CONCMRISPGRCLOSED, CONCMRISPGROPEN, CONCMRISPGCPX
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-03

%% Input checks
% Units validation
if isfield(tracerParam,'r1') && isfield(imgParam,'tr') ...
&& exist('T10','var') && ~isempty(T10)
    r1 = tracerParam.r1;
    tr = imgParam.tr;
    meanT10 = mean(T10(mask));
    [~, ~, ~,t10Scaling, ~,r1, ~, ~,~] = checksignalconcunits([], [], [], meanT10, [], r1, [],[], []);
    R10 = 1./(T10*t10Scaling);
    F = tr*exp(-tr*R10)./(1-exp(-tr*R10));
    % Size conversion
    [listSignal, nIdxT, listSignal0, listF] = dyn3dconversion(signal,mask,[],signal0,F);
elseif isfield(tracerParam,'specificPropCst')
    listF = tracerParam.specificPropCst;
    r1 = 1000; % 1/(mM s)
    % Size conversion
    [listSignal, nIdxT, listSignal0] = dyn3dconversion(signal,mask,[],signal0);
else % Relative concentration
    listF = 1;
    r1 = 1000; % 1/(mM s)
    [listSignal, nIdxT, listSignal0] = dyn3dconversion(signal,mask,[],signal0);
end


%% Concentration calculation
% Preparation
r1ms = r1/1000; % from 1/(mM s) to 1/(mM ms)
[~, nT] = size(listSignal);
dr1 = NaN(size(listSignal));
for iT=1:nT
    dr1(:,iT) = (listSignal(:,iT)./listSignal0 - 1)./listF;
end
listConc = dr1/r1ms; % Relative concentration

%% Output size conversion
conc = mat2dyn3d(listConc, nIdxT,mask);
outliers = (T10==0) && (listSignal0==0);
end