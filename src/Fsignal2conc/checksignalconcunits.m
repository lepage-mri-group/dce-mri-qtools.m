function [fa, te, tr, t10Scaling, t20Scaling, r1, r2, r22, xiM] = checksignalconcunits(fa, te, tr, meanT10, meanT20, r1, r2,r22, xiM)
% CHECKSIGNALCONCUNITS Check the units of typical variables for
% concentration calculation
%
%   [fa, te, tr, t10Scaling, t20Scaling, r1, r2, r22, xiM] = CHECKSIGNALCONCUNITS(fa, te, tr, meanT10, meanT20, r1, r2,r22, xiM)
%   Replace unwatnted parameters with empy vector if needed.
%
% See also 
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

if exist('fa','var') && ~isempty(fa) && fa>pi % Expected units : Radians
    warning('Flip angle is superior to PI. Degree flip angle is suspected and was switched to radians.');
    fa = fa/180*pi;
end
if exist('te','var') && ~isempty(te) && any(te < 0.5) % Expected units : ms
    warning('TE is under 0.5 ms. TE is probably in [s], converting to [ms]');
    te = te*1000;
end
if exist('tr','var') && ~isempty(tr) && tr < 3 % Expected units : ms
    warning('TR is under 3 ms. TR is probably in [s], converting to [ms]');
    tr = tr*1000;
end
if exist('meanT10','var') && ~isempty(meanT10) && meanT10<100  % Expected units : ms
    warning('T1 map average is under 100 ms in the mask. T1 is probably in [s], converting to [ms]');
    t10Scaling = 1000;
else
    t10Scaling = 1;
end
if exist('meanT20','var') && ~isempty(meanT20) && meanT20<1  % Expected units : ms
    warning('T2 map average is under 1 ms in the mask. T2 is probably in s, converting to [ms]');
    t20Scaling = 1000;
else
    t20Scaling = 1;
end
if exist('r1','var') && ~isempty(r1) && r1>1001  % Expected units : 1/(s mM)
    warning('T1 relaxivity is over 1001 [1/(s mM)], is probably in [1/(s M)], converting to [1/(s mM)]');
    r1 = r1/1000;
end
if exist('r2','var') && ~isempty(r2) && r2>1001  % Expected units : 1/(s mM)
    warning('T2 relaxivity is over 1001 [1/(s mM)], is probably in [1/(s M)], converting to [1/(s mM)]');
    r2 = r2/1000;
end
if exist('r22','var') && ~isempty(r22) && r22>1e4  % Expected units : 1/(s mM�)
    warning('T2 quadratic relaxivity coefficient is over 20 [1/(s mM�)], is probably in [1/(s M�)], converting to [1/(s mM�)]');
    r22 = r22/(1000)^2;
end
if exist('xiM','var') && ~isempty(xiM) && xiM>1e-4  % Expected units : 1/(mM)
    warning('Molar susceptibility is over 1e-4 [1/mM], is probably in [1/M], converting to [1/mM]');
    r22 = r22/(1000)^2;
end