function [conc, outliers] = concmrispgrclosed(signal,signal0,tracerParam,T10,imgParam,mask,b1Map)
% CONCMRISPGRCLOSED Computes contrast agent concentration from the MRI 
% RF-spoiled Gradient-recalled echo steady-state signal with the analytical
% non-linear equation neglecting T2*w effects. This is the recommended 
% Method A of QIBA, 2012 with added concentration thresholds to limit error
% propagation.
%
%   signal = CONCMRISPGRCLOSED(conc,preInjIdx,tracerParam,tissueParam,imgParam)
%   Concentrations dynamic curves are computed from the tissue signal given
%   tracer parameters (relaxivity (r1,r2s)), tissue parameters (initial
%   relaxation rate (R10, R20) and initial signal (S0)) and sequence
%   parameters (te, tr, fa).
%
% See also CONCMRISPGRSIMPLE, CONCMRISPGROPEN, CONCMRISPGCPX
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

%% Input checks
% Units validation
if isfield(tracerParam,'r1')
    r1 = tracerParam.r1;
else
    warning('Missing T1 relaxivity, DR1 data wont be scaled to concentration.');
    r1 = 1000; % 1/(mM s)
end
tr = imgParam.tr;
fa = imgParam.fa;

meanT10 = mean(T10(mask));
[fa,~,tr,t10Scaling,~,r1, ~, ~,~] = checksignalconcunits(fa, [], tr, meanT10, [], r1, [],[], []);
R10 = 1./(T10*t10Scaling);

% Size conversion
if exist('b1Map','var') && ~isempty(b1Map)
    [listSignal, nIdxT, listSignal0, listR10,listB1Map] = dyn3dconversion(signal,mask,[],signal0,R10,b1Map);
else
    [listSignal, nIdxT, listSignal0, listR10] = dyn3dconversion(signal,mask,[],signal0,R10);
    listB1Map = 1;
end

%% Concentration calculation
% Preparation
r1ms = r1/1000; % from 1/(mM s) to 1/(mM ms)
fa = fa*listB1Map;

[nPx,nT] = size(listSignal);
dr1 = NaN(nPx,nT);
listOutliers = false(nPx,nT);
f10 = (1-cos(fa).*exp(-tr.*listR10)) ./ (1-exp(-tr.*listR10));

% Non-linear equation is sensitive to noise propagation at extremum
% Max and min concentration are fixed and labeled as outliers to avoid fluctuation
minDr1 = -0.025; % (ms)^-1
% Arbitrary factor (~0.8 to 1.0) to limit usage of signal equation under f10 theorical
% limit. This factor can be used to limit error propagation.
maxSignalRatioFactor = 1;
maxSignalRatio = maxSignalRatioFactor*f10;
if maxSignalRatioFactor~=1
    maxDr1Value = log((1-maxSignalRatioFactor*cos(fa))/(1-maxSignalRatioFactor))/tr - listR10;
else
    maxDr1Value = inf(nPx,1);
end

for iT=1:nT
    currentSignalRatio = listSignal(:,iT)./listSignal0;
    currentDr1 = log((f10-cos(fa).*currentSignalRatio) ./ ...
        (f10 - currentSignalRatio) )/tr - listR10;
    
    inferiorToMin = currentDr1<minDr1;
    superiorToMax = currentSignalRatio > maxSignalRatio;
    isImaginary = imag(currentDr1)~=0;
    currentDr1(isImaginary) = 0;
    currentDr1(inferiorToMin) = minDr1;
    currentDr1(superiorToMax) = maxDr1Value(superiorToMax);
    dr1(:,iT) = currentDr1;
    
    listOutliers(:,iT) = listOutliers(:,iT) | inferiorToMin | superiorToMax | isImaginary;
end
listConc = dr1/r1ms;

%% Output size conversion
[conc, outliers] = mat2dyn3d(listConc, nIdxT,mask,listOutliers);
end