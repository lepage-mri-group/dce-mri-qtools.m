function [conc, outliers] = concmrispgrphase(signalPhase,signalPhase0,tracerParam,sssB0Ang,imgParam,mask)
% CONCMRISPGRPHASE Computes contrast agent concentration from MRI 
% RF-spoiled Gradient-recalled echo steady-state signal phase.
% Requires a the molar susceptinility of the contrast agent. 
% 
%   conc = CONCMRISPGRPHASE(signalPhase,signalPhase0,tracerParam,tissueParam,imgParam)
%   Concentrations dynamic curves are computed from the tissue signal given
%   tracer parameters (susceptibility (xm)), initial signal phase (S0)) and
%   sequence parameters (te, tr, fa).
%
% See also CONCMRISPGRSIMPLE, CONCMRISPGRCLOSED, CONCMRISPGROPEN, CONCMRISPGCPX
% Author: Benoit Bourassa-Moreau
% Creation date: 2021-01-06

%% Input checks
% Units validation
if isfield(tracerParam,'xiM')   
    xiM = tracerParam.xiM;
else
    xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
end
te = imgParam.te;
if length(te)>1
    te = te(1);
    warning('Multi echo sequence detected. CPX conc calculation only uses first echo by default.');
end
h1GyromagRatio = 2.675e8; % rad/(T s)
if isfield(imgParam,'B0')
    larmorRad = h1GyromagRatio*imgParam.B0;
else
    defaultField = 3; % T
    larmorRad = h1GyromagRatio*defaultField; % (rad/s) @3T
end
% Size conversion

if exist('sssB0Ang','var') && ~isempty(sssB0Ang)
    [listSignalPhase, nIdxT, listSignalPhase0,listSssB0Ang] = dyn3dconversion(signalPhase,mask,[],signalPhase0,sssB0Ang);
else
    [listSignalPhase, nIdxT, listSignalPhase0] = dyn3dconversion(signalPhase,mask,[],signalPhase0);
    listSssB0Ang = zeros(size(listSignalPhase,1),1);
end
%% Concentration calculation
% Preparation
listConc = phase2conc(listSignalPhase,listSignalPhase0,te,larmorRad,xiM,listSssB0Ang);

%% Output size conversion
conc = mat2dyn3d(listConc, nIdxT,mask);
outliers = [];
end

function conc = phase2conc(signalPhase,signalPhase0,te,larmorRad,xiM,sssB0Ang)
% Concentration changes
nT = size(signalPhase,2);
onesNt = ones(nT,1);

conc = (signalPhase-repmat(signalPhase0,[1,nT]))./((te/1e3)*larmorRad*xiM*(cos(repmat(sssB0Ang,[1,nT])).^2-1/3)/2);
end