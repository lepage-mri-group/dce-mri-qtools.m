function [corrConcMat,corrBloodFractMap,pvcFailedMask,integralRatioMap,open2ndSolMap] = ...
    aifpvesignal(time,vofConc,magDyn,phaseDyn,t10Blood,B1Map,...
    bloodParamTracer,seqParam,...
    rangeIdx0,...
    maskAif,pxListAif,maskSurround,pxListSurround,...
    pveOptions)
% AIFPVESIGNAL AIF concentration calculation correcting for the partial 
% volume effect (PVE) by conservation of the area under VOF concentration 
% curve (AUC). PVE is corrected on arterial signal, not concentration 
% (Hansen et al., 2009). This approach was modified to consider constant 
% pre-enhancement tissue signal:
%   Hansen approach :   (Arterial signal) = (Measured signal)/PVE
%   Our approach :      (Arterial signal) = ((Measured signal)/PVE - (1 - PVE)*(Tissue signal))/PVE 
%  
%   [corrConcMat,corrBloodFractMap,pvcFailedMask,integralRatioMap,open2ndSolMap] = ...
%       aifpvesignal(time,vofConc,magDyn,phaseDyn,t10Blood,B1Map,...
%       bloodParamTracer,seqParam,rangeIdx0,...
%       maskAif,pxListAif,maskSurround,pxListSurround,pveOptions)
%
% See also: AIFPVECONC
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-08-09

%% Input validation and size conversion
nT = length(time);

correctR2s = false;
if isfield(bloodParamTracer,'enableBloodR2') && bloodParamTracer.enableBloodR2
    correctR2s = true;
end
if exist('pveOptions','var') && isfield(pveOptions,'tissCorrType') && ~isempty(pveOptions.tissCorrType)
    if strcmp(pveOptions.tissCorrType,'static')
        staticTissueCorr = true;
    else
        staticTissueCorr = false;
    end
else
    staticTissueCorr = false;
end

if isempty(phaseDyn)
    phaseDyn = zeros(size(magDyn));
end
nPve = length(pveOptions.bloodFractions);

[listAifSignalMag, nIdxT, listAifSignalPhase,listB1Map] = dyn3dconversion(magDyn,maskAif,pxListAif,phaseDyn,B1Map);
[listTissuesSignalMag, ~,listTissuesSignalPhase] = dyn3dconversion(magDyn,maskSurround,pxListSurround,phaseDyn);
if isfield(pveOptions,'initBloodFrac')
    initBloodFracList = pveOptions.initBloodFrac(maskAif);
    limitPve = true;
else
    limitPve = false;
end

aifCpxSignal = listAifSignalMag.*exp(-1j*listAifSignalPhase);
tissCpxSignal = listTissuesSignalMag.*exp(-1j*listTissuesSignalPhase);

%% VOF area under the curve (AUC) - Reference "PVE free" AUC 
aucVof = timeconcintegral(time, vofConc, pveOptions.rangeVofIAUC(1):pveOptions.rangeVofIAUC(2));% (mM min)

shiftedAifRange = pveOptions.rangeVofIAUC;
shiftedAifRange(1) = find(time >= time(pveOptions.rangeVofIAUC(1))-pveOptions.reducedAifTime,1,'first');
shiftedAifRange(2) = find(time >= time(pveOptions.rangeVofIAUC(2))-pveOptions.reducedAifTime,1,'first');

%% Corrected arterial signal for each blood fraction - Remove tissue signal from measured signal
tissuesSignal0 = mean(tissCpxSignal(:,rangeIdx0(1):rangeIdx0(2)),2);

[nPts,~] = size(aifCpxSignal);

arterialSignal = NaN(nPts,nPve,nT);
for iPve = 1:nPve
    for iPts = 1:nPts
        if staticTissueCorr
            arterialSignal(iPts,iPve,:) = reshape(abs(aifCpxSignal(iPts,:)/pveOptions.bloodFractions(iPve) - ((1-pveOptions.bloodFractions(iPve))/pveOptions.bloodFractions(iPve))*tissuesSignal0(iPts)),[1 1 nT]);
        else
            arterialSignal(iPts,iPve,:) = reshape(abs(aifCpxSignal(iPts,:)/pveOptions.bloodFractions(iPve) - ((1-pveOptions.bloodFractions(iPve))/pveOptions.bloodFractions(iPve))*tissCpxSignal(iPts,:)),[1 1 nT]);
        end
    end
end

%% Compute arterial concentration for each blood fraction
arterialSignal0 = mean(arterialSignal(:,:,rangeIdx0(1):rangeIdx0(2)),3);
T10 = t10Blood*ones(nPts,nPve);
b1MapPve = repmat(listB1Map,[1 nPve]);
maskConcCalc = true(nPts,nPve);
if correctR2s
    for iPts = 1:nPts
        if limitPve && initBloodFracList(iPts)>0 && ~isnan(initBloodFracList(iPts))
            maskConcCalc(iPts,:) = pveOptions.bloodFractions > (initBloodFracList(iPts) - pveOptions.bloodFractionGap) & ...
                pveOptions.bloodFractions < (initBloodFracList(iPts) + pveOptions.bloodFractionGap) & ...
                repmat(~isnan(tissuesSignal0(iPts)),1,nPve);
        else
            maskConcCalc(iPts,:) = repmat(~isnan(tissuesSignal0(iPts)),1,nPve); % Remove voxels without surrounding tissues
        end
    end
    [conc1Pve, outliersPve,conc2Pve] = concmrispgropen(arterialSignal,arterialSignal0,bloodParamTracer,T10,seqParam,maskConcCalc,b1MapPve);
    % Mixing the two solutions for final concPve is a challenging step
    
    % 1st solution (conc1Pve) is the true concentation for a vast majority
    % of physiological concentrations
    concPve = conc1Pve;
    % 2nd solution (conc2Pve) are non-realistic except for some of the 
    % saturated peak AIF and VOF.
    % VOF data being available for PVE compensation, we use this to guide
    % solution selection. The best AIF solution should be close to VOF.
    % However, 2nd solution should be used with caution.
    % - this is at risk of matching AUC at lower blood fraction thant the true
    % solution.
    % - delay and dispersion between AIF voxels and VOF might cause 
    % problems.
    % We use 3 criterion:
    % 1. 2nd solution is closer to VOF concentration than the 1st solution
    maxFractionDiff = 1.3; % 2. 2nd solution cannot be over venous concentration by more than this ratio
    maxConcGapFraction = 0.6; % 3. Gaps between 1st and 2nd solution cannot be bigger than this fration of the maximum venous concentration
    % Venous concentration is shifted for the approximate delay to help
    vofConcDefaultShift = interp1(time-pveOptions.reducedAifTime,vofConc,time,'linear',0);
    use2ndSolution = mixopenconcsolutions(conc1Pve,conc2Pve,vofConcDefaultShift,maxFractionDiff,maxConcGapFraction);
    concPve(use2ndSolution) =  conc2Pve(use2ndSolution);
else
    [concPve, outliersPve] = concmrispgrclosed(arterialSignal,arterialSignal0,bloodParamTracer,T10,seqParam,maskConcCalc,b1MapPve);
end
outliersPve(isnan(concPve)) = true;

%% Voxelwise arterial area under the curve 
aucArterial = NaN(nPts,nPve);
nCurrentOutliers = NaN(nPts,nPve);
for iPve = 1:nPve
    for iPts = 1:nPts
        currentOutliers = permute(outliersPve(iPts,iPve,:),[1,3,2]);
        aucKeptData = ~currentOutliers;
        if shiftedAifRange(1)>1
            aucKeptData(1:(shiftedAifRange(1)-1)) = false;
        end
        if shiftedAifRange(2)<nT
            aucKeptData((shiftedAifRange(2)+1):end) = false;
        end
        nCurrentOutliers(iPts,iPve) = sum(currentOutliers);
        if sum(aucKeptData)>1 
            aucArterial(iPts,iPve) = timeconcintegral(time, reshape(concPve(iPts,iPve,:),[1,nT]), aucKeptData);% (mM min)
        else
            aucArterial(iPts,iPve) = NaN;
        end
    end
end

%% Find PVE correction with maximum time integral conservation
nMaxOut = 5;
maxPveIdx = zeros(1,nPts);
allMinIdx = zeros(1,nPts);
integralRatioList = zeros(nPts,1);
corrConcList = zeros(nPts,nT);
open2ndSol = false(nPts,nT);
corrbloodFractList = zeros(nPts,1);
pvcFailedList = false(nPts,nT);
nConcFailed = 0;
for iPts = 1:nPts
    % Find max PVE without bad conc values
    [~,currentMaxPve] = find(nCurrentOutliers(iPts,:)>nMaxOut,1,'first');
    if isempty(currentMaxPve)
        maxPveIdx(iPts) = nPve;
    else
        maxPveIdx(iPts) = currentMaxPve-1;
    end
    if maxPveIdx(iPts)>1
        [~,allMinIdx(iPts)] = min(abs(aucArterial(iPts,1:(maxPveIdx(iPts)-1))/aucVof-1),[],2);
        integralRatioList(iPts) = aucArterial(iPts,allMinIdx(iPts))/aucVof;
        corrConcList(iPts,:) = reshape(concPve(iPts,allMinIdx(iPts),:),[1, nT]);
        corrbloodFractList(iPts) = pveOptions.bloodFractions(allMinIdx(iPts));
        if correctR2s
            open2ndSol(iPts,:) = reshape(use2ndSolution(iPts,allMinIdx(iPts),:),[1, nT]);
        end
        pvcFailedList(iPts,:) = reshape(outliersPve(iPts,allMinIdx(iPts),:),[1, nT]);
    else
        nConcFailed = nConcFailed +1;
        pvcFailedList(iPts,:) = true;
    end
end

%% Output size conversion
if correctR2s
    [corrConcMat,corrBloodFractMap,pvcFailedMask,integralRatioMap,open2ndSolMap] = mat2dyn3d(corrConcList, nIdxT,maskAif,corrbloodFractList,pvcFailedList,integralRatioList,open2ndSol);
else
    open2ndSolMap = [];
    [corrConcMat,corrBloodFractMap,pvcFailedMask,integralRatioMap] = mat2dyn3d(corrConcList, nIdxT,maskAif,corrbloodFractList,pvcFailedList,integralRatioList);
end
%% Debug
% figPve = displayaifpveresults(pveOptions.bloodFractions,aucArterial,aucVof,nPts,allMinIdx,maxPveIdx);
end

function concTimeInt = timeconcintegral(injTime, conc, rangeIdxPve)
concTimeInt = trapz(injTime(rangeIdxPve),conc(rangeIdxPve)); % (mM min)
end

function use2ndSolution = mixopenconcsolutions(conc1Pve,conc2Pve,vofConc,maxFractionDiff,maxConcGapFraction)
[nPts,nPve,nT] = size(conc1Pve);
[maxVofConc,peakVofIdx] = max(vofConc);
maxConcGap = maxConcGapFraction*maxVofConc;

vofMat = repmat(reshape(vofConc,[1 1 nT]),nPts,nPve);
use2ndSolution = (abs(conc1Pve-vofMat) > abs(conc2Pve-vofMat)) & ((conc2Pve-vofMat)<maxFractionDiff*vofMat);
chosenSolutionChanges = diff(use2ndSolution,1,3);
for iPve = 1:nPve
    for iPts = 1:nPts
        nGaps = sum(chosenSolutionChanges(iPts,iPve,:)~=0)/2;
        if nGaps>0
            startIdx = find(chosenSolutionChanges(iPts,iPve,:)==1);
            stopIdx = find(chosenSolutionChanges(iPts,iPve,:)==-1);
            if length(stopIdx)>length(startIdx)
                % Starts with 2nd solution
                startIdx = [0, startIdx];
            elseif length(stopIdx)<length(startIdx)
                % Finishes with 2nd solution
                stopIdx = [stopIdx, nT];
            end
            
            for iGap = 1:nGaps
                if startIdx(iGap) == 0
                    startGap = conc2Pve(iPts,iPve,startIdx(iGap)+1);
                else
                    startGap = (conc2Pve(iPts,iPve,startIdx(iGap)+1) - conc1Pve(iPts,iPve,startIdx(iGap)));
                end
                if stopIdx(iGap) == nT
                    stopGap = (conc2Pve(iPts,iPve,stopIdx(iGap)) - conc1Pve(iPts,iPve,stopIdx(iGap)));
                else
                    stopGap = (conc2Pve(iPts,iPve,stopIdx(iGap)) - conc1Pve(iPts,iPve,stopIdx(iGap))+1);
                end
                if (startGap+stopGap)/2 > maxConcGap
                    use2ndSolution(iPts,iPve,startIdx(iGap)+1:stopIdx(iGap)) = false;
                end
            end
        end
    end
end
end

function figPve = displayaifpveresults(bloodFraction,aucArterial,aucVof,nPts,allMinIdx,maxPveIdx)

figPve = figure; 
set(figPve,'position',[277         481        1267         420]);
legendName = cell(1,nPts);
pLeg = plot(bloodFraction,(aucArterial/aucVof));
hold on;
for iPts = 1:nPts
    if ~isnan(maxPveIdx(iPts))
        plot(bloodFraction(maxPveIdx(iPts)),(aucArterial(iPts,maxPveIdx(iPts))/aucVof),'xk');
    end
    if allMinIdx(iPts)>0
        plot(bloodFraction(allMinIdx(iPts)),(aucArterial(iPts,allMinIdx(iPts))/aucVof),'.r');
    end
    legendName{iPts} = ['Pt #' num2str(iPts) ' -> X'];
end
% legend(pLeg,legendName);
plot(xlim,[1 1],'--k')
hold off;
title('PVE factor optimisation');
xlabel('PVE factor (-)');
ylabel('\int Ca (PVE) / \int Cp (mM min)');
end
