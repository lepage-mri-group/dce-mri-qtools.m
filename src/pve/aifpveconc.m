function [corrConcMat,corrBloodFractMap,outlierMap,intRatioMap] = aifpveconc(...
    time,vofConc,concDyn,maskAif,pveOptions)
% AIFPVECONC AIF concentration calculation correcting for the partial 
% volume effect (PVE) by conservation of the area under VOF concentration 
% curve (AUC). PVE is corrected on arterial concentration.
%  
%   [corrConcMat,corrBloodFractMap] = AIFPVECONC(...
%       time,vofConc,concDyn,maskAif,pveOptions)
%
% See also: AIFPVESIGNAL
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-08-09

nT = length(time);

%% Input size conversion
[listAifConc, nIdxT] = dyn3dconversion(concDyn,maskAif);

%% Reference "PVE free" time conc integral
aucVof = timeconcintegral(time, vofConc, pveOptions.rangeVofIAUC);% (mM min)

reducedAifRange = pveOptions.rangeVofIAUC;
reducedAifRange(2) = find(time > time(pveOptions.rangeVofIAUC(2))-pveOptions.reducedAifTime,1,'first');

%% Compute AUC ratio and PVC
[nPts,nT] = size(listAifConc);
aucArterial = NaN(nPts,1);
bloodFractionList = NaN(nPts,1);
intRatioList = ones(nPts,1);
corrConcList = NaN(nPts,nT);
outlierList = false(nPts,nT);
for iPts = 1:nPts
    aucArterial(iPts) = timeconcintegral(time, listAifConc(iPts,:), reducedAifRange);% (mM min)
    if aucArterial(iPts) <= 0
        outlierList(iPts,:) = true;
        intRatioList(iPts) = 0;
    else
        bloodFractionList(iPts) = aucArterial(iPts)/aucVof;
        corrConcList(iPts,:) = listAifConc(iPts,:)/bloodFractionList(iPts);
    end
end

%% Output size conversion
[corrConcMat,corrBloodFractMap,outlierMap,intRatioMap] = mat2dyn3d(corrConcList,nIdxT,maskAif,bloodFractionList,outlierList,intRatioList);
end

function concTimeInt = timeconcintegral(injTime, conc, rangeIdxPve)
concTimeInt = trapz(injTime(rangeIdxPve(1):rangeIdxPve(2)),conc(rangeIdxPve(1):rangeIdxPve(2))); % (mM min)
end