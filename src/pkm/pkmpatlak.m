function conc = pkmpatlak(time,aif,patientParam,dataWeight,varargin)
% PKMPATLAK Computes the tissue concentration based on a pharmacokinetic 
% Patlak model
%
%   conc = PKMPATLAK(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKMPATLAK(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKMPATLAK(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKMETOFTS(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMETOFTS, PKM2CLARSSON
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-12-18

%% Assign parameters
patlakStructFieldOrder = {'Ktrans','Vp'};
paramVector = manageflexibleinput(varargin,patlakStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
nT = length(time);
if isempty(dataWeight)
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
Ktrans = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
Vp = paramVector(2)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution

% AIF integration
[trapzAif, dataUsed] = pkmtrapz(time,timeRange,aif);

% Sample AIF vector
concAif = sampleaif(time,aif,time);

conc = zeros(nT,1);
conc(dataUsed) = 1/(1-patientParam.hematocrit)*(Ktrans*trapzAif(dataUsed) + Vp*concAif(dataUsed));
%% Final concentration
if timeIsRow
    conc = (dataWeight*conc)';
else
    conc = (dataWeight*conc);
end
end

function patlakIrf = patlaktissueresponse(~,Ktrans)
patlakIrf = Ktrans;
end
