function conc = pkmetofts(time,aif,patientParam,dataWeight,varargin)
% PKMETOFTS Computes the tissue concentration based on a pharmacokinetic 
% extended Tofts-Kety model
%
%   conc = PKMETOFTS(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKMETOFTS(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKMETOFTS(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKMETOFTS(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKM2CLARSSON
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-05

%% Assign parameters
eToftsStructFieldOrder = {'Ktrans','Ve','Vp'};
paramVector = manageflexibleinput(varargin,eToftsStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
if isempty(dataWeight)
    nT = length(time);
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
Ktrans = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
Ve = paramVector(2)*(patientParam.brainDensity/100); % (-)
Vp = paramVector(3)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution
% Tofts-Kety convolution
FHandleTofts = @(timeVar) toftstissueresponse(timeVar,Ktrans,Ve);
[conc, dataUsed] = pkmirfconv(time,timeRange,aif,FHandleTofts);
% Extended Tofts-Kety
concAif = sampleaif(time,aif,time);
% if isstruct(aif) && isfield(aif,'FHandle')% 2C-MAR requires AIF to be a parametrized biexponential
%     aifP = aif.pValues;
%     if isfield(aif,'conc') % AIF calculation was already done
%         concAif = aif.conc;
%     else % AIF calculation
%         concAif = aif.FHandle(time, aifP);
%     end
% elseif isvector(aif)
%     concAif = aif;
% end
% if isrow(concAif)
%     concAif = concAif';
% else
%     concAif = concAif;
% end
conc(dataUsed) = 1/(1-patientParam.hematocrit)*(conc(dataUsed) + Vp*concAif(dataUsed));
%% Final concentration
if timeIsRow
    conc = (dataWeight*conc)';
else
    conc = (dataWeight*conc);
end
end

function toftsIrf = toftstissueresponse(time,Ktrans,Ve)
kep = Ktrans/Ve;
toftsIrf = Ktrans*exp(-kep*time);
end
