function conc = pkm2clarsson(time,aif,patientParam,varargin)
% PKM2CLARSSON Computes the tissue concentration based on a pharmacokinetic 
% model from the work of (Larsson et al., Magn Res Med. 2009;62(5):1270-1281) 
%
%   conc = PKM2CLARSSON(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKM2CLARSSON(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKM2CLARSSON(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKM2CLARSSON(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKMETOFTS, PKM2CMAR
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-04
%% Assign parameters
larssonStructFieldOrder = {'F','Ki','Vb','Ve'};
paramVector = manageflexibleinput(varargin,larssonStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
% All k quantities units include density (mL/100g). This needs to be removed
F = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
Ki = paramVector(2)*(patientParam.brainDensity/100); % (1/min)
Vb = paramVector(3)*(patientParam.brainDensity/100); % (-)
Ve = paramVector(4)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution
FHandleBiexpIrf = @(timeVar) biexptissueresponse(timeVar,F,Ki,Vb,Ve,patientParam.hematocrit);
[conc, ~] = pkmirfconv(time,timeRange,aif,FHandleBiexpIrf);

%% Final concentration
if timeIsRow
    conc = conc';
end
end

function biexpIrf = biexptissueresponse(time,F,Ki,Vb,Ve,hct)
% Constant definition from (Larsson et al., 2009)
% Vtis was removed, because it only appears for the gamma x beta product of
% (a, b). The product yields (Vtis/Vtis) = 1
alpha = (F+Ki)/Vb;
theta = Ki*(1-hct)/Ve;
constSquareRoot = sqrt(theta^2 + alpha^2 - 2*theta*alpha + ...
    4*Ki*(1-hct)*Ki/(Vb*Ve));
a = (theta + alpha + constSquareRoot)/2;
b = (theta + alpha - constSquareRoot)/2;
biexpIrf = F*((a - theta - Ki/Vb)*exp(-a*time) + ...
    (-b + theta + Ki/Vb)*exp(-b*time)) / (a - b); % (-)
end