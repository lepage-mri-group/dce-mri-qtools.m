function weightedSignal = pkmsignalspgr(pkmFHandle,imgParam,tissueParam,...
    time,aif,patientParam,dataWeight,varargin)
% PKMSIGNALSPGR Create SPGR-MRI signal data from any pharmacokinetic model.
% Can be useful if PKM fitting is on signal level.
%
%   signal = PKMSIGNALSPGR(time,aif,patientParam,paramStruct) Creates tissue
%   signal curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   signal = PKMSIGNALSPGR(time,aif,patientParam,paramVec) Creates tissue
%   signal curve from the AIF and with user defined model 
%   parameters vector.
%
%   signal = PKMSIGNALSPGR(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue signal curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   signal = PKMSIGNALSPGR(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue signal curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMETOFTS, PKMPATLAK, PKM2CLARSSON
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-10-31

%% Assign parameters
signalStructFieldOrder = {'S0','R10','R20s','fa'}; % Signal parameters are passed as fixed model parameters.
if isequal(pkmFHandle,@pkmvascular)
    pkmStructFieldOrder = {'Vp'};
elseif isequal(pkmFHandle,@pkmetofts)
    pkmStructFieldOrder = {'Ktrans','Ve','Vp'};
elseif isequal(pkmFHandle,@pkm2clarsson)
    pkmStructFieldOrder = {'F','Ki','Vb','Ve'};
elseif isequal(pkmFHandle,@pkm2clarsson)
    pkmStructFieldOrder = {'k(1)','k(2)','k(3)','k(4)','Vb'};
elseif isequal(pkmFHandle,@pkmctu)
    pkmStructFieldOrder = {'Fp','PS','Vp'};
elseif isequal(pkmFHandle,@pkm2cxm)
    pkmStructFieldOrder = {'Fp','PS','Ve','Vp'};
else
    % Unknown PKM model. Assuming ZERO PKM.
    pkmStructFieldOrder = {};
end
globalStructureFieldOrder = cat(2,signalStructFieldOrder,pkmStructFieldOrder);
pkmParamIdx = (length(signalStructFieldOrder)+1):length(globalStructureFieldOrder);
paramVector = manageflexibleinput(varargin,globalStructureFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

nT = length(time);
noConcWeight = eye(nT,nT);
if isempty(dataWeight)
    dataWeight = noConcWeight;
end

%% Compute concentration
conc = pkmFHandle(time,aif,patientParam,noConcWeight,paramVector(pkmParamIdx));

%% Compute signal
% tissueParam.tracer.r1
% tissueParam.tracer.r2s
% imgParam.te
% imgParam.tr
tissueParam.S0 = paramVector(1);
tissueParam.R10 = paramVector(2);
tissueParam.R20s = paramVector(3);
imgParam.fa = paramVector(4);

[signal, ~] = signalmrispgr(conc,imgParam.injStartFrame,tissueParam,imgParam);
%% Final concentration
if timeIsRow
    weightedSignal = (dataWeight*signal)';
else
    weightedSignal = (dataWeight*signal);
end

end