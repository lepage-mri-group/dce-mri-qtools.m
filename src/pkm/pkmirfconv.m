function [conc, dataUsed] = pkmirfconv(time,timeRange,aif,FHandleIrf)
% PKMIRFCONV Numerical convolution of the AIF and the tissue impulse 
% response funcion for pharmacokinetic modeling. Adapted to handle non 
% constant sampling.
%
%   concAif = PKMIRFCONV(time,timeRange,aif,FHandleBiexp) Creates tissue
%   concentration curve from the AIF and with user defined tissue impulse 
%   response function. 
%
% See also: PKM2CLARSSON, PKMETOFTS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-29

% Concentration calculation limited by timeRange(2).
% timeRange(1) unused for numerical convolution based PKM

%% Constant time
nT = length(time);
dataUsed = (time <=timeRange(2));
t_btf = time(dataUsed);
allDTime = diff(t_btf);
dt = min(allDTime);
dicomCstTimeErrorThreshold = 0.001; % min
if any(allDTime-dt > dicomCstTimeErrorThreshold) % Time is not constant
    cstTime = (t_btf(1):dt:(t_btf(end)+dt))';
    interpConc = true;
else
    cstTime = t_btf;
    interpConc = false;
end
nTCst = length(cstTime);
%% Constant AIF
cstTimeConcAif = sampleaif(t_btf,aif,cstTime);
% if isstruct(aif) % Parameterized AIF
%     cstTimeConcAif = aif.FHandle(cstTime,aif.pValues);
% elseif isvector(aif) && length(aif)==nT % Sampled AIF
%     if isrow(aif)
%         aif = aif';
%     end
%     if interpConc
%         cstTimeConcAif = interp1(t_btf,aif,cstTime,'linear');
%     else
%         cstTimeConcAif = aif(dataUsed);
%     end
% end
%% Constant tissue IRF
irfTimeFromZero = cstTime - cstTime(1);
tissueIrf = FHandleIrf(irfTimeFromZero);

%% Convolution
cstTimeConc = conv(cstTimeConcAif,tissueIrf)*dt;

%% Resample on original time frames
if interpConc
    conc_btf = interp1(cstTime,cstTimeConc(1:nTCst),t_btf,'linear','extrap');
else
    conc_btf = cstTimeConc(1:nTCst);
end
conc = zeros(nT,1);
conc(dataUsed) = conc_btf;
end