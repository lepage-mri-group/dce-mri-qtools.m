function [aifTrapz, dataUsed] = pkmtrapz(time,timeRange,aif)
% PKMTRAPZ Trapezoidal integer of AIF adapted to handle non constant 
% sampling.
%
%   concAif = PKMTRAPZ(time,timeRange,aif,FHandleBiexp) Creates tissue
%   concentration curve from the AIF and with user defined tissue impulse 
%   response function. 
%
% See also: PKMPATLAK
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-12-18

% Concentration calculation limited by timeRange(2).
% timeRange(1) unused for numerical convolution based PKM

%% Constant time
nT = length(time);
dataUsed = (time >=timeRange(1)) & (time <=timeRange(2));
t_btf = time(dataUsed);
allDTime = diff(t_btf);
dt = min(allDTime);
dicomCstTimeErrorThreshold = 0.001; % min
if any(allDTime-dt > dicomCstTimeErrorThreshold) % Time is not constant
    cstTime = (t_btf(1):dt:(t_btf(end)+dt))';
    interpConc = true;
else
    cstTime = t_btf;
    interpConc = false;
end
nTCst = length(cstTime);
%% Constant AIF
cstTimeConcAif = sampleaif(t_btf,aif,cstTime);
% if isstruct(aif) % Parameterized AIF
%     cstTimeConcAif = aif.FHandle(cstTime,aif.pValues);
% elseif isvector(aif) && length(aif)==nT % Sampled AIF
%     if isrow(aif)
%         aif = aif';
%     end
%     if interpConc
%         cstTimeConcAif = interp1(t_btf,aif,cstTime,'linear');
%     else
%         cstTimeConcAif = aif(dataUsed);
%     end
% end

%% Convolution
cstTimeAifTrapz = cumtrapz(cstTime,cstTimeConcAif);

%% Resample on original time frames
if interpConc
    conc_aifTrapz_btf = interp1(cstTime,cstTimeAifTrapz(1:nTCst),t_btf,'linear','extrap');
else
    conc_aifTrapz_btf = cstTimeAifTrapz(1:nTCst);
end
aifTrapz = zeros(nT,1);
aifTrapz(dataUsed) = conc_aifTrapz_btf;
end