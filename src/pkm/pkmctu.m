function conc = pkmctu(time,aif,patientParam,dataWeight,varargin)
% PKMCTU Computes the tissue concentration based on a pharmacokinetic 
% compartmental tissue-uptake model (CTU)
%
%   conc = PKMCTU(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKMCTU(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKMCTU(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKMCTU(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKM2CLARSSON, PKMETOFTS, PKM2CXM
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-09-01

%% Assign parameters
twocxmStructFieldOrder = {'Fp','PS','Vp'};
paramVector = manageflexibleinput(varargin,twocxmStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
if isempty(dataWeight)
    nT = length(time);
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
Fp = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
PS = paramVector(2)*(patientParam.brainDensity/100); % (1/min)
Vp = paramVector(3)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution
% CTU convolution
FHandlCtu = @(timeVar) ctutissueresponse(timeVar,Fp,Vp,PS);
[conc, dataUsed] = pkmirfconv(time,timeRange,aif,FHandlCtu);

conc(dataUsed) = Fp*1/(1-patientParam.hematocrit)*(conc(dataUsed));
%% Final concentration
if timeIsRow
    conc = (dataWeight*conc)';
else
    conc = (dataWeight*conc);
end
end

function ctuIrf = ctutissueresponse(time,Fp,Vp,PS)
E = PS/(Fp+PS);
Tp = Vp/(Fp+PS);

ctuIrf = ((1-E)*exp(-time/Tp)+E);
end
