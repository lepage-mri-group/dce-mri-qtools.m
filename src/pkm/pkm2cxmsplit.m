function conc = pkm2cxmsplit(time,aif,patientParam,dataWeight,varargin)
% PKM2CXMSPLIT Computes the plasma and EES concentration based on a 
% pharmacokinetic two-compartment exchange model (2CXM)
%
%   conc = PKM2CXMSPLIT(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKM2CXMSPLIT(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKM2CXMSPLIT(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKM2CXMSPLIT(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKM2CLARSSON, PKMETOFTS, PKM2CXM
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-09-01

%% Assign parameters
twocxmStructFieldOrder = {'Fp','PS','Ve','Vp'};
paramVector = manageflexibleinput(varargin,twocxmStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
if isempty(dataWeight)
    nT = length(time);
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
Fp = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
PS = paramVector(2)*(patientParam.brainDensity/100); % (1/min)
Ve = paramVector(3)*(patientParam.brainDensity/100); % (-)
Vp = paramVector(4)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution
% 2CXM convolution
FHandlePlasma = @(timeVar) twocmxplasmaresponse(timeVar,Fp,Vp,PS,Ve);
[concCapillary, dataUsed] = pkmirfconv(time,timeRange,aif,FHandlePlasma);
FHandleEes = @(timeVar) twocmxeesresponse(timeVar,Fp,Vp,PS,Ve);
[concEes, ~] = pkmirfconv(time,timeRange,aif,FHandleEes);

% Arterial hematocrit normalization
% From AIF in blood concentration to AIF in plasma concentration
conc = NaN(nT,2);
conc(dataUsed,1) = 1/(1-patientParam.hematocrit)*(concCapillary(dataUsed));
conc(dataUsed,2) = 1/(1-patientParam.hematocrit)*(concEes(dataUsed));
% Not normalized by tissue plasma hematocrit (Vp - plasma volume)
%% Final concentration
if timeIsRow
    concColumn = conc;
    conc = NaN(2,nT);
    conc(1,:) = (dataWeight*concColumn(:,1))';
    conc(2,:) = (dataWeight*concColumn(:,2))';
else
    conc(:,1) = (dataWeight*conc(:,1));
    conc(:,2) = (dataWeight*conc(:,2));
end
end

function twocmxPlasmaIrf = twocmxplasmaresponse(time,Fp,Vp,PS,Ve)
Tc = Vp/Fp;
Te = Ve/PS;
T = (Vp+Ve)/Fp;
sigmaRoot = sqrt((T+Te)^2-4*Tc*Te);
sigmaP = ((T+Te)+sigmaRoot)/(2*Tc*Te);
sigmaM = ((T+Te)-sigmaRoot)/(2*Tc*Te);

twocmxPlasmaIrf = sigmaP*sigmaM*((1-Te*sigmaM)*exp(-time*sigmaM)+(Te*sigmaP-1)*exp(-time*sigmaP))/(sigmaP - sigmaM);
end

function twocmxEesIrf = twocmxeesresponse(time,Fp,Vp,PS,Ve)
Tc = Vp/Fp;
Te = Ve/PS;
T = (Vp+Ve)/Fp;
sigmaRoot = sqrt((T+Te)^2-4*Tc*Te);
sigmaP = ((T+Te)+sigmaRoot)/(2*Tc*Te);
sigmaM = ((T+Te)-sigmaRoot)/(2*Tc*Te);

twocmxEesIrf = sigmaP*sigmaM*(exp(-time*sigmaM) - exp(-time*sigmaP))/(sigmaP - sigmaM);
end
