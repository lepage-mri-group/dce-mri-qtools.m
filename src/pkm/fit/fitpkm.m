function [xMat, errorInfo,outputConc] = fitpkm(time,concAif,conc,fitWeight,mask,pkmFHandle,pkmParam,x0,lb,ub,rb,fitOptions,order)
% FITPKM Fit the PKM to the model specified by pkmFHandle.
%
%   [xMat, errorInfo] = FITPKM(time,concAif,conc,fitWeight,mask,pkmFHandle,pkmParam,x0,lb,ub,rb,fitOptions,order)
%
% See also: PKMETOFTS, PKMPATLAK, PKM2CLARSSON
% Author: Benoit Bourassa-Moreau
% Date: 2019-08-19

%% Input check
if nargout == 3
    saveConc = true;
else
    saveConc = false;
end

nT = length(time);
if isfield(pkmParam,'timeRange') && ~isempty(pkmParam.timeRange)
    timeRange = pkmParam.timeRange;
    dataUsed = (time>=timeRange(1) & time <=timeRange(2));
else
    timeRange = [time(1) time(end)];
    dataUsed = true(1,nT);
end
nData = sum(dataUsed);
pkmParam.dataUsed = dataUsed;
if isfield(pkmParam,'display') && ~isempty(pkmParam.display)
    display = pkmParam.display;
else
    display = 0;
end
if isempty(fitWeight)
    fitWeight = eye(sum(dataUsed));
end
% Size conversion
[nX,nY,nZ] = size(mask);
changingAif = false;
if ismatrix(concAif)
    if ndims(concAif) == ndims(conc) && all(size(concAif)==size(conc))
        changingAif = true;
    end
end
if changingAif
    [listConc, nIdxT, listAif] = dyn3dconversion(conc,mask,[],concAif);
else
    [listConc, nIdxT] = dyn3dconversion(conc,mask,[]);
end

% Values of fixed and starting parameters can change for each voxels
if any(order)
    fitPkmFunc = true;
else
    fitPkmFunc = false; % All fixed parameters - used to compute error (R2,RSS, etc.)
end
nK = length(x0);
nKFit = sum(order);
lb=lb(order);
ub=ub(order);
relativeBoundsReduced = rb(order);
allP2FitP = find(order); 
withMap = false(1,nK);
currentX0 = NaN(1,nK);
if iscell(x0)
    % Starting and fixed parameters might be a combination of maps and constants
    listX0 = cell(1,nK);
    for iSp = 1:nK
        if (size(x0{iSp},1)==nX) && (size(x0{iSp},2)==nY) && (size(x0{iSp},3)==nZ)
            % Current starting or fixed parameter is a map
            withMap(iSp) = true; % F must be defined for each points
            listX0{iSp} = x0{iSp}(mask);
        elseif numel(x0{iSp}) == 1
            listX0{iSp} = x0{iSp};
            currentX0(iSp) = x0{iSp};
        else
            warning('Size of starting parameter matrix does not correspond to the size of concentration map. First value is used');
            listX0{iSp} = x0{iSp}(1);
            currentX0(iSp) = x0{iSp}(1);
        end
    end
else
    currentX0 = x0;
end

if (all(~withMap)) % x0 is either a cell or a vector of constant values 
    changingSp = false;
    % Constant starting and fixed parameters
    fixedX0 = currentX0(~order);
    varX0 = currentX0(order); % Starting parameters removing fixed parameters
    % Only one function definition is needed
    if ~changingAif
        lsqFun = @(varX,calledTime)pkmFHandle(calledTime,concAif(dataUsed),pkmParam,fitWeight,varX,fixedX0,order);
    end
else
    changingSp = true;
end

%% Fitting
[nPx,nT] = size(listConc);
x = zeros(nPx,nK);
errorInfo.resnorm = NaN(nPx,1);

hWait=waitbar(0,'Computing voxel PKM');
cUb = ub;
cLb = lb;
for iSp = 1:nKFit
    if relativeBoundsReduced(iSp)
        if order(iSp)
            cUb(iSp) = ub(iSp)*(currentX0(allP2FitP(iSp)));
            cLb(iSp) = lb(iSp)*(currentX0(allP2FitP(iSp)));
        end
    end
end

for iPx=1:nPx
    waitbar(iPx/nPx, hWait);
    if changingSp
        currentX0 = NaN(1,nK);
        for iSp = 1:nK
            if withMap(iSp)
                currentX0(iSp) = listX0{iSp}(iPx);
            else
                currentX0(iSp) = listX0{iSp}(1);
            end
        end
        for iSp = 1:nKFit
            if relativeBoundsReduced(iSp)
                if order(iSp)
                    cUb(iSp) = ub(iSp)*(currentX0(allP2FitP(iSp)));
                    cLb(iSp) = lb(iSp)*(currentX0(allP2FitP(iSp)));
                end
            end
        end
        fixedX0 = currentX0(~order);
        varX0 = currentX0(order); % Starting parameters removing fixed parameters
    end
    if changingAif
        lsqFun = @(varX,calledTime)pkmFHandle(calledTime,listAif(iPx,dataUsed),pkmParam,fitWeight,varX,fixedX0,order);
    elseif changingSp
        lsqFun = @(varX,calledTime)pkmFHandle(calledTime,concAif(dataUsed),pkmParam,fitWeight,varX,fixedX0,order);
    end
    
    if fitPkmFunc
        try
            [xVar,errorInfo.resnorm(iPx)] = lsqcurvefit(lsqFun,varX0,time(dataUsed),(fitWeight*listConc(iPx,dataUsed)')',cLb,cUb,fitOptions);
        catch ME
            warning('LSQCURVEFIT of PKM failed for initial parameters.')
            xVar = currentX0(order);
            resnorm(iPx) = NaN;
        end
        x(iPx,order) = xVar;
    end
    x(iPx,~order) = fixedX0;
end
close(hWait);
%% Output size conversion
if nargout == 1
    [xMat] = mat2dyn3d(x, nIdxT,mask);
else
    % Assemble fitted values
    errorInfo.n = nData;
    errorInfo.k = nKFit;
    
    % Error values
    RSS = zeros(nPx,1);
    R2 = zeros(nPx,1);
    if (display == 1) || saveConc || (nPx == 1)
        errorInfo.timeFit = time;
        outputConc = NaN(nPx,nT);
    end
    allVar = true(1,nK);
    for iPx=1:nPx
        if changingAif
            fittedConc = pkmFHandle(time,listAif(iPx,:),pkmParam,[],x(iPx,:),[],allVar);
        else
            fittedConc = pkmFHandle(time,concAif,pkmParam,[],x(iPx,:),[],allVar);
        end
        RSS(iPx) = sum((fittedConc(dataUsed) - listConc(iPx,dataUsed)).^2); % Residual sum of square
        R2(iPx) = 1 - RSS(iPx,:)/((nData-1)*var(listConc(iPx,dataUsed))); % Coefficient of determination
        if (display == 1) || saveConc
            outputConc(iPx,:) = fittedConc;
        end
    end
    
    [xMat,errorInfo.RSS,errorInfo.R2] = mat2dyn3d(x, nIdxT,mask,RSS,R2);
    if nPx == 1 % Single value debug
        errorInfo.concFit = fittedConc;
    end
    if display == 1
        if nPx == 1 % Single value debug
            errorInfo.concFit = fittedConc;
            figure;
            plot(time,conc(:),'.k');
            hold on;
            plot(time(dataUsed),conc(dataUsed),'.r');
            plot(errorInfo.timeFit,errorInfo.concFit,'--k');
            hold off; xlabel('Time (min)'); ylabel('C(t) (mM)');
            %         xlim(XCaRange);
            ylim(mean(conc(:)) + (max(conc(:))-min(conc(:)))*[-1 1]);
            legend('All data','Fit data','PKM Fit')
            errorInfo.rangeFit = [find(dataUsed,1,'first'),find(dataUsed,1,'last')];
        else
            [yData] = mat2dyn3d(outputConc, nIdxT,mask);
            preNT = 1:(nIdxT-1);
            nPreNt = length(preNT);
            if nPreNt<3
                shiftDimVec = [preNT, nIdxT+(1:(3-nPreNt)), nIdxT];
            else
                shiftDimVec = [preNT(1:3), nIdxT];
            end
            fit(1).ydata = permute(yData,shiftDimVec);
            conc4D = permute(conc,shiftDimVec);
            
            fit(1).label = 'linear';
            fit(1).xdata = time;
            %     chosenSlice = ceil(1.5*size(conc,3)/2);
            fprintf('\n--- DEBUG FITTING ---\nLeft-Click on one point to check the fit.\n Middle-click to move through slices:\n   - left side of img, one slice down\n   - right side of img, one slide up\n CTRL-click or right-click when done\n');
            
            fit(1).param = xMat(:,:,:,find(order,1,'first'));
            fit(1).resnorm = permute(errorInfo.R2,shiftDimVec);
            debugLabels = {'First Parameter (?)','C_t Or Signal (?)','Time (min)'};
            plotdebugfitdata(xMat(:,:,:,1),...
                conc4D,time, debugLabels,...
                fit);
        end
    end
end

end