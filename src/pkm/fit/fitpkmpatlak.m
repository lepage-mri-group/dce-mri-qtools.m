function [xMat, errorInfo] = fitpkmpatlak(time,concAif,conc,~,mask,~,pkmParam,~,~,~,~,~,~)
% FITPKMPATLAK Fit the Patlak PKM with the linearization of the equation.
%
%   [xMat, errorInfo] = FITPKMPATLAK(time,concAif,conc,fitWeight,mask,pkmFHandle,pkmParam,x0,lb,ub,rb,fitOptions,order)
%
% See also: PKMPATLAK, FITPKM
% Author: Benoit Bourassa-Moreau
% Date: 2019-12-18

%% Input check
nT = length(time);
if isfield(pkmParam,'timeRange') && ~isempty(pkmParam.timeRange)
    timeRange = pkmParam.timeRange;
    dataUsed = (time>=timeRange(1) & time <=timeRange(2));
    firstRangeIdx = find(time<timeRange(1),1,'last');
else
    timeRange = [time(1) time(end)];
    dataUsed = true(1,nT);
    firstRangeIdx = [];
end
nData = sum(dataUsed);
pkmParam.dataUsed = dataUsed;
if isfield(pkmParam,'display') && ~isempty(pkmParam.display)
    display = pkmParam.display;
else
    display = 0;
end
% Size conversion
[nX,nY,nZ] = size(mask);
changingAif = false;
if ismatrix(concAif)
    if ndims(concAif) == ndims(conc) && all(size(concAif)==size(conc))
        changingAif = true;
    end
end
if changingAif
    [listConc, nIdxT, listAif] = dyn3dconversion(conc,mask,[],concAif);
else
    [listConc, nIdxT] = dyn3dconversion(conc,mask,[]);
end

fitPkmFunc = true;

%% Fitting
[nPx,nT] = size(listConc);
nK = 2;
nKFit = nK;
x = zeros(nPx,nK);
intCa = zeros(nPx,nT);
errorInfo.resnorm = NaN(nPx,1);

hWait=waitbar(0,'Computing voxel PKM');

for iPx=1:nPx
    waitbar(iPx/nPx, hWait);
    if changingAif
        currentAif = listAif(iPx,:);
        if isempty(firstRangeIdx)
            intCa(iPx,:) = cumtrapz(time,currentAif);
        else
            intCa(iPx,:) = cumtrapz(time,currentAif) - trapz(time(1:firstRangeIdx),currentAif(1:firstRangeIdx));
        end
        intCaRatio = intCa(iPx,:)./currentAif;
        conc2CaRatio = listConc(iPx,:)./currentAif;
    else
        if iPx == 1
            currentAif = concAif;
            if isempty(firstRangeIdx)
                intCa(iPx,:) = cumtrapz(time,currentAif);
            else
                intCa(iPx,:) = cumtrapz(time,currentAif) - trapz(time(1:firstRangeIdx),currentAif(1:firstRangeIdx));
            end
            intCaRatio = intCa(iPx,:)./currentAif;
        end
        conc2CaRatio = listConc(iPx,:)./currentAif;
    end
    
    if fitPkmFunc
        try
            [xVar,S] = polyfit(intCaRatio(pkmParam.dataUsed),conc2CaRatio(pkmParam.dataUsed),1);
            errorInfo.resnorm(iPx) = S.normr;
        catch ME
            warning('Linear fitting failed.')
            xVar = [0 0];
            errorInfo.resnorm(iPx) = NaN;
        end
        x(iPx,:) = xVar;
    end
end
close(hWait);
%% Output size conversion
if nargout == 1
    [xMat] = mat2dyn3d(x, nIdxT,mask);
else
    % Assemble fitted values
    errorInfo.n = nData;
    errorInfo.k = nKFit;
    
    % Error values
    RSS = zeros(nPx,1);
    R2 = zeros(nPx,1);
    fittedConc = zeros(1,nT);
    if display == 1
        fitConcDisplay = NaN(nPx,nT);
    end
    for iPx=1:nPx
        if changingAif
            currentAif = listAif(iPx,:);
        else
            if iPx == 1
                currentAif = concAif;
            end
        end
        fittedConc = x(iPx,1)*intCa(iPx,:) + x(iPx,2)*currentAif;
        RSS(iPx) = sum((fittedConc(dataUsed) - listConc(iPx,dataUsed)).^2); % Residual sum of square
        R2(iPx) = 1 - RSS(iPx,:)/((nData-1)*var(listConc(iPx,dataUsed))); % Coefficient of determination
        if display == 1
            fitConcDisplay(iPx,:) = fittedConc;
        end
    end
    
    [xMat,errorInfo.RSS,errorInfo.R2] = mat2dyn3d(x, nIdxT,mask,RSS,R2);
    if nPx == 1 % Single value debug
        errorInfo.timeFit = time;
        errorInfo.concFit = fittedConc;
    end
    if display == 1
        if nPx == 1 % Single value debug
            errorInfo.timeFit = time;
            errorInfo.concFit = fittedConc;
            
            if isfield(pkmParam,'displayHandles') && ~isempty(pkmParam.displayHandles)
                axTimeConc = pkmParam.displayHandles{1};
                axLinSpace = pkmParam.displayHandles{2};
                displaySymbol = pkmParam.displaySymbol;
            else
                figPkmFit = figure;
                figPkmFit.Position = [338   566   689   300];
                axTimeConc = subplot(1,2,1);
                axLinSpace = subplot(1,2,2);
                displaySymbol = 'o';
            end
            axes(axTimeConc);
            hold on;
            plot(time,conc(:),[displaySymbol 'k'],'MarkerSize',4);
            plot(time(dataUsed),conc(dataUsed),[displaySymbol 'r'],'MarkerSize',4);
            plot(errorInfo.timeFit,errorInfo.concFit,'--k');
            plot(errorInfo.timeFit(dataUsed),errorInfo.concFit(dataUsed),'--r');
            hold off; xlabel('Time (min)'); ylabel('C(t) (mM)');
            %         xlim(XCaRange);
%             ylim(mean(conc(:)) + (max(conc(:))-min(conc(:)))*[-1 1]);
            legend('All data','Included data','All fit','Included fit')
            title('Time curve');
            errorInfo.rangeFit = [find(dataUsed,1,'first'),find(dataUsed,1,'last')];
            
            axes(axLinSpace);
            hold on;
            plot(intCaRatio,conc2CaRatio,[displaySymbol 'k'],'MarkerSize',4);
            plot(intCaRatio(dataUsed),conc2CaRatio(dataUsed),[displaySymbol 'r'],'MarkerSize',4);
            plot(intCaRatio,polyval(xVar,intCaRatio),'--k');
            xlim([min(intCaRatio(dataUsed)),max(intCaRatio(dataUsed))]);
%             ylim([min(conc2CaRatio(dataUsed)),max(conc2CaRatio(dataUsed))]);
            xlabel('\int ca(\tau) d\tau/ ca(t) (min)'); ylabel('C(t)/ca(t) (-)');
            title('Graphical representation');
        else
            [yData] = mat2dyn3d(fitConcDisplay, nIdxT,mask);
            preNT = 1:(nIdxT-1);
            nPreNt = length(preNT);
            if nPreNt<3
                shiftDimVec = [preNT, nIdxT+(1:(3-nPreNt)), nIdxT];
            else
                shiftDimVec = [preNT(1:3), nIdxT];
            end
            fit(1).ydata = permute(yData,shiftDimVec);
            conc4D = permute(conc,shiftDimVec);
            
            fit(1).label = 'linear';
            fit(1).xdata = time;
            %     chosenSlice = ceil(1.5*size(conc,3)/2);
            fprintf('\n--- DEBUG FITTING ---\nLeft-Click on one point to check the fit.\n Middle-click to move through slices:\n   - left side of img, one slice down\n   - right side of img, one slide up\n CTRL-click or right-click when done\n');
            
            fit(1).param = xMat(:,:,:,1);
            fit(1).resnorm = permute(errorInfo.R2,shiftDimVec);
            debugLabels = {'First Parameter (?)','C_t Or Signal (?)','Time (min)'};
            plotdebugfitdata(xMat(:,:,:,1),...
                conc4D,time, debugLabels,...
                fit);
        end
    end
end

end