function conc = pkm2cxm(time,aif,patientParam,dataWeight,varargin)
% PKM2CXM Computes the tissue concentration based on a pharmacokinetic 
% two-compartment exchange model (2CXM)
%
%   conc = PKM2CXM(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKM2CXM(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKM2CXM(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKM2CXM(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKM2CLARSSON, PKMETOFTS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-09-01

%% Assign parameters
twocxmStructFieldOrder = {'Fp','PS','Ve','Vp'};
paramVector = manageflexibleinput(varargin,twocxmStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
if isempty(dataWeight)
    nT = length(time);
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
Fp = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
PS = paramVector(2)*(patientParam.brainDensity/100); % (1/min)
Ve = paramVector(3)*(patientParam.brainDensity/100); % (-)
Vp = paramVector(4)*(patientParam.brainDensity/100); % (-)

timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end

%% Convolution
% 2CXM convolution
FHandleTofts = @(timeVar) twocmxtissueresponse(timeVar,Fp,Vp,PS,Ve);
[conc, dataUsed] = pkmirfconv(time,timeRange,aif,FHandleTofts);

% Arterial hematocrit normalization
% From AIF in blood concentration to AIF in plasma concentration
conc(dataUsed) = Fp*1/(1-patientParam.hematocrit)*(conc(dataUsed));
% Not normalized by tissue plasma hematocrit (Vp - plasma volume)
%% Final concentration
if timeIsRow
    conc = (dataWeight*conc)';
else
    conc = (dataWeight*conc);
end
end

function twocmxIrf = twocmxtissueresponse(time,Fp,Vp,PS,Ve)
Tc = Vp/Fp;
Te = Ve/PS;
T = (Vp+Ve)/Fp;
sigmaRoot = sqrt((T+Te)^2-4*Tc*Te);
sigmaP = ((T+Te)+sigmaRoot)/(2*Tc*Te);
sigmaM = ((T+Te)-sigmaRoot)/(2*Tc*Te);

twocmxIrf = ((Tc*sigmaP-1)*exp(-time*sigmaM)+(1-Tc*sigmaM)*exp(-time*sigmaP))/Tc/(sigmaP - sigmaM);
% twocmxIrf = ((T*sigmaP-1)*sigmaM*exp(-time*sigmaM)+(1-T*sigmaM)*sigmaP*exp(-time*sigmaP))/(sigmaP - sigmaM);
end
