function conc = pkm2cmar(time,aif,patientParam,dataWeight,varargin)
% PKM2CMAR Computes the tissue concentration based on a pharmacokinetic 
% model from the work of (Richard, et. al., J Nucl Med. 2017;58(8):1278-1284.)
%
%   conc = PKM2CMAR(time,aif,patientParam,paramStruct) Creates tissue
%   concentration curve from the AIF and with user defined explicit model 
%   parameters structure.
%
%   conc = PKM2CMAR(time,aif,patientParam,paramVec) Creates tissue
%   concentration curve from the AIF and with user defined model 
%   parameters vector.
%
%   conc = PKM2CMAR(time,aif,patientParam,varParamVec,fixedParamVec,isVar)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters.
%
%   conc = PKM2CMAR(time,aif,patientParam,varParamVec,fixedParamVec,isVar, timeRange)
%   Creates tissue concentration curve from the AIF and with user defined
%   model parameters vector. Useful for curve fitting with a mixture of
%   variable and fixed parameters. Time range can be defined to reduce
%   calculation time.
%
% See also: PKMPATLAK, PKMETOFTS, PKM2CLARSSON
% Author: Benoit Bourassa-Moreau
% Based on the work of Marie Anne Richard (mar_calculateconcentration), 
% Vincent Turgeon (obtainKfromtac.m)
% Creation date: 2019-06-05

%% Assign parameters
biexpStructFieldOrder = {'k(1)','k(2)','k(3)','k(4)','Vb'};
% Units : [1/min], [1/min], [1/min], [1/min], [-]
paramVector = manageflexibleinput(varargin,biexpStructFieldOrder);
if isempty(paramVector) 
    error('Model parameters are unknown.');
end
if (numel(varargin)>=4)
    timeRange = varargin{4};
else
    timeRange = time([1 end]);
end
if isempty(dataWeight)
    nT = length(time);
    dataWeight = eye(nT,nT);
end
% All k quantities units include density (mL/100g). This needs to be removed
normParamVector(1) = paramVector(1)*(patientParam.brainDensity/100); % (1/min)
normParamVector(2) = paramVector(2)*(patientParam.brainDensity/100); % (-)
normParamVector(3) = paramVector(3)*(patientParam.brainDensity/100); % (1/min)
normParamVector(4) = paramVector(4)*(patientParam.brainDensity/100); % (-)
normParamVector(5) = paramVector(5)*(patientParam.brainDensity/100); % (-)


timeIsRow = false;
if isrow(time)
    time = time';
    timeIsRow = true;
end


% AIF
if isfield(aif,'FHandle') && isequal(aif.FHandle,@aifbiexp) % 2C-MAR requires AIF to be a parametrized biexponential
    aifP = aif.pValues;
    if isfield(aif,'AIFFit') % AIF calculation was already done
        if isrow(aif.AIFFit)
            concAif = aif.AIFFit';
        else
            concAif = aif.AIFFit;
        end
    else % AIF calculation
        concAif = aif.FHandle(time, aifP);
    end
end

%% Constant definition. 
% Definition of a1 and a2.
a1 = 1/2*((normParamVector(2) + normParamVector(3) + normParamVector(4)) - sqrt((normParamVector(2) + normParamVector(3)+ normParamVector(4))^2 - 4*normParamVector(2)*normParamVector(4)));
a2 = 1/2*((normParamVector(2) + normParamVector(3) + normParamVector(4)) + sqrt((normParamVector(2) + normParamVector(3)+ normParamVector(4))^2 - 4*normParamVector(2)*normParamVector(4)));

% The numerical convolution (conv function) is problematic if the time
% vector does not begin at 0. There is also a problem with heaviside function if t<0, so the
% time vector is split and parts are calculated separately.

% Split time vectors
nT = length(time);
dataUsed = (time>=timeRange(1) & time <=timeRange(2));
t_bti = time(time<aifP(1) & dataUsed);
t_btf = time(time>=aifP(1) & time <aifP(2) & dataUsed);
t_atf = time(time>=aifP(2) & dataUsed);

%% Convolution
if normParamVector(4)~=0; % if k4 different from zero
    % Constants
    gamma=normParamVector(1)/(a2-a1);
    beta=normParamVector(3)+normParamVector(4)-a1;
    psi=a2-normParamVector(3)-normParamVector(4);
    wete=aifP(5)*aifP(7);
    wdtd=aifP(4)*aifP(6);
    denomte1=(a1-(1/aifP(7)));
    denomte2=(a2-(1/aifP(7)));
    denomtd1=(a1-(1/aifP(6)));
    denomtd2=(a2-(1/aifP(6)));
    
    % Do convolution by parts
    Ct_btf = aifP(3)*gamma*(beta*exp(-a1*t_btf).*(wete*((exp(a1*t_btf)-exp(a1*aifP(1)))/a1-exp(aifP(1)/aifP(7))*(exp(t_btf*denomte1)-exp(aifP(1)*denomte1))/denomte1)...
        + wdtd*((exp(a1*t_btf)-exp(a1*aifP(1)))/a1-exp(aifP(1)/aifP(6))*(exp(t_btf*denomtd1)-exp(aifP(1)*denomtd1))/denomtd1))+psi*exp(-a2*t_btf).*...
        (wete*((exp(a2*t_btf)-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(7))*(exp(t_btf*denomte2)-exp(aifP(1)*denomte2))/denomte2)...
        + wdtd*((exp(a2*t_btf)-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(6))*(exp(t_btf*denomtd2)-exp(aifP(1)*denomtd2))/denomtd2)));
    
    Ct_atf = aifP(3)*gamma*(beta*exp(-a1*t_atf).*(wete*((exp(a1*aifP(2))-exp(a1*aifP(1)))/a1-exp(aifP(1)/aifP(7))/denomte1*((exp(aifP(2)*denomte1)-exp(aifP(1)*denomte1))-(exp((aifP(2)-aifP(1))/aifP(7))-1)...
        *(exp(t_atf*denomte1)-exp(aifP(2)*denomte1)))) + wdtd*((exp(a1*aifP(2))-exp(a1*aifP(1)))/a1-exp(aifP(1)/aifP(6))/denomtd1*((exp(aifP(2)*denomtd1)-exp(aifP(1)*denomtd1))-(exp((aifP(2)-aifP(1))/aifP(6))-1)...
        *(exp(t_atf*denomtd1)-exp(aifP(2)*denomtd1))))) + psi*exp(-a2*t_atf).*(wete*((exp(a2*aifP(2))-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(7))/denomte2*((exp(aifP(2)*denomte2)-exp(aifP(1)*denomte2))-(exp((aifP(2)-aifP(1))/aifP(7))-1)...
        *(exp(t_atf*denomte2)-exp(aifP(2)*denomte2)))) + wdtd*((exp(a2*aifP(2))-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(6))/denomtd2*((exp(aifP(2)*denomtd2)-exp(aifP(1)*denomtd2))-(exp((aifP(2)-aifP(1))/aifP(6))-1)...
        *(exp(t_atf*denomtd2)-exp(aifP(2)*denomtd2))))));
else % if k4 = 0
    % Constants
    A1=normParamVector(1)*normParamVector(3)/a2;
    wete=aifP(5)*aifP(7);
    wdtd=aifP(4)*aifP(6);
    denomte=(a2-(1/aifP(7)));
    denomtd=(a2-(1/aifP(6)));
    
    A2=normParamVector(1)*normParamVector(2)/a2*exp(-a2*t_btf);
    
    % Convolution by parts
    Ct_btf = aifP(3)*(wete*A1.*((t_btf-aifP(1))+aifP(7)*(exp((aifP(1)-t_btf)/aifP(7))-1))...
        +wete*A2.*((exp(a2*t_btf)-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(7))*(exp(t_btf*denomte)-exp(aifP(1)*denomte))/denomte)...
        +(wdtd*A1.*((t_btf-aifP(1))+aifP(6)*(exp((aifP(1)-t_btf)/aifP(6))-1)))...
        +wdtd*A2.*((exp(a2*t_btf)-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(6))*(exp(t_btf*denomtd)-exp(aifP(1)*denomtd))/denomtd));
    
    A2=normParamVector(1)*normParamVector(2)/a2*exp(-a2*t_atf);
    
    Ct_atf = aifP(3)*(wete*A1.*((aifP(2)-aifP(1))+aifP(7)*(exp((aifP(1)-aifP(2))/aifP(7))-1)...
        -aifP(7)*(exp((aifP(2)-aifP(1))/aifP(7))-1)*exp(aifP(1)/aifP(7))*(exp(-t_atf/aifP(7))-exp(-aifP(2)/aifP(7))))...
        +wete*A2.*((exp(a2*aifP(2))-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(7))*(exp(aifP(2)*denomte)-exp(aifP(1)*denomte))/denomte...
        +exp(aifP(1)/aifP(7))*(exp((aifP(2)-aifP(1))/aifP(7))-1)*(exp(t_atf*denomte)-exp(aifP(2)*denomte))/denomte)...
        +(wdtd*A1.*((aifP(2)-aifP(1))+aifP(6)*(exp((aifP(1)-aifP(2))/aifP(6))-1)...
        -aifP(6)*(exp((aifP(2)-aifP(1))/aifP(6))-1)*exp(aifP(1)/aifP(6))*(exp(-t_atf/aifP(6))-exp(-aifP(2)/aifP(6))))...
        +wdtd*A2.*((exp(a2*aifP(2))-exp(a2*aifP(1)))/a2-exp(aifP(1)/aifP(6))*(exp(aifP(2)*denomtd)-exp(aifP(1)*denomtd))/denomtd...
        +exp(aifP(1)/aifP(6))*(exp((aifP(2)-aifP(1))/aifP(6))-1)*(exp(t_atf*denomtd)-exp(aifP(2)*denomtd))/denomtd)));
end

%% Final concentration
if isrow(time)
    catfunc = @horzcat;
    Ct_bti = zeros(1,numel(t_bti)); % Before ti AIF = 0
    conc = zeros(1,nT);
else
    catfunc = @vertcat;
    Ct_bti = zeros(numel(t_bti),1); % Before ti AIF = 0
    conc = zeros(nT,1);
end
Ct = catfunc(Ct_bti, Ct_btf, Ct_atf);

% Final result with AIF contribution
conc(dataUsed)=Ct+normParamVector(5)*concAif(dataUsed);

if any(isnan(conc)) || any(isinf(conc))
    conc(isnan(conc))=0; conc(isinf(conc)) = 0;
end
%% Final concentration
if timeIsRow
    conc = (dataWeight*conc)';
else
    conc = (dataWeight*conc);
end
end
