function concAif = aifbiexp(time,varargin)
% AIFBIEXP Create AIF model based on the work of (Pellerin et al. 2007)
%   concAif = AIFBIEXP(time,t0) Creates a default biexponential AIF (Fig.
%   4. of paper)
%
%   concAif = AIFBIEXP(time,paramStruct) Creates biexponential AIF with user
%   defined explicit parameter structure.
%
%   concAif = AIFBIEXP(time,paramVec) Creates biexponential AIF with user
%   defined parameter vector. Useful for curve fitting.
%
%   concAif = AIFBIEXP(time,varParamVec,fixedParamVec,isVar) Creates
%   biexponential AIF with user defined parameter vector. Useful for curve
%   fitting with a mixture of variable and fixed parameters.
%
% See also AIFPARKER, AIFSCHABEL, AIFHORSFIELD, AIFRECT
% Author: Benoit Bourassa-Moreau
% Based on the work of Marie Anne Richard (mar_calculateaif), Vincent
% Turgeon (calculateaif.m) and Jeremie Fouquet (fq_aiffunc.m)
% Creation date: 2019-05-31

%% Assign parameters
biexpStructFieldOrder = {'t0','tf','A','wd','we','td','te'};
paramVector = manageflexibleinput(varargin,biexpStructFieldOrder);

if isempty(paramVector) % Default AIF
    t0 = varargin{1}; % ti, min
    
    t0Default = 80.5/60;
    tf = 128/60-t0Default+t0; % tf, min
    A = 0.7857; % A, mM
    wd = 140/60;%0.7823; % wd, min
    we = 62/60; % we, min
    td = 26/60; % td, min
    te = 25; % te, min
else % AIF from input
    t0 = paramVector(1);
    tf = paramVector(2);
    A = paramVector(3);
    wd = paramVector(4);
    we = paramVector(5);
    td = paramVector(6);
    te = paramVector(7);
end

%% Basis AIF functions calculation
t_bti = time(time<t0);
t_btf = time(time>=t0 & time <tf);
t_atf = time(time>=tf);
% Convolution by parts
aifBeforeTf = A*(wd*td*(1-exp(-(t_btf-t0)/td))+we*te*...
    (1-exp(-(t_btf-t0)/te)));
aifAfterTf = A*(wd*td*exp(-(t_atf-t0)/td)*(exp((tf-t0)/td)-1)...
    +we*te*exp(-(t_atf-t0)/te)*(exp((tf-t0)/te)-1));
if isrow(time)
    catfunc = @horzcat;
    aifBeforeTi = zeros(1,numel(t_bti)); % Before ti AIF = 0
else
    catfunc = @vertcat;
    aifBeforeTi = zeros(numel(t_bti),1); % Before ti AIF = 0
end
%% Final AIF
concAif = catfunc(aifBeforeTi, aifBeforeTf, aifAfterTf);

end