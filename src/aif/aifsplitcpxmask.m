function [maskMagVenousInflowIauc, maskPhaseB0AngMaxEnh,phaseEnhancement,debugInfo] = aifsplitcpxmask(...
    magDyn,phaseDyn,venousMask,splitCpxOptions,conc,sssB0Ang,T1Map)
% AIFSPLITCPXMASK Find the optimal voxels to compute magnitude and phase 
% based concentration for a vessel near parallel with main magnetic
% field. Also corrects the phase.
%   - Average phase enhancement curve
%       - Slices with minimum vein to B0 angulation
%       - Correct each dynamic phase curves for
%           - background phase variation (e.g. respiration and blood oxygenation)
%           - vein angulation, and
%           - remove initial phase
%       - Voxels with maximum peak phase enhancement
%       - Averaged phase enhancement of the chosen voxels
%   - Select voxels with ideal magnitude enhancement
%       - Slices with minimal inflow
%       - Voxels with maximum basic conc area under the curve
%
%   [] = AIFSPLITCPXMASK()
%       Computes the complex form VOF with default clustering options.
%
% See also: GLOBALVOF,CONCMRISPGCPX
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-02-21

[nX,nY,nZ,nT] = size(magDyn);
if ~exist('sssB0Ang','var') 
    sssB0Ang = zeros(nZ,1); % [radians]
end
if ~isfield(splitCpxOptions.phase,'gmMask') || isempty(splitCpxOptions.phase.gmMask)
    splitCpxOptions.phase.gmMask = true(nX,nY,nZ);
end
if ~isfield(splitCpxOptions.phase,'debug') || isempty(splitCpxOptions.phase.debug)
    splitCpxOptions.phase.debug = false;
end
%% Phase enhancement curve
% Select phase slices
isLowAngSlices = false(nZ,1);

voxelsPerSlices = squeeze(sum(sum(venousMask,1),2));
slicesWithVoxels = (voxelsPerSlices>0);
sssB0Ang(~slicesWithVoxels) = NaN;
b0AngUnderThresh = sssB0Ang < (splitCpxOptions.phase.B0AngThresh/pi*180);
[~,idxSortedB0Ang] = sort(sssB0Ang);
if sum(b0AngUnderThresh) < splitCpxOptions.phase.nMinSlices
    isLowAngSlices(idxSortedB0Ang(1:splitCpxOptions.phase.nMinSlices)) = true;
elseif sum(b0AngUnderThresh) < splitCpxOptions.phase.nMaxSlices
    isLowAngSlices = b0AngUnderThresh;
else
    isLowAngSlices(idxSortedB0Ang(1:splitCpxOptions.phase.nMaxSlices)) = true;
end
nPhaseSlices = sum(isLowAngSlices);
minVoxels = splitCpxOptions.phase.nMinSlices*splitCpxOptions.phase.maxVoxelsPerSlice;
nPhaseVoxels = splitCpxOptions.phase.maxVoxelsPerSlice*nPhaseSlices;
maskPhase = venousMask;
maskPhase(:,:,~isLowAngSlices) = false;

% Select phase voxels
% Correct phase curves
b0AngMat = repmat(reshape(sssB0Ang,[1 1 nZ]),[nX,nY,1]);
[listVesselPhase,nIdxT,b0AngList] = dyn3dconversion(phaseDyn,maskPhase,[],b0AngMat);
[nPx,~] = size(listVesselPhase);

initPhase = mean(phaseDyn(:,:,:,splitCpxOptions.framesPre(1):splitCpxOptions.framesPre(2)),4);
nIaucRange = 1+diff(splitCpxOptions.iaucRangeIdx);
maxEnhancementVol = max(phaseDyn(:,:,:,splitCpxOptions.iaucRangeIdx(1):splitCpxOptions.iaucRangeIdx(2)) - repmat(initPhase,[1 1 1 nIaucRange]),[],4);
[maskNeigh,chosenNeighbourg,vesselPxls] = findsurroundingtissue(abs(maxEnhancementVol),phaseDyn,splitCpxOptions.phase.gmMask,maskPhase,6:3,0.1);
[listBkgPhase, ~] = dyn3dconversion(phaseDyn,maskNeigh,chosenNeighbourg);
bkgPhaseVar = nanmean(listBkgPhase,1);
% Background correction: voxelwise
listBkgPhase(isnan(listBkgPhase)) = 0;
listBkgCorrPhase = (listVesselPhase-listBkgPhase);
% Background correction: average
% listBkgCorrPhase = (listVesselPhase-repmat(bkgPhaseVar,[nPx,1]));

listB0AngCorrPhase = 2/3*listBkgCorrPhase./((cos(repmat(b0AngList,[1 nT]))).^2-1/3);
listInitPhaseCorr = mean(listB0AngCorrPhase(:,splitCpxOptions.framesPre(1):splitCpxOptions.framesPre(2)),2);
listCorrPhase = listB0AngCorrPhase - repmat(listInitPhaseCorr,[1 nT]);

% Select voxels with maximum phase IAUC to be less sensitive to phase noise
iaucEnhancement = sum(listCorrPhase(:,splitCpxOptions.iaucRangeIdx(1):splitCpxOptions.iaucRangeIdx(2)),2);
[sortEnh,maxSortIdx] = sort(iaucEnhancement,'descend');
% % OLD - Select voxels with maximum phase enhancement
% maxPeakEnhancement = max(listCorrPhase(:,splitCpxOptions.iaucRangeIdx(1):splitCpxOptions.iaucRangeIdx(2)),[],2);
% [sortEnh,maxSortIdx] = sort(maxPeakEnhancement,'descend');

keptEnhancement = false(nPx,1);
if nPx < nPhaseVoxels
    keptEnhancement(:) = true;
elseif any(isnan(sortEnh(1:nPhaseVoxels)))
    idxLastNotNaN = find(isnan(sortEnh),1,'first')-1;
    if idxLastNotNaN < minVoxels
        warning('% kept phase voxels is under %d. Might lead to bad peak enhancement.',idxLastNotNaN,minVoxels);
    else
        keptEnhancement(maxSortIdx(1:idxLastNotNaN)) = true;
    end
else
    keptEnhancement(maxSortIdx(1:nPhaseVoxels)) = true;
end
phaseEnhancement = mean(listCorrPhase(keptEnhancement,:),1);

if splitCpxOptions.phase.debug
    debugInfo.vesselPxls = vesselPxls;
    debugInfo.neighPxls = chosenNeighbourg;
    debugInfo.rawPhase = listVesselPhase;
    debugInfo.bkgPhase = listBkgPhase;
    debugInfo.bkgCorrPhase = listBkgCorrPhase;
    debugInfo.b0Ang = b0AngList;
    debugInfo.angCorrPhase = listCorrPhase;
    debugInfo.phaseIauc = iaucEnhancement;
    debugInfo.angCorrPhaseMap = mat2dyn3d(listCorrPhase, nIdxT,maskPhase);

    figure;
    subplot(1,3,1);
    curveBkg = plot((bkgPhaseVar(1,1:splitCpxOptions.iaucRangeIdx(2)) - mean(bkgPhaseVar(1,splitCpxOptions.framesPre(1):splitCpxOptions.framesPre(2)),2))/pi*180,'k-','Linewidth',1.5); 
    hold on;
    plot((listVesselPhase(keptEnhancement,1:splitCpxOptions.iaucRangeIdx(2)) - repmat(mean(listVesselPhase(keptEnhancement,splitCpxOptions.framesPre(1):splitCpxOptions.framesPre(2)),2),[1 splitCpxOptions.iaucRangeIdx(2)]))'/pi*180); 
    hold off; legend(curveBkg,'Avg. bkg.');
    xlabel('Time idx (-)'); ylabel('Phase (\circ)'); title('Raw');
    subplot(1,3,2);plot((listBkgCorrPhase(keptEnhancement,1:splitCpxOptions.iaucRangeIdx(2)) - repmat(mean(listBkgCorrPhase(keptEnhancement,splitCpxOptions.framesPre(1):splitCpxOptions.framesPre(2)),2),[1 splitCpxOptions.iaucRangeIdx(2)]))'/pi*180);xlabel('Time idx (-)'); title('Background');
    subplot(1,3,3);plot(listCorrPhase(keptEnhancement,1:splitCpxOptions.iaucRangeIdx(2))'/pi*180);xlabel('Time idx (-)'); title('Angulation');
end
%% Magnitude voxel selection
% Select mag slices
minDceMagSlice = splitCpxOptions.mag.slabSlices;
if splitCpxOptions.cerebellumFov % Exclude slices to avoid inflow
    maxDceMagSlice = (nZ-splitCpxOptions.mag.inflowSlices);
else % Exclude slices for slice profile
    maxDceMagSlice = (nZ-splitCpxOptions.mag.slabSlices);
end
minMagVoxels = splitCpxOptions.phase.maxVoxelsPerSlice*splitCpxOptions.mag.minMagSlices;
keptMagSlices = false(nZ,1);
if sum(slicesWithVoxels(minDceMagSlice:maxDceMagSlice)) > splitCpxOptions.mag.minMagSlices
    keptMagSlices(minDceMagSlice:maxDceMagSlice) = slicesWithVoxels(minDceMagSlice:maxDceMagSlice);
else
    keptMagSliceIdx = find(cumsum(slicesWithVoxels(minDceMagSlice:end))>=splitCpxOptions.mag.minMagSlices,1,'first');
    if isempty(keptMagSliceIdx)
        keptMagSlices = slicesWithVoxels;
    else
        keptMagSlices(1:(keptMagSliceIdx+minDceMagSlice-1)) = slicesWithVoxels(1:(keptMagSliceIdx+minDceMagSlice-1));
    end
end
maskMag = venousMask;
maskMag(:,:,~keptMagSlices) = false;


% Removed - Poor results with AUC pre-selection
% keptMagIaucFraction = splitCpxOptions.mag.voxelsPerSlice/mean(voxelsPerSlices(keptMagSlices));
% 
% % Keep voxels with maximum IAUC
% [listBasicConc, ~] = dyn3dconversion(conc,maskMag);
% bolusIauc = mean(listBasicConc(:,splitCpxOptions.framesPre(2):splitCpxOptions.iaucRangeIdx(2)),2);
% iaucThreshold = prctile(bolusIauc,100*(1-keptMagIaucFraction));
% isOverIaucThresh = bolusIauc > iaucThreshold;
% 
%% Prepare output masks
% maskMagVenousInflowIauc = false(nX,nY,nZ);
% maskMagVenousInflowIauc(maskMag) = isOverIaucThresh;

% T1 values decrease posterior to SSS caused by bone proximity. Exclude low blood T1 values
maskMagVenousInflowIauc = (maskMag & (T1Map >= splitCpxOptions.mag.minT1Blood));
maskPhaseB0AngMaxEnh = false(nX,nY,nZ);
maskPhaseB0AngMaxEnh(maskPhase) = keptEnhancement;
end