function concAif = aifrect(time,varargin)
% AIFRECT Create rectangular AIF model for testing purpose. Make sure that
% t0 and tf are defined exctly between time points for a true rect function
%   concAif = AIFRECT(time,t0) Creates a default rect AIF 
%
%   concAif = AIFRECT(time,paramStruct) Creates rect AIF with user
%   defined explicit parameter structure.
%
%   concAif = AIFRECT(time,paramVec) Creates rect AIF with user
%   defined parameter vector. Useful for curve fitting.
%
%   concAif = AIFRECT(time,varParamVec,fixedParamVec,isVar) Creates
%   rect AIF with user defined parameter vector. Useful for curve
%   fitting with a mixture of variable and fixed parameters.
%
% See also AIFPARKER, AIFSCHABEL, AIFHORSFIELD
% Author: Benoit Bourassa-Moreau

%% Assign parameters
biexpStructFieldOrder = {'t0','tf','At'};
paramVector = manageflexibleinput(varargin,biexpStructFieldOrder);

if isempty(paramVector) % Default AIF
    t0 = varargin{1}; % ti, min
    tf = 6/60 + t0; % tf, min
    At = 0.2; % A, (mM min)
else % AIF from input
    t0 = paramVector(1);
    tf = paramVector(2);
    At = paramVector(3);
end

timeIsRow = false;
if isrow(time)
    time = reshape(time,[],1);
    timeIsRow = true;
end

%% Basis AIF functions calculation
nT = length(time);
tIdx0 = find(time>=t0,1,'first');
tIdxf = find(time<tf,1,'last');
if isempty(tIdx0) || isempty(tIdxf)
    error('AIF defined outside acquisition time. Change acquisition time or AIF (t0,tf)');
end

% Compute amplitude to preserve At product
A = At/(tf-t0); % mM
concAif = zeros(nT,1);
concAif(tIdx0:tIdxf) = A;
% Adjust rect border to preserve amplitude with discrete time sampling
Afraction0 = (time(tIdx0)-t0)/(time(tIdx0) - time(tIdx0-1)) - 0.5;
if Afraction0 >= 0
    concAif(tIdx0-1) = Afraction0*A;
else
    concAif(tIdx0) = (1+Afraction0)*A;
end
Afraction1 = (tf-time(tIdxf))/(time(tIdxf+1) - time(tIdxf)) - 0.5;
if Afraction1 >= 0
    concAif(tIdxf+1) = Afraction1*A;
else
    concAif(tIdxf) = (1+Afraction1)*A;
end
%% Final AIF
if timeIsRow
    concAif = concAif';
end
end