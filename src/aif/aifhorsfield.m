function concAif = aifhorsfield(time,varargin)
% AIFHORSFIELD Create AIF model based on the work of (Horsfield et al. 
% Phys Med Biol. 2009;54(9):2933-2949) and generalized for (Georgiou et al.
% , Magn Reson Med. 2018;(August):1-9). The Hosfield AIF equation form is 
% modified with a normalized gamma-variate tail.
%   concAif = AIFHORSFIELD(time,t0) Creates the default Horsfield AIF
%   (table 3 and supplementary materials figure S2 of Georgiou paper)
%
%   concAif = AIFHORSFIELD(time,paramStruct) Creates Horsfield AIF with user 
%   defined explicit parameter structure.
%
%   concAif = AIFHORSFIELD(time,paramVec) Creates Horsfield AIF with user
%   defined parameter vector. Useful for curve fitting.
%
%   concAif = AIFHORSFIELD(time,varParamVec,fixedParamVec,isVar) Creates
%   Horsfield AIF with user defined parameter vector. Useful for curve
%   fitting with a mixture of variable and fixed parameters.
%
% See also AIFBIEXP, AIFPARKER, AIFSCHABEL
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-31

%% Assign parameters
horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
paramVector = manageflexibleinput(varargin,horsfieldStructFieldOrder);

if isempty(paramVector) % Default AIF
    t0 = varargin{1}; % ti, min
    % Fixed model parameters for Georgiou version
    
    A = [10.06,0.33,0.37]; % mM
    m = [16.02,1.17, 0.11];% 1/min
%     A = [0.37, 0.33, 10.06]; % mM*min
%     m = [0.11, 1.17, 16.02];% 1/min
    alpha = 5.26; % -
    beta = 0.032; % min
    tau = 0.129; % min
    kN = 1; % (-)
    k = 3.0214;%9.1746*(beta*(alpha+1)+tau); % min unknown arbitrary scaling? 
    
    % Fixed model parameters for Horsfield version
    % Bi-exponential extravasation and excretion (Tofts and Kermode 1991)
%         m = [NaN, 0.0024, 0.000185]*60; % (1/min)
%         A = [NaN, 3.99, 4.78]; % (kg/L)*min
%         % Recirculation delay
%         tau = 10.6/60; % (min)
%         
%         % Subject 3 Aorta parameters
%         alpha = 3.05; % (-)
%         beta = 3.61/60; % (min)
%         m(1) = 0.053*60; % (1/min)
%         A(1) = 42.8; % (kg/L)
%         kN = 0.340; % (-)
%         
%         dose = 0.1; % mmol/kg
%         k = dose*75*(beta*(alpha+1)+tau); % 8.77*dose*(beta*(alpha+1)+tau)/kN; % (s mmol/kg)
else % AIF from input
    t0 = paramVector(1); % (min)
    A = paramVector(2:4); % (mM)
    m = paramVector(5:7); % (1/min)
    
    alpha = paramVector(8); % (-)
    beta = paramVector(9); % (min)
    tau = paramVector(10); % (min)
    
    k = paramVector(11); % (min)
    kN = 1; % (-)
end

timeIsRow = false;
if isrow(time)
    time = reshape(time,[],1);
    timeIsRow = true;
end

%% Basis AIF functions calculation
nT = length(time);
shiftT = time - t0;

% Gamma-variate functions 
% Number of functions is defined by time range
nGamma = ceil(shiftT(end)/tau) - 1;
gammaFuncs = zeros(nT,nGamma+1);
for iGamma = 0:nGamma
    isInG = (iGamma*tau < shiftT);% & (shiftT < (iGamma+1)*tau);
    cT = shiftT(isInG) - iGamma*tau;
    cAlpha = (iGamma+1)*alpha+iGamma;
    gammaFuncs(isInG,iGamma+1) = cT.^cAlpha.*exp(-cT/beta)/(beta^(cAlpha+1)*gamma(cAlpha+1));
end

% Tripple exponential functions
expFuncsTimesGamma = zeros(nT,3);
isInExp = shiftT >= 0;
for iExp = 1:3
    expFuncsTimesGamma(isInExp,iExp) = A(iExp)*exp(-m(iExp)*shiftT(isInExp));
end
%% Final AIF
sumGamma = sum(gammaFuncs,2);
% For long time values (>200 s) inf and NaN values are created by gamma function.
gammaInfValues = any(isnan(gammaFuncs) | isinf(gammaFuncs),2);
% Appxoximate the sum of gamma function by the steady state value
normalizeSumGamma = (beta*(alpha+1)+tau);
steadyStateValue = 1/normalizeSumGamma;
sumGamma(gammaInfValues) = steadyStateValue;

sumGammaNorm = sumGamma*normalizeSumGamma; % Normalized so that sumGammaNorm(end) -> 1;

sumExp = sum(expFuncsTimesGamma,2);
% sumExpNorm = sumExp/(sum(A)); % Normalized so that max(sumExpNorm) -> 1

% Plasma concentration (Cb)
concAif = k*kN*sumExp.*sumGammaNorm;
if timeIsRow
    concAif = concAif';
end
end