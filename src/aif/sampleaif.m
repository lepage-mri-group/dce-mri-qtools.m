function sampledAif = sampleaif(time,aif,timeSample)
% SAMPLEAIF Samples the AIF at specific time points with the
% parametrized AIF model when available. If not available, a linear 
% interpolation of AIF data points is used.
%   concAif = SAMPLEAIF(time,aifVector,timeSample) AIF to interpolate. Uses
%       linear interpolation to sample the provided AIF.
%
%   concAif = SAMPLEAIF(time,aifStructure,timeSample) AIF model to sample.
%       Uses the specified AIF structure and function to compute AIF at 
%       specified time.
%
% See also: AIFHORSFIELD, AIFBIEXP, AIFPARKER, AIFSCHABEL
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-31

%% Find the input AIF form
nT = length(time);
interpAif = true;
if isstruct(aif) 
    if isfield(aif,'FHandle') && ~isempty(aif.FHandle) % Parameterized AIF
        if isfield(aif,'pValues') && ~isempty(aif.pValues)
            interpAif = false;
        elseif isfield(aif,'AIFit') && ~isempty(aif.AIFit)
            if isrow(aif.AIFit)
                aifInitVec = aif.AIFit';
            else
                aifInitVec = aif.AIFit;
            end
        end
        if isfield(aif,'minXIdx') && ~isempty(aif.minXIdx)
            minXIdx = aif.minXIdx;
        else
            minXIdx = 1;
        end
    else
        error('Unknown AIF structure.')
    end
elseif isvector(aif) && length(aif)==nT % Sampled AIF
    if isrow(aif)
        aifInitVec = aif';
    else
        aifInitVec = aif;
    end
end

%% Interpolate AIF data or model
if interpAif
    if length(timeSample) == nT && all(timeSample==time)
        sampledAif = aifInitVec;
    else
        sampledAif = interp1(time,aifInitVec,timeSample,'linear','extrap');
    end
else
    sampledAif = aif.FHandle(timeSample,aif.pValues(minXIdx,:));
end
end