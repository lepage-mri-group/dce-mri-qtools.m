function concAif = aifschabel(time,varargin)
% AIFSCHABEL Create AIF model based on the work of (Schabel and Parker. 
% Phys Med Biol. 2008;53(9):2345-2373)
%   concAif = AIFSCHABEL(time,t0) Creates the default Schabel AIF (Fig. 7 
%   of paper).
%
%   concAif = AIFSCHABEL(time,paramStruct) Creates Schabel AIF with user 
%   defined explicit parameter structure.
%
%   concAif = AIFSCHABEL(time,paramVec) Creates Schabel AIF with user
%   defined parameter vector. Useful for curve fitting.
%
%   concAif = AIFSCHABEL(time,varParamVec,fixedParamVec,isVar) Creates
%   Schabel AIF with user defined parameter vector. Useful for curve
%   fitting with a mixture of variable and fixed parameters.
%
% See also AIFBIEXP, AIFPARKER, AIFHORSFIELD
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-31

%% Assign parameters
schabelStructFieldOrder = {'t0','A(1)','A(2)','A(3)','A(4)','alpha(1)','alpha(2)','tau(1)','tau(2)','tau(3)','T'};
paramVector = manageflexibleinput(varargin,schabelStructFieldOrder);

if isempty(paramVector) % Default AIF
    t0 = varargin{1}; % ti, min
    
    A = [0.7, 5.8589, 0.9444, 0.4888]; % Optimized to match fig 7. % t0 = 0.1563;
    Delta = t0*[1 1 1 1]; % min
    alpha = [7.9461, 2.5393, 7.9461, 7.9461];
    tau = [0.1400, 0.04286, 0.06873, 0.1400]; % min
    
    T = 9.6319; % min
else % AIF from input
    % Using Schabel parameter reduction (see paper Appendix)
    alpha = zeros(1,4);
    tau = zeros(1,4);
    
    t0 = paramVector(1); % (min)
    A = paramVector(2:5);
    Delta = t0*[1 1 1 1]; % Delta1 = Delta2 = Delta3 = Delta0
    alpha(1) = paramVector(6);
    alpha(2) = paramVector(7);
    alpha([3,4]) = alpha(1); % alpha2 = alpha3 = alpha0
    tau(1) = paramVector(8);
    tau([2, 3]) = paramVector(9:10);
    tau(4) = tau(1); % tau3 = tau0
    
    T = paramVector(11);
end

timeIsRow = false;
if isrow(time)
    warning('Time vector should be column');
    time = reshape(time,[],1);
    timeIsRow = true;
end
%% Basis AIF functions calculation
% Three normalized gamma function
nGamma = 3;
nT = length(time);
gammaVariate = NaN(nT,nGamma);
for iGamma = 1:nGamma
    tFunc = time-Delta(iGamma+1);
    tUnderZero = tFunc<0;
    gammaVariate(:,iGamma) = (exp(1)/(tau(iGamma+1)*(alpha(iGamma+1)-1)))^(alpha(iGamma+1)-1)*tFunc.^(alpha(iGamma+1)-1).*exp(-tFunc/tau(iGamma+1));
    gammaVariate(tUnderZero,iGamma) = 0;
end
% Sigmoid curve
tFunc = time-Delta(1);
tUnderZero = tFunc<=0;
sigmoid = T/((T-tau(1))*gamma(alpha(1)))*(tFunc/T).^(-tau(1)/(T-tau(1))).*exp(-tFunc/T).*gammainc((1/tau(1)-1/T)*tFunc,alpha(1));
sigmoid(tUnderZero) = 0;
sigmoid = sigmoid/max(sigmoid);

%% Final AIF
concAif = A(1)*sigmoid;
for iGamma = 1:nGamma
    concAif = concAif + A(iGamma+1)*gammaVariate(:,iGamma);
end
if timeIsRow
    concAif = concAif';
end
end