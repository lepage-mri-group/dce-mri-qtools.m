function [bkgCorrPhase, angCorrPhase,maskPhaseExcludeDrift,avgPhaseEnh,stdPhaseEnh,debugInfo] = aifphasecorr(...
    iaucMag,phaseDyn,venousMask,surrMask,framesPre,iaucRangeIdx,sssB0Ang,sssB0AngThresh,bigGapIdx,limitGapPhase,nMinSlices,nMaxSlices,maxVoxelsPerSlice,plotDebug)
% AIFPHASECORR Compensates vascular phase for background variations and 
% vessel angulation. Compures a final average phase enhancement curve from
%       - Slices with minimum vein to B0 angulation
%       - Voxels with maximum peak phase enhancement
%       - Averaged phase enhancement of the chosen voxels
%
%   [] = AIFPHASECORR()
%       Computes the complex form VOF with default clustering options.
%
% See also: GLOBALVOF,CONCMRISPGCPX,AIFSPLITCPXMASK
% Author: Benoit Bourassa-Moreau
% Creation date: 2021-01-12

[nX,nY,nZ,nT] = size(phaseDyn);
if ~exist('sssB0Ang','var') 
    sssB0Ang = zeros(nZ,1); % [radians]
end
if isempty(surrMask)
    surrMask = true(nX,nY,nZ);
end
if ~exist('plotDebug','var') || isempty(plotDebug)
    plotDebug = false;
end
if isempty(sssB0AngThresh)
    sssB0AngThresh = atan(sqrt(2))/pi*180-5;
end
%% Background phase correction
% Find surrounding tissues in GM mask with minimum phase enhancement
% initPhase = mean(phaseDyn(:,:,:,framesPre(1):framesPre(2)),4);
% nIaucRange = 1+diff(iaucRangeIdx);
% maxEnhancementVol = max(phaseDyn(:,:,:,iaucRangeIdx(1):iaucRangeIdx(2)) - repmat(initPhase,[1 1 1 nIaucRange]),[],4);
% [maskNeigh,chosenNeighbourg,vesselPxls] = findsurroundingtissue(abs(maxEnhancementVol),phaseDyn,gmMask,venousMask,6:3,0.1);

surrMask(iaucMag==0) = false;
initPhase = mean(phaseDyn(:,:,:,framesPre(1):framesPre(2)),4);
nIaucRange = 1+diff(iaucRangeIdx);
iaucPhase = max(abs(phaseDyn(:,:,:,iaucRangeIdx(1):iaucRangeIdx(2)) - repmat(initPhase,[1 1 1 nIaucRange])),[],4);
nRad = 4;
[chosenNeighbourg,vesselPxls] = avgsurroundingtissue(iaucMag,iaucPhase,surrMask,venousMask,nRad);

% Extract curves
[listVesselPhase,nIdxT] = dyn3dconversion(phaseDyn,venousMask);
nPts = size(listVesselPhase,1);
listBkgPhase = NaN(nPts,nT);
for iPts = 1:nPts
            nMaskPx = size(chosenNeighbourg{iPts},1);
            listAllPxlsBkgPhase = NaN(nMaskPx,nT);
            for iMaskPx = 1:nMaskPx
                listAllPxlsBkgPhase(iMaskPx,:) = reshape(phaseDyn(chosenNeighbourg{iPts}(iMaskPx,1),chosenNeighbourg{iPts}(iMaskPx,2),chosenNeighbourg{iPts}(iMaskPx,3),:),[1 nT]);
            end
            listBkgPhase(iPts,:) = mean(listAllPxlsBkgPhase,1);
end
% Background correction: voxelwise
listBkgPhase(isnan(listBkgPhase)) = 0;
listBkgCorrPhase = (listVesselPhase-listBkgPhase);

bkgCorrPhase = mat2dyn3d(listBkgCorrPhase, nIdxT,venousMask);

%% Angulation compensation
% Select phase slices with valid angulation measurement
isLowAngSlices = false(nZ,1);

voxelsPerSlices = squeeze(sum(sum(venousMask,1),2));
slicesWithVoxels = (voxelsPerSlices>0);
sssB0Ang(~slicesWithVoxels) = NaN;
b0AngUnderThresh = sssB0Ang < sssB0AngThresh;
[~,idxSortedB0Ang] = sort(sssB0Ang);
if sum(b0AngUnderThresh) < nMinSlices
    isLowAngSlices(idxSortedB0Ang(1:nMinSlices)) = true;
elseif sum(b0AngUnderThresh) < nMaxSlices
    isLowAngSlices = b0AngUnderThresh;
else
    isLowAngSlices(idxSortedB0Ang(1:nMaxSlices)) = true;
end
nPhaseSlices = sum(isLowAngSlices);
maskPhase = venousMask;
maskPhase(:,:,~isLowAngSlices) = false;

% Correct phase curves
b0AngMat = repmat(reshape(sssB0Ang,[1 1 nZ]),[nX,nY,1]);
[listBkgCorrPhase,nIdxT,b0AngList] = dyn3dconversion(bkgCorrPhase,maskPhase,[],b0AngMat);
[nPx,~] = size(listBkgCorrPhase);

listB0AngCorrPhase = 2/3*listBkgCorrPhase./((cos(repmat(b0AngList,[1 nT]))).^2-1/3);

angCorrPhase = mat2dyn3d(listB0AngCorrPhase,nIdxT,maskPhase);

%% Remove major phase drift in time gap
nMinVox = 20;
if nPx> nMinVox && exist('limitGapPhase','var') && ~isempty(limitGapPhase) && exist('bigGapIdx','var') && ~isempty(bigGapIdx) % bigGapIdx
    bigGapPhaseShift =  mean(listB0AngCorrPhase(:,(bigGapIdx+1):nT),2) - mean(listB0AngCorrPhase(:,(2*bigGapIdx-nT):bigGapIdx),2);
    keptPhaseCurves = (bigGapPhaseShift<limitGapPhase(1)) & (bigGapPhaseShift>limitGapPhase(2));    
    if sum(keptPhaseCurves)<nMinVox
        [~,gapSortIdx] = sort(abs(bigGapPhaseShift-mean(limitGapPhase)));
        keptPhaseCurves = false(nPx,1);
        keptPhaseCurves(gapSortIdx(1:nMinVox)) = true;
    end
else
    keptPhaseCurves = true(nPx,1);
end
maskPhaseExcludeDrift = maskPhase;
maskPhaseExcludeDrift(maskPhase) = keptPhaseCurves;
%% Single average phase
listInitPhaseCorr = mean(listB0AngCorrPhase(:,framesPre(1):framesPre(2)),2);
listCorrPhaseEnh = listB0AngCorrPhase - repmat(listInitPhaseCorr,[1 nT]);

% Select voxels with maximum phase IAUC to be less sensitive to phase noise
iaucEnhancement = sum(listCorrPhaseEnh(:,iaucRangeIdx(1):iaucRangeIdx(2)),2);
[sortEnh,maxSortIdx] = sort(iaucEnhancement,'descend');

% Select voxels with maximum phase IAUC
minVoxels = nMinSlices*maxVoxelsPerSlice;
nPhaseVoxels = maxVoxelsPerSlice*nPhaseSlices;

keptEnhancement = false(nPx,1);
if nPx < nPhaseVoxels
    keptEnhancement(:) = true;
elseif any(isnan(sortEnh(1:nPhaseVoxels)))
    idxLastNotNaN = find(isnan(sortEnh),1,'first')-1;
    if idxLastNotNaN < minVoxels
        warning('% kept phase voxels is under %d. Might lead to bad peak enhancement.',idxLastNotNaN,minVoxels);
    else
        keptEnhancement(maxSortIdx(1:idxLastNotNaN)) = true;
    end
else
    keptEnhancement(maxSortIdx(1:nPhaseVoxels)) = true;
end
avgPhaseEnh = mean(listCorrPhaseEnh(keptEnhancement,:),1);
stdPhaseEnh = std(listCorrPhaseEnh(keptEnhancement,:),[],1);

%% Plot debug figure
debugInfo.vesselPxls = vesselPxls;
debugInfo.neighPxls = chosenNeighbourg;
debugInfo.bkgPhaseVar = nanmean(listBkgPhase,1);
debugInfo.phaseIauc = iaucEnhancement;
debugInfo.maskPhaseB0AngMaxEnh = false(nX,nY,nZ);
debugInfo.maskPhaseB0AngMaxEnh(maskPhase) = keptEnhancement;
debugInfo.validPhaseGap = keptPhaseCurves;
if plotDebug
    
    figure;
    subplot(1,3,1);
    curveBkg = plot((debugInfo.bkgPhaseVar(1,1:iaucRangeIdx(2)) - mean(debugInfo.bkgPhaseVar(1,framesPre(1):framesPre(2)),2))/pi*180,'k-','Linewidth',1.5); 
    hold on;
    plot((listVesselPhase(keptEnhancement,1:iaucRangeIdx(2)) - repmat(mean(listVesselPhase(keptEnhancement,framesPre(1):framesPre(2)),2),[1 iaucRangeIdx(2)]))'/pi*180); 
    hold off; legend(curveBkg,'Avg. bkg.');
    xlabel('Time idx (-)'); ylabel('Phase (\circ)'); title('Raw');
    subplot(1,3,2);plot((listBkgCorrPhase(keptEnhancement,1:iaucRangeIdx(2)) - repmat(mean(listBkgCorrPhase(keptEnhancement,framesPre(1):framesPre(2)),2),[1 iaucRangeIdx(2)]))'/pi*180);xlabel('Time idx (-)'); title('Background');
    subplot(1,3,3);plot(listCorrPhaseEnh(keptEnhancement,1:iaucRangeIdx(2))'/pi*180);xlabel('Time idx (-)'); title('Angulation');
end
end