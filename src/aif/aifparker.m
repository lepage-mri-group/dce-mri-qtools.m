function concAif = aifparker(time,varargin)
% AIFPARKER Create AIF model based on the work of (Parker et al., 
% Magn Reson Med. 2006;56(5):993-1000)
%   concAif = AIFPARKER(time,t0) Creates the default Parker AIF.
%
%   concAif = AIFPARKER(time,paramStruct) Creates Parker AIF with user 
%   defined parameters.
%
%   concAif = AIFPARKER(time,paramVec) Creates Parker AIF with user
%   defined parameter vector. Useful for curve fitting.
%
%   concAif = AIFPARKER(time,varParamVec,fixedParamVec,isVar) Creates
%   Parker AIF with user defined parameter vector. Useful for curve
%   fitting with a mixture of variable and fixed parameters.
%
% See also AIFBIEXP, AIFSCHABEL, AIFHORSFIELD
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-31

%% Assign parameters
parkerStructFieldOrder = {'t0','A(1)','A(2)','T(1)','T(2)','sigma(1)','sigma(2)','alpha','beta','s','tau'};
paramVector = manageflexibleinput(varargin,parkerStructFieldOrder);

if isempty(paramVector) % Default AIF
    t0 = varargin{1}; % ti, min
    % Parker et al. injection protocol
    A = [0.809, 0.330]; % mmol*min
    T = [0.17046, 0.365]; % min
    sigma = [0.0563, 0.132]; % min
    alpha = 1.050; % mmol
    beta = 0.1685; % 1/min
    s = 38.078; % 1/min
    tau = 0.483; % min
else % AIF from input
    t0 = paramVector(1); % ti, min
    % Parker et al. injection protocol
    A = paramVector(2:3); % mmol*min
    T = paramVector(4:5); % min
    sigma = paramVector(6:7); % min
    alpha = paramVector(8); % mmol
    beta = paramVector(9); % 1/min
    s = paramVector(10); % 1/min
    tau = paramVector(11); % min
end

nT = length(time);
timeIsRow = false;
if isrow(time)
    warning('Time vector should be column');
    time = reshape(time,[],1);
    timeIsRow = true;
end

%% Basis AIF functions calculation
% Two normalized gamma function
gammaVariate = NaN(nT,2);
for iGamma = 1:2
    tFunc = time-t0;
    tUnderZero = tFunc<0;
    gammaVariate(:,iGamma) = A(iGamma)/sigma(iGamma)/sqrt(2*pi)*...
        exp(-(tFunc-T(iGamma)).^2/(2*sigma(iGamma)^2));
    gammaVariate(tUnderZero,iGamma) = 0;
end
% Sigmoid function
tFunc = time-t0;
tUnderZero = tFunc<=0;
sigmoid = alpha*exp(-beta*tFunc)./(1+exp(-s*(tFunc-tau)));
sigmoid(tUnderZero) = 0;

%% Final AIF
concAif = sigmoid + sum(gammaVariate,2);
if timeIsRow
    concAif = concAif';
end
end