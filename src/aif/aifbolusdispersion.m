function [delayedAifNorm,dispersionModel] = aifbolusdispersion(time,aifModel,dispersionModel)
% AIFBOLUSDISPERSION Models AIF dispersion based on the vascular transport
% function (VTF). The VTF is modeled as a log-normal distribution (Willats
% et al., ISMRM Berlin. 2007. p. 1445).
%
%   [vofNorm, dispersionModel] = AIFBOLUSDISPERSION(time,aifModel,dispersionModel)
%
% See also AIFPARKER, AIFSCHABEL, AIFHORSFIELD, AIFRECT
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-08-02

%% Check input and assign default values
% The dispersion model requires seconds units
dt = 0.1; % s
timeVtf = 0:dt:30; % s
% Default - Severe dispersion (Willats)
t0VTF = 0; % s
sigmaVTF = 0.78; % s
muVTF = 0.59; % s

% VTF time is long enough so that VTF approaches 0 at the end for typical
% dispersion parameters.

if exist('dispersionModel','var') && ~isempty(dispersionModel)
    t0VTF = dispersionModel.t0;
    sigmaVTF = dispersionModel.sigma; % s
    muVTF = dispersionModel.mu;
    if isfield(dispersionModel,'time')
        timeVtf = dispersionModel.time;
        dt = diff(timeVtf(1:2));
    else
        dispersionModel.time = timeVtf;
    end
else
    dispersionModel.t0 = t0VTF;
    dispersionModel.sigma = sigmaVTF;
    dispersionModel.mu = muVTF;
    dispersionModel.time = timeVtf;
end

%% Compute dispersion VTF
dispersionModel.vvtt = (exp((sigmaVTF)^2)-1)*exp(sigmaVTF^2+2*muVTF); % variance of the vascular transit time [s^2]
dispersionModel.mvtt = exp(muVTF + sigmaVTF^2/2);  % mean of the vascular transit time [s]

% VTF units : 1/s�
dispersionModel.vtf = 1./((timeVtf-t0VTF)*sigmaVTF*sqrt(2*pi)).*exp(-1/(2*sigmaVTF^2)*(log(timeVtf-t0VTF)-muVTF).^2);
dispersionModel.vtf((timeVtf-t0VTF)<=0) = 0;

%% Interp AIF fof convolution
timeSec = 60*time;
timeAifConv = (timeSec(1)):dt:(timeSec(end)); % s

aifSample = sampleaif(time,aifModel,time);
aifConv = sampleaif(time,aifModel,timeAifConv/60);

%% Compute delayed AIF (or VOF)
nConv = length(timeAifConv);
delayedAif = conv(aifConv,dispersionModel.vtf);
delayedAifInterp = interp1(timeAifConv,delayedAif(1:nConv),timeSec);

aifInt = trapz(timeSec,aifSample);
delayedAifInt = trapz(timeSec,delayedAifInterp);
delayedAifNorm = delayedAifInterp/delayedAifInt*aifInt;
end