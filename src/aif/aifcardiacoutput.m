function [cardiacOutput, AUC1st] = aifcardiacoutput(aifModel,dose)
% AIFCARDIACOUTPUT Computes the AIF cardiac output and area under the first 
% pass curve from the modeled AIF. Currently only implemented for Horsfield 
% AIF, but could be extended on other AIF models. Based on the work of 
% (Georgiou et al., Magn Reson Med. 2018;(August):1-9).
%
%   [cardiacOutput, AUC1st] = AIFCARDIACOUTPUT(aifModel,dose)
%
% See also: AIFHORSFIELD
% Author: Beno�t Bourassa-Moreau
% Creation date: 2019-05-31

if isequal(aifModel.FHandle,@aifhorsfield)
    % (Georgiou et al., 2018) Eq. 2-3.
    % Modified for Hosfield AIF function with a normalized gamma-variate tail 
    horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
    if isfield(aifModel,'pValues')
        paramVector = aifModel.pValues(aifModel.minXIdx,:);
    elseif isfield(aifModel,'Params')
        paramVector = manageflexibleinput(aifModel.Params,horsfieldStructFieldOrder);
    end
    m = paramVector(5:7); % 1/min
    alpha = paramVector(8); % -
    beta = paramVector(9); % min
    tau = paramVector(10); % min
    A = paramVector(2:4)*paramVector(11)*(beta*(alpha+1)+tau); % mM min
    AUC1st = sum((A/beta^(alpha+1)).*((m+1/beta).^(-(alpha+1))));
else
    error('Area under the first pass is not implemented for the current AIF model.');
end
% dose in mmol
% AUC1st in min x mmol/L
cardiacOutput = (dose)/AUC1st; % L/min
end