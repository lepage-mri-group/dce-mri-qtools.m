function [aifFHandle,lb,ub,x0, fitOptions] = initfitaifrect(time,concAif, t0Idx,tPeakIdx)
% INITFITAIFRECT Prepare inital values and bounds for rectangular AIF model 
% fitting for testing purpose. 
%
%   [lb,ub,x0, fitOptions] = INITFITAIFRECT(time,concAif, t0Idx,tPeakIdx)
%
% See also: AIFRECT, FITAIF, INITFITRAND, INITFITAIFHORSFIELD, INITFITAIFBIEXP
% Author: Benoit Bourassa-Moreau
% Date: 2019-07-17
aifFHandle = @aifrect;
if tPeakIdx<t0Idx
    error('Concentration peaks before injection starts');
end
dt = time(2)-time(1);

% (1) Init time. ti, min
x0(1) = time(t0Idx);
lb(1) = min(x0(1)-5*dt,time(1));
ub(1) = x0(1)+5*dt;

% (2) Final time. tf, min
x0(2) = 2*time(tPeakIdx)-time(t0Idx); % 2 times the duration of start to peak
lb(2) = x0(1);
ub(2) = 6*time(tPeakIdx)-5*time(t0Idx); % 5 times the duration of start to peak

% (3) Amplitude. A, (mM min)
tfIdx = find(time>x0(2),1,'first');
x0(3) = mean(concAif(t0Idx:tfIdx)); % Average of the peak concentration
lb(3) = 0;
ub(3) = max(concAif);

fitOptions  = optimoptions('lsqcurvefit');
end