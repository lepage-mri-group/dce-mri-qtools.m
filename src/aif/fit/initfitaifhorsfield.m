function [aifFHandle,lb,ub,x0,fitOptions] = initfitaifhorsfield(time,concAif,t0Idx,tPeakIdx,initFitOptions)
% INITFITAIFHORSFIELD Prepare inital values and bounds for Horsfield AIF 
% model fitting. Multi-parametric fitting solutions are highly dependant of
% these initial values.
%
%   [aifFHandle,lb,ub,x0,fitOptions] = INITFITAIFHORSFIELD(time,concAif,t0Idx,tPeakIdx)
%
% See also: AIFHORSFIELD, FITAIF, INITFITRAND
% Author: Benoit Bourassa-Moreau
% Date: 2019-07-17

if ~exist('initFitOptions')
    initFitOptions = [];
end

aifFHandle = @aifhorsfield;
if tPeakIdx<t0Idx
    error('Concentration peaks before injection starts');
end
dt = time(2)-time(1);
nT = length(time);

fitOptions  = optimoptions('lsqcurvefit');
if isfield(initFitOptions,'display')
    fitOptions.Display = initFitOptions.display;
end

% (1) Init time. ti, min
lb(1) = min(time(t0Idx)-2*dt,time(1));
ub(1) = time(t0Idx)+10*dt;
defaultT0 = time(t0Idx);
T0Func = @(specified) defaultT0+specified;
t0 = setinitvalue(defaultT0,'t0',initFitOptions.init,T0Func,lb(1),ub(1));

% (8) alpha [-]
lb(8) = 1;
ub(8) = 50; 
defaultAlpha = 2.5; %[2.5, 7];
AlphaFunc = @(specified) specified;
alpha = setinitvalue(defaultAlpha,'alpha',initFitOptions.init,AlphaFunc,lb(8),ub(8));

% (9) beta [min]
lb(9) = 1/60;
ub(9) = 30/60;
defaultBeta = NaN;  % Computed below
BetaFunc = @(specified) specified;
beta = setinitvalue(defaultBeta,'beta',initFitOptions.init,BetaFunc,lb(9),ub(9));

% (11) k [-] - Useless - Fix this parameter during fitting
lb(11) = 0.999;
ub(11) = 1.001;
defaultK = 1; % min
KFunc = @(specified) specified;
k = setinitvalue(defaultK,'k',initFitOptions.init,KFunc,lb(11),ub(11));

% (10) tau [min]
lb(10) = 4.5/60;
ub(10) = 15/60;
defaultTau = 10.6/60; % min [5/60, 10.6/60]
TauFunc = @(specified) specified;
tau = setinitvalue(defaultTau,'tau',initFitOptions.init,TauFunc,lb(10),ub(10));


% (5:7) m decay constants of exponential, (1/min): From fast to slow
lb(5:7) = [0.5 0.13 0.005];
ub(5:7) = [30 5 0.13];
defaultM = {16.02, 1.17, 0.11};
MFunc = @(specified) specified;
m{1} = setinitvalue(defaultM{1},'m1',initFitOptions.init,MFunc,lb(5),ub(5));
m{2} = setinitvalue(defaultM{2},'m2',initFitOptions.init,MFunc,lb(6),ub(6));
m{3} = setinitvalue(defaultM{3},'m3',initFitOptions.init,MFunc,lb(7),ub(7));

% (2:4) Amplitude of exponentials A, (mM)
initNormRatio = [10.06,0.33,0.37];
initNormRatio = initNormRatio/sum(initNormRatio); % Normalized A - Georgiou
defaultA = {initNormRatio(1), initNormRatio(2), initNormRatio(3)};
AFunc = @(specified) specified;
initA{1} = setinitvalue(defaultA{1},'A1',initFitOptions.init,AFunc,0,inf);
initA{2} = setinitvalue(defaultA{2},'A2',initFitOptions.init,AFunc,0,inf);
initA{3} = setinitvalue(defaultA{3},'A3',initFitOptions.init,AFunc,0,inf);

%% Assemble init vector
x0 = multix0fromvalues(t0,initA{1},initA{2},initA{3},m{1},m{2},m{3},alpha,beta,tau,k);

%% Compute 
% Beta from alpha
x0(:,9) = (time(tPeakIdx)-time(t0Idx))./x0(:,8); % From Madsen et al. 1992 tPeak = alpha*beta + t0;
x0(x0(:,9)<lb(9),9) = lb(9);
x0(x0(:,9)>ub(9),9) = ub(9);

% A from peak (alpha, beta, tau, k)
peakGammaVariate = gammapeaknodecay(x0(:,8),x0(:,9),x0(:,10),x0(:,11));
expDecayAtPeak = sum(x0(:,2:4).*exp(-x0(:,5:7)*(time(tPeakIdx)-time(t0Idx))),2);
scalingOfA = concAif(tPeakIdx)./peakGammaVariate./expDecayAtPeak;
x0(:,2:4) = repmat(scalingOfA,1,3).*x0(:,2:4); % Use peak AIF to normalize starting values
lb(2:4) = min(scalingOfA)/2*[0.5 0.01 0.01];
ub(2:4) = 2*max(scalingOfA)*[3 0.5 0.5];

% Last exp (A(3); m(3)) from last data points
% Slow decay approximation
% Check if at least 5 data points are available for the last 45 second of the 
% end of time sampling and 3.5 min post injection.
tIdx45sEnd = find(time>time(end)-0.75,1,'first');
if ~isempty(tIdx45sEnd) && (time(tIdx45sEnd)-time(t0Idx))>3.5 && (nT-tIdx45sEnd) > 5
    % Fit exponential decay
    conc45s = mean(concAif(tIdx45sEnd:nT));
    expFunc = @(x,t) x(1)*exp(-x(2)*(t-time(t0Idx)));
    initExp = [max(conc45s,lb(4)) x0(7)];
    lbExp = [lb(4) lb(7)];
    ubExp = [ub(4) ub(7)];
    fitExp = lsqcurvefit(expFunc,initExp,time(tIdx45sEnd:nT),concAif(tIdx45sEnd:nT),lbExp,ubExp,fitOptions);
    x0(:,4) = fitExp(1);
    x0(:,7) = fitExp(2);
    debug = 0;
    if debug == 1
        figure;
        plot(time,concAif,time(tIdx45sEnd:nT),concAif(tIdx45sEnd:nT),time,expFunc(initExp,time),time,expFunc(fitExp,time));
        legend('Conc','final conc)','Initial guess','Fitted exp');
    end
end


end

function value = setinitvalue(defaultValue,fieldName,initStruct,valueFunc,lb,ub)

if isfield(initStruct,fieldName)
    value = valueFunc(initStruct.(fieldName));
    if any(value<lb)
        lb = min(value);
        warning('Specified initial value is lower than lower bound. Bound was extended to specified value.');
    end
    if any(value>ub)
        ub = max(value);
        warning('Specified initial value is lower than lower bound. Bound was extended to specified value.');
    end
else
    value = defaultValue;
end

end

function peakGammaVariate = gammapeaknodecay(alpha,beta,tau,k)

gamma1Peak = (alpha.*beta).^(alpha).*exp(-alpha)./...
    ((beta).^(alpha+1).*gamma(alpha+1));
peakGammaVariate = gamma1Peak.*k.*(beta.*(alpha+1)+tau);
end

function x0 = multix0fromvalues(varargin)
nP = nargin;
lengthOfP = NaN(nP,1);
for iP = 1:nP
    lengthOfP(iP) = length(varargin{iP});
end
nVar = prod(lengthOfP);

prevRepetitions = 1;
nextRepetitions = nVar;
x0 = NaN(nVar,nP);
for iP = 1:nP
    nextRepetitions = nextRepetitions/lengthOfP(iP);
    x0(:,iP) = repmat(repelem(reshape(varargin{iP},[lengthOfP(iP),1]),prevRepetitions,1),nextRepetitions,1);
    prevRepetitions = prevRepetitions*lengthOfP(iP);
end

end

function x0 = horsfieldsolutionexamples(t0)
nSolutions = 10;
nParams = 11;
x0 = NaN(nSolutions,nParams);
x0(:,1) = t0;
% Georgiou
x0(1,2:4) = [10.06,0.33,0.37]; % A [mM]
x0(1,5:7) =  [16.02,1.17, 0.11];% m [1/min]
x0(1,8) = 5.26; % alpha [-]
x0(1,9) = 0.032; % beta [min]
x0(1,10) = 0.129; % tau [min]
x0(1,11) = 98.7186*(beta*(alpha+1)+tau); % k unknown arbitrary scaling?
% Horsfield shared params
x0(2:9,2:4) = [NaN, 3.99, 4.78]; % A [kg/L]
x0(2:9,5:7) = [NaN, 0.0024, 0.000185]*60; % m [1/min]
x0(2:9,10) = 10.6/60; % min
% Horsfield - Patient 1 - Aorta
iHors = 2;
x0(iHors,2) = 29.7; % A1 (kg/L)
x0(iHors,5) = 0.063*60; % m1 (1/min)
x0(iHors,8) = 3.75; % alpha (-)
x0(iHors,9) = 2.15/60; % beta (min)
x0(iHors,11) = 0.449*20; % scaling
% Horsfield - Patient 2 - Aorta
iHors = 3;
x0(iHors,2) = 33.1; % A1 (kg/L)
x0(iHors,5) = 0.047*60; % m1 (1/min)
x0(iHors,8) = 2.50; % alpha (-)
x0(iHors,9) = 3.73/60; % beta (min)
x0(iHors,11) = 0.445*20; % scaling
% Horsfield - Patient 3 - Aorta
iHors = 4;
x0(iHors,2) = 42.8; % A1 (kg/L)
x0(iHors,5) = 0.053*60; % m1 (1/min)
x0(iHors,8) = 3.05; % alpha (-)
x0(iHors,9) = 3.61/60; % beta (min)
x0(iHors,11) = 0.340*45; % scaling
% Horsfield - Patient 4 - Aorta
iHors = 5;
x0(iHors,2) = 31.1; % A1 (kg/L)
x0(iHors,5) = 0.115*60; % m1 (1/min)
x0(iHors,8) = 3.17; % alpha (-)
x0(iHors,9) = 2.19/60; % beta (min)
x0(iHors,11) = 0.414*23; % scaling
% Horsfield - Patient 1 - Portal vein
iHors = 6;
x0(iHors,2) = 14.4; % A1 (kg/L)
x0(iHors,5) = 0.019*60; % m1 (1/min)
x0(iHors,8) = 1.2; % alpha (-)
x0(iHors,9) = 13.3/60; % beta (min)
x0(iHors,11) = 0.442*20; % scaling
% Horsfield - Patient 2 - Portal vein
iHors = 7;
x0(iHors,2) = 27.3; % A1 (kg/L)
x0(iHors,5) = 0.012*60; % m1 (1/min)
x0(iHors,8) = 3.08; % alpha (-)
x0(iHors,9) = 9.85/60; % beta (min)
x0(iHors,11) = 0.780*20; % scaling
% Horsfield - Patient 3 - Portal vein
iHors = 8;
x0(iHors,2) = 17.9; % A1 (kg/L)
x0(iHors,5) = 0.015*60; % m1 (1/min)
x0(iHors,8) = 1.16; % alpha (-)
x0(iHors,9) = 20.5/60; % beta (min)
x0(iHors,11) = 0.741*45; % scaling
% Horsfield - Patient 4 - Portal vein
iHors = 9;
x0(iHors,2) = 14.5; % A1 (kg/L)
x0(iHors,5) = 0.031*60; % m1 (1/min)
x0(iHors,8) = 1.22; % alpha (-)
x0(iHors,9) = 7.50/60; % beta (min)
x0(iHors,11) = 0.466*23; % scaling
end
