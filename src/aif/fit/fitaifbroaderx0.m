function artStruct = fitaifbroaderx0(time,artStruct,x0,lb,ub,nRand,aifFHandle,fitOptions,fixedAmplitude)
% FITAIFBROADERX0 Broadens the typical initial values and bounds of the AIF
% model. Is usefull for subjects with uncommon AIF that fits poorly the 
% typical initial values. 
%
%   artStruct = FITAIFBROADERX0(time,artStruct,x0,lb,ub,nRand,aifFHandle,fitOptions,fixedAmplitude)
%
% See also INITFITRAND, FITAIF
% Author: Benoit Bourassa-Moreau
% Date: 2019-09-18

%% Compute a vector of random initial values
boundsRandX0Factor = 1.5;
ubRandRange = ub*boundsRandX0Factor; ubRandRange(1) = ub(1);
lbRandRange = lb/boundsRandX0Factor; lbRandRange(1) = lb(1);
[x0Rand] = initfitrand(lbRandRange,ubRandRange,x0,nRand);
x0Rand(:,~fixedAmplitude) = repmat(x0(~fixedAmplitude),[nRand,1]);
%% Broaden the bounds for weird AIFs
boundsLimitFactor = 10;
ubW = ub*boundsLimitFactor; ubW(1) = ub(1);
lbW = lb/boundsLimitFactor; lbW(1) = lb(1);

%% Fit AIF
artStruct.regFit = artStruct.fit;

artStruct.fit.param.FHandle = aifFHandle;
artStruct.fit.param.x0 = x0Rand;
artStruct.fit.param.lb = lbW;
artStruct.fit.param.ub = ubW;
artStruct.fit.param.fitOptions = fitOptions;
artStruct.fit.param.fixedAmplitude = fixedAmplitude;
[artStruct.fit.pValues, artStruct.fit.resnorm,artStruct.fit.minXIdx,artStruct.fit.conc] = ...
    fitaif(time,artStruct.cluster.mean,aifFHandle,x0Rand,lbW,ubW,fitOptions,fixedAmplitude);

%% Plot fit differences
if fitOptions.Display
    figH = figure; plot(time,aifFHandle(time,x0),'-r'); hold on;
    plot(time,artStruct.regFit.conc,'--b');
    plot(time,artStruct.fit.conc,'-k');
    plot(time,artStruct.cluster.mean,'o');
    legend('Typical starting values','Regular fit','Optimal fit','Data');
end
end