function [aifFHandle,lb,ub,x0, fitOptions] = initfitaifschabel(time,concAif, t0Idx,tPeakIdx)
% INITFITAIFBIEXP Prepare inital values and bounds for bi-exponential AIF model 
% fitting for testing purpose. 
%
%   [lb,ub,x0, fitOptions] = INITFITAIFBIEXP(time,concAif, t0Idx,tPeakIdx)
%
% See also: AIFSCHABEL, FITAIF, INITFITRAND, INITFITAIFHORSFIELD, INITFITAIFRECT
% Author: Benoit Bourassa-Moreau
% Date: 2019-07-17
aifFHandle = @aifschabel;
if tPeakIdx<t0Idx
    error('Concentration peaks before injection starts');
end
dt = time(2)-time(1);
% (1) Init time. t0, min
x0(1) = time(t0Idx);
lb(1) = min(x0(1)-2*dt,time(1));
ub(1) = x0(1)+2*dt;


% (6) Sigmoid and gamma variate 2 and 3 alpha
x0(6) = 7.9461;
lb(6) = 1;
ub(6) = 15; 

% (8) sigmoid tau - min
x0(8) = 0.14;
lb(8) = 1/60;
ub(8) = 30/60;

% (11) sigmoid T % min
x0(11) = 9.6319;
lb(11) = 1;
ub(11) = 30;

% (7) gamma variates alpha1. Alpha2 and 3 are all equal to sigmoid
x0(7) = 2.5393;
lb(7) = 1;
ub(7) = 10; 

% (9:10) gamma variate (tau1,tau2) - min
x0(9:10) = (time(tPeakIdx)-time(t0Idx))/(x0(7)-1); % From Madsen et al. 1992 tPeak = alpha*beta + t0;
lb(9:10) = 1/60;
ub(9:10) = 30/60;
if x0(9)<lb(9)
    x0(9:10)=lb(9);
elseif x0(9)>ub(9)
    x0(9:10)=ub(9);
end


% (3:5) Gamma variates amplitude - mM
initNormRatio = [0.7,5.8589, 0.9444, 0.4888]; % Schabel and Parker fig 7.
initNormRatio = initNormRatio/sum(initNormRatio);
peakGammaVariate = gammapeaknodecay(x0(7),x0(9),1);
scalingOfA = concAif(tPeakIdx)/peakGammaVariate;
x0(3:5) = scalingOfA*initNormRatio(2:4);
lb(3:5) = scalingOfA/2*[0.5 0.01 0.01];
ub(3:5) = 2*scalingOfA*[1 0.5 0.5];

% (2) Sigmoid amplitude - mM
x0(2) = scalingOfA*initNormRatio(1);
lb(2) = scalingOfA/2*0.04;
ub(2) = 2*scalingOfA*0.15;

% Slow decay approximation
% Check if at least 5 data points are available for the last 45 second of the 
% end of time sampling and 3.5 min post injection.
% nT = length(time);
% tIdx45sEnd = find(time>time(end)-0.75,1,'first');
% if ~isempty(tIdx45sEnd) && (time(tIdx45sEnd)-time(t0Idx))>3.5 && (nT-tIdx45sEnd) > 5
%     % Fit exponential decay
%     conc45s = mean(concAif(tIdx45sEnd:nT));
%     expFunc = @(x,t) x(1)*exp(-x(2)*(t-time(t0Idx)));
%     initExp = [max(conc45s,lb(4)) x0(7)];
%     lbExp = [lb(4) lb(7)];
%     ubExp = [ub(4) ub(7)];
%     fitExp = lsqcurvefit(expFunc,initExp,time(tIdx45sEnd:nT),concAif(tIdx45sEnd:nT),lbExp,ubExp);
%     x0(4) = fitExp(1);
%     x0(7) = fitExp(2);
%     figure; 
%     plot(time,concAif,time(tIdx45sEnd:nT),concAif(tIdx45sEnd:nT),time,expFunc(initExp,time),time,expFunc(fitExp,time));
%     legend('Conc','final conc)','Initial guess','Fitted exp');
% end
fitOptions  = optimoptions('lsqcurvefit');
end

function peakGammaVariate = gammapeaknodecay(alpha,tau,k)
alphaM1 = alpha-1;
peakGammaVariate = k*(exp(1)/(tau*alphaM1))^alphaM1*(tau*alphaM1)^alphaM1*exp(-alphaM1);
end