function [aifFHandle,lb,ub,x0, fitOptions] = initfitaifbiexp(time,concAif, t0Idx,tPeakIdx)
% INITFITAIFBIEXP Prepare inital values and bounds for bi-exponential AIF 
% model fitting for testing purpose. 
%   [lb,ub,x0, fitOptions] = INITFITAIFBIEXP(time,concAif, t0Idx,tPeakIdx)
%
% See also: AIFBIEXP, FITAIF
% Author: Benoit Bourassa-Moreau
% Date: 2019-07-17
aifFHandle = @aifbiexp;
if tPeakIdx<t0Idx
    error('Concentration peaks before injection starts');
end
dt = time(2)-time(1);

% (1) Init time. ti, min
x0(1) = time(t0Idx);
lb(1) = min(x0(1)-5*dt,time(1));
ub(1) = x0(1)+5*dt;

% (2) Final time. tf, min
x0(2) = time(tPeakIdx);
lb(2) = x0(1);
ub(2) = 3*time(tPeakIdx)-2*time(t0Idx); % 2 times the duration of start to peak

% (3) Amplitude. A, (mM min) - Extra parameter useless for fitting
% procedure. Fixed to unity.
x0(3) = 1; 
lb(3) = 0.999;
ub(3) = 1.001;

% (7) te, min - slow decay
x0(7) = (x0(2)-x0(1))/log(concAif(end)/concAif(tPeakIdx)); % two point (peak to end) exponential approximation
if isnan(x0(7)) || x0(7)<10 || x0(7)>60
    x0(7) = 25; % Fixed value
end
lb(7) = 5;
ub(7) = 60;

% (6) td, min - fast decay
x0(6) = x0(7)/100;
lb(6) = 5/60;
ub(6) = 120/60;

tfIdx = find(time>x0(2),1,'first');
averagePeakAmplitudeScaling = 3*mean(concAif(t0Idx:tfIdx)); % Average of the peak concentration
% (4) wd, min - fast decay amplitude
x0(4) = averagePeakAmplitudeScaling/x0(6);
lb(4) = x0(4)/20;
ub(4) = 5*x0(4);
% (5) we, min - slow decay amplitude
x0(5) = averagePeakAmplitudeScaling/x0(7);
lb(5) = 0;
ub(5) = ub(4)/2;

fitOptions  = optimoptions('lsqcurvefit');
end