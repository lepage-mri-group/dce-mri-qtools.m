function [x0Rand] = initfitrand(lb,ub,x0,nRand)
% INITFITRAND Generates random initial value from a distribution based on
% lb, ub and x0.
%
%   [x0Rand] = INITFITRAND(lb,ub,x0,nRand)
%
% See also: FITAIF, FITAIFBROADERX0, INITFITAIFHORSFIELD
% Author: Benoit Bourassa-Moreau
% Date: 2019-09-12

uDiff = ub - x0;
lDiff = x0 - lb;
randSd = (uDiff+lDiff)/2/2;
nP = length(x0);
x0Rand = zeros(nRand,nP);
for iP = 1:nP
    x0Rand(:,iP) = randn(nRand,1)*randSd(iP)+x0(iP);
    x0Rand(x0Rand(:,iP) < lb(iP),iP) = lb(iP);
    x0Rand(x0Rand(:,iP) > ub(iP),iP) = ub(iP);
end

end
