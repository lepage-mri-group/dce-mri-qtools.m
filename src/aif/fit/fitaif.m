function [x, resnorm,minXIdx,concFit] = fitaif(time,concAif,aifFHandle,x0,lb,ub,fitOptions,order,dataIncluded)
% FITAIF Fit the AIF to the model specified by aifFHandle. Use initfit*
% functions to set starting parameters and bounds.
%
%   [x, resnorm,minXIdx,concFit] = FITAIF(time,concAif,aifFHandle,x0,lb,ub,fitOptions,order)
%
% See also: INITFITAIFHORSFIELD, INITFITRAND
% Author: Benoit Bourassa-Moreau
% Date: 2019-07-17
if ~exist('dataIncluded','var') || isempty(dataIncluded)
    dataIncluded = true(size(time));
end
%% Fitting
fixedParams = x0(1,~order);
lsqFun = @(x,t)aifFHandle(t,x,fixedParams,order);

nInitValues = size(x0,1);
if isempty(lb)
    lbVec = lb;
elseif size(lb,1)==nInitValues
    lbVec = lb;
else
    lbVec = repmat(lb,[nInitValues,1]);
end
if isempty(ub)
    ubVec = ub;
elseif size(ub,1)==nInitValues
    ubVec = ub;
else
    ubVec = repmat(ub,[nInitValues,1]);
end
x = x0;
resnorm = NaN(nInitValues,1);
if nInitValues>10
    hWait = waitbar(0,'Multiple AIF fitting');
end
for iInitValues=1:nInitValues
    try
        [xVar,resnorm(iInitValues)] = lsqcurvefit(lsqFun,x0(iInitValues,order),time(dataIncluded),concAif(dataIncluded),lbVec(iInitValues,order),ubVec(iInitValues,order),fitOptions);
    catch ME
        warning('LSQCURVEFIT of AIF failed for initial parameters.')
        xVar = x0(iInitValues,order);
        resnorm(iInitValues) = NaN;
    end
    x(iInitValues,order) = xVar;
    if nInitValues>10
        waitbar(iInitValues/nInitValues,hWait);
    end
end
if nInitValues>10
    close(hWait);
end
[~,minXIdx] = min(resnorm);
if ~(isempty(lbVec) || isempty(ubVec)) && ...
        (any(xVar==lbVec(minXIdx,order)) || any(xVar==ubVec(minXIdx,order)))
    warning('At least one of the final AIF parameters reached a lower or upper bound.');
end
if nargout == 4
    concFit = aifFHandle(time,x(minXIdx,:));
end

end