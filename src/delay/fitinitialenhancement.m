function [timeIndex, range, enhancementError] = fitinitialenhancement(time,c,nPre,rangeAif,rangeVof,use2ndDeriv)
% FITINITIALENHANCEMENT Find starting index of contrast enfancement in 
% contrast curve. First it defines a range between initial data and peak 
% concentration, where the peak is maximum of concentration curve (and 
% maximum second derivative if use2ndDeriv is true). Partly based on the
% procedure described by (Larsson et al., 2008) :
% - Range from initial to max
% - Using maximum curvature
% - Replaced B-spline fitting with an exponential function.
%
% [timeIndex, range, enhancementError] = FITINITIALENHANCEMENT(time,c,nPre)
%
% [timeIndex, range, enhancementError] = FITINITIALENHANCEMENT(time,c,nPre,rangeAif,rangeVof)
%
% [timeIndex, range, enhancementError] = FITINITIALENHANCEMENT(time,c,nPre,rangeAif,rangeVof,use2ndDeriv)
%
% See also: GLOBALAIF,GLOBALVOF,INCLUSIONCRITERIACLUSTER
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-09-17


if ~exist('use2ndDeriv','var') || ~islogical(use2ndDeriv)
    use2ndDeriv = false;
end

% Define range of pre-enhancement and enhancement
range(1) = 1; % Pre-enhancement starts
range(2) = NaN; % Pre-enhancement stops
range(3) = NaN; % Enhancement fit starts
range(4) = NaN; % Enhancement fit stops
range(5) = NaN; % Enhancement peak

% Parameters
nbPtsBeforeThresh = 1;
nbPtsMin = 4; % 4 parameters define the EXP function

% 1. Find peak enhancement ------------------------------------
% Peak is found at area of high intensity AND maximum second derivative
% High intensity :
time = time(:);
c = c(:);
minCNoise = mean(c(1:nPre));
normConc = ((c-minCNoise)/(max(c)-minCNoise)); % 1 is highest, 0 is lowest
binomialFilter = [1 2 4 2 1]/10;
sizeFilt = 5; displace = (sizeFilt-1)/2;
normConcMovAvg = filter(binomialFilter,1,normConc);
normConcFltrd = [normConcMovAvg((1+displace):end); normConcMovAvg(end)*ones(displace,1)];

% Maximum second derivative :
% Filter data
if use2ndDeriv
    % AIF or VOF. Data is not noisy and second derivative can help to
    % identify peak
    % Use 2nd degree polynomial fitting over 5 points to find second derivative
    cppFit = zeros(size(normConcFltrd));
    for i = 3:(length(c)-2)
        p = polyfit([1; 2; 3; 4; 5],normConcFltrd(i-2:i+2),2);
        cppFit(i) = p(1);
    end
    secDeriv = -cppFit/abs(min(cppFit));
    weightFunc = normConc + secDeriv;
else
    % Tissue concentration. Data is too noisy to use second derivative
    weightFunc = normConcFltrd;
end
[~,iPeak] = max(weightFunc);

if iPeak < nPre
    % Major signal drift or weak enhancement
    % Enhancement will fail
    timeIndex = time(nPre);
    enhancementError = true;
else
    % 2. Find starting enhancement ------------------------------------
    prenEnhWeightFunc = normConc(1:nPre);
    weightThresh = mean(prenEnhWeightFunc)+2*std(prenEnhWeightFunc);
    iUnderThresh = find((normConc(1:iPeak))<weightThresh,1,'last');
    
    % 3. Define ranges -------------------------------------------------
    if isempty(iUnderThresh)
        range(2) = nPre;
        iUnderThresh = range(2) + nbPtsBeforeThresh;
    else
        range(2) = max(iUnderThresh-nbPtsBeforeThresh, nPre);
    end
    
    % 4. Correct ranges for noisy tissue enhancement -----------------
    % Use AIF and VOF ranges and enhancement to correct ranges
    if exist('rangeAif','var') && exist('rangeVof','var') && ~isempty(rangeAif) && ~isempty(rangeVof)
        % Correct pre enhancement
        if range(2)>rangeVof(2)
            range(2) = round(mean(rangeAif(2),rangeVof(2)));
        elseif range(2)< rangeAif(2)
            range(2) = rangeAif(2);
        end
        % Correct peak value
        if iPeak > rangeVof(5) % For high permeability slow enhancement tissues
            iPeak = rangeVof(5);
        elseif iPeak < rangeAif(3) % Peak enhancement detected before AIF enhancement starts
            % Noisy data. Peak idx set to AIF peak
            iPeak = rangeAif(5);
        end
    end
    range(3) = min(iUnderThresh, iPeak - nbPtsMin + 1);
    
    range(4) = range(3) + nbPtsMin - 1;
    range(5) = iPeak;
    
    % Average of pre-enhancement data
    % c = normConcFltrd;
    cAvgPre = mean(c(range(1):range(2)));
    iTs = (range(3):range(4))';
    
    % Quadratic fit of enhancement (does not fit well)
    % quadCoeff = polyfit(iTs,c(iTs),2);
    %
    % intersection = (- quadCoeff(2) + sqrt(quadCoeff(2)^2 ...
    %     - 4*quadCoeff(1)*(quadCoeff(3)-cAvgPre)))/...
    %     (2*quadCoeff(1));
    % minQuad = - quadCoeff(2) / (2*quadCoeff(1));
    % if isreal(intersection)
    %     timeIndex = intersection;
    % else
    %     timeIndex = minQuad;
    % end
    
    % Exponential fit of enhencement
    lsqOptions = optimset('lsqcurvefit');
    lsqOptions = optimset(lsqOptions,'TolFun',1e-8, 'TolX', 1e-8,'Display', 'Off');
    expFunc = @(k,x) k(1)*exp(k(2)*(x-(k(3)))) + k(4);
    startVal = [1 1 time(range(3)) cAvgPre];
    lb = [0 0 time(range(1)) -max(c)];
    ub = [1e3 1e3 time(range(4)) (cAvgPre-0.5*abs(cAvgPre))];
    nanPeakConc = isnan(c(iTs));
    if sum(nanPeakConc) > 2
        error('Too many NaN values in peak concentration for enhancement.');
    else
        iTs = iTs(~nanPeakConc);
    end
    if iTs(1)-1 > 0
        kFinal = lsqcurvefit(expFunc, startVal, time([iTs(1)-1; iTs]), [cAvgPre; c(iTs)], lb, ub, lsqOptions);
    else
        kFinal = lsqcurvefit(expFunc, startVal, [(2*time(iTs(1)) - time(iTs(2))) ; time(iTs)], [cAvgPre; c(iTs)], lb, ub, lsqOptions);
    end
    intersection = log((cAvgPre-kFinal(4))/kFinal(1))/kFinal(2) + kFinal(3);
    
    if intersection < time(range(2) - nbPtsBeforeThresh)  % Pre-enhancement stops
        %     warning('Enhancement intersection detected prior to pre-enhancement data.');
        timeIndex = time(range(2) - nbPtsBeforeThresh);
        enhancementError = true;
    elseif intersection > time(range(4))  % Permissive constrain. Enhancement fit stops
        %     warning('Enhancement intersection detected after enhancement data.');
        timeIndex = time(range(4));
        enhancementError = true;
    else
        timeIndex = intersection;
        enhancementError = false;
    end
    
    debug=0; % local debug
    if debug==1
        figure; plot(time,c,'.-');
        hold on;
        plot(time(iTs),c(iTs),'.');
        %     plot(range(2):0.1:range(4),polyval(quadCoeff,range(2):0.1:range(4)),'k--');
        moreTime = linspace(time(range(2)),time(range(4)),32);
        plot(moreTime,expFunc(kFinal,moreTime),'k--');
        plot([1 range(4)],[1 1]*cAvgPre);
        hold off;
    end
end

end
