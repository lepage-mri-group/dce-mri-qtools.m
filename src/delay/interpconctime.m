function [timeAll,cInterpArtList, cInterpVoxList, W] = interpconctime(time,concArt,concVox,shiftTissueCurve,shiftList)
% INTERPCONCTIME Interpolates the voxel and arterial time curves to
% - interpolate a time gap in the acquired data (split acquisition), and
% - shift delayed enhancement. 
% Arterial curve (concArt) can either be a vector or a fitted AIF structure.
%
% [timeAll,cInterpArtList, cInterpVoxList, W] = INTERPCONCTIME(time,concArt,concVox) 
%    Interpolates time gap in acquired data.
%
% [timeAll,cInterpArtList, cInterpVoxList, W] = INTERPCONCTIME(time,concArt,concVox,shiftTissueCurve,shiftList) 
%    Interpolates time gap in acquired data and corrects time curve delay.
%
% See also: FITINITIALENHANCEMENT, BATCHEONG
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-01-23

nP = size(concVox,1);
nT = length(time);

if ~exist('shiftList','var')
    shiftTissueCurve = false;
end
if ~exist('shiftList','var') || isempty(shiftList)
    noShift = true;
    shiftList = zeros(nP,1);
else
    noShift = false;
end

%% Is the dynamic scan time of data CONSTANT?
timeDiff = diff(time);
minTimeDiff = min(timeDiff);
% Time from dicom data has numerisation errors :
needsInterpolation = (timeDiff>(minTimeDiff*1.0001))|(timeDiff<(minTimeDiff*0.9999));
% Detect a single big time gap to correct linear interpolation
bigTimeGapIdx = find(timeDiff>(minTimeDiff*20));
if length(bigTimeGapIdx) == 1
    nDataInGap = (timeDiff(bigTimeGapIdx)/minTimeDiff);
    nDataInt = floor(nDataInGap);
    % Number of points to include in extrapolation. Min = Gap duration / 10. Max 10 pts.
    nAvgDur = min(round(nDataInGap/10),20);
    % Points used before the gap
    nSpanBefore = (bigTimeGapIdx-nAvgDur+1):bigTimeGapIdx;
    % Points used after the gap
    nSpanAfter = (bigTimeGapIdx+1):(bigTimeGapIdx+nAvgDur);
    timeInterp = [time(1:(bigTimeGapIdx)), ...
        time(bigTimeGapIdx)+20*eps, ...
        time(bigTimeGapIdx+1)-20*eps, ...
        time((bigTimeGapIdx+1):end)];
elseif length(bigTimeGapIdx)>1
    error('Temporal interpolation can''t correct for multiple big time gaps.')
end

%% 
if any(needsInterpolation)
    % New time vector
    timeAll = (time(1):minTimeDiff:time(end));
    nTimeAll = length(timeAll);
    
    cInterpArtList = NaN(nP,nTimeAll);
    cInterpVoxList = NaN(nP,nTimeAll);
    
    % W to reduce weighting of interpolated points
    % Will be corrected for big time gap
    relativeWeight = minTimeDiff./(timeDiff);
    W = eye(nTimeAll);
    for iX = 1:nTimeAll
        iT = find(time<timeAll(iX),1,'last');
        if isempty(iT)
            W(iX,iX) = relativeWeight(1);
        else
            W(iX,iX) = relativeWeight(iT);
        end
    end
    weightNorm = (2/(nDataInt+1))^2;
    for iGap = 1:nDataInt
        W(bigTimeGapIdx + iGap,bigTimeGapIdx + iGap) = weightNorm*(iGap - (nDataInt+1)/2)^2;
    end
    
    for iP = 1:nP
        % Interp tissue conc for time gap
        currentC = concVox(iP,:);
        if length(bigTimeGapIdx)==1
            pBefore = polyfit(time(nSpanBefore),currentC(nSpanBefore),1);
            pAfter = polyfit(time(nSpanAfter),currentC(nSpanAfter),1);
            cTInterp = [currentC(1:(bigTimeGapIdx)), ...
                polyval(pBefore,time(bigTimeGapIdx)+10*eps), ...
                polyval(pAfter,time(bigTimeGapIdx+1)-10*eps), ...
                currentC((bigTimeGapIdx+1):end)];
        else
            cTInterp = currentC;
        end
        if shiftTissueCurve
            currentInterpC = interp1(timeInterp,cTInterp,timeAll + shiftList(iP),'linear','extrap');
            cInterpArtList(iP,:) = concArt.param.FHandle(timeAll,concArt.pValues(concArt.minXIdx,:));
        else % Shift AIF curve
            currentInterpC = interp1(timeInterp,cTInterp,timeAll,'linear','extrap');
            cInterpArtList(iP,:) = concArt.param.FHandle(timeAll - shiftList(iP),concArt.pValues(concArt.minXIdx,:));
        end
        cInterpVoxList(iP,:) = currentInterpC;
    end
else
    if shiftTissueCurve
        cInterpArtList = repmat(concArt.param.FHandle(time,concArt.pValues(concArt.minXIdx,:)),[nP,1]);
        if noShift
            cInterpVoxList = concVox;
        else
            cInterpVoxList = NaN(nP,nT);
            for iP=1:nP
                cInterpVoxList(iP,:) = interp1(time,concVox(iP,:),time + shiftList(iP),'linear','extrap');
            end
        end
    else
        cInterpArtList = NaN(nP,nT);
        for iP=1:nP
            cInterpArtList(iP,:) = concArt.param.FHandle(time - shiftList(iP),concArt.pValues(concArt.minXIdx,:));
        end
        cInterpVoxList = concVox;
    end
    W = eye(nT);
    timeAll = time;
end
