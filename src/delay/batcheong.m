function [bat, output] = batcheong(conc,varargin)
% BATCHEONG Estimate bolus arrival time (BAT) from dynamic concentration list of
% voxels with Cheong's Linear-Quadratic piece-wise function either with the
% constrained (default) of unconstrained (faster, less robust) methods.
% This is based on (Cheong, et al. Phys. Med. Biol., 2003).
%
% [bat, output] = BATCHEONG(conc,time,nPre) Basic function call. nPre
% specifies the conservative estimate of the number of frames without 
% enhancement.
%
% [bat, output] = BATCHEONG(conc,time,nPre,timeResStep) Specify the 
% arrivial time step of the fit. If,
%   - timeResStep <= 0 -> Choose the unconstrained method with data time
%   resolution fixed as a step
%   - timeResStep > 0 && timeResStep < dataTimeRes -> Choose the 
%   constrained method with user specified step.
%   - timeResStep >= dataTimeRes -> arrival time step fixed as data time
%   resolution.
%
% [bat, output] = BATCHEONG(conc,time,nPre,timeResStep,outlierConc)
% Concentration outlier logical matrix is specified to exclude during BAT
% calculation. Same size as conc.
%
% See also: FITINITIALENHANCEMENT, INTERPCONCTIME
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-12-16
%% Check input
startIdx = 1;
[nP, nT] = size(conc);
time = varargin{1}; % [min]
dt = min(diff(time));
nPre = varargin{2};
if nargin >= 4
    if isempty(varargin{3}) || varargin{3} <= 0
        fitType = 'unconstrained';
    else
        fitType = 'constrained';
        if varargin{3}>=dt
            timeResStep = 0; % Time resolution is arrival time step (Default Cheong)
        else
            timeResStep = varargin{3}; % Specified time resolution
        end
    end
else
    timeResStep = 0;
    fitType = 'constrained';
end
if nargin >= 5
    outlierConc = varargin{4};
else
    outlierConc = false(nP,nT);
end
if nargin >= 6
    dtMinPastInj = varargin{5}; % [min]
else
    dtMinPastInj = 2; % default [min]
end
constrainedFit = false;
switch fitType
    case 'unconstrained'
        constrainedFit = false;
    case 'constrained'
        constrainedFit = true;
        X0 = [0 1 1];
        LB = [-1 0 0];
        UB = [1 inf inf];
        fitOptions  = optimoptions('lsqcurvefit');
        fitOptions.Display = 'off'; % 'notify' (default) | 'final' | 'iter' | 'off'
end

meanConc0 = mean(conc(:,startIdx:nPre),2);

%% Find peak time
n2MinPastInj = find(time>(time(nPre)+dtMinPastInj),1,'first');
[~,peakIdx] = max(conc(:,1:n2MinPastInj),[],2);

%% Find bolus arrival time
bat = NaN(nP,1);
sseOpt = NaN(1,nP);
betaOpt = NaN(3,nP);

for iP = 1:nP
    includedConc = ~outlierConc(iP,startIdx:peakIdx(iP));
    if sum(includedConc) >= 4
        cConc = conc(iP,startIdx:peakIdx(iP)) - meanConc0(iP); % Baseline conc shifted to 0
        
        if timeResStep == 0
            tk = time(startIdx:(peakIdx(iP)-1)); % Default Cheong method
        else
            tk = time(startIdx):timeResStep:time(peakIdx(iP)-1); % Allows smaller time steps
        end
        nK = length(tk);
        SSE = NaN(1,nK);
        betaLsq = NaN(3,nK);
        
        if constrainedFit
            for iK = 1:nK
                cTime = time(startIdx:peakIdx(iP)) - tk(iK);
                cTime(cTime<0) = 0;
                [betaLsq(:,iK),SSE(iK)] = lsqcurvefit(@linearquadratic,X0,cTime(includedConc),cConc(includedConc),LB,UB,fitOptions);
            end
        else
            % Unconstrained least squares
            % Constrains beta1 >= 0 and beta2 >= 0 should be added
            vectorConc = cConc';
            XInit = ones(nK+1,3);
            for iK = 1:nK
                X = XInit;
                X(:,2) = time(startIdx:peakIdx(iP))' - tk(iK);
                X(X(:,2)<0,2) = 0;
                X(:,3) = X(:,2).^2;
                
                betaLsq(:,iK) = (X'*vectorConc)'/(X'*X);
                diffC = vectorConc - X*betaLsq(:,iK);
                SSE(iK) = diffC'*diffC;
            end
        end
        [~,batIdx] = min(SSE);
        bat(iP) = tk(batIdx);
        sseOpt(iP) = SSE(batIdx);
        betaOpt(:,iP) = betaLsq(:,batIdx);
%     figure(1); 
%     fitConc = betaOpt(1,iP) + betaOpt(2,iP)*(tk-tk(batIdx)) + betaOpt(3,iP)*(tk-tk(batIdx)).^2;
%     fitConc(tk<=tk(batIdx)) = betaOpt(1,iP);
%     plot([tk, time(peakIdx(iP))],cConc,bat(iP)*[1 1+10*eps],[0 max(cConc)],tk,fitConc);
    end
end

output.peakIdx = peakIdx;
output.sseOpt = sseOpt;
output.betaOpt = betaOpt;
end

function conc = linearquadratic(betaVals,time)
conc = betaVals(1) + betaVals(2)*time + betaVals(3)*time.^2;
end
