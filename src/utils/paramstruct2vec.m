function parameterVector = paramstruct2vec(parameterStructure,structureList)
% PARAMSTRUCT2VEC Create parameter vector from structure
%
%   parameterVector = PARAMSTRUCT2VEC(parameterStructure,structureList) 
%   Creates a parameter vector from parameter structure with provided 
%   structure list.
%
% See also: PARAMVEC2STRUCT, COMPARESTRUCT
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-05

nParam = length(structureList);
parameterVector = NaN(1,nParam);
for iParam = 1:nParam
    openIdx = strfind(structureList{iParam},'(');
    closeIdx = strfind(structureList{iParam},')');
    if ~isempty(openIdx) && ~isempty(closeIdx) % specified index number
        fieldName = structureList{iParam}(1:(openIdx-1));
        currentFieldIndex = str2double(structureList{iParam}((openIdx+1):(closeIdx-1)));
    else
        fieldName = structureList{iParam};
        currentFieldIndex = 1;
    end
    if isfield(parameterStructure,fieldName)
        parameterVector(iParam) = parameterStructure.(fieldName)(currentFieldIndex);
    else
        warning('Unknown parameter structure field %s. NaN value was passed.',structureList{iParam});
    end
end