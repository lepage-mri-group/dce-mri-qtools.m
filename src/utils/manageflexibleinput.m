function paramVector = manageflexibleinput(unknownInputFormatCell,structFieldOrder)
% MANAGEFLEXIBLEINPUT Find input parameter format and return vector
%
%   paramVector = MANAGEFLEXIBLEINPUT(unknownInputFormatCell,structFieldOrder) 
%   Creates a parameter vector from unknown parameter format with provided 
%   structure list.
%
% See also: PARAMSTRUCT2VEC
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-05

nParam = length(structFieldOrder);

lengthFirstInput = length(unknownInputFormatCell{1});
if (numel(unknownInputFormatCell)==1) && lengthFirstInput == nParam % Input parameters in vector
    paramVector = unknownInputFormatCell{1};
elseif (numel(unknownInputFormatCell)>=3) && ...
        (length(unknownInputFormatCell{1}) + length(unknownInputFormatCell{2})) == nParam  && ...
        length(unknownInputFormatCell{3}) == nParam % Input variable and fixed parameters in vectors (fitting)
    paramVector = zeros(1,numel(unknownInputFormatCell{3}));
    paramVector(unknownInputFormatCell{3})=unknownInputFormatCell{1};
    paramVector(~unknownInputFormatCell{3})=unknownInputFormatCell{2};
elseif isstruct(unknownInputFormatCell{1}) % Input parameter in explicit structure
    paramVector = paramstruct2vec(unknownInputFormatCell{1},structFieldOrder);
else
    paramVector = [];
end