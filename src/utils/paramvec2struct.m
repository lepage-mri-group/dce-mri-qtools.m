function parameterStructure = paramvec2struct(parameterVector,structureList)
% PARAMVEC2STRUCT Create parameter structure from vector
%
%   parameterStructure = PARAMVEC2STRUCT(parameterVector,structureList) 
%   Creates a parameter structure from parameter vector with provided 
%   structure list.
%
% See also PARAMSTRUCT2VEC, COMPARESTRUCT
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-05

nParam = length(structureList);
for iParam = 1:nParam
    openIdx = strfind(structureList{iParam},'(');
    closeIdx = strfind(structureList{iParam},')');
    if ~isempty(openIdx) && ~isempty(closeIdx) % specified index number
        fieldName = structureList{iParam}(1:(openIdx-1));
        currentFieldIndex = str2double(structureList{iParam}((openIdx+1):(closeIdx-1)));
    else
        fieldName = structureList{iParam};
        currentFieldIndex = 1;
    end
    parameterStructure.(fieldName)(currentFieldIndex) = parameterVector(iParam);
end