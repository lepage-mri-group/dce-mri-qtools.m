function [b0Ang, refAng] = fitmasklinearangulation(maskToFit,maskAffineMatrix,refAffineMatrix,debugDisplay)
% FITMASKLINEARANGULATION Fit the angulation (in degree) of the cloud of
% voxels defined in the provided mask. The angulations is calculated
% relative to B0 field (3rd dimension) and against a reference affine matrix.
%   [b0Ang, refAng] = FITMASKLINEARANGULATION(maskToFit,maskAffineMatrix,refAffineMatrix,debugDisplay)
%
% See also MASKSLICEANG, SSSMASKSTATS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-02-24

%% Extract an arterial mask from tofImg
% Threshold over data
% Get mask pixel indexes
%% Find mask pixel coordinates and resample with affine orientation
voxel2Fit = pixelfrommask(maskToFit);
nPxl = size(voxel2Fit,1);

voxelAffine = NaN(nPxl,3);
for iPoint = 1:nPxl
    currentCoordinates = [voxel2Fit(iPoint,:)';1] - [1; 1; 1; 0];
    affineTransformCoordinates = maskAffineMatrix*currentCoordinates;
    voxelAffine(iPoint,1) = affineTransformCoordinates(1);
    voxelAffine(iPoint,2) = affineTransformCoordinates(2);
    voxelAffine(iPoint,3) = affineTransformCoordinates(3);
end

%% Fit the 3d line
[chosenEigVector, averagePoint] = fit3dline(voxelAffine);

%% Compare orientation with reference scan and B0 field
% Encoding axis orientations
mOriIdx = 2;
pOriIdx = 1;
sOriIdx = 3;
mOri = refAffineMatrix(1:3,mOriIdx); % RL
pOri = refAffineMatrix(1:3,pOriIdx); % AP
sOri = refAffineMatrix(1:3,sOriIdx); % FH
b0Ori = [0 0 1];

refAng.M = acos(dot(chosenEigVector,mOri)/norm(mOri)/norm(chosenEigVector))/pi*180;
if refAng.M >90
    refAng.M = 180-refAng.M;
end
refAng.P = acos(dot(chosenEigVector,pOri)/norm(pOri)/norm(chosenEigVector))/pi*180;
if refAng.P >90
    refAng.P = 180-refAng.P;
end
refAng.S = acos(dot(chosenEigVector,sOri)/norm(sOri)/norm(chosenEigVector))/pi*180;
if refAng.S >90
    refAng.S = 180-refAng.S;
end
b0Ang = acos(dot(chosenEigVector,b0Ori)/norm(chosenEigVector))/pi*180;
if b0Ang >90
    b0Ang = 180-b0Ang;
end

%% Plot fitted pixels and resulting line
if exist('debugDisplay','var')
    if isfield(debugDisplay,'axHandle')
        axes(debugDisplay.axHandle);
    else
        figTof = figure;
        set(figTof,'position',[191   319   878   633]);
    end
    voxel2Fit = pixelfrommask(debugDisplay.wholeMask);
    nPxl = size(voxel2Fit,1);
    
    voxelWholeAffine = NaN(nPxl,3);
    showThisPx = true(nPxl,1);
    for iPoint = 1:nPxl
        currentCoordinates = [voxel2Fit(iPoint,:)';1] - [1; 1; 1; 0];
        affineTransformCoordinates = maskAffineMatrix*currentCoordinates;
        voxelWholeAffine(iPoint,1) = affineTransformCoordinates(1);
        voxelWholeAffine(iPoint,2) = affineTransformCoordinates(2);
        voxelWholeAffine(iPoint,3) = affineTransformCoordinates(3);
        if any(voxelWholeAffine(iPoint,1) == voxelAffine(:,1)) && ...
                any(voxelWholeAffine(iPoint,2) == voxelAffine(:,2)) && ...
                any(voxelWholeAffine(iPoint,3) == voxelAffine(:,3))
            showThisPx(iPoint) = false;
        end
    end

    t = linspace(-40,40,256);
    
    axC = [0 0 0]; %'k'
    axBkgdC = 0.9*[1 1 1]; %'w'
    % Plot limits
    limRatio = 1.1;
    halfDiffX = (max(voxelWholeAffine(:,1)) - min(voxelWholeAffine(:,1)))/2;
    meanX = (max(voxelWholeAffine(:,1)) + min(voxelWholeAffine(:,1)))/2;
    xLimTof = meanX + limRatio*halfDiffX*[-1 1];
    halfDiffY = (max(voxelWholeAffine(:,2)) - min(voxelWholeAffine(:,2)))/2;
    meanY = (max(voxelWholeAffine(:,2)) + min(voxelWholeAffine(:,2)))/2;
    yLimTof = meanY + limRatio*halfDiffY*[-1 1];
    halfDiffZ = (max(voxelWholeAffine(:,3)) - min(voxelWholeAffine(:,3)))/2;
    meanZ = (max(voxelWholeAffine(:,3)) + min(voxelWholeAffine(:,3)))/2;
    zLimTof = meanZ + limRatio*halfDiffZ*[-1 1];
    
    scatter3(voxelWholeAffine(showThisPx,1),voxelWholeAffine(showThisPx,2),voxelWholeAffine(showThisPx,3),'.y');hold on;
    scatter3(voxelAffine(:,1),voxelAffine(:,2),voxelAffine(:,3),'.r');
    plot3(chosenEigVector(1)*t+averagePoint(1),chosenEigVector(2)*t+averagePoint(2),chosenEigVector(3)*t+averagePoint(3),'-','Linewidth',2,'Color',axC);
    axTof = gca;
    set(axTof,'color',axBkgdC);
    axTof.PlotBoxAspectRatio = [diff(xLimTof) diff(yLimTof) diff(zLimTof)];
    axTof.CameraPosition = 1.0e+03*[-0.7834   -1.5328    0.3588];
    axTof.Box = 'on';
    axTof.XColor = axC;
    axTof.YColor = axC;
    axTof.ZColor = axC;
    
    axTof.XTick = [];
    axTof.YTick = [];
    axTof.ZTick = [];
    % Plot a 50 mm scale bar
    plot3([-50 0] + xLimTof(2) - 2*(limRatio-1)*diff(xLimTof),[0 0]+ yLimTof(1) + (limRatio-1)*diff(yLimTof),[0 0]+ zLimTof(1) + (limRatio-1)*diff(zLimTof),'-','Linewidth',2,'Color',axC);
    
    xlim(xLimTof);
    ylim(yLimTof);
    zlim(zLimTof);
    hold off;
end
end

function [chosenEigVector, averagePoint] = fit3dline(maskPxls)

nPts = size(maskPxls,1);
averagePoint = mean(maskPxls,1);

% Correct the coordinates for the average values
subPointMinusAvg = NaN(nPts,3);
for iPoint=1:nPts
    subPointMinusAvg(iPoint,:) = maskPxls(iPoint,:) - averagePoint;
end
% Covariance matrix
covMatrix = NaN(3,3);
for iPoint=1:3
    for iPoint2=1:3
        covMatrix(iPoint,iPoint2) = mean(subPointMinusAvg(:,iPoint).*subPointMinusAvg(:,iPoint2));
    end
end
% Find eigenvector
[eigVector,eigValues] = eig(covMatrix);
% Find max eigen value
[~,maxEigValue] = max(diag(eigValues));
% Extract associated eigenvector
chosenEigVector = eigVector(:,maxEigValue);
averagePoint = averagePoint';
end