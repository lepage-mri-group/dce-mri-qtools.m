function [checkStruct, missingFieldList] = comparestruct(checkStruct,defaultStruct,verbrose,previousFields)
% COMPARESTRUCT Compares structure fields to default structure
%
% [checkStruct, missingFieldList] = COMPARESTRUCT(checkStruct,defaultStruct,verbrose,previousFields)
%
% See also: PARAMSTRUCT2VEC, PARAMVEC2STRUCT
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-28

defaultFieldNames = fieldnames(defaultStruct);
if ~exist('previousFields','var')
    previousFields = '';
end

nField = length(defaultFieldNames);
iMissedFields = 1;
missingFieldList = {};
for iField = 1:nField
    if isfield(checkStruct,defaultFieldNames{iField})
        if isstruct(defaultStruct.(defaultFieldNames{iField}))
            % Recursively check structures
            [checkStruct.(defaultFieldNames{iField}), subMissedFields] = ...
                comparestruct(checkStruct.(defaultFieldNames{iField}),defaultStruct.(defaultFieldNames{iField}),verbrose,[previousFields,'.',defaultFieldNames{iField}]);
            nMissedFields = length(subMissedFields);
            missingFieldList(1,iMissedFields+(0:(nMissedFields-1))) = subMissedFields;
            iMissedFields = iMissedFields + nMissedFields;
        end
    else
        missingFieldList{iMissedFields} = [previousFields,'.',defaultFieldNames{iField}];
        if verbrose
            warning('Field name ''%s'' is missing from structure. Default field value is assigned.',missingFieldList{iMissedFields});
        end
        iMissedFields = iMissedFields+1;
        checkStruct.(defaultFieldNames{iField}) = defaultStruct.(defaultFieldNames{iField});
    end
end
end