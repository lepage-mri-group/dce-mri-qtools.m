function fullDirPath = mksubdirifneeded(parentDir,subDir)
% MKSUBDIRIFNEEDED Create a sub directory if needed. Avoids the mkdir
% warning if directory already exists. Returns path to directory.
%
%   fullDirPath = MKSUBDIRIFNEEDED(parentDir,subDir) 
%
% See also MKDIR, FULLFILE
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-14

fullDirPath = fullfile(parentDir,subDir);
if exist(fullDirPath,'file')~=7 % avoids the mkdir warning
    mkdir(fullDirPath);
end
end