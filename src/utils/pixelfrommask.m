function pixelMat = pixelfrommask(mask)
% PIXELFROMMASK Returns individual indices (i,j,k) of pixels in a 3D mask 
% in a (nPx x 3) vector.
%
% pixelMat = PIXELFROMMASK(mask)
%
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-14

% Add Px
nZ = size(mask,3);
slicesPx = find(any(any(mask,1),2));
pixel = cell(length(slicesPx),1);
currentPx = 1;
for iZ=1:nZ
    if any(iZ==slicesPx)
        [row, col] = find(mask(:,:,iZ));
        pixel{currentPx} = [row, col, iZ*ones(length(row),1)];
        currentPx = currentPx + 1;
    end
end
pixelMat = cell2mat(pixel);