function [dynMat, nIdxT, varargout] = dyn3dconversion(dynImg,mask,pxList,varargin)
% DYN3DCONVERSION Convert an arbitrary dynamic image with time index either
% the first or last dimension to a matrix of size (nPxl x nT) with chosen
% points defined in an optional mask. The inverse operation is done with 
% MAT2DYN3D.
%
%   [dynMat,nIdxT] = DYN3DCONVERSION(dynImg) Convert dynamic image assuming last
%   dimension is time and all points are kept.
%   
%   [dynMat,nIdxT] = DYN3DCONVERSION(dynImg,mask) Convert dynamic image keeping
%   only the points specified in mask
%   
%   [dynMat,nIdxT,varargout] = DYN3DCONVERSION(dynImg,mask,[],varargin) Convert 
%   dynamic image keeping only the points specified in mask. Also transform
%   other static or dynamic images.
%
%   [dynMat,nIdxT,varargout] = DYN3DCONVERSION(dynImg,mask,pxList,varargin) Convert 
%   dynamic image keeping only the points specified in the specified pixel 
%   list associated with the mask. Also transform other static or dynamic 
%   images.
%
% See also: MAT2DYN3D
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

%% Find time dimension and check input dimensions
sizeOfDynImg = size(dynImg);
nUknDimDyn = length(sizeOfDynImg);
dimIsTime = true(1,nUknDimDyn);

sortByList = false;
if exist('mask','var') && ~isempty(mask)
    sizeOfMask = size(mask);
    [nDim, sizeOfMask] = correcteddim(sizeOfMask,nUknDimDyn);
    for iDim = 1:nDim
        iCDim = find((sizeOfMask(iDim)==sizeOfDynImg) & dimIsTime,1,'first');
        if iDim>1 && isempty(iCDim)
            error('Dynamic image and mask dimensions does not match.');
        else
            dimIsTime(iCDim) = false;
        end
    end
    dimIsTime = dimIsTime & sizeOfDynImg>1;
    if sum(dimIsTime)>1
        error('More than one dimension in dynamic image could be time. Validate mask and dynamic image size.');
    end
    if exist('pxList','var') && ~isempty(pxList)
        sortByList = true;
    end
else % No mask provided - Assumes last dimension is time and all data preserved
    sizeOfMask = sizeOfDynImg(1:end-1);
    nDim = nUknDimDyn - 1;
    if nDim == 1
        mask = true(sizeOfMask,1);
    else
        mask = true(sizeOfMask);
    end
    dimIsTime(1:end-1) = false;
end
% Check other input dimensions
if ~exist('varargin','var')
    varargin = {};
end
nOther = length(varargin);

%% Convert dynImg
nIdxT = find(dimIsTime);
nT = sizeOfDynImg(dimIsTime);
if sortByList
    nDimList = size(pxList,2);
    dynMat = resampledynimgbylist(dynImg,pxList,nIdxT,nUknDimDyn,nT);
else
    dynMat = resampledynimg(dynImg,mask,nIdxT,nUknDimDyn,nT);
end

%% Convert other matrix
varargout = cell(1,nOther);
for iOther = 1:nOther
    sizeOfOther = size(varargin{iOther});
    [nDimOther, sizeOfOther] = correcteddim(sizeOfOther,nUknDimDyn);
    if nDim == nDimOther && all(sizeOfMask==sizeOfOther)
        if sortByList
            varargout{iOther} = sortstaticpixellist(varargin{iOther},pxList,nDimList);
        else
            varargout{iOther} = reshape(varargin{iOther}(mask),[],1);
        end
    elseif nUknDimDyn == length(sizeOfOther) && all(sizeOfDynImg==sizeOfOther)
        if sortByList
            varargout{iOther} = resampledynimgbylist(varargin{iOther},pxList,nIdxT,nUknDimDyn,nT);
        else
            varargout{iOther} = resampledynimg(varargin{iOther},mask,nIdxT,nUknDimDyn,nT);
        end
    else
        error('Additional image and mask or dynamic dimensions does not match.');
    end
end
end

function dynMat = resampledynimg(dynImg,mask,nIdxT,nUknDim,nT)
nPx = sum(mask(:));
dynMat = NaN(nPx,nT);
if (nIdxT==nUknDim)
    if nUknDim==1 % Single time curve
        dynMat = dynImg';
    elseif nUknDim==2 % Single/multiple time curve
        dynMat = dynImg(mask,:);
    elseif nUknDim==3
        for iT=1:nT
            currentImg = dynImg(:,:,iT);
            dynMat(:,iT) = currentImg(mask);
        end
    elseif nUknDim==4
        for iT=1:nT
            currentImg = dynImg(:,:,:,iT);
            dynMat(:,iT) = currentImg(mask);
        end
    elseif nUknDim==5
        for iT=1:nT
            currentImg = dynImg(:,:,:,:,iT);
            dynMat(:,iT) = currentImg(mask);
        end
    end
elseif (nIdxT==1)
    if nUknDim==1 % Single time curve
        dynMat = dynImg';
    elseif nUknDim==2 % Single/multiple time curve
        dynMat = dynImg(:,mask)';
    elseif nUknDim==3
        for iT=1:nT
            currentImg = squeeze(dynImg(iT,:,:));
            dynMat(:,iT) = currentImg(mask);
        end
    elseif nUknDim==4
        for iT=1:nT
            currentImg = squeeze(dynImg(iT,:,:,:));
            dynMat(:,iT) = currentImg(mask);
        end
    elseif nUknDim==5
        for iT=1:nT
            currentImg = squeeze(dynImg(iT,:,:,:,:));
            dynMat(:,iT) = currentImg(mask);
        end
    end
else
    error('Time dimension must be the first or the last dimension of dynamic image.');
end
end

function dynMat = resampledynimgbylist(dynImg,pxList,nIdxT,nUknDim,nT)
nPx = size(pxList,1);
dynMat = NaN(nPx,nT);
if (nIdxT==nUknDim)
    if nIdxT==1 % Single time curve
        dynMat = dynImg';
    elseif nIdxT==2 % Single/multiple time curve
        dynMat = dynImg(pxList(~isnan(pxList)),:);
    elseif nIdxT==3
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(pxList(iPx,1),pxList(iPx,2),:),[1 nT]);
            end
        end
    elseif nIdxT==4
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(pxList(iPx,1),pxList(iPx,2),pxList(iPx,3),:),[1 nT]);
            end
        end
    elseif nIdxT==5
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(pxList(iPx,1),pxList(iPx,2),pxList(iPx,3),pxList(:,4),:),[1 nT]);
            end
        end
    end
elseif (nIdxT==1)
    if nIdxT==1 % Single time curve
        dynMat = dynImg';
    elseif nIdxT==2 % Single/multiple time curve
        dynMat = dynImg(:,pxList(~isnan(pxList)));
    elseif nIdxT==3
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(:,pxList(iPx,1),pxList(iPx,2)),[1 nT]);
            end
        end
    elseif nIdxT==4
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(:,pxList(iPx,1),pxList(iPx,2),pxList(iPx,3)),[1 nT]);
            end
        end
    elseif nIdxT==5
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx,:) = NaN(1,nT);
            else
                dynMat(iPx,:) = reshape(dynImg(:,pxList(iPx,1),pxList(iPx,2),pxList(iPx,3),pxList(iPx,4)),[1 nT]);
            end
        end
    end
else
    error('Time dimension must be the first or the last dimension of dynamic image.');
end
end

function dynMat = sortstaticpixellist(dynImg,pxList,nDimList)
nPx = size(pxList,1);
dynMat = NaN(nPx,1);
switch nDimList
    case 1
        dynMat(~isnan(pxList(:,1))) = dynImg(pxList(~isnan(pxList(:,1)),1));
    case 2
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx) = NaN;
            else
                dynMat(iPx) = dynImg(pxList(iPx,1),pxList(iPx,2));
            end
        end
    case 3
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx) = NaN;
            else
                dynMat(iPx) = dynImg(pxList(iPx,1),pxList(iPx,2),pxList(iPx,3));
            end
        end
    case 4
        for iPx=1:nPx
            if any(isnan(pxList(iPx,:)))
                dynMat(iPx) = NaN;
            else
                dynMat(iPx) = dynImg(pxList(iPx,1),pxList(iPx,2),pxList(iPx,3),pxList(iPx,4));
            end
        end
end
end

function [nDim, sizeOfMask] = correcteddim(sizeOfMask,nUknDimDyn)
lastOver1 = find(sizeOfMask>1,1,'last'); %     nDim = length(sizeOfMask)-sum(sizeOfMask==1);
if isempty(lastOver1)
    nDim = 1;
elseif (lastOver1<(nUknDimDyn-1))
    nDim = (nUknDimDyn-1);
    if length(sizeOfMask) < nDim
        tempSize = sizeOfMask;
        nRedDim = length(sizeOfMask);
        sizeOfMask = ones(1,nDim);
        sizeOfMask(1:nRedDim) = tempSize;
    end
else
    nDim = lastOver1;
end
end