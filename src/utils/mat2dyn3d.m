function [dynImg, varargout] = mat2dyn3d(dynMat,nIdxT,mask,varargin)
% MAT2DYN3D Convert a dynamic matrix of size (nPxl x nT) to a dynamic image with 
% specified time index either at the first or last dimension to with chosen
% points defined in an optional mask. The inverse operation is done with 
% DYN3DCONVERSION.
%
%   [dynMat] = MAT2DYN3D(dynImg) Convert dynamic matrix assuming last
%   dimension is time and all points are kept.
%   
%   [dynMat] = MAT2DYN3D(dynImg,mask) Convert dynamic matrix keeping
%   only the points specified in mask
%   
%   [dynMat,varargout] = MAT2DYN3D(dynImg,mask,varargin) Convert 
%   dynamic matrix keeping only the points specified in mask. Also transform
%   other static or dynamic images.
%
% See also: DYN3DCONVERSION
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-04

%% Check input dimensions
[nPx, nT] = size(dynMat);

sizeOfMask = size(mask);
[nDim, sizeOfMask] = correcteddim(sizeOfMask,nIdxT);
maskIdx = find(mask);
nUknDimDyn = (nDim+1);
%% Convert dynImg
dynImg = resampledynmat(dynMat, maskIdx, nIdxT, nUknDimDyn, sizeOfMask, nT);
%% Convert other matrix
nOther = length(varargin);
varargout = cell(1,nOther);
for iOther = 1:nOther
    [nPxOther,nTOther] = size(varargin{iOther});
    if nPxOther == nPx && nTOther == 1
        varargout{iOther} = zeros(sizeOfMask);
        varargout{iOther}(maskIdx) = varargin{iOther};
    elseif nPxOther == nPx && nTOther == nT
        varargout{iOther} = resampledynmat(varargin{iOther}, maskIdx, nIdxT, nUknDimDyn, sizeOfMask, nT);
    else
        error('Additional image and mask or dynamic dimensions does not match.');
    end
end

end

function dynImg = resampledynmat(dynMat, maskIdx, nIdxT, nUknDim, nPerDim, nT)
if prod(nPerDim)==1 % Single point case
    nUknDim = nIdxT;
    nPerDim = ones(1,nIdxT);
end

if (nIdxT==nUknDim)
    if nUknDim==1 % Single time curve
        dynImg = dynMat';
    elseif nUknDim==2 % Single/multiple time curve
        dynImg = zeros(nPerDim(1),nT);
        dynImg(maskIdx,:) = dynMat;
    elseif nUknDim==3
        dynImg = zeros(nPerDim(1),nPerDim(2),nT);
        currentImage = zeros(nPerDim(1),nPerDim(2));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(:,:,iT) = currentImage;
        end
    elseif nUknDim==4
        dynImg = zeros(nPerDim(1),nPerDim(2),nPerDim(3),nT);
        currentImage = zeros(nPerDim(1),nPerDim(2),nPerDim(3));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(:,:,:,iT) = currentImage;
        end
    elseif nUknDim==5
        dynImg = zeros(nPerDim(1),nPerDim(2),nPerDim(3),nPerDim(4),nT);
        currentImage = zeros(nPerDim(1),nPerDim(2),nPerDim(3),nPerDim(4));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(:,:,:,:,iT) = currentImage;
        end
    end
elseif (nIdxT==1)
    if nUknDim==1 % Single time curve
        dynImg = dynMat';
    elseif nUknDim==2 % Single/multiple time curve
        dynImg = zeros(nT,max(nPerDim));
        dynImg(:,maskIdx) = dynMat';
    elseif nUknDim==3
        dynImg = zeros(nT,nPerDim(1),nPerDim(2));
        currentImage = zeros(nPerDim(1),nPerDim(2));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(iT,:,:) = currentImage;
        end
    elseif nUknDim==4
        dynImg = zeros(nT,nPerDim(1),nPerDim(2),nPerDim(3));
        currentImage = zeros(nPerDim(1),nPerDim(2),nPerDim(3));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(iT,:,:,:) = currentImage;
        end
    elseif nUknDim==5
        dynImg = zeros(nT,nPerDim(1),nPerDim(2),nPerDim(3),nPerDim(4));
        currentImage = zeros(nPerDim(1),nPerDim(2),nPerDim(3),nPerDim(4));
        for iT=1:nT
            currentImage(maskIdx) = dynMat(:,iT);
            dynImg(iT,:,:,:,:) = currentImage;
        end
    end
else
    error('Time dimension must be the first or the last dimension of dynamic image.');
end
end

function [nDim, sizeOfMask] = correcteddim(sizeOfMask,nUknDimDyn)
lastOver1 = find(sizeOfMask>1,1,'last'); %     nDim = length(sizeOfMask)-sum(sizeOfMask==1);
if isempty(lastOver1)
    nDim = 1;
elseif (lastOver1<(nUknDimDyn-1))
    nDim = (nUknDimDyn-1);
    if length(sizeOfMask) < nDim
        tempSize = sizeOfMask;
        nRedDim = length(sizeOfMask);
        sizeOfMask = ones(1,nDim);
        sizeOfMask(1:nRedDim) = tempSize;
    end
else
    nDim = lastOver1;
end
end