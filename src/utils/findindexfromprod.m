function individualIndex = findindexfromprod(productIndex,individualLength)
% FINDINDEXFROMPROD Find individual indices from an index that ranges from
% 1 to the product of each indices length
%
% individualIndex = FINDINDEXFROMPROD(productIndex,individualLength)
%
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-28

nIndividual = length(individualLength);
individualIndex = ones(1,nIndividual);
for iInd = 1:nIndividual
    if productIndex~=0
        individualIndex(iInd) = mod(productIndex-1,individualLength(iInd))+1;
        productIndex = (productIndex-individualIndex(iInd))/individualLength(iInd)+1;
    end
end

end