function vessel2B0Angulation = masksliceang(vesselMask,nSlicesFit,affineHr,debugDisplay)
% MASKSLICEANG Computes the B0 angulation of a vessel defined by its mask
% iteratively for all slices.
%
%   vessel2B0Angulation = MASKSLICEANG(vesselMask,nSlicesFit,affineHr)
%
% See also: FITMASKLINEARANGULATION,SSSMASKSTATS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-02-24

if exist('debugDisplay','var')
    displaySlice = debugDisplay.detailedSlice;
    debugDisplay.wholeMask = vesselMask;
else
    displaySlice = NaN;
end

slicesWithMask = find(squeeze(any(any(vesselMask,1),2)));
nMaskSlices = length(slicesWithMask);
startSlice = min(slicesWithMask);
stopSlice = max(slicesWithMask);

[nX,nY,nZ] = size(vesselMask);
vessel2B0Angulation = 1.0*vesselMask;
sliceAngulation = NaN(nZ,1);
for iSlices = 1:nMaskSlices
    subSssMask = false(nX,nY,nZ);
    if (slicesWithMask(iSlices) - ceil(nSlicesFit/2)) < startSlice
        subSssMask(:,:,startSlice:(startSlice+nSlicesFit)) = vesselMask(:,:,startSlice:(startSlice+nSlicesFit));
    elseif slicesWithMask(iSlices) + ceil(nSlicesFit/2) > stopSlice
        subSssMask(:,:,startSlice:(startSlice+nSlicesFit)) = vesselMask(:,:,(stopSlice-nSlicesFit):stopSlice);
    else
        subSssMask(:,:,startSlice:(startSlice+nSlicesFit)) = vesselMask(:,:,(slicesWithMask(iSlices) - ceil(nSlicesFit/2)):(slicesWithMask(iSlices) + ceil(nSlicesFit/2)));
    end
    if iSlices==displaySlice
        [b0Ang,~] = fitmasklinearangulation(subSssMask,affineHr,affineHr,debugDisplay);
    else
        [b0Ang,~] = fitmasklinearangulation(subSssMask,affineHr,affineHr);
    end
    sliceAngulation(slicesWithMask(iSlices)) = b0Ang;
    vessel2B0Angulation(:,:,slicesWithMask(iSlices)) = vesselMask(:,:,slicesWithMask(iSlices))*b0Ang;
end
end