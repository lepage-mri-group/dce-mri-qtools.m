function [chosenNeighbourg,maskIdx] = avgsurroundingtissue(iaucM,iaucP,maskBrain,maskAif,nRad)
% AVGSURROUNDINGTISSUES Select the average phase from 10% of the  nearest 
% neighbourgs of a vessel voxel within brain mask with smallest integrated 
% area under the phase curve (IAUC) within the 0-80% IAUC magnitude.
%
%   [maskNeigh,chosenNeighbourg,maskIdx] = AVGSURROUNDINGTISSUES(iaucM,iaucP,maskBrain,maskAif,nRad)
%
% See also: INCLUSIONCRITERIAAUC,FINDSURROUNDINGTISSUES
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-03-16

%% Define spherical indices
% nRad = 4;
x = -nRad:nRad;
y = -nRad:nRad;
z = -nRad:nRad;
[X, Y, Z] = ndgrid(x,y,z);
localSpereMask = X.^2+Y.^2+Z.^2<=nRad.^2;
nPxls = sum(localSpereMask(:));
nPrctiles = 10;
nAvg = floor(nPxls/nPrctiles);

% if ~exist('iaucThreshFraction','var') || isempty(iaucThreshFraction)
%     iaucThreshFraction = 0.2;% Default for AIF - closest voxels first maximum distance of 2 voxels
% end
% Find nearest neighbourgs
%% Find neighbourgh
[nX,nY,nZ] = size(iaucP);

maskIdx = pixelfrommask(maskAif);
nPts = size(maskIdx,1);

chosenNeighbourg = cell(nPts,1);
for iPts = 1:nPts
    cPtIdx = maskIdx(iPts,:);
    
    rangeX = max(cPtIdx(1)-nRad,1):min(cPtIdx(1)+nRad,nX);
    rangeY = max(cPtIdx(2)-nRad,1):min(cPtIdx(2)+nRad,nY);
    rangeZ = max(cPtIdx(3)-nRad,1):min(cPtIdx(3)+nRad,nZ);
    rangeSX = rangeX-cPtIdx(1)+(nRad+1);
    rangeSY = rangeY-cPtIdx(2)+(nRad+1);
    rangeSZ = rangeZ-cPtIdx(3)+(nRad+1);
    cSurrMask = false(nX,nY,nZ);
    cSurrMask(rangeX,rangeY,rangeZ) = localSpereMask(rangeSX,rangeSY,rangeSZ);
    cSurrMask(~maskBrain) = false;
    cSurrMask(maskAif) = false;
    
    nCSurr = sum(cSurrMask(:));
    nSurrCat = floor(nCSurr/nPrctiles);
    cSurrIauc = iaucM(cSurrMask);
    cSurrIaucP = iaucP(cSurrMask);
    localSpereIdx = pixelfrommask(cSurrMask);
    [sortedIauc, surrSortIdx] = sort(cSurrIauc);
    
    avgMagInCat = NaN(nPrctiles,1);
    avgPhaseInCat = NaN(nPrctiles,1);
    for iPrct = 1:nPrctiles
        cCatIdx = surrSortIdx(nSurrCat*(iPrct-1)+(1:nSurrCat));
        avgMagInCat(iPrct) = mean(cSurrIauc(cCatIdx));
        avgPhaseInCat(iPrct) = mean(cSurrIaucP(cCatIdx));
    end
    [~,minPhaseIdx] = min(avgPhaseInCat(1:(nPrctiles-2)));
    cCatIdx = surrSortIdx(nSurrCat*(minPhaseIdx-1)+(1:nSurrCat));
    chosenNeighbourg{iPts} = localSpereIdx(cCatIdx,:);
end

