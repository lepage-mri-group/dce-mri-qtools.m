function [px, mask, pxSquare] = selecthighintensityregionslices(img, maskHighlights, nSym, nTotPoints, manualRoiType, climPrc, inclusionMask)
% SELECTHIGHINTENSITYREGIONSLICES is a simple GUI to manually identify 
% vascular regions in image slices.
%   [px, mask, pxSquare] = SELECTHIGHINTENSITYREGIONSLICES(img, maskHighlights, nSym, nTotPoints, manualRoiType, climPrc, inclusionMask)
%
% See also: SEMIAUTOMASKSVENARTTISS,AUTOVESSELMASK,AIFCONCCOMPAREMASKS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-03-16

% Select Npoints of maximum intensity in a manually drawn ROI in the chosen
% slice dans subsequent slices.

[nX,nY,nZ] = size(img);
if ~exist('inclusionMask','var') || isempty(inclusionMask)
    inclusionMask = ~isnan(img);
else
    inclusionMask = inclusionMask & ~isnan(img);
end
if ~exist('climPrc','var') ||(isempty(climPrc))
    climImg = [prctile(img(inclusionMask),1) prctile(img(inclusionMask),99)];
else
    climImg = [prctile(img(inclusionMask),climPrc(1)) prctile(img(inclusionMask),climPrc(2))];
end
if ~exist('maskHighlights','var')
    chosenColormap = 'parula';
    maskHighlights = [];
    nHighlights = 0;
else
    chosenColormap = 'parula';
    nHighlights = length(maskHighlights);
    legendHigh = cell(1,nHighlights);
    for iHighlights = 1:nHighlights
        legendHigh{iHighlights} = maskHighlights(iHighlights).name;
        if ~isfield(maskHighlights(iHighlights),'coordinates') || isempty(maskHighlights(iHighlights).coordinates)
            for iSlice = 1:nZ
                [yHigh, xHigh] = find(maskHighlights(iHighlights).mask(:,:,iSlice));
                nPoints = length(yHigh);
                maskHighlights(iHighlights).coordinates{iSlice} = NaN(nPoints,2);
                maskHighlights(iHighlights).coordinates{iSlice}(:,1) = xHigh;
                maskHighlights(iHighlights).coordinates{iSlice}(:,2) = yHigh;
            end
        end
    end
end

highIntensityOnly = false;
if ~isempty(nTotPoints)
    highIntensityOnly = true;
    nPtsPerSym = round(nTotPoints/nSym);
    nTotReal = nPtsPerSym*nSym;
    if nTotReal~=nTotPoints
        warning('Total number of pixel was increased to be a multiple of number of slice and symmetry.')
    end
    pointIndex0 = 1;
    px = NaN(nTotReal,3);
end


matlabSelect = true;
if strncmp(manualRoiType,'squares',7)
    matlabSelect = false;
    maxNumberOfSquares = 256;
    squareValid = cell(nZ,1);
    for iSlice=1:nZ
        squareValid{iSlice}= true(maxNumberOfSquares,1);
    end
    squarePlotHandle = cell(maxNumberOfSquares,1);
    nSquares = zeros(nZ,1);
    nSquaresRemoved = 0;
    sizeSquare = 9;
    squareIdx = (1:sizeSquare)-ceil(sizeSquare/2);
end

fRoiSel = figure(97);
set(fRoiSel,'Position',[-1021          68         927         875]);

iSlice = round(nZ/2);
ax1 = updatesliceimage(img,climImg,iSlice,'',nHighlights,maskHighlights,legendHigh,chosenColormap);
% imagesc(img(:,:,iSlice),climImg);
% if nHighlights>0
%     hold on;
%     pHigh = NaN(1,nHighlights);
%     for iHighlights = 1:nHighlights
%         [yHigh, xHigh] = find(maskHighlights(iHighlights).mask(:,:,iSlice));
%         pHigh(iHighlights) = plot(xHigh, yHigh,'.r');
%     end
%     legend(pHigh,legendHigh);
%     hold off;
% end
% ax1 = gca;
% axis off;
% title(['Slice #' num2str(iSlice)]);
currentMask3d = false(nX,nY,nZ,nSym);
for iSym = 1:nSym
    fprintf('Symetry = %d/%d --- Use keyboard arrow to find tissue slice\n',iSym,nSym);
    
    allPos = cell(nZ,1);
    button = 1;
    while (button ~= 3) % Right-click: exits loop
        [xx, yy, button] = ginput(1);
        if ~isempty(button) && ((button == 29) || (button == 28))
            if button == 29
                iSlice = min(iSlice + 1,nZ);
            else
                iSlice = max(iSlice - 1,1);
            end
            switch manualRoiType
                case {'squares','squaresInterp'}
                    otherInfo = [' - ' num2str(sum(nSquares)-nSquaresRemoved) ' squares over image'];
                otherwise
                    otherInfo = '';
            end
            ax1 = updatesliceimage(img,climImg,iSlice,otherInfo,nHighlights,maskHighlights,legendHigh,chosenColormap);
            if ~isempty(allPos{iSlice})
                switch manualRoiType
                    case 'rect'
                        h = imrect(ax1,allPos{iSlice});
                    case 'ellipse'
                        h = imellipse(ax1,allPos{iSlice});
                    case 'poly'
                        h = impoly(ax1,allPos{iSlice});
                    case {'squares','squaresInterp'}
                        squarePlotHandle = cell(maxNumberOfSquares,1);
                        for iSquare = 1:nSquares(iSlice)
                            if squareValid{iSlice}(iSquare)
                                squarePlotHandle{iSquare} = plotsquare(ax1,allPos{iSlice}{iSquare},sizeSquare);
                            end
                        end
                        
                end
            elseif strcmp(manualRoiType,'squaresInterp')
                manualSquaresExists = false;
                squarePlotHandle = cell(maxNumberOfSquares,1);
                for iSquare = 1:nSquares(iSlice)
                    if squareValid{iSlice}(iSquare)
                        manualSquaresExists = true;
                        squarePlotHandle{iSquare} = plotsquare(ax1,allPos{iSlice}{iSquare},sizeSquare);
                    end
                end
                if ~manualSquaresExists
                    startSquareSliceIdx = find(nSquares(1:(iSlice-1))>0,1,'last');
                    stopSquareSliceIdx = find(nSquares((iSlice+1):end)>0,1,'first');
                    if (~isempty(startSquareSliceIdx) && ~isempty(stopSquareSliceIdx)) && ...
                            sum(squareValid{startSquareSliceIdx}(1:nSquares(startSquareSliceIdx))) == 1 && sum(squareValid{stopSquareSliceIdx +iSlice}(1:nSquares(stopSquareSliceIdx +iSlice))) == 1
                        stopSquareSliceIdx = stopSquareSliceIdx +iSlice;
                        interpSquarePos = NaN(1,2);
                        startSqareExistsIdx = find(squareValid{startSquareSliceIdx}(1:nSquares(startSquareSliceIdx)));
                        stopSqareExistsIdx = find(squareValid{stopSquareSliceIdx}(1:nSquares(stopSquareSliceIdx)));
                        interpSquarePos(1) = (allPos{stopSquareSliceIdx}{stopSqareExistsIdx}(1)-allPos{startSquareSliceIdx}{startSqareExistsIdx}(1))/(stopSquareSliceIdx-startSquareSliceIdx)*(iSlice-startSquareSliceIdx)+allPos{startSquareSliceIdx}{startSqareExistsIdx}(1);
                        interpSquarePos(2) = (allPos{stopSquareSliceIdx}{stopSqareExistsIdx}(2)-allPos{startSquareSliceIdx}{startSqareExistsIdx}(2))/(stopSquareSliceIdx-startSquareSliceIdx)*(iSlice-startSquareSliceIdx)+allPos{startSquareSliceIdx}{startSqareExistsIdx}(2);
                        interpSquarePlotHandle = plotsquare(ax1,interpSquarePos,sizeSquare,'g');
                    end
                end
            end
        elseif ~isempty(button) && button == 1 % left click : draw ROI
            fprintf('     Draw region of interest for current slice = %d/%d\n',iSlice,nZ);
            switch manualRoiType
                case 'rect'
                    if isempty(allPos{iSlice})
                        h = imrect(ax1);
                    else
                        h = imrect(ax1,allPos{iSlice});
                    end
                case 'ellipse'
                    if isempty(allPos{iSlice})
                        h = imellipse(ax1);
                    else
                        h = imellipse(ax1,allPos{iSlice});
                    end
                case 'poly'
                    if isempty(allPos{iSlice})
                    h = impoly(ax1);
                    else
                    h = impoly(ax1,allPos{iSlice});
                    end
                case {'squares','squaresInterp'}
                    currentSquareCentre = [round(xx,0),round(yy,0)];
                    if currentSquareCentre(1)-(sizeSquare-1)/2 <= 0 || ...
                            currentSquareCentre(2)-(sizeSquare-1)/2 <= 0 || ...
                            currentSquareCentre(1)+(sizeSquare-1)/2 > nY || ...
                            currentSquareCentre(2)+(sizeSquare-1)/2 > nX
                        warning('Squared region out of bounds.');
                    else
                        squareRemoved = false;
                        for iSquare = 1:nSquares(iSlice)
                            if squareValid{iSlice}(iSquare)
                                if sum((currentSquareCentre - allPos{iSlice}{iSquare}).^2) < 4 % remove square at proximity
                                    squareRemoved = true;
                                    nSquaresRemoved = nSquaresRemoved+1;
                                    squareValid{iSlice}(iSquare) = false;
                                    squarePlotHandle{iSquare}(1).Visible = 'off';
                                    squarePlotHandle{iSquare}(2).Visible = 'off';
                                    squarePlotHandle{iSquare}(3).Visible = 'off';
                                    squarePlotHandle{iSquare}(4).Visible = 'off';
                                    squarePlotHandle{iSquare}(5).Visible = 'off';
                                end
                            end
                        end
                        
                        if ~squareRemoved % add square
                            nSquares(iSlice) = nSquares(iSlice)+1;
                            squarePlotHandle{nSquares(iSlice)} = plotsquare(ax1,currentSquareCentre,sizeSquare);
                            allPos{iSlice}{nSquares(iSlice)} = currentSquareCentre;
                        end
                    end
            end
            if matlabSelect
                wait(h);
                BW = createMask(h);
                allPos{iSlice} = getPosition(h);
            else
                BW = false(nX,nY);
                for iSquare = 1:nSquares(iSlice)
                    if squareValid{iSlice}(iSquare)
                        BW(allPos{iSlice}{iSquare}(2)+squareIdx, ...
                            allPos{iSlice}{iSquare}(1)+squareIdx) = true;
                    end
                end
            end
            currentMask3d(:,:,iSlice,iSym) = (inclusionMask(:,:,iSlice)&BW);
            fprintf('     -> Done : Drawn region is saved for current slice\n');
        end
    end
end

switch manualRoiType
    case 'squares'
        iAllSquare = 1;
        pxSquare = NaN(maxNumberOfSquares,3);
        for iSlice = 1:nZ
            for iSquare = 1:nSquares(iSlice)
                if squareValid{iSlice}(iSquare)
                    pxSquare(iAllSquare,:) = [allPos{iSlice}{iSquare}(2), ...
                        allPos{iSlice}{iSquare}(1), iSlice];
                    iAllSquare= iAllSquare+1;
                end
            end
        end
        pxSquare = pxSquare(1:(iAllSquare-1),:);
    case 'squaresInterp'
        iAllSquare = 1;
        pxSquare = NaN(maxNumberOfSquares,3);
        for iSlice = 1:nZ
            manualSquaresExists = false;
            for iSquare = 1:nSquares(iSlice)
                if squareValid{iSlice}(iSquare)
                    manualSquaresExists = true;
                    pxSquare(iAllSquare,:) = [allPos{iSlice}{iSquare}(2), ...
                        allPos{iSlice}{iSquare}(1), iSlice];
                    iAllSquare= iAllSquare+1;
                end
            end
            % Add interpolated square idx
            if ~manualSquaresExists
                startSquareSliceIdx = find(nSquares(1:(iSlice-1))>0,1,'last');
                stopSquareSliceIdx = find(nSquares((iSlice+1):end)>0,1,'first');
                if (~isempty(startSquareSliceIdx) && ~isempty(stopSquareSliceIdx)) && ...
                        sum(squareValid{startSquareSliceIdx}(1:nSquares(startSquareSliceIdx))) == 1 && sum(squareValid{stopSquareSliceIdx +iSlice}(1:nSquares(stopSquareSliceIdx +iSlice))) == 1
                    stopSquareSliceIdx = stopSquareSliceIdx +iSlice;
                    startSqareExistsIdx = find(squareValid{startSquareSliceIdx}(1:nSquares(startSquareSliceIdx)));
                    stopSqareExistsIdx = find(squareValid{stopSquareSliceIdx}(1:nSquares(stopSquareSliceIdx)));
                    pxSquare(iAllSquare,1) = round((allPos{stopSquareSliceIdx}{stopSqareExistsIdx}(1)-allPos{startSquareSliceIdx}{startSqareExistsIdx}(1))/(stopSquareSliceIdx-startSquareSliceIdx)*(iSlice-startSquareSliceIdx)+allPos{startSquareSliceIdx}{startSqareExistsIdx}(1));
                    pxSquare(iAllSquare,2) = round((allPos{stopSquareSliceIdx}{stopSqareExistsIdx}(2)-allPos{startSquareSliceIdx}{startSqareExistsIdx}(2))/(stopSquareSliceIdx-startSquareSliceIdx)*(iSlice-startSquareSliceIdx)+allPos{startSquareSliceIdx}{startSqareExistsIdx}(2));
                    pxSquare(iAllSquare,3) = iSlice;
                    BW = false(nX,nY);
                    BW(pxSquare(iAllSquare,2)+squareIdx, ...
                        pxSquare(iAllSquare,1)+squareIdx) = true;
                    currentMask3d(:,:,iSlice,iSym) = (inclusionMask(:,:,iSlice)&BW);
                    
                    iAllSquare= iAllSquare+1;
                end
            end
        end
        pxSquare = pxSquare(1:(iAllSquare-1),:);
    otherwise
        pxSquare = [];
end

allMasks = any(currentMask3d,4);
if highIntensityOnly
    maxVal = sort(img(allMasks));
    for iSlice = 1:nZ
        [row, col] = find((img(:,:,iSlice).*allMasks(:,:,iSlice))>maxVal(end-nPtsPerSym));
        nCurrentSlicePoints = length(row);
        if nCurrentSlicePoints>0
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),1) = row;
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),2) = col;
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),3) = iSlice*ones(nCurrentSlicePoints,1);
            
            pointIndex0 = pointIndex0 + nCurrentSlicePoints;
        end
    end
    if pointIndex0<nTotReal
        warning('Could only fill %d / %d data points with provided ROIs and inclusion criteria',pointIndex0,nTotReal);
    end
    mask = false(size(img));
    for iP = 1:length(px)
        mask(px(iP,1),px(iP,2),px(iP,3)) = true;
    end
else
    mask = allMasks;
    nPts = sum(mask(:));
    px = NaN(nPts,3);
    pointIndex0 = 1;
    for iSlice = 1:nZ
        [row, col] = find(mask(:,:,iSlice));
        nCurrentSlicePoints = length(row);
        if nCurrentSlicePoints>0
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),1) = row;
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),2) = col;
            px(pointIndex0:(pointIndex0+nCurrentSlicePoints-1),3) = iSlice*ones(nCurrentSlicePoints,1);
            pointIndex0 = pointIndex0 + nCurrentSlicePoints;
        end
    end
end

close(fRoiSel);
end

function p = plotsquare(ax,squarePos,sizeSquare,squareColor)
if ~exist('squareColor','var')
    squareColor = 'r';
end
hold(ax,'on');
p(1) = plot(squarePos(1)+sizeSquare/2*[-1 1],squarePos(2)+sizeSquare/2*[-1 -1],['-' squareColor],'Linewidth',2);
p(2) = plot(squarePos(1)+sizeSquare/2*[-1 1],squarePos(2)+sizeSquare/2*[1 1],['-' squareColor],'Linewidth',2);
p(3) = plot(squarePos(1)+sizeSquare/2*[-1 -1],squarePos(2)+sizeSquare/2*[-1 1],['-' squareColor],'Linewidth',2);
p(4) = plot(squarePos(1)+sizeSquare/2*[1 1],squarePos(2)+sizeSquare/2*[-1 1],['-' squareColor],'Linewidth',2);

p(5) = plot(squarePos(1),squarePos(2),['-' squareColor]);
hold(ax,'off');
end

function ax1 = updatesliceimage(img,climImg,iSlice,otherInfo,nHighlights,maskHighlights,legendHigh,chosenColormap)

imagesc(img(:,:,iSlice),climImg);
if nHighlights>0
    dispLegend = true(1,nHighlights);
    highlightsColor = 'kwcmy';
    hold on;
    pHigh = NaN(1,nHighlights);
    for iHighlights = 1:nHighlights
        if ~isempty(maskHighlights(iHighlights).coordinates{iSlice})
            pHigh(iHighlights) = plot(maskHighlights(iHighlights).coordinates{iSlice}(:,1), ...
                maskHighlights(iHighlights).coordinates{iSlice}(:,2),['.' highlightsColor(iHighlights)]);
        else
            dispLegend(iHighlights) = false;
        end
    end
    if any(dispLegend)
        legend(pHigh(dispLegend),legendHigh(dispLegend));
    end
    hold off;
end
ax1 = gca;
axis off;
title(['Slice #' num2str(iSlice) otherInfo]);
colormap(ax1,chosenColormap);
end

