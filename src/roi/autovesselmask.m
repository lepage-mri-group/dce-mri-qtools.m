function [vesselROI, options] = autovesselmask(time,conc,mask,options)
% AUTOVESSELMASK initial simple automatic mask to identify vessels based on
% highest integrated area under the curve (IAUC) and lowest time to peak
% (TTP)
%   [vesselROI, options] = AUTOVESSELMASK(time,conc,mask,options)
%
% See also: INCLUSIONCRITERIATTP, INCLUSIONCRITERIAAUC
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-02

[nR,nC,nS,nT]=size(conc);
if ~exist('options','var') || isempty(options)
    % Display options
    options.waitbar=1; % 0:off, 1:on
    
    options.display=2;
    options.aif.nSlice = round(nS/2); % Displayed slice for debug plots
    
    options.aif.pArea= 0.7000; % AUC voxel rejection fraction
    options.aif.pTTP= 0.45000; % TTP rejection fraction for vessels to tissue separation
    
    options.aif.diffPicco= 0.0400; % Cluster selection criterion: Threshold between Peak or TTP criterion
    
    options.aif.nVoxelMax= 10; % maximum number of voxel in final AIF cluster
    options.aif.nVoxelMin= 5; % minimum number of voxel in final AIF cluster
end
options.nR=nR;
options.nC=nC;
options.nS=nS;
options.nT=nT;
options.time = time;

% (obsolete)
%     options.aif.pReg= 0.100; % Dynamic irregularity rejection fraction
% options.aif.nSlice= 0; % AIF slice  (0: user manually selects the slice)
% options.aif.semiasseMaggiore= 0.3500; % Ellipse AIF area : major axis dimension
% options.aif.semiasseMinore= 0.1500; % Ellipse AIF area : minor axis dimension
% % Correction of the AIF to MR-signal relation
% options.qr.enable= 0; % 0: no-correction, 1: correction applied
% options.qr.b= 5.7400e-004;
% options.qr.a= 0.0076;
% options.qr.r= 0.0440;
% % Fit final AIF
% options.aif.enable= 1; % 0: do not fit, 1: fit AIF
% options.aif.ricircolo= 1; % 0: without recirculation, 1: fit recriculation
%% Basic criteria over entire head
% AUC criteria
[aucROI, options] = inclusioncriteriaauc(conc,mask,options);

% Irregularity criteria - REMOVED - saturated VOF and AIF signal might 
% appear irregular in regular conc
% [regROI, options] = inclusioncriteriareg(conc,aucROI,options);

% TTP criteria 
[vesselROI, options] = inclusioncriteriattp(conc,aucROI,options);
