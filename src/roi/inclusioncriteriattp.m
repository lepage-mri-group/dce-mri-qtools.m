function [ROI, options] = inclusioncriteriattp(concData,ROI,options)
% INCLUSIONCRITERIATTP finds the fraction of voxels with the lowest time
% to peak.
%   [ROI, options] = INCLUSIONCRITERIATTP(concData,ROI,options)
%
% See also: INCLUSIONCRITERIAAUC, INCLUSIONCRITERIAREG
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-02
%
% Based on: (Please cite his paper on AIF selection)
% Ultima modifica: Denis Peruzzo 08/06/2010
% Funzione del pacchetto DSC_mri - DSC_mri_aif
% Autore: Denis Peruzzo - Universite di Padova - DEI

pTTP        = options.aif.pTTP;
[nR,nC,nZ,nT]=size(concData);

% 2.2) Selezione a causa del TTP
totCandidati=sum(sum(sum(ROI)));
totCandidatiDaTenere=ceil(totCandidati.*(1-pTTP));
[~,TTP]=max(concData,[],4);
TTP=TTP.*ROI;

ciclo=true;
soglia=1;
while ciclo
    if (sum(sum(sum(TTP<soglia)))-sum(sum(sum(TTP==0))))>=totCandidatiDaTenere
        ciclo=false;
    else
        soglia=soglia+1;
    end
end


ROIttp=2*ROI-ROI.*(TTP<soglia); % Vale 2 per i voxel scartati, 1 per quelli tenuti.

if options.display > 2
    disp(' ')
    disp(' Candidate voxel selection via TTP criteria')
    disp(['  Voxel initial amount: ' num2str(totCandidati)])
    disp(['  Survived voxels:      ' num2str(sum(sum(sum(ROI))))])
    
    if isfield(options,'hfAif')
        figure(options.hfAif);
    else
        options.hfAif=figure();
    end
    subplot(232)
    imagesc(ROIttp(:,:,options.aif.nSlice))
    title('TTP criteria - survived voxels')
    set(gca,'xtick',[],'ytick',[])
    axis square
    
    subplot(235)
    plot(options.time,zeros(1,nT),'b-')
    hold on
    plot(options.time,zeros(1,nT),'r-')
    plot(options.time,zeros(1,nT),'k-')
    for c=1:nC
        for r=1:nR
            for z=1:nZ
                if ROIttp(r,c,z)==1
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'r-')
                elseif ROIttp(r,c,z)==2
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'b-')
                end
            end
        end
    end
    set(gca,...
        'FontSize',12)
    legend('Accepted','Reiected')
    title('TTP')
    xlabel('time')
end

ROI=ROI.*(TTP<soglia);
end