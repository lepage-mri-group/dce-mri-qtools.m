function [maskNeigh,chosenNeighbourg,maskIdx] = findsurroundingtissue(iauc,dynImg,maskBrain,maskAif,neighOrder,iaucThreshFraction)
% FINDSURROUNDINGTISSUES Select nearest neighbourg of selected AIF within 
% brain mask with smallest integrated area under the curve (IAUC) under a 
% fraction of 20% of the AIF IAUC.
%
%   [maskNeigh,chosenNeighbourg,maskIdx] = FINDSURROUNDINGTISSUES(concData,ROI,options)
%
% See also: INCLUSIONCRITERIAAUC
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-03-16

neighbourgs{1} = [1 0 0;-1 0 0;0 1 0;0 -1 0;0 0 1;0 0 -1];
neighbourgs{2} = [1 1 0;1 -1 0;-1 1 0;-1 -1 0;1 0 1;-1 0 1;0 1 1;0 -1 1;1 0 -1;-1 0 -1;0 -1 -1;0 1 -1];
neighbourgs{3} = [1 1 1; 1 1 -1; 1 -1 1; 1 -1 -1; -1 1 1; -1 1 -1; -1 -1 1; -1 -1 -1; 2 0 0;-2 0 0;0 2 0; 0 -2 0; 0 0 2; 0 0 -2];
neighbourgs{4} = [1 0 2;-1 0 2;0 1 2;0 -1 2;1 0 -2;-1 0 -2;0 1 -2;0 -1 -2;2 0 1;-2 0 1;0 2 1;0 -2 1;2 0 -1;-2 0 -1;0 2 -1;0 -2 -1;1 2 0;1 -2 0;-1 2 0;-1 -2 0;2 1 0;2 -1 0;-2 1 0;-2 -1 0];
neighbourgs{5} = [1 2 1;1 -2 1;-1 2 1;-1 -2 1;2 1 1;2 -1 1;-2 1 1;-2 -1 1;1 2 -1;1 -2 -1;-1 2 -1;-1 -2 -1;2 1 -1;2 -1 -1;-2 1 -1;-2 -1 -1;1 1 2;1 -1 2;-1 1 2;-1 -1 2;1 1 -2;1 -1 -2;-1 1 -2;-1 -1 -2];
neighbourgs{6} = [2 2 0;2 -2 0;-2 2 0;-2 -2 0;2 0 2;-2 0 2;0 2 2;0 -2 2;2 0 -2;-2 0 -2;0 -2 -2;0 2 -2];
nTotLevels = length(neighbourgs);
nNeigh = NaN(nTotLevels,1);
for iLevel=1:nTotLevels
    nNeigh(iLevel) = length(neighbourgs{iLevel});
end

if ~exist('neighOrder','var') || isempty(neighOrder)
    neighOrder = 1:3; % Default for AIF - closest voxels first maximum distance of 2 voxels
end
if ~exist('iaucThreshFraction','var') || isempty(iaucThreshFraction)
    iaucThreshFraction = 0.2;% Default for AIF - closest voxels first maximum distance of 2 voxels
end
nLevels = length(neighOrder);

% Find nearest neighbourgs

[nX,nY,nZ,nT] = size(dynImg);

maskIdx = pixelfrommask(maskAif);
nPts = size(maskIdx,1);

chosenNeighbourg = NaN(nPts,3);
maskNeigh = false(nX,nY,nZ);
minBound = [1 1 1];
maxBound = [nX,nY,nZ];
for iPts = 1:nPts
    cPtIdx = maskIdx(iPts,:);
    cMinIauc = inf;
    cAifIauc = iauc(cPtIdx(1),cPtIdx(2),cPtIdx(3));
    nextLevel = true;
    iLevel = 1;
    while nextLevel && iLevel <= nLevels
        for iNeigh = 1:nNeigh(neighOrder(iLevel))
            cCoords = cPtIdx + neighbourgs{neighOrder(iLevel)}(iNeigh,:);
            underBounds = cCoords<minBound;
            cCoords(underBounds) = minBound(underBounds);
            overBounds = cCoords>maxBound;
            cCoords(overBounds) = maxBound(overBounds);
            coordsIauc = iauc(cCoords(1),cCoords(2),cCoords(3));
            if maskBrain(cCoords(1),cCoords(2),cCoords(3)) && ~maskAif(cCoords(1),cCoords(2),cCoords(3)) && coordsIauc<cMinIauc
                cMinIauc = coordsIauc;
                chosenNeighbourg(iPts,:) = cCoords;
            end
        end
        if cMinIauc < cAifIauc*iaucThreshFraction
            nextLevel = false;
        else
            iLevel = iLevel +1;
        end
    end
    if all(~isnan(chosenNeighbourg(iPts,:)))
        maskNeigh(chosenNeighbourg(iPts,1),chosenNeighbourg(iPts,2),chosenNeighbourg(iPts,3)) = true;
    end
end