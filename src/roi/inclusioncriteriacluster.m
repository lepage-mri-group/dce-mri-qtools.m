function [maskAIF, centroidi, clusterScelto, dati2D] = inclusioncriteriacluster(concData,ROI,options)
% INCLUSIONCRITERIACLUSTER group time curves together with iterative
% clustering. Selects the cluster with highest integrated area under the 
% curve (IAUC) or blood volume fraction (BVF). Time to peak can be added as
% a second level of selection.
%   [maskAIF, centroidi, clusterScelto, dati2D] = INCLUSIONCRITERIACLUSTER(concData,ROI,options)
%
% See also: INCLUSIONCRITERIAAUC, INCLUSIONCRITERIATTP,INCLUSIONCRITERIAREG
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-02
%
% Based on: (Please cite his paper on AIF selection)
% Ultima modifica: Denis Peruzzo 08/06/2010
% Funzione del pacchetto DSC_mri - DSC_mri_aif
% Autore: Denis Peruzzo - Universite di Padova - DEI

% 3) APPLICAZIONE DELL'ALGORITMO DI CLUSTER PER LA RICERCA DELL'ARTERIALE
if options.display
    disp('   Arterial voxels extraction')
end
if ~isfield(options.aif,'selectionType')
    options.aif.selectionType = 'IAUC'; % default
end
diffPicco   = options.aif.diffPicco;
nVoxelMax   = options.aif.nVoxelMax;
nVoxelMin   = options.aif.nVoxelMin;
rangeIdx = options.aif.iaucRangeIdx;
% 3.1) Preparazione della matrice contenente i dati
% Flexible input
[nR,nC,nZ,nT]=size(concData);
nT2 = length(options.time);
if (nC==nT2) && (nZ == 1) && (nT == 1)
    dati2D = concData;
    maskAIF = true(nR,1);
    vectorInput = true;
    nT = nC;
else
    dati2D=zeros(sum(sum(sum(ROI))),nT);
    ind=find(ROI);
    k=nR*nC*nZ;
    for t=1:nT
        dati2D(:,t)=concData(ind+k*(t-1));
    end
    maskAIF=ROI;
    vectorInput = false;
    if isfield(options.aif,'pveFactorMap')
        bloodVolFracList = options.aif.pveFactorMap(ind);
    else
        bloodVolFracList = ones(size(ind));
    end
end

% 3.2) Applico l'algoritmo di Cluster Gerarchico in modo ricorsivo

ciclo=true;
nCiclo=0;
clear concData

while ciclo
    nCiclo=nCiclo+1;
    if options.display > 2
        disp(' ')
        disp(' ------------------------------------')
        disp(['  CICLE N# ' num2str(nCiclo)])
    end
    
    % Applico il cluster gerarchico
    [vettCluster,centroidi]=clusterGerarchico(dati2D,2);
    
    % Choosing which cluster to keep (clusterScelto) - With maximum IAUC
    switch options.aif.selectionType
        case 'IAUC'
            c1stCrit1 = timeconcintegral(options.time, centroidi(1,:), rangeIdx);
            c1stCrit2 = timeconcintegral(options.time, centroidi(2,:), rangeIdx);
        case 'BVF'
            c1stCrit1 = mean(bloodVolFracList(vettCluster==1));
            c1stCrit2 = mean(bloodVolFracList(vettCluster==2));
    end
    
    % Confronto i cluster e scelgo quale tenere
    % Choosing which cluster to keep (clusterScelto) - Either with minimum TTP or maximum IAUC
    [MC1,TTP1]=max(centroidi(1,:));
    [MC2,TTP2]=max(centroidi(2,:));
    if (((max([c1stCrit1 c1stCrit2])-min([c1stCrit1 c1stCrit2]))/max([c1stCrit1 c1stCrit2]))<diffPicco)&&(TTP1~=TTP2) %% (((max([MC1 MC2])-min([MC1 MC2]))/max([MC1 MC2]))<diffPicco)&&(TTP1~=TTP2)
        % La differenza tra i picchi � minore della soglia, scelgo basandomi sul TTP
        % Clusters peak AIF relative difference is inferior to the threshold (diffPicco) - Choosing the cluster with minimum TTP as selection
        clusterScelto=1+(TTP2<TTP1); % Il risultato vale 1 se TTP1<TTP2 e 2 se TTP2<TTP1
        
        if options.display > 2
            disp('  Cluster selected via TTP criteria')
            disp(['   Selected cluster: ' num2str(clusterScelto)])
        end
        
    else
        % Scelgo basandomi sulla differenza tra picchi
        % Choosing the cluster with maximum IAUC
        clusterScelto=1+(c1stCrit1<c1stCrit2); % =1 if cIAUC1>cIAUC2 and =2 if cIAUC2>cIAUC1
%         clusterScelto=1+(MC2>MC1); % Il risultato vale 1 se MC1>MC2 e 2 se MC2>MC1
        
        if options.display > 2
            disp('  Cluster selected via MC criteria')
            disp(['   Selected cluster: ' num2str(clusterScelto)])
        end
    end
    
    if (sum(vettCluster==clusterScelto)<nVoxelMin)&&(sum(vettCluster==(3-clusterScelto))>=nVoxelMin)
        % La popolazione del cluster scelto � inferiore al numero minimo di
        % voxel accettati, mentre l'altro cluster ne ha a sufficenza.
        % Scelgo l'altro cluster.
        clusterScelto=3-clusterScelto; % Inverto il cluster scelto (se era 2 diventa 1, se era 1 diventa2)
        
        if options.display > 2
            disp('  Cluster selected switched because of minimum voxel bound')
            disp(['   Selected cluster: ' num2str(clusterScelto)])
        end
    end
    
    % Tengo solo i dati relativi al cluster scelto
    voxelScelti=(vettCluster==clusterScelto);
    indMask=find(maskAIF);
    maskAIF(indMask)=voxelScelti;
    
    indVoxel=find(voxelScelti);
    nL=length(indVoxel);
    dati2Dold=dati2D;
    dati2D=zeros(nL,nT);
    for t=1:nT
        dati2D(:,t)=dati2Dold(indVoxel,t);
    end
    
    if (options.display > 2 ) && ~vectorInput
        disp(' ')
        disp([' Resume cicle n# ' num2str(nCiclo)])
        disp(['  Voxel initial amount: ' num2str(length(indMask))])
        disp(['  Survived voxels:  ' num2str(nL)])
        disp(['  Cluster 1: MC       ' num2str(MC1)])
        disp(['             TTP      ' num2str(TTP1)])
        disp(['             voxel    ' num2str(sum(vettCluster==1))])
        disp(['  Cluster 2: MC       ' num2str(MC2)])
        disp(['             TTP      ' num2str(TTP2)])
        disp(['             voxel    ' num2str(sum(vettCluster==2))])
        disp(['  Selected cluster: ' num2str(clusterScelto)])
        
        % Preparo l'immagine per le eventuali visualizzazioni
        immagine.img=sum(concData(:,:,options.aif.nSlice,:),4);
        vettImmagine=sort(immagine.img(1:nR*nC));
        immagine.bound=[0 vettImmagine(round(0.95*nR*nC))];
        
        eval(['hf.img_centr' num2str(nCiclo) '=figure();']);
        subplot(1,2,1)
        [posC,posR]=find(maskAIF);    
        imagesc(immagine.img,immagine.bound)
        colormap(gray)
        hold on
%         plot(xROI,yROI,'r')
        plot(posR,posC,'r.','MarkerSize',1)
        title(['Cicle n#' num2str(nCiclo) ' - candidate voxels'])
        set(gca,'xtick',[],'ytick',[],'fontsize',12)
        axis square
        
        subplot(1,2,2)
        plot(options.time,centroidi,'k-')
        hold on
        plot(options.time,centroidi(clusterScelto,:),'r-')
        title('Cluster centroids')
        xlabel('time')
        set(gca,'fontsize',12)
        
    end
    
    
    
    % Controllo i criteri di uscita
    if (nL<=nVoxelMax)||(nCiclo>=100)
        ciclo=false;
    end
end
end

%% ------------------------------------------------------------------------
function [vettAssegnazioni, centroidi] = clusterGerarchico(dati,nCluster)
% Applica l'algoritmo di cluster gerarchico ai dati e li divide in
% nCluster. Restutiusce un vettore contenente il numero del cluster cui �
% stato assegnato ciascun voxel e il centroide di tale cluster.

distance=pdist(dati);
tree=linkage(distance,'ward');
vettAssegnazioni=cluster(tree,'maxclust',nCluster);

nT=size(dati,2);
centroidi=zeros(nCluster,nT);
for k=1:nCluster
    ind=find(vettAssegnazioni==k);
    
    datiCluster=zeros(length(ind),nT);
    for t=1:nT
        datiCluster(:,t)=dati(ind,t);
    end
    
    centroidi(k,:)=mean(datiCluster,1);
end
end

function concTimeInt = timeconcintegral(injTime, conc, rangeIdxPve)
concTimeInt = trapz(injTime(rangeIdxPve(1):rangeIdxPve(2)),conc(rangeIdxPve(1):rangeIdxPve(2))); % (mM min)
end