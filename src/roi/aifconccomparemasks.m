function [cluster, all] = aifconccomparemasks(conc,vesselAutoMask,vesselTrueMask,clusterOptions,concOutlier)
% AIFCONCCOMPAREMASKS Averages the arterial (or venous) concentration over
% a mask (all) and with a clustering procedure (cluster). Returned
% structures offers statistics of the selected voxels. 
%
%   [cluster, all] = AIFCONCCOMPAREMASKS(conc,vesselAutoMask,vesselTrueMask,clusterOptions,concOutliers)
%
% See also: GLOBALAIF,GLOBALVOF,INCLUSIONCRITERIACLUSTER
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-09-16
if ~exist('concOutlier','var') || isempty(concOutlier)
    concOutlier = false(size(conc));
end
%% Clustering
if sum(vesselAutoMask(:))<clusterOptions.aif.nVoxelMax
    warning('Number of valid PVC voxels are under the final cluster size. Skipping clustering.')
    cluster.mask = vesselAutoMask;
    cluster.chosenCentroid = 1;
else
    % Interpolate concentration at outliers
    concCluster = interpoutlierconc(conc,vesselAutoMask,concOutlier);
    
    [cluster.mask, cluster.finalCentroids, cluster.chosenCentroid,...
        cluster.allVoxelConc] = inclusioncriteriacluster(concCluster,vesselAutoMask,clusterOptions);
end
%% Extract concentration of masks
[cluster.concList,~,clusterExcludeList] = dyn3dconversion(conc,cluster.mask,[],concOutlier);

all.mask = vesselTrueMask;
[all.concList,~,allExcludeList] = dyn3dconversion(conc,vesselTrueMask,[],concOutlier);

%% Keep the saturated conc values at the extremity of exclusions
se = strel('line',3,0);
cluster.excludeConc = imerode(clusterExcludeList,se) | isnan(cluster.concList) | isinf(cluster.concList);
all.excludeConc = imerode(allExcludeList,se) | isnan(all.concList)  | isinf(all.concList);

%% Compute statistics
nT = size(cluster.concList,2);
cluster.mean = NaN(1,nT);
cluster.std = NaN(1,nT);
all.mean = NaN(1,nT);
all.std = NaN(1,nT);
for iT = 1:nT
    clusterIncluded = ~cluster.excludeConc(:,iT);
    cluster.mean(iT) = mean(cluster.concList(clusterIncluded,iT),1);
    cluster.std(iT) = std(cluster.concList(clusterIncluded,iT),[],1);
    allIncluded = ~all.excludeConc(:,iT);
    all.mean(iT) = mean(all.concList(allIncluded,iT),1);
    all.std(iT) = std(all.concList(allIncluded,iT),[],1);
end
cluster.meanNotSat = ~isnan(cluster.mean);
all.meanNotSat = ~isnan(all.mean);
% clusterOld.mean = mean(cluster.concList,1);
% clusterOld.std = std(cluster.concList,[],1);
% 
% allOld.mean = mean(all.concList,1);
% allOld.std = std(all.concList,[],1);
end

function concInterpOutlier = interpoutlierconc(conc,mask,concOutlier)
    
[listConc, nIdxT, listOutliers] = dyn3dconversion(conc,mask,[],concOutlier);
[nPx,nT] = size(listConc);
listConcInterp = listConc;
timeIdx = 1:nT;
for iPx = 1:nPx
    listConcInterp(iPx,:) = interp1(timeIdx(1,~listOutliers(iPx,:)),listConc(iPx,~listOutliers(iPx,:)),timeIdx);
end

[concInterpOutlier] = mat2dyn3d(listConcInterp, nIdxT,mask);
end



