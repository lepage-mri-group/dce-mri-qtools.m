function [aifMask, tissueMask] = reviewrectaifselection(img, injTime, dynImg,bolusRange,rectCentre,aifMask,aifList,tissueMask,tissueList,allDynImg,refVof)
% REVIEWAIFSELECTION is a simple GUI to manually review and select the
% voxels selected of AIF and VOF.
%   [aifMask, tissueMask] = REVIEWAIFSELECTION(img, injTime, dynImg,bolusRange,rectCentre,aifMask,aifList,tissueMask,tissueList,allDynImg,refVof)
%
% See also: SEMIAUTOMASKSVENARTTISS,AUTOVESSELMASK,AIFCONCCOMPAREMASKS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-10

[nY,nX,nZ,nT] = size(dynImg);
if ~exist('aifList','var') || isempty(aifList)
    aifList = pixelfrommask(aifMask);
end
if exist('tissueMask','var') && ~isempty(tissueMask) && exist('tissueList','var') && ~isempty(tissueList)
    dualMask = true;
else
    dualMask = false;
    tissueMask = [];
    tissueList = [];
    allDynImg = [];
end
if nargin < 4
    bolusRange = [1 nT];
end

if ~exist('refVof','var')
    refVof = [];
end

rectShown = 1;
nRect = size(rectCentre,1);
sliceShown = rectCentre(rectShown,3);
sizeSquare = 9;
squareIdx = (1:sizeSquare)-ceil(sizeSquare/2);

squareBoxSize = 2;
squareBoxDim = 2*squareBoxSize+1;
horiSubPlot = squareBoxDim + squareBoxSize + 1;
subPlotIdx = NaN(squareBoxDim,squareBoxDim);
for iDimX = 1:squareBoxDim
    for iDimY = 1:squareBoxDim
        subPlotIdx(iDimX,iDimY) = (iDimX - 1)*horiSubPlot + (horiSubPlot-squareBoxDim) + iDimY;
    end
end
% Inputs labels
dataLabels{1} = 'IAUC ';
dataLabels{2} = 'Signal';
dataLabels{3} = 'Time';

% Initialize image
nSubIaucHori = 4;
nSubHori = sizeSquare+nSubIaucHori;
nSubVert = sizeSquare;
nSubIaucVert = ceil(3/4*nSubVert);
nSubAifVert = nSubVert - nSubIaucVert;

timeRange = bolusRange(1):bolusRange(2);

subPlotGap = 0.01;
marg_w = 0.05;
marg_h = 0.13; % 0.05 without ylabels
iaucSubsIdx = reshape(kron(ones(1,(nSubIaucHori)),(0:nSubHori:(nSubIaucVert-1)*nSubHori)')' + kron((1:(nSubIaucHori)),ones(nSubIaucVert,1))',1,[]);
aifSubsIdx = reshape(kron(ones(1,(nSubIaucHori)),(nSubIaucVert*nSubHori:nSubHori:nSubVert*(nSubHori-1))')' + kron((1:(nSubIaucHori)),ones(nSubAifVert,1))',1,[]);

clim = [prctile(img(:),5),prctile(img(:),95)];
if diff(clim) == 0
    clim = clim(1)*[0 1]+[0 1];
end
fig123 = figure(123);
set(fig123,'position',[8          49        1663         917]);

titleOfIauc = ['Square #1 of' num2str(nRect)];
[ax1, aText] = showiauc(img, clim, aifMask, aifList,tissueList, sliceShown, nZ, nSubVert,nSubHori,iaucSubsIdx,titleOfIauc);
plotsquare(ax1,rectCentre(rectShown,[2, 1]),sizeSquare);
[currentAif, ~] = dyn3dconversion(dynImg,aifMask);
startingAif = mean(currentAif);
notOutlierAif = ~(isnan(startingAif) | isinf(startingAif));
concLim = [0, max(startingAif(notOutlierAif))];
concLim = mean(concLim) + 1*diff(concLim)*[-1 1];
plotrectsubs(injTime,dynImg,timeRange,aifMask, aifList, tissueList,allDynImg, sliceShown, concLim, nSubVert,nSubHori,nSubIaucHori,squareIdx,rectCentre(rectShown,[2, 1]),subPlotGap,marg_h,marg_w,mean(currentAif),refVof);

axC = subtightplot(nSubVert,nSubHori,aifSubsIdx);
plot(injTime,startingAif,'.-k');xlabel('Time (min)'); ylabel('C (mM)');

button = 1;
subPlotPositionX = round((0.33:0.07:0.89),2);
subPlotPositionY = round((0.8:-0.083:0.13),2);
subPlotFigPos = round([0.13, 0.46],2);
while (button ~= 3) % Right-click: exits loop
    figure(fig123)
    %     axes(ax1);
    [xx, yy, button] = ginput(1);
    currentSubplotPosition = round(get(gca,'position'),2);
    if (button == 29) || (button == 28) % right arrow : next slice
        if button == 29
            rectShown = rectShown -1;
            if (rectShown<1)
                rectShown = nRect;
            end
        else
            rectShown = rectShown + 1;
            if (rectShown>nRect)
                rectShown = 1;
            end
        end
        sliceShown = rectCentre(rectShown,3);
        clear('hRect');
        figure(fig123);
        delete(aText);        
        titleOfIauc = ['Square #' num2str(rectShown) ' of' num2str(nRect)];
        [ax1, aText] = showiauc(img, clim, aifMask, aifList, tissueList, sliceShown, nZ, nSubVert,nSubHori,iaucSubsIdx,titleOfIauc,subPlotGap,marg_h,marg_w);
        plotsquare(ax1,rectCentre(rectShown,[2, 1]),sizeSquare);
        plotrectsubs(injTime,dynImg,timeRange,aifMask, aifList, tissueList,allDynImg, sliceShown, concLim, nSubVert,nSubHori,nSubIaucHori,squareIdx,rectCentre(rectShown,[2, 1]),subPlotGap,marg_h,marg_w,mean(currentAif),refVof);
    else
        % Find wich subplot was selecteda
        [~,iDimY] = min(abs(currentSubplotPosition(1) - subPlotPositionX));
        [~,iDimX] = min(abs(currentSubplotPosition(2) - subPlotPositionY));
        if (~isempty(iDimX)) && (~isempty(iDimY))
            if (button==97)||(button==116)
                if button==97
                    % If 'a' was pressed for arterial
                    if aifMask(rectCentre(rectShown,1)+squareIdx(iDimX),rectCentre(rectShown,2)+squareIdx(iDimY),sliceShown)
                        aifMask(rectCentre(rectShown,1)+squareIdx(iDimX),rectCentre(rectShown,2)+squareIdx(iDimY),sliceShown) = false;
                    else
                        %                             tissueMask(boxMatY(iDimX,iDimY),boxMatX(iDimX,iDimY),sliceShown) = false;
                        aifMask(rectCentre(rectShown,1)+squareIdx(iDimX),rectCentre(rectShown,2)+squareIdx(iDimY),sliceShown) = true;
                    end
%                 elseif (button==116)&&(dualMask)
%                     % If 't' was pressed for tissue
%                     if tissueMask(boxMatY(iDimX,iDimY),boxMatX(iDimX,iDimY),sliceShown)
%                         tissueMask(boxMatY(iDimX,iDimY),boxMatX(iDimX,iDimY),sliceShown) = false;
%                     else
%                         tissueMask(boxMatY(iDimX,iDimY),boxMatX(iDimX,iDimY),sliceShown) = true;
%                         aifMask(boxMatY(iDimX,iDimY),boxMatX(iDimX,iDimY),sliceShown) = false;
%                     end
                end
                delete(aText);
                plotrectsubs(injTime,dynImg,timeRange,aifMask, aifList, tissueList,allDynImg, sliceShown, concLim, nSubVert,nSubHori,nSubIaucHori,squareIdx,rectCentre(rectShown,[2, 1]),subPlotGap,marg_h,marg_w,mean(currentAif),refVof);
                [currentAif, ~] = dyn3dconversion(dynImg,aifMask);
                axC = subtightplot(nSubVert,nSubHori,aifSubsIdx);
                plot(injTime,mean(currentAif),'.-k');xlabel('Time (min)'); ylabel('C (mM)');
            end
        end
    end
end

end

function p = plotsquare(ax,squarePos,sizeSquare)

hold(ax,'on');
p(1) = plot(squarePos(1)+sizeSquare/2*[-1 1],squarePos(2)+sizeSquare/2*[-1 -1],'-r','Linewidth',2);
p(2) = plot(squarePos(1)+sizeSquare/2*[-1 1],squarePos(2)+sizeSquare/2*[1 1],'-r','Linewidth',2);
p(3) = plot(squarePos(1)+sizeSquare/2*[-1 -1],squarePos(2)+sizeSquare/2*[-1 1],'-r','Linewidth',2);
p(4) = plot(squarePos(1)+sizeSquare/2*[1 1],squarePos(2)+sizeSquare/2*[-1 1],'-r','Linewidth',2);

% p(5) = plot(squarePos(1),squarePos(2),'.r');
hold(ax,'off');
end

function [ax1, aText] = showiauc(img, clim,aifMask,aifList, tissueList, sliceShown, nZ, nSubVert,nSubHori,iaucSubs,titleOfIauc,subPlotGap,marg_h,marg_w)
highlightsColor = 'rgycm';
currentAif = aifList(:,3) == sliceShown;
% [yHigh, xHigh] = find(aifMask(:,:,sliceShown));

axC = subtightplot(nSubVert,nSubHori,iaucSubs); % 25 26 27 33 34 35
P = get(axC,'pos');    % Get the position.
delete(axC)            % Delete the subplot axes
ax1 = axes;
imagesc(img(:,:,sliceShown));
% ax2 = axes;
% imagesc(aifMask(:,:,sliceShown)*1.0 + tissueMask(:,:,sliceShown)*2.0,'AlphaData', aifMask(:,:,sliceShown) | tissueMask(:,:,sliceShown));
set(ax1,'pos',P)
% set(ax2,'pos',P)
% linkaxes([ax1,ax2])
% ax1.Visible = 'off';
ax1.XTick = [];
ax1.YTick = [];
ax1.PlotBoxAspectRatio = [1 1 1];
ax1.CLim = clim;
if exist('titleOfIauc','var')
    title(ax1,titleOfIauc);
end
% ax2.Visible = 'off';
% ax2.XTick = [];
% ax2.YTick = [];
% ax2.PlotBoxAspectRatio = [1 1 1];
% ax2.CLim = [1 2];
colormap(ax1,'parula')
hold on;
plot(aifList(currentAif,2), aifList(currentAif,1),['.' highlightsColor(1)]);
if ~isempty(tissueList)
    plot(tissueList(currentAif,2), tissueList(currentAif,1),['o' highlightsColor(2)]);
end
hold off;
% colormap(ax2,'gray')
aText = annotation(gcf,'textbox',...
    [0.05 0.35 0.2 0.0338345864661654],...
    'Color',[1 1 1],...
    'String',{['Slice = ', num2str(sliceShown),'/',num2str(nZ)]},...
    'LineStyle','none',...
    'FontWeight','bold',...
    'FontSize',12,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);
end

function plotrectsubs(time,dyn, timeRange, aifMask, aifList, tissueList,allDyn, sliceShown, concLim, nSubVert,nSubHori,nIaucSubs,squareIdx,squareCentre,subPlotGap,marg_h,marg_w,refAif,refVof)
timeLim = time([timeRange(1),timeRange(end)]);

for iSubVert = 1:nSubVert
    for iSubHori=(nIaucSubs+1):nSubHori
        subIdx = nSubHori*(iSubVert-1)+iSubHori;
        h=subtightplot(nSubVert,nSubHori,subIdx,subPlotGap,marg_h,marg_w);
        cXCoord = squareCentre(2) + squareIdx(iSubVert);
        cYCoord = squareCentre(1) + squareIdx(iSubHori-nIaucSubs);
        cConc = squeeze(dyn(cXCoord,cYCoord,sliceShown,:));
        cConcSum = sum(cConc);
        if (cConcSum~=0) && ~isnan(cConcSum)
            if aifMask(cXCoord,cYCoord,sliceShown)
                plot(time(timeRange),cConc(timeRange),'.-r');
                if ~isempty(tissueList)
                    aifIdx = find((aifList(:,1)==cXCoord)&(aifList(:,2)==cYCoord)&(aifList(:,3)==sliceShown),1);
                    cTConc = squeeze(allDyn(tissueList(aifIdx,1),tissueList(aifIdx,2),tissueList(aifIdx,3),:));
                    hold on;
                    plot(time(timeRange),cTConc(timeRange),'.-g');
                    hold off;
                end
            else
                plot(time(timeRange),cConc(timeRange),'.-k');
            end
            hold on;
            if ~isempty(refAif)
                plot(time(timeRange),refAif(timeRange),'--r');
            end
            if ~isempty(refVof)
                plot(time(timeRange),refVof(timeRange),'--b');
            end
            hold off;
        else
            plot(1);
        end
        xlim(timeLim);
        ylim(concLim);
        if ~(iSubVert==nSubVert && iSubHori == (nIaucSubs+1))
            h.XTickLabel = '';
            h.YTickLabel = '';
        end
    end
end
end
