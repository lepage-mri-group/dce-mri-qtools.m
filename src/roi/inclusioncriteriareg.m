function [ROI, options] = inclusioncriteriareg(concData,ROI,options)
% INCLUSIONCRITERIAREG finds the fraction of voxels with the highest time
% curve irregularities based on the roughness index (area under the second
% derivative).
%   [ROI, options] = INCLUSIONCRITERIAREG(concData,ROI,options)
%
% See also: INCLUSIONCRITERIAAUC, INCLUSIONCRITERIATTP
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-02
%
% Based on: (Please cite his paper on AIF selection)
% Ultima modifica: Denis Peruzzo 08/06/2010
% Funzione del pacchetto DSC_mri - DSC_mri_aif
% Autore: Denis Peruzzo - Universite di Padova - DEI

pReg        = options.aif.pReg; % Fraction of voxels under the roughness threshold
[nR,nC,nZ,nT]=size(concData);
includedReg = options.aif.regRange(1):options.aif.regRange(2);

% Compute roughness
totCandidates = sum(sum(sum(ROI)));
totCandidatesToKeep = ceil(totCandidates.*(1-pReg));
REG = calcolaReg(concData(:,:,:,includedReg),options.time(includedReg),ROI);

% Find roughness threshold that keeps the desired fraction of voxels with
% higher roughness
ciclo=true;
nCiclo=0;
REGdown=min(min(min(REG)));
REGup=max(max(max(REG)));
while ciclo
    nCiclo=nCiclo+1;
    threshold = 0.5*(REGup+REGdown);
    nCandidati=sum(sum(sum(REG>threshold)));
    
    if nCandidati==totCandidatesToKeep
        ciclo=false;
    elseif nCandidati<totCandidatesToKeep
        REGup=threshold;
    else
        REGdown=threshold;
    end
    if ((REGup-REGdown)<0.001)||(nCiclo>=100)
        ciclo=false;
    end
end

ROIreg=2*ROI-ROI.*(REG>threshold); % Vale 2 per i voxel scartati, 1 per quelli tenuti.
if options.display > 2
    disp(' ')
    disp(' Candidate voxel selection via regularity criteria')
    disp(['  Voxel initial amount: ' num2str(totCandidates)])
    disp(['  Survived voxels: ' num2str(sum(sum(sum(ROI))))])
    
    if isfield(options,'hfAif')
        figure(options.hfAif);
    else
        options.hfAif=figure();
    end
    subplot(2,3,3)
    imagesc(ROIreg(:,:,options.aif.nSlice))
    title('Regularity criteria - survived voxels')
    set(gca,'xtick',[],'ytick',[])
    axis square
    
    subplot(236)
    plot(options.time,zeros(1,nT),'b-')
    hold on
    plot(options.time,zeros(1,nT),'r-')
    plot(options.time,zeros(1,nT),'k-')
    for c=1:nC
        for r=1:nR
            for z=1:nZ
                if ROIreg(r,c,z)==1
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'r-')
                elseif ROIreg(r,c,z)==2
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'b-')
                end
            end
        end
    end
    set(gca,...
        'FontSize',12)
    legend('Accepted','Reiected')
    title('I_r_e_g')
    xlabel('time')
end
ROI=ROI & (REG>threshold);


if options.display > 2
    % Preparo l'immagine per le eventuali visualizzazioni
    immagine.img=sum(concData(:,:,options.aif.nSlice,:),4);
    vettImmagine=sort(immagine.img(1:nR*nC));
    immagine.bound=[0 vettImmagine(round(0.95*nR*nC))];
    
    hf.sel_voxel=figure();
    subplot(121)
    [posCok,posRok]=find(ROI(:,:,options.aif.nSlice));
    imagesc(immagine.img,immagine.bound),colormap(gray)
    hold on
%     plot(xROI,yROI,'r')
%     plot(centro(1),centro(2),'r+')
    plot(posRok,posCok,'r.','MarkerSize',1)
    title('Cadidate voxels')
    set(gca,'xtick',[],'ytick',[])
    axis square
    
    subplot(122)
    plot(options.time,reshape(concData(posRok(1),posCok(1),options.aif.nSlice,:),1,nT),'r-')
    if isfield(options,'ROIiniziale')
        [posCno,posRno]=find(options.ROIiniziale(:,:,options.aif.nSlice)-ROI(:,:,options.aif.nSlice));
        hold on
        plot(options.time,reshape(concData(posRno(1),posCno(1),options.aif.nSlice,:),1,nT),'b-')
        for c=max([length(posCno),length(posCok)]):-1:2
            if c<=length(posCno)
                plot(options.time,reshape(concData(posRno(c),posCno(c),options.aif.nSlice,:),1,nT),'b-')
            end
            if c<=length(posCok)
                plot(options.time,reshape(concData(posRok(c),posCok(c),options.aif.nSlice,:),1,nT),'r-')
            end
        end
    end
    legend('Accepted','Rejected')
    title('ROI voxels')
    xlabel('time')
end
end

%% ------------------------------------------------------------------------
function [REG]                         = calcolaReg(y,x,mask)
% Computes the irregularity index based on the area under the second 
% derivative of the time curve for each voxels of ROI.
%
% Calcola l'indice di irregolarit� dell'andamento della curva di
% concentrazione per ciascun voxel. L'indice viene calcolato normalizzando
% l'area a ! in modo da non penalizzare voxel con aree elevate.
% La formula usata per calcolare l'indice �:
%
% CTC=integrale((C"(t))^2 dt)

[nR,nC,nZ,nT]=size(y);
AUC=sum(y,4);
AUC=AUC+(AUC==1);
for t=1:nT
    y(:,:,:,t)=y(:,:,:,t)./AUC;
end

% calcolo della derivata seconda
derivata2=zeros(nR,nC,nZ,nT);
for r=1:nR
    for c=1:nC
        for z=1:nZ
            if mask(r,c,z)==1
                for t=1:nT
                    if (t>1)&&(t<nT)
                        % Caso standard
                        derivata2(r,c,z,t)=((y(r,c,z,t+1)-y(r,c,z,t))/(x(t+1)-x(t))-(y(r,c,z,t)-y(r,c,z,t-1))/(x(t)-x(t-1)))/(x(t)-x(t-1));
                    elseif t==1
                        % Manca il campione precedente
                        derivata2(r,c,z,t)=(y(r,c,z,t+1)-y(r,c,z,t))/((x(t+1)-x(t))^2);
                    else
                        % Manca il campione successivo
                        derivata2(r,c,z,t)=(y(r,c,z,t)-y(r,c,z,t-1))/((x(t)-x(t-1))^2);
                    end
                end
            end
        end
    end
end

% Calcolo dell'indice di irregolarit�
derivata2=derivata2.^2;
REG=trapz(x,derivata2,4);
end