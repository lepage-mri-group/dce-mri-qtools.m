function [ROI, options] = inclusioncriteriaauc(concData,ROI,options)
% INCLUSIONCRITERIAAUC finds the fraction of voxels with the highest 
% integrated area under the curve (IAUC).
%   [ROI, options] = INCLUSIONCRITERIAAUC(concData,ROI,options)
%
% See also: INCLUSIONCRITERIATTP, INCLUSIONCRITERIAREG
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-02
%
% Based on: (Please cite his paper on AIF selection)
% Ultima modifica: Denis Peruzzo 08/06/2010
% Funzione del pacchetto DSC_mri - DSC_mri_aif
% Autore: Denis Peruzzo - Universite di Padova - DEI

% 2) DECIMAZIONE DEI VOXEL CANDIDATI
pArea       = options.aif.pArea;
[nR,nC,nZ,nT]=size(concData);
if options.display > 0
    disp('   Candidate voxel analysis')
end
% 2.1) Selezione a causa dell'area sotto la curva.
totCandidati=sum(sum(sum(ROI)));
totCandidatiDaTenere=ceil(totCandidati.*(1-pArea));
AUC=sum(concData,4); % calcolo l'AUC di ogni voxel.
AUC=AUC.*ROI;
AUC(isinf(AUC))=0;

ciclo=true;
nCiclo=0;
AUCdown=min(min(min(AUC)));
AUCup=max(max(max(AUC)));
while ciclo
    nCiclo=nCiclo+1;
    soglia=0.5*(AUCup+AUCdown);
    nCandidati=sum(sum(sum(AUC>soglia)));
    
    if nCandidati==totCandidatiDaTenere
        ciclo=false;
    elseif nCandidati>totCandidatiDaTenere
        AUCdown=soglia;
    else
        AUCup=soglia;
    end
    if ((AUCup-AUCdown)<0.01)||(nCiclo>100)
        ciclo=false;
    end
end


ROIauc=2.*ROI-ROI.*(AUC>soglia); % Vale 2 per i voxel scartati, 1 per quelli tenuti.
if options.display > 2 
    disp(' ')
    disp(' Candidate voxel selection via AUC criteria')
    disp(['  Voxel initial amount: ' num2str(totCandidati)])
    disp(['  Survived voxels:      ' num2str(sum(sum(sum(ROI))))])
    
    if isfield(options,'hfAif')
        figure(options.hfAif);
    else
        options.hfAif=figure();
    end
    subplot(2,3,1)
    imagesc(ROIauc(:,:,options.aif.nSlice))
    title('AUC criteria - survived voxel')
    set(gca,'xtick',[],'ytick',[])
    axis square
    
    subplot(2,3,4)
    plot(options.time,zeros(1,nT),'b-')
    hold on
    plot(options.time,zeros(1,nT),'r-')
    plot(options.time,zeros(1,nT),'k-')
    
    for c=1:nC
        for r=1:nR
            for z=1:nZ
                if ROIauc(r,c,z)==2
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'b-')
                end
            end
        end
    end
    for c=1:nC
        for r=1:nR
            for z=1:nZ
                if ROIauc(r,c,z)==1
                    plot(options.time,reshape(concData(r,c,z,:),1,nT),'r-')
                end
            end
        end
    end
    
    set(gca,...
        'FontSize',12)
    legend('Accepted','Reiected')
    title('AUC')
    xlabel('time')
end

ROI=ROI.*(AUC>soglia);
end