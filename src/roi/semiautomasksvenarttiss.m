function [venousMask, arterialMask, arterialPxList, tissuesMask, tissuesPxList, ...
    venSquareCentre, artSquareCentre] = semiautomasksvenarttiss(time,conc,iauc,initMask,gmMask,hrT1wImg,options)
% SEMIAUTOMASKSVENARTTISS guide the user to manually identify regions with
% veins and arteries to consider for semi-automatic vessel selection. 
%   [venousMask, arterialMask, arterialPxList, tissuesMask, tissuesPxList, ...
%    venSquareCentre, artSquareCentre] = SEMIAUTOMASKSVENARTTISS(time,conc,iauc,initMask,gmMask,hrT1wImg,options)
%
% See also: AUTOVESSELMASK,AIFCONCCOMPAREMASKS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-03-16

%% Preliminary vessel mask
if ~exist('options','var') || isempty(options)
    options.waitbar=1; % 0:off, 1:on
    options.display=2;
    options.aif.nSlice = round(size(conc,3)/2); % Displayed slice for debug plots
    options.aif.pArea= 0.7000; % AUC voxel rejection fraction
    options.aif.pTTP= 0.45000; % TTP rejection fraction for vessels to tissue separation
end
[vesselROI, ~] = autovesselmask(time,conc,initMask,options);

%% Venous mask on HR T1w image
fprintf('--- Manually select Superior Sagittal Sinus (SSS) venous region ---\n');
fprintf('   1. Left-Click to place a single square on the SSS.\n');
fprintf('   2. Use arrows (<-, ->) to change slice.\n');
fprintf('   3. Place slices to follow the entire SSS over the FOV.\n');
fprintf('   4. Some slices can be skipped. The FOV is interpolated between manual slices.\n');
fprintf('   5. CTRL-Click to exit when done.\n');
[~, venManMask, venSquareCentre] = selecthighintensityregionslices(hrT1wImg, [], 1, [], 'squaresInterp',[1 99],[]);
sssThreshold = prctile(hrT1wImg(venManMask),50);
venousMask = ((hrT1wImg > sssThreshold) & venManMask);

%% Manual venous mask
maskHighlights(1).name = 'Vessels';
maskHighlights(1).mask = vesselROI;
% fprintf('--- Manually select Superior Sagittal Sinus (SSS) venous region ---\n');
% fprintf('   1. Black dots indicates vessel like voxels in terms of TTP and AUC.\n');
% fprintf('   2. Left-Click to place a single square on about 20 slices.\n');
% fprintf('   3. Avoid venous inflow by choosing lower slices if top of the head is out of the field of view.\n');
% fprintf('   4. Use arrows (<-, ->) to change slice.\n');
% fprintf('   5. CTRL-Click to exit when done.\n');
% 
% [~, venManMask, venSquareCentre] = selecthighintensityregionslices(iauc, maskHighlights, 1, [], 'squares',[1 95],initMask);
% venousMask = venManMask & vesselROI;
%% Manual arterial mask
fprintf('--- Manually select arterial regions ---\n');
fprintf('   1. Left-Click to place about 20 squares over arteries far from inflow effect.\n');
fprintf('   2. Open the subject reference average DCE (meanDCE_m.nii) with the structural T1w overlay (Struct_T1w.nii) on ITK-Snap.\n');
fprintf('   3. Compare the subject arteries location with anatomical references (e.g. e-Anatomy - Arteries of brain).\n');
fprintf('   4. Avoid arterial inflow by choosing upper slices far from circle of Willis.\n');
fprintf('   5. Select downstream segments of the MCA, ACA and PCA.\n');
fprintf('   6. Use arrows (<-, ->) to change slice.\n');
fprintf('   7. CTRL-Click to exit when done.\n');
[~, artManMask, artSquareCentre] = selecthighintensityregionslices(iauc, maskHighlights, 1, [], 'squares',[1 90],initMask);
arterialMask = artManMask & vesselROI;
%% Automatic surrounding tissues
% From gray matter mask
[tissuesMask, tissuesPxList,arterialPxList] = findsurroundingtissue(iauc,conc,gmMask,arterialMask);
end
