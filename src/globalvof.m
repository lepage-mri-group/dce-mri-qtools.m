function [fitVof,clusterVof, allVof, venConcCalc] = globalvof(...
    time,magDyn,phaseDyn,T1Map,B1Map,...
    bloodParamTracer,seqParam,venousMask,vofOptions,conc)
% GLOBALVOF Single function call to compute the venous output function 
% (VOF) globally. Computation steps are:
%   - Venous contrast agent (CA) concentration calculation with complex
%   fitting.
%   - Venous voxel clustering
%   - VOF Horsfield model fitting
%
%   [fitVof,clusterVof, allVof, venConcCpx] = GLOBALVOF(time,magDyn,phaseDyn,T1Map,B1Map,bloodParamTracer,seqParam,venousMask,vofOptions,conc)
%       Computes the complex form VOF with provided options structure.
%       Basic conc calulation (conc) is provided as a sanity check on the
%       results.
%
% See also: GLOBALAIF,FITAIF, AIFPVEDEFINEOPTIONS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-09-16

[nX,nY,nZ,nT] = size(magDyn);
if ~isfield(vofOptions,'split') || ~isfield(vofOptions.split,'sssB0Ang') || isempty(vofOptions.split.sssB0Ang)
    sssB0Ang = zeros(nZ,1);
else
    sssB0Ang = vofOptions.split.sssB0Ang;
end
noSssB0 = [];
%% Reduce voxels in SSS
iaucMag = mean(magDyn(:,:,:,vofOptions.split.iaucRangeIdx(1):vofOptions.split.iaucRangeIdx(2)),4);
maskPhaseIauc = reducemaskiauc(iaucMag,venousMask,vofOptions.split.nKeptPerSlice);

%% Venous conc calculation
switch lower(vofOptions.conc.type)
    case 'complex_split'
        [bkgCorrPhase, angCorrPhase,venConcCalc.maskPhase,venConcCalc.phaseCurve,venConcCalc.phaseCurveErr,debugInfo] = aifphasecorr(...
            iaucMag,phaseDyn,maskPhaseIauc,vofOptions.split.phase.gmMask,...
            vofOptions.split.framesPre,vofOptions.split.iaucRangeIdx,sssB0Ang,vofOptions.split.phase.B0AngThresh,...
            vofOptions.split.phase.gapIdx,vofOptions.split.phase.limitGapPhase,...
            vofOptions.split.phase.nMinSlices,vofOptions.split.phase.nMaxSlices,vofOptions.split.phase.maxVoxelsPerSlice,vofOptions.split.phase.debug);
        venConcCalc.vesselPxls = debugInfo.vesselPxls;
        venConcCalc.neighPxls = debugInfo.neighPxls;
        venConcCalc.maskPhaseFinal = debugInfo.maskPhaseB0AngMaxEnh;
        
        [venConcCalc.maskMag] = vofmaginflowslab(magDyn,maskPhaseIauc,T1Map,...
            vofOptions.split.cerebellumFov,vofOptions.split.mag.slabSlices,vofOptions.split.mag.inflowSlices,vofOptions.split.mag.minMagSlices,vofOptions.split.mag.minT1Blood);
        
        validVenousMask = venConcCalc.maskMag;
        sssB0AngMat = [];%repmat(permute(sssB0Ang,[2 3 1]),[nX,nY,1]);
        singlePhaseMap = repmat(reshape(venConcCalc.phaseCurve,[1 1 1 nT]),[nX,nY,nZ]);
        [venConcCalc.conc, venConcCalc.outliers,venConcCalc.fa,venConcCalc.density,venConcCalc.phase0] = concmrispgrcpx(magDyn,singlePhaseMap,...
            bloodParamTracer,T1Map,seqParam,validVenousMask,B1Map,sssB0AngMat,time);
        excludeVenous = filtercpxsolutions(time,venConcCalc,validVenousMask,conc,vofOptions.cluster.aif.iaucRangeIdx(2));
    case 'complex'
        [bkgCorrPhase, angCorrPhase,venConcCalc.maskPhase,~,~,debugInfo] = aifphasecorr(...
            iaucMag,phaseDyn,maskPhaseIauc,vofOptions.split.phase.gmMask,...
            vofOptions.split.framesPre,vofOptions.split.iaucRangeIdx,sssB0Ang,vofOptions.split.phase.B0AngThresh,...
            vofOptions.split.phase.gapIdx,vofOptions.split.phase.limitGapPhase,...
            vofOptions.split.phase.nMinSlices,vofOptions.split.phase.nMaxSlices,vofOptions.split.phase.maxVoxelsPerSlice,vofOptions.split.phase.debug);
        venConcCalc.vesselPxls = debugInfo.vesselPxls;
        venConcCalc.neighPxls = debugInfo.neighPxls;
        
        [venConcCalc.maskMag] = vofmaginflowslab(magDyn,venConcCalc.maskPhase,T1Map,...
            vofOptions.split.cerebellumFov,vofOptions.split.mag.slabSlices,vofOptions.split.mag.inflowSlices,vofOptions.split.mag.minMagSlices,vofOptions.split.mag.minT1Blood);
        
        validVenousMask = venConcCalc.maskMag;
        sssB0AngMat = [];%repmat(permute(sssB0Ang,[2 3 1]),[nX,nY,1]);
        venConcCalc.phaseCorr = angCorrPhase;
        [venConcCalc.conc, venConcCalc.outliers,venConcCalc.fa,venConcCalc.density,venConcCalc.phase0] = concmrispgrcpx(magDyn,angCorrPhase,...
            bloodParamTracer,T1Map,seqParam,validVenousMask,B1Map,sssB0AngMat,time);
        
        excludeVenous = filtercpxsolutions(time,venConcCalc,validVenousMask,conc,vofOptions.cluster.aif.iaucRangeIdx(2));
    case 'complex_inflow'
        [bkgCorrPhase, angCorrPhase,venConcCalc.maskPhase,~,~,debugInfo] = aifphasecorr(...
            iaucMag,phaseDyn,maskPhaseIauc,vofOptions.split.phase.gmMask,...
            vofOptions.split.framesPre,vofOptions.split.iaucRangeIdx,sssB0Ang,vofOptions.split.phase.B0AngThresh,...
            vofOptions.split.phase.gapIdx,vofOptions.split.phase.limitGapPhase,...
            vofOptions.split.phase.nMinSlices,vofOptions.split.phase.nMaxSlices,vofOptions.split.phase.maxVoxelsPerSlice,vofOptions.split.phase.debug);
        
        venConcCalc.vesselPxls = debugInfo.vesselPxls;
        venConcCalc.neighPxls = debugInfo.neighPxls;
        
        [venConcCalc.maskMag] = vofmaginflowslab(magDyn,venConcCalc.maskPhase,T1Map,...
            vofOptions.split.cerebellumFov,vofOptions.split.mag.slabSlices,vofOptions.split.mag.inflowSlices,vofOptions.split.mag.minMagSlices,vofOptions.split.mag.minT1Blood);
        
        validVenousMask = venConcCalc.maskMag;
        sssB0AngMat = [];%repmat(permute(sssB0Ang,[2 3 1]),[nX,nY,1]);
        venConcCalc.phaseCorr = angCorrPhase;
        [venConcCalc.conc, venConcCalc.outliers,venConcCalc.rRf,venConcCalc.density,venConcCalc.phase0] = concmrispgrcpxinflow(magDyn,angCorrPhase,...
            bloodParamTracer,T1Map,seqParam,validVenousMask,B1Map,sssB0AngMat,time);
        
        excludeVenous = false(nX,nY,nZ);
        % Does not work for major inflow
%         excludeVenous = filtercpxsolutions(time,venConcCalc,validVenousMask,conc,vofOptions.cluster.aif.iaucRangeIdx(2));
    case 'open-form'
        signal0 = mean(magDyn(:,:,:,bloodParamTracer.framesBaseline(1):bloodParamTracer.framesBaseline(2)),4);
        [venConcCalc.conc1, venConcCalc.outliers,venConcCalc.conc2] = concmrispgropen(...
            magDyn,signal0,bloodParamTracer,T1Map,...
            seqParam,maskPhaseIauc,B1Map);
        venConcCalc.conc = venConcCalc.conc1;
        excludeVenous = any(venConcCalc.outliers,4);
        validVenousMask = maskPhaseIauc;
    case 'magnitude'
        [venConcCalc.maskMag] = vofmaginflowslab(magDyn,maskPhaseIauc,T1Map,...
            vofOptions.split.cerebellumFov,vofOptions.split.mag.slabSlices,vofOptions.split.mag.inflowSlices,vofOptions.split.mag.minMagSlices,vofOptions.split.mag.minT1Blood);
        
        validVenousMask = venConcCalc.maskMag;
        signal0 = mean(magDyn(:,:,:,bloodParamTracer.framesBaseline(1):bloodParamTracer.framesBaseline(2)),4);
        [venConcCalc.conc, venConcCalc.outliers] = concmrispgrclosed(magDyn(:,:,:,:,1), signal0,...
            bloodParamTracer,T1Map,seqParam,validVenousMask,B1Map);
%         venConcCalc.conc = conc;
%         venConcCalc.outliers = isnan(conc);
        excludeVenous = any(venConcCalc.outliers,4);
%     case 'phase'
%         [venConcCalc.bkgCorrPhase,~,~,~,~,venConcCalc.debugInfo] = aifphasecorr(...
%             phaseDyn,venousMask,vofOptions.split.phase.gmMask,...
%             vofOptions.split.framesPre,vofOptions.split.iaucRangeIdx,sssB0Ang,vofOptions.split.phase.B0AngThresh,...
%             vofOptions.split.phase.nMinSlices,vofOptions.split.phase.nMaxSlices,vofOptions.split.phase.maxVoxelsPerSlice,vofOptions.split.phase.debug);
%         venConcCalc.vesselPxls = venConcCalc.debugInfo.vesselPxls;
%         venConcCalc.neighPxls = venConcCalc.debugInfo.neighPxls;
%         venConcCalc.maskPhase = venousMask;
%         
%         bkgPhaseDyn0 = mean(venConcCalc.bkgCorrPhase(:,:,:,bloodParamTracer.framesBaseline(1):bloodParamTracer.framesBaseline(2)),4);
%         
%         validVenousMask = venConcCalc.maskPhase;
%         [venConcCalc.conc, ~] = concmrispgrphase(venConcCalc.bkgCorrPhase,bkgPhaseDyn0,bloodParamTracer,noSssB0,seqParam,validVenousMask);
%         venConcCalc.outliers = isnan(conc);
%         excludeVenous = any(venConcCalc.outliers,4);
    case 'phase'
        [venConcCalc.bkgCorrPhase,venConcCalc.angCorrPhase,venConcCalc.maskPhase,~,~,venConcCalc.debugInfo] = aifphasecorr(...
            iaucMag,phaseDyn,maskPhaseIauc,vofOptions.split.phase.gmMask,...
            vofOptions.split.framesPre,vofOptions.split.iaucRangeIdx,sssB0Ang,vofOptions.split.phase.B0AngThresh,...
            vofOptions.split.phase.gapIdx,vofOptions.split.phase.limitGapPhase,...
            vofOptions.split.phase.nMinSlices,vofOptions.split.phase.nMaxSlices,vofOptions.split.phase.maxVoxelsPerSlice,vofOptions.split.phase.debug);

        venConcCalc.vesselPxls = venConcCalc.debugInfo.vesselPxls;
        venConcCalc.neighPxls = venConcCalc.debugInfo.neighPxls;
        
        angPhaseDyn0 = mean(venConcCalc.angCorrPhase(:,:,:,bloodParamTracer.framesBaseline(1):bloodParamTracer.framesBaseline(2)),4);
        
        validVenousMask = venConcCalc.maskPhase;
        [venConcCalc.conc, ~] = concmrispgrphase(venConcCalc.angCorrPhase,angPhaseDyn0,bloodParamTracer,noSssB0,seqParam,validVenousMask);
        venConcCalc.outliers = isnan(conc) ;
        excludeVenous = any(venConcCalc.outliers,4);
    otherwise
        warning('Unknown VOF calculation type. Trying to compute VOF with provided concentration data (type = basic).');
        venConcCalc.conc = conc;
        venConcCalc.outliers = isnan(conc);
        excludeVenous = any(venConcCalc.outliers,4);
        validVenousMask = maskPhaseIauc;
end


%% Mask based on complex fitting procedure
% Exclude VOF voxels with initial concentration over zero
keptVenousMask = (validVenousMask & ~excludeVenous);
if sum(keptVenousMask(:)) == 0
    warning('VOF concentration calculation results in outliers. Trying to continue with all voxels.');
    keptVenousMask = validVenousMask;
end

%% Average and cluster
[clusterVof, allVof] = aifconccomparemasks(...
    venConcCalc.conc,keptVenousMask,keptVenousMask,vofOptions.cluster,venConcCalc.outliers);

%% Fit Horsfield VOF
batShiftStep = 0.5/60;
[bat, batOutput] = batcheong(allVof.mean(allVof.meanNotSat),time(allVof.meanNotSat),bloodParamTracer.framesBaseline(2),batShiftStep,allVof.excludeConc);
batIdx = find(time>bat,1,'first')-1;
if isempty(batIdx)
    warning('Unknown BAT. VOF might be badly conditionned.');
    batIdx = 1;
    if batOutput.peakIdx<=batIdx
        batOutput.peakIdx = 2;
    end
end
[vofFHandle,lb,ub,x0, fitOptions] = initfitaifhorsfield(time(allVof.meanNotSat),allVof.mean(allVof.meanNotSat), batIdx,batOutput.peakIdx,vofOptions.fit);

% use2ndDeriv = true;
% [~, rangeVof, ~] = fitinitialenhancement(time,allVof.mean,bloodParamTracer.framesBaseline(2),[],[], use2ndDeriv);
% [vofFHandle,lb,ub,x0, fitOptions] = initfitaifhorsfield(time,allVof.mean, rangeVof(2),rangeVof(5),vofOptions.fit);

fixedAmplitude = true(1,11);fixedAmplitude(11) = false;
fitVof.param.FHandle = vofFHandle;
fitVof.param.x0 = x0;
fitVof.param.lb = lb;
fitVof.param.ub = ub;
fitVof.param.fitOptions = fitOptions;
fitVof.param.fixedAmplitude = fixedAmplitude;
[fitVof.pValues, fitVof.resnorm,fitVof.minXIdx,fitVof.conc] = ...
    fitaif(time,allVof.mean,vofFHandle,x0,lb,ub,fitOptions,fixedAmplitude);

end

function maskPhaseIauc = reducemaskiauc(iaucMag,venousMask,nKeptPerSlice)
maskPhaseIauc = venousMask;
nZ = size(venousMask,3);
for iSlice = 1:nZ
    nVoxels = sum(sum(venousMask(:,:,iSlice)));
    if nVoxels>nKeptPerSlice
        cMagSlice = iaucMag(:,:,iSlice);
        cMask = venousMask(:,:,iSlice);
        [~,sortedIdx] = sort(cMagSlice(cMask),1,'descend');
        keptVoxels = false(nVoxels,1);
        keptVoxels(sortedIdx(1:nKeptPerSlice)) = true;
        cMask(cMask) = keptVoxels;
        maskPhaseIauc(:,:,iSlice) = cMask;
    end
end
end

function excludeVenous = filtercpxsolutions(time,venConcCalc,venousMask,conc,peakIdxEnd)

% Complex fitting allows absurd solutions. Remove max conc under zero with a 0.1 mM threshold
zeroConcThresh = 0.1; % mM
concUnderZero = any(venConcCalc.outliers,4) | ...
    (max(venConcCalc.conc(:,:,:,:),[],4) < zeroConcThresh);
%             (abs(mean(venConcCalc.conc(:,:,:,bloodParamTracer.framesBaseline(1):bloodParamTracer.framesBaseline(2)),4)) > zeroConcThresh) | ...
saturationThresh = 0.5; % mM
% Removes post peak concentrations diverging from basic concentration calculation
nT = length(time);
venousAfterPeak = repmat(venousMask,[1,1,1,nT]);
venousAfterPeak(:,:,:,1:(peakIdxEnd+5)) = false;
divergedFromRefConc = false(size(venousAfterPeak));
divergedFromRefConc(venousAfterPeak) = abs(venConcCalc.conc(venousAfterPeak) - conc(venousAfterPeak))>saturationThresh;
excludeVenous = concUnderZero | any(divergedFromRefConc,4);
if sum(sum(sum(~excludeVenous))) == 0
    warning('Cpx concentration diverges strongly with basic concentration for all points. Basic concentration matching criteria was removed to keep VOF points.');
    excludeVenous = concUnderZero;
end
end

function [maskMagVenousInflowIauc] = vofmaginflowslab(...
    magDyn,venousMask,T1Map,isCerebellumFov,nSlabSlices,nInflowSlices,minMagSlices,minT1Blood)
nZ = size(magDyn,3);
voxelsPerSlices = squeeze(sum(sum(venousMask,1),2));
slicesWithVoxels = (voxelsPerSlices>0);
%% Magnitude voxel selection
% Select mag slices
minDceMagSlice = 1+nSlabSlices;
if isCerebellumFov % Exclude slices to avoid inflow
    maxDceMagSlice = (nZ-nInflowSlices);
else % Exclude slices for slice profile
    maxDceMagSlice = (nZ-nSlabSlices);
end
keptMagSlices = false(nZ,1);
if sum(slicesWithVoxels(minDceMagSlice:maxDceMagSlice)) >= minMagSlices
    keptMagSlices(minDceMagSlice:maxDceMagSlice) = slicesWithVoxels(minDceMagSlice:maxDceMagSlice);
elseif sum(slicesWithVoxels)<= minMagSlices
    keptMagSlices = slicesWithVoxels;
else
    nMissingSlices = minMagSlices - sum(slicesWithVoxels(minDceMagSlice:maxDceMagSlice));
    keptMagSlices(minDceMagSlice:maxDceMagSlice) = slicesWithVoxels(minDceMagSlice:maxDceMagSlice);
    slicesCentre = mean(find(keptMagSlices));
    if isempty(slicesCentre) || isnan(slicesCentre)
        slicesCentre = (maxDceMagSlice+minDceMagSlice)/2;
    end
    otherSlicesWithVoxels = 1:nZ;
    otherSlicesWithVoxels(~slicesWithVoxels) = inf;
    otherSlicesWithVoxels(minDceMagSlice:maxDceMagSlice) = inf;
    [~,addSliceIdx] = sort(abs(otherSlicesWithVoxels-slicesCentre));
    keptMagSlices(addSliceIdx(1:nMissingSlices)) = slicesWithVoxels(addSliceIdx(1:nMissingSlices));
end
maskMag = venousMask;
maskMag(:,:,~keptMagSlices) = false;


% Removed - Poor results with AUC pre-selection
% keptMagIaucFraction = splitCpxOptions.mag.voxelsPerSlice/mean(voxelsPerSlices(keptMagSlices));
% 
% % Keep voxels with maximum IAUC
% [listBasicConc, ~] = dyn3dconversion(conc,maskMag);
% bolusIauc = mean(listBasicConc(:,splitCpxOptions.framesPre(2):splitCpxOptions.iaucRangeIdx(2)),2);
% iaucThreshold = prctile(bolusIauc,100*(1-keptMagIaucFraction));
% isOverIaucThresh = bolusIauc > iaucThreshold;
% 
%% Prepare output masks
% maskMagVenousInflowIauc = false(nX,nY,nZ);
% maskMagVenousInflowIauc(maskMag) = isOverIaucThresh;

% T1 values decrease posterior to SSS caused by bone proximity. Exclude low blood T1 values
nMinVox = 5;
t1OfVessel = T1Map(maskMag);
nMagVox = sum(maskMag(:));
if nMagVox<=nMinVox
    maskMagVenousInflowIauc = maskMag;
elseif sum(t1OfVessel >= minT1Blood)>=nMinVox
    maskMagVenousInflowIauc = (maskMag & (T1Map >= minT1Blood));
else
    [~,t10Idx] = sort(t1OfVessel,1,'descend');
    keptT10 = false(nMagVox,1);
    keptT10(t10Idx(1:nMinVox)) = true;
    maskMagVenousInflowIauc = maskMag;
    maskMagVenousInflowIauc(maskMag) = keptT10;
end
end