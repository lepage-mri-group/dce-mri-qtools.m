function [tACQdata, tTissueSignal, tTissueConc, tParamMap] = ...
    drotile(qibaTileOrder,ACQdata, tissueSignal, tissueConc, paramMap)
% DROTILE Tile the digital reference object (DRO) parameters to reduce
% dimension. qibaTileOrder is a cell of a length corresponding to the N 
% tiled dimensions where each elements are a vector of parameter index that 
% are tiled in the dimension.
%
% [tACQdata, tTissueSignal, tTissueConc, tParamMap] = DROTILE(
%   qibaTileOrder,ACQdata, tissueSignal, tissueConc, paramMap) Tiles the DRO.
%
% See also: DROCREATE
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-30

%% Find tiled dimensions
paramSize = size(paramMap);
nParams = paramSize(1);
paramLength = paramSize(2:(nParams+1));
signalSize = size(ACQdata);
nT = signalSize(1);
nE = signalSize(2);
acqLength = signalSize(3:(nParams+2));

nNoise = acqLength(1)/paramLength(1);

nTiledDims = length(qibaTileOrder);
nParamPerDims = zeros(1,nTiledDims);
for iDims = 1:nTiledDims
    nParamPerDims(iDims) = prod(paramLength(qibaTileOrder{iDims}));
end
% Keep the noise dimensions on 1 and 2
nAcqPerDims = nParamPerDims;
nAcqPerDims(1) = nNoise*nParamPerDims(1);
nAcqPerDims(2) = nNoise*nParamPerDims(2);

%% Transfer data
% Initialize tiled matrix
tACQdata = NaN([nT,nE,nAcqPerDims]);
tTissueSignal = NaN([nT,nE,nParamPerDims]);
tTissueConc = NaN([nT,nParamPerDims]);
tParamMap = NaN([nParams,nParamPerDims]);

nParameterCombinations = prod(paramLength);
paramIdx = ones(1,8);
tParamIdx = ones(1,8);
acqIdx = cell(1,8); acqIdx(:) = {1};
tAcqIdx = cell(1,8); tAcqIdx(:) = {1};
FHandleRecursiveIndex = @(iLast,iNext,nNext) nNext*(iLast-1)+iNext;
for iComb = 1:nParameterCombinations
    paramIdx(1:nParams) = findindexfromprod(iComb,paramLength);
    acqIdx{1} = nNoise*(paramIdx(1)-1) + (1:nNoise);
    acqIdx{2} = nNoise*(paramIdx(2)-1) + (1:nNoise);
    for iParam = 3:nParams
        acqIdx{iParam} = paramIdx(iParam);
    end
    for iDims = 1:nTiledDims
        cParamIdx = qibaTileOrder{iDims};
        nCIdx = length(cParamIdx);
        if nCIdx == 1
            tParamIdx(iDims) = paramIdx(cParamIdx(1));
        else
            tParamIdx(iDims) = paramIdx(cParamIdx(1));
            for iCdx = 1:(nCIdx-1)
                tParamIdx(iDims) = ...
                    FHandleRecursiveIndex(tParamIdx(iDims),paramIdx(cParamIdx(iCdx+1)),paramLength(cParamIdx(iCdx+1)));
            end
        end
    end
    tAcqIdx{1} = nNoise*(tParamIdx(1)-1) + (1:nNoise);
    tAcqIdx{2} = nNoise*(tParamIdx(2)-1) + (1:nNoise);
    for iParam = 3:nParams
        tAcqIdx{iParam} = tParamIdx(iParam);
    end
    tParamMap(:,tParamIdx(1),tParamIdx(2),tParamIdx(3),tParamIdx(4),...
        tParamIdx(5),tParamIdx(6),tParamIdx(7),tParamIdx(8)) = ...
        paramMap(:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8));
    tTissueConc(:,tParamIdx(1),tParamIdx(2),tParamIdx(3),tParamIdx(4),...
        tParamIdx(5),tParamIdx(6),tParamIdx(7),tParamIdx(8)) = ...
        tissueConc(:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8));
    tTissueSignal(:,:,tParamIdx(1),tParamIdx(2),tParamIdx(3),tParamIdx(4),...
        tParamIdx(5),tParamIdx(6),tParamIdx(7),tParamIdx(8)) = ...
        tissueSignal(:,:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8));
    tACQdata(:,:,tAcqIdx{1},tAcqIdx{2},tAcqIdx{3},tAcqIdx{4},...
        tAcqIdx{5},tAcqIdx{6},tAcqIdx{7},tAcqIdx{8}) = ...
        ACQdata(:,:,acqIdx{1},acqIdx{2},acqIdx{3},acqIdx{4},...
        acqIdx{5},acqIdx{6},acqIdx{7},acqIdx{8});
end
