function [acqData, tissueSignal, tissueConc, paramMap, aifModel,modelOutParams] = ...
    drocreate(time,aifModel,pkModel,imgModel) 
% DROCREATE Create a digital reference object (DRO) for defined arterial 
% input function, pharmacokinetics and image models. 
%
% [acqData, tissueSignal, tissueConc, paramMap, aifModel,modelOutParams] = DROCREATE(time,aifModel,pkModel,imgModel) creates the default DRO.
%
% See also: AIFPVEDROCREATE, AIFHORSFIELD, PKMETOFTS, SIGNALMRISPGR,
% NOISEMRI, DROTILE, DEFAULTMODELPARAMS
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-28
%% Check input data
if isrow(time)
    time = time';
end

%% Generate blood arterial input function (AIF)
aifModel.AIFFit = aifModel.FHandle(time,aifModel.pValues);
aifModel.AIFPlasma = aifModel.AIFFit/(1-pkModel.hematocrit);
n0 = find(time>aifModel.pValues(1),1,'first');

%% Generate the tissue concentration and signal
% Initialization
nT = length(time);

nParams = length(pkModel.paramNames);
maxParams = 8;
if nParams>maxParams
    error('Number of parameters is superior to the limit of the DRO (maxParams = 8). If this needs to be increased change value in source code and change indexing accordingly in the iteration loop.');
end

paramLength = NaN(1,nParams);
paramIdx = ones(1,maxParams);
for iParam = 1:nParams
    paramLength(iParam) = length(pkModel.(pkModel.paramNames{iParam}));
end
nParameterCombinations = prod(paramLength);

paramMap = NaN([nParams,paramLength]);
tissueConc = NaN([nT, paramLength]);
tissueSignal = NaN([nT, 1, paramLength]);

% Iterate over all parameter combinations
if isfield(aifModel,'concIsPlasma') && aifModel.concIsPlasma
    patientParam.hematocrit = 0;
else
    patientParam.hematocrit = pkModel.hematocrit;
end
patientParam.brainDensity = pkModel.brainDensity; % g/mL
for iComb = 1:nParameterCombinations
    paramIdx(1:nParams) = findindexfromprod(iComb,paramLength);
    cParamsValues = combinestructparam(pkModel,paramIdx(1:nParams),nParams);
    % Compute current concentration
    cConc = pkModel.FHandle(time,aifModel,patientParam,[],cParamsValues);
    % Signal
    imgModel.tissueParam.units = aifModel.units;
    if isequal(imgModel.FHandle,@signalmrispgrmeso)
        if isfield(pkModel,'Ve') && isfield(pkModel,'Vp')
            veIdx = find(strcmp(pkModel.paramNames,'Ve'));
            vpIdx = find(strcmp(pkModel.paramNames,'Vp'));
            % Tissue Vp and Ve changes at each iterations
            imgModel.tissueParam.ve = pkModel.Ve(paramIdx(veIdx))*(patientParam.brainDensity/100);
            imgModel.tissueParam.vp = pkModel.Vp(paramIdx(vpIdx))*(patientParam.brainDensity/100);
        end
        if isfield(pkModel,'macrovascFraction') && ~isempty(pkModel.macrovascFraction) && pkModel.macrovascFraction>0
            keptCConc = imgModel.tissueParam.vp*cConc(:,1) + imgModel.tissueParam.ve*cConc(:,2)+ pkModel.macrovascFraction*aifModel.AIFFit;
        else
            keptCConc = imgModel.tissueParam.vp*cConc(:,1) + imgModel.tissueParam.ve*cConc(:,2);
        end
    else
        if isfield(pkModel,'macrovascFraction') && ~isempty(pkModel.macrovascFraction) && pkModel.macrovascFraction>0
            keptCConc = cConc + pkModel.macrovascFraction*aifModel.AIFFit;
        else
            keptCConc = cConc;
        end
    end
    [cSignal, imgModel.tissueParam] = imgModel.FHandle(cConc,n0,...
        imgModel.tissueParam,imgModel.seqParam);
    % Assign maps
    paramMap(:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8)) = cParamsValues';
    
    tissueConc(:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8)) = keptCConc;
    
    tissueSignal(:,:,paramIdx(1),paramIdx(2),paramIdx(3),paramIdx(4),...
        paramIdx(5),paramIdx(6),paramIdx(7),paramIdx(8)) = cSignal;
end

%% Add noise to signal
acqData = imgModel.noiseParam.FHandle(tissueSignal,imgModel.noiseParam);

modelOutParams.aifModel = aifModel;
modelOutParams.pkModel = pkModel;
modelOutParams.imgModel = imgModel;
end

function paramValues = combinestructparam(modelInParams,indvidualParamIndices,nParams)
paramValues = ones(1,nParams);
for iParam = 1:nParams
    paramValues(iParam) = modelInParams.(modelInParams.paramNames{iParam})(indvidualParamIndices(iParam));
end
end
