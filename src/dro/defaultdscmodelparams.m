function [aifModel,pkModel,imgModel] = defaultdscmodelparams(time,t0,aifModelDce,pkModelDce,imgModelDce,tissueParam,seqParam)
% DEFAULTMODELPARAMS Generates the default parameters for a MRI DSC SPGR
% digital reference object from the paramaters from the same MRI DCE SPGR object.
%
%   [aifModel,pkModel,imgModel] = DEFAULTDSCMODELPARAMS(time,t0,aifPeak,tissueParam,bloodParam,seqParam)
%       Generates the DRO with a specified AIF peak concentration, tissue
%       relaxation parameters, blood relaxation parameters and sequence
%       parameters.
%
% See also: DROCREATE, DEFAULTMODELPARAMS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-11-24


%% Default Horsfield AIF model
aifModel = aifModelDce;
nAifParam = length(aifModel.pValues);
aifModel.pValues = NaN(1,2*nAifParam);
aifModel.pValues(1:nAifParam) = aifModelDce.pValues;
aifModel.pValues(1) = t0(1);
aifModel.pValues(nAifParam+(1:nAifParam)) = aifModelDce.pValues;
aifModel.pValues(nAifParam+1) = t0(end);
aifModel.pValues(end) = aifModelDce.pValues(end);

aifModel.FHandle = @(time,pValues) (aifhorsfield(time,pValues(1:11)) + aifhorsfield(time,pValues(12:end)));

%% PKModel
pkModel = pkModelDce;
pkModel.FHandle = @pkm2cxmsplit;
%% MRI signal
% Reference tissue
imgModel.FHandle = @signalmrispgrmeso;
imgModel.tissueParam.tracer.Kp = tissueParam.KpGm*tissueParam.Kp2GmRatio;
imgModel.tissueParam.tracer.Ke = tissueParam.Ke2KpRatio*imgModel.tissueParam.tracer.Kp;
% Changes from QIBA_v4 - T2* relaxation 
imgModel.tissueParam.R20s = 1/tissueParam.T20s; % 1/ms 
% r2* -> r2
imgModel.tissueParam.tracer.r2s = tissueParam.r2; % Kjolby et al. 2006 (1/(mM*s)

imgModel.tissueParam.S0 = imgModelDce.tissueParam.S0; % QIBA4, Equilibrium magnetization. a.u
imgModel.tissueParam.R10 = imgModelDce.tissueParam.R10; % QIBA4, 1/ms 
imgModel.tissueParam.tracer.r1 = imgModelDce.tissueParam.tracer.r1; % QIBA4, 1/(mM*s)
% Blood
imgModel.bloodParam.S0 = imgModelDce.bloodParam.S0; % QIBA_v4, Equilibrium magnetization. a.u
imgModel.bloodParam.R10 = imgModelDce.bloodParam.R10;  % QIBA_v4, 1/ms
% Changes from QIBA_v4 - T2* relaxation 
imgModel.bloodParam.R20s = imgModelDce.bloodParam.R20s; % 1/ms (Half white matter T20) Stanisz et al. 2005
imgModel.bloodParam.tracer.r1 = imgModelDce.bloodParam.tracer.r1; % 1/(mM*s)
imgModel.bloodParam.tracer.r2s = imgModelDce.bloodParam.tracer.r2s; % [(s mM)^-1]
if imgModelDce.bloodParam.tracer.enableQuadR2
    imgModel.bloodParam.tracer.r22s =  imgModelDce.bloodParam.tracer.r22s; % [(s mM�)^-1]
    imgModel.bloodParam.tracer.enableQuadR2 = imgModelDce.bloodParam.tracer.enableQuadR2;
else
    imgModel.bloodParam.tracer.enableQuadR2 = false;
end
if imgModelDce.bloodParam.tracer.enablePhase
    imgModel.bloodParam.tracer.xiM = imgModelDce.bloodParam.tracer.xiM; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
    % imgModel.bloodParam.tracer.xiM = 3.20e-7; % (1/mM) = 320 ppm/M Gadobutrol, Diameter 13.5 mm, Parallel, Manganese solution (900 ms) (Korporaal, 2011)
    imgModel.bloodParam.tracer.enablePhase = true;
    imgModel.phase.FHandle = @signalmrispgrphase;
    if isfield(imgModelDce.bloodParam,'vesselTheta') && ~isempty(imgModelDce.bloodParam.vesselTheta)
        imgModel.bloodParam.theta = imgModelDce.bloodParam.vesselTheta; % radians
    else
        imgModel.bloodParam.theta = 0; % radians
    end
else
    imgModel.bloodParam.tracer.enablePhase = false;
end
nPre = find(time>(t0(end)-0.5),1,'first')-1;
n0 = find(time>t0(end),1,'first')-1;
imgModel.bloodParam.tracer.framesBaseline = [nPre n0];

% Changes from QIBA_v4 - DCE sequence parameters
imgModel.seqParam.te = seqParam.te;
imgModel.seqParam.nAcq = length(imgModel.seqParam.te);
imgModel.seqParam.tr = seqParam.tr; % ms
imgModel.seqParam.fa = seqParam.fa; % rad

imgModel.seqParam.B0 = seqParam.B0; % Tesla
imgModel.seqParam.voxelRes = seqParam.voxelRes; % mm
imgModel.seqParam.time = time;
imgModel.seqParam.t0 = t0(end);

% Changes from QIBA_v4 - Noise is added
imgModel.noiseParam.nNoise = seqParam.nNoise;
imgModel.noiseParam.seed = 700;
imgModel.noiseParam.repeatedPattern = false;
imgModel.noiseParam.FHandle = @noisemri;
imgModel.noiseParam.snr = seqParam.snr;
imgModel.noiseParam.standardDeviation = ...
    imgModel.tissueParam.S0/imgModel.noiseParam.snr;