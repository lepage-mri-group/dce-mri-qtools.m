function [figHandle, meanParam,mseParam, stdParam, cLimMap, cLimError]  = drodisplaymapserror(droMaps,droTrueMaps,droParam,aifMaskF,cLimMap,cLimError,showColorBar,subPlotHandles,transposeDisplay)
% DRODISPLAYMAPSERROR Display the DRO parameters and their percent errors
% against their true values.
%
% [figHandle, meanParam,biasParam, stdParam, cLimMap, cLimError]  = DRODISPLAYMAPSERROR(droMaps,droTrueMaps,droParam,aifMaskF,cLimMap,cLimError) 
%
% See also: DROCREATE, DROTILE
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-28

% Divergent colormap
rT=[33 103 209 247 253 239 178]/255;
gT=[102 169 229 247 219 138 24]/255;
bT=[172 207 240 247 199 98 43]/255;
pos=linspace(0,1,7);
%Interpolates given the colormap positions
posDesired=linspace(0,1,128);
r = interp1(pos,rT,posDesired);
g = interp1(pos,gT,posDesired);
b = interp1(pos,bT,posDesired);
% cMap = [r.' g.' b.']; 
cMap = colormap('jet');

if ~exist('transposeDisplay','var') || isempty(transposeDisplay)
    transposeDisplay = false;
end

[nX,nY,~,nP] = size(droMaps);
[nXT, nYT,~] = size(droTrueMaps);
if ~exist('cLimMap','var') || isempty(cLimMap)
    computeClimMap = true;
    cLimMap = cell(1,nP);
else
    computeClimMap = false;
end
if ~exist('cLimError','var') || isempty(cLimError)
    computeClimErr = true;
    cLimError = cell(1,nP);
else
    computeClimErr = false;
end

if ~exist('showColorBar','var') || isempty(showColorBar)
    showColorBar = true;
end
if ~exist('subPlotHandles','var') || isempty(subPlotHandles)
    figHandle = figure;
    figHandle.Position = [469    50   657   859];
    createFigure = true;
else
    createFigure = false;
    figHandle = [];
end


nNoise = droParam.imgModel.noiseParam.nNoise;
aifIdx = find(all(aifMaskF~=0,2));

droXTick = (0:nNoise:nY);
nYTick = (nX-length(aifIdx))/nNoise+1+length(aifIdx);
droYTick = NaN(1,nYTick);
droYTick(1) = 0;
iPIdx = NaN(1,nXT);
iPT = 1;
for iYTick = 1:(nYTick-1)
    if any((aifIdx-1)==droYTick(iYTick))
        droYTick(iYTick+1) = droYTick(iYTick) + 1;
    else
        iPIdx(iPT) = iYTick; iPT = iPT + 1;
        droYTick(iYTick+1) = droYTick(iYTick) + nNoise;
    end
end

nVert = 2;
nHori = nP;

gap = 0.01;
marg_h = 0.08;
marg_w = 0.08;
noiseSubIdx = 1:nNoise;
meanParam = NaN(nXT,nYT,nP);
mseParam = NaN(nXT,nYT,nP);
stdParam = NaN(nXT,nYT,nP);

for iP = 1:nP
    if computeClimMap
        maxMap = prctile(reshape(droMaps(:,:,1,iP),[nX*nY, 1]),98);
        maxParam = maxMap;%max(maxMap,droParam.pkModel.(droParam.pkModel.paramNames{iP})(end));
        if maxParam == 0
            cLimMap{iP} = [0 1];
        else
            cLimMap{iP} = [0 maxParam];
        end
    end
    if createFigure || ~isempty(subPlotHandles(iP).absolute)
        if createFigure
            ax1 = subtightplot(nVert,nHori,iP,gap,marg_h,marg_w);
        else
            ax1 = subPlotHandles(iP).absolute;
            axes(ax1);
        end
        if transposeDisplay
            imagesc(droMaps(:,:,1,iP)',cLimMap{iP});
            ax1.PlotBoxAspectRatio = [nX,nY,1];
            ax1.XTick = droYTick + 0.5;
            ax1.YTick = droXTick + 0.5;
        else
            imagesc(droMaps(:,:,1,iP),cLimMap{iP});
            ax1.PlotBoxAspectRatio = [nY,nX,1];
            ax1.XTick = droXTick + 0.5;
            ax1.YTick = droYTick + 0.5;
        end
        colormap(cMap);
        ax1.XTickLabel = [];
        ax1.YTickLabel = [];
        ax1.FontWeight = 'bold';
        if createFigure
            ax1.LineWidth = 1.5;
            title([droParam.pkModel.displayNames{iP} '' droParam.pkModel.unitNames{iP}]);
        else
            ax1.LineWidth = 0.5;
        end
        if showColorBar
            c = colorbar;
            c.LineWidth = 1.5;
        end
    end
    errorMap = NaN(nX,nY);
    for iPXT = 1:nXT
        for iPYT = 1:nYT
            currentNoiseParam = droMaps(droYTick(iPIdx(iPXT))+noiseSubIdx,droXTick(iPYT)+noiseSubIdx,1,iP);
            meanParam(iPXT,iPYT,iP) = mean(currentNoiseParam(:));
            mseParam(iPXT,iPYT,iP) = sum((currentNoiseParam(:) - droTrueMaps(iPXT,iPYT,iP)).^2)/numel(currentNoiseParam);
            stdParam(iPXT,iPYT,iP) = std(currentNoiseParam(:));
            if droTrueMaps(iPXT,iPYT,iP) == 0
                errorMap(droYTick(iPIdx(iPXT))+noiseSubIdx,droXTick(iPYT)+noiseSubIdx) = NaN;                
            else
                errorMap(droYTick(iPIdx(iPXT))+noiseSubIdx,droXTick(iPYT)+noiseSubIdx) = ...
                    (currentNoiseParam - droTrueMaps(iPXT,iPYT,iP))/droTrueMaps(iPXT,iPYT,iP)*100;
            end
        end
    end
    
    if computeClimErr
        minMap = prctile(reshape(errorMap(:,:),[nX*nY, 1]),2);
        maxMap = prctile(reshape(errorMap(:,:),[nX*nY, 1]),98);
        maxParam = max(abs(minMap),maxMap);
        if maxParam==0
            cLimError{iP} = [-1 1];
        else
            cLimError{iP} = [-maxParam maxParam];
        end
    end
    if createFigure || ~isempty(subPlotHandles(iP).error)
        if createFigure
            ax1 = subtightplot(nVert,nHori,iP+nHori,gap,marg_h,marg_w);
        else
            ax1 = subPlotHandles(iP).error;
            axes(ax1);
        end
        if transposeDisplay
            imagesc(errorMap',cLimError{iP});
            ax1.PlotBoxAspectRatio = [nX,nY,1];
            ax1.XTick = droYTick + 0.5;
            ax1.YTick = droXTick + 0.5;
        else
            imagesc(errorMap,cLimError{iP});
            ax1.PlotBoxAspectRatio = [nY,nX,1];
            ax1.XTick = droXTick + 0.5;
            ax1.YTick = droYTick + 0.5;
        end
        colormap(cMap);
        ax1.XTickLabel = [];
        ax1.YTickLabel = [];
        ax1.LineWidth = 1.5;
        ax1.FontWeight = 'bold';
        %     title(['% error in ' droParam.pkModel.displayNames{iP}]);
        if showColorBar
            c = colorbar;
            c.LineWidth = 1.5;
        end
    end
end