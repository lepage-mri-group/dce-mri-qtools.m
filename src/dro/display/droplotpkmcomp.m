function [modelComp,paramComp,paramCompPlot] = droplotpkmcomp(pkmLesion1st,mapOrder,lesionDroOptions,tLesionParamMap,param2Plot,transposeDisplay)

if ~exist('transposeDisplay','var') || isempty(transposeDisplay)
    transposeDisplay = false;
end

nModels = length(pkmLesion1st);

nTParam = length(lesionDroOptions.pkModel.paramNames);
nParamLength = zeros(1,nTParam);
for iParam = 1:nTParam
    nParamLength(iParam) = length(lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{iParam}));
end

figAbsAllParamComp = figure;
figAbsAllParamComp.Position = [10    -50   265        1007];
gap = 0.01;
marg_h = 0.08;
marg_w = 0.08;
nMaxParam = nTParam;
subPlotHandles(nModels-1,nMaxParam).absolute = [];
subPlotHandles(nModels-1,nMaxParam).error = [];
errorClim = {[-100 100],[-100 100],[-100 100],[-100 100]};

%% Direct model plot
dispTParamMap = permute(tLesionParamMap,[3 2 1]);
modelComp(nModels).figHandle = [];
modelComp(nModels).mean = [];
modelComp(nModels).bias = [];
modelComp(nModels).std = [];
tParamModelIdx = zeros(nModels,nTParam);
for iModel = 2:nModels
    xMat = permute(pkmLesion1st(iModel).pValues,[2 1 3 4]);
    nParams = length(pkmLesion1st(iModel).paramNames);
    trueParamList = NaN(1,nParams);
    for iParam = 1:nParams
        if any(strcmp({'Ktrans','PS'},pkmLesion1st(iModel).paramNames{iParam}))
            paramIdxMri = find(strcmp(lesionDroOptions.pkModel.paramNames,'PS'));
            paramIdxPet = find(strcmp(lesionDroOptions.pkModel.paramNames,'K1'));
            if ~isempty(paramIdxMri)
                paramIdx = paramIdxMri;
            elseif ~isempty(paramIdxPet)
                paramIdx = paramIdxPet;
            end
            trueParamList(iParam) = paramIdx;
            tParamModelIdx(iModel,paramIdx) = iParam;
        else
            paramIdx = find(strcmp(lesionDroOptions.pkModel.paramNames,pkmLesion1st(iModel).paramNames{iParam}));
            trueParamList(iParam) = paramIdx;
            tParamModelIdx(iModel,paramIdx) = iParam;
        end
        subPlotHandles(iModel,iParam).absolute = subtightplot(nMaxParam,nModels-1,(nModels-1)*(mapOrder(paramIdx)-1)+(iModel-1),gap,marg_h,marg_w);
    end
    droTrueMaps = dispTParamMap(:,:,trueParamList);
    currentDroOptions = lesionDroOptions;
    currentDroOptions.pkModel.paramNames = lesionDroOptions.pkModel.paramNames(trueParamList);
    currentDroOptions.pkModel.displayNames = lesionDroOptions.pkModel.displayNames(trueParamList);
    currentDroOptions.pkModel.unitNames = lesionDroOptions.pkModel.unitNames(trueParamList);
    absClim  = lesionDroOptions.pkModel.paramPlotLim(trueParamList);
    aifMask = 0*droTrueMaps;
    [~, modelComp(iModel).mean,modelComp(iModel).mse,modelComp(iModel).std,cLimMapOpen,cLimErrorOpen] = drodisplaymapserror(xMat,droTrueMaps,currentDroOptions,aifMask,absClim,errorClim,false,subPlotHandles(iModel,1:nParams),transposeDisplay);
    modelComp(iModel).figHandle = figAbsAllParamComp;
end

%% Param comp
defaultPlotColors = {[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840]};
nPlotColor = length(defaultPlotColors);
nDispTParam = length(param2Plot);
if nDispTParam==0
    paramComp = [];
    paramCompPlot = [];
else
    paramComp(nDispTParam).figHandle = [];
    paramComp(nDispTParam).allMat = [];
    paramComp(nDispTParam).meanMat = [];
    paramComp(nDispTParam).stdMat = [];
    paramComp(nDispTParam).legend = [];
    paramComp(nDispTParam).figH = [];
    
    plotIndividualModel = false;
    
    gap = 0.04;
    marg_h = 0.08;
    marg_w = 0.08;
    
    paramCompPlot(nDispTParam).figH = [];
    
    nNoise = lesionDroOptions.imgModel.noiseParam.nNoise;
    [nParamX,nParamY,~] = size(modelComp(nModels).mean);
end
for iParam = 1:nDispTParam
    cTIdx = param2Plot(iParam).idx;
    modelParamIdx = tParamModelIdx(:,cTIdx);
    dispModelIdx = find(modelParamIdx>0);
    nDispModel = length(dispModelIdx);
    paramComp(iParam).legend = cell(1,nDispModel+1);
    paramComp(iParam).legend{1} = 'True';
    
    paramComp(iParam).allMat = NaN(lesionDroOptions.imgModel.noiseParam.nNoise*nParamX,lesionDroOptions.imgModel.noiseParam.nNoise*nParamY,1,nDispModel);
    paramComp(iParam).meanMat = NaN(nParamX,nParamY,nDispModel);
    paramComp(iParam).stdMat = NaN(nParamX,nParamY,nDispModel);
    cbvDroOptions.pkModel.paramNames = cell(1,nDispModel);
    cbvDroOptions.pkModel.displayNames = cell(1,nDispModel);
    cbvDroOptions.pkModel.unitNames = cell(1,nDispModel);
    prctCbvTol = cell(1,nDispModel); prcCbvTolValue = 100;
    for iModel = 1:nDispModel
        if param2Plot(iParam).normParam
            cMeanParam = squeeze(modelComp(dispModelIdx(iModel)).mean(:,:,modelParamIdx(dispModelIdx(iModel))));
            cStdParam = squeeze(modelComp(dispModelIdx(iModel)).std(:,:,modelParamIdx(dispModelIdx(iModel))));
            normModelFactor = param2Plot(iParam).normAvg(dispModelIdx(iModel));
            normModelError = param2Plot(iParam).normStd(dispModelIdx(iModel));
            paramComp(iParam).allMat(:,:,1,iModel) = permute(pkmLesion1st(dispModelIdx(iModel)).pValues(:,:,:,modelParamIdx(dispModelIdx(iModel))),[2 1 3 4])/normModelFactor;
            paramComp(iParam).meanMat(:,:,iModel) = cMeanParam/normModelFactor;
            paramComp(iParam).stdMat(:,:,iModel) = abs(paramComp(iParam).meanMat(:,:,iModel)).*...
                sqrt((cStdParam./cMeanParam).^2+(normModelError/normModelFactor)^2);
        else
            paramComp(iParam).allMat(:,:,1,iModel) = permute(pkmLesion1st(dispModelIdx(iModel)).pValues(:,:,:,modelParamIdx(dispModelIdx(iModel))),[2 1 3 4]);
            paramComp(iParam).meanMat(:,:,iModel) = squeeze(modelComp(dispModelIdx(iModel)).mean(:,:,modelParamIdx(dispModelIdx(iModel))));
            paramComp(iParam).stdMat(:,:,iModel) = squeeze(modelComp(dispModelIdx(iModel)).std(:,:,modelParamIdx(dispModelIdx(iModel))));
        end
        paramComp(iParam).legend{iModel+1} = pkmLesion1st(dispModelIdx(iModel)).name;
        
        cbvDroOptions.pkModel.paramNames{iModel} = lesionDroOptions.pkModel.paramNames{cTIdx};
        cbvDroOptions.pkModel.displayNames{iModel} = pkmLesion1st(dispModelIdx(iModel)).name;
        cbvDroOptions.pkModel.unitNames{iModel} = '';
        prctCbvTol{iModel} = prcCbvTolValue*[-1 1];% %
    end
    cbvDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx}) = currentDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx});
    cbvDroOptions.imgModel.noiseParam.nNoise = currentDroOptions.imgModel.noiseParam.nNoise;
    
    currentTrueMaps = repmat(dispTParamMap(:,:,cTIdx),1,1,(nDispModel));
    aifMask = 0*currentTrueMaps;
    if param2Plot(iParam).normParam
        normTrueFactor = param2Plot(iParam).normTrue;
        currentTrueMaps = currentTrueMaps/normTrueFactor;
        cbvDroOptions.pkModel.Vp = cbvDroOptions.pkModel.Vp/normTrueFactor;
    else
        normTrueFactor = 1;
    end
    if isfield(param2Plot(iParam),'absClim') && ~isempty(param2Plot(iParam).absClim)
        absClim = param2Plot(iParam).absClim(dispModelIdx);
    else
        absClim = [];
    end
    [paramComp(iParam).figH,~,~,~,~,~] = drodisplaymapserror(paramComp(iParam).allMat,currentTrueMaps,cbvDroOptions,aifMask,absClim,prctCbvTol,false,[],transposeDisplay);
    paramComp(iParam).figH.Position = [467   131   488   829];
    
    %% Plot error bars
    trueNoiseParam = repelem(dispTParamMap,nNoise,nNoise);
    switch lesionDroOptions.pkModel.paramNames{cTIdx}
        % MRI 2CXM Manual axis
        case 'Fp'
            subHorIdx = 3;
            subVerIdx = 2;
        case 'PS'
            subHorIdx = 1;
            subVerIdx = 3;
        case 'Ve'
            subHorIdx = 1;
            subVerIdx = 2;
%         case 'Vp'
%             subHorIdx = 1;
%             subVerIdx = 2;
        otherwise
            otherIdx = true(nTParam,1);
            otherIdx(cTIdx) = false;
            if isfield(param2Plot(iParam),'fixedIdx')
                fixedIdx = param2Plot(iParam).fixedIdx;
                fixedIdxValues = param2Plot(iParam).fixedIdxValues;
                otherIdx(fixedIdx) = false;
                fixedParamMap = (trueNoiseParam(:,:,fixedIdx)==lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{fixedIdx})(fixedIdxValues));
            else
                fixedParamMap = true(size(trueNoiseParam,1),size(trueNoiseParam,2));
            end
            subHorIdx = find(otherIdx,1,'first');
            otherIdx(subHorIdx) = false;
            subVerIdx = find(otherIdx,1,'first');
            otherIdx(subVerIdx) = false;
    end
    nSubHor = nParamLength(subHorIdx);
    nSubVer = nParamLength(subVerIdx);
    nMixed = prod(nParamLength(otherIdx));
    nLegend = nDispModel+1;
    
    paramCompPlot(iParam).allFigH = figure;
    paramCompPlot(iParam).allFigH.Position = [467   131   994   829];
    linPlotLim = [0 lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx})(end)/normTrueFactor];
    allCompLim = ceil(lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx})(end)/normTrueFactor*1.1/5)*5*[-0.1 1];
    pLinError = NaN(nDispModel+1,1);
    pLinError(1) = plot(allCompLim,allCompLim,'k--','Linewidth',2);
    
    paramCompPlot(iParam).allDetailFigH = figure;
    paramCompPlot(iParam).allDetailFigH.Position = [449 88 1000 839];%[680   297   655   681];
    cbvLim = [-0.5 (max(lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx}))/normTrueFactor+0.5)];
    
    paramCompPlot(iParam).axH = cell(nSubHor,nSubVer);
    paramCompPlot(iParam).fitParam = cell(nLegend,1);
    paramCompPlot(iParam).fitParamPart = cell(nSubHor,nSubVer,nLegend);
    
    pLineHandle = NaN(1,nLegend);
    for iModel = 1:nDispModel
        figure(paramCompPlot(iParam).allDetailFigH);
        cAllParamMap = paramComp(iParam).allMat(:,:,1,iModel);
        cParamTrue = repelem(currentTrueMaps(:,:,iModel),nNoise,nNoise)/normTrueFactor;
        for iSubHor = 1:nSubHor
            for iSubVer = 1:nSubVer
                if isempty(paramCompPlot(iParam).axH{iSubHor,iSubVer})
                    paramCompPlot(iParam).axH{iSubHor,iSubVer} = subtightplot(nSubVer,nSubHor,iSubHor+nSubHor*(iSubVer-1),gap,marg_h,marg_w);
                else
                    axes(paramCompPlot(iParam).axH{iSubHor,iSubVer});
                    hold on;
                end
                pLineHandle(1) = plot(cbvLim,cbvLim,'--k','Linewidth',2); hold on;
%                 switch lesionDroOptions.pkModel.paramNames{cTIdx}
%                     case 'Fp'
%                         paramIdxHor = iModel+nCbv*(iSubVer-1);
%                         paramIdxVer = iSubHor+nSubHor*(0:(nParamLength(1)-1));
%                     case 'PS'
%                         paramIdxHor = iModel+nCbv*(0:(nParamLength(2)-1));
%                         paramIdxVer = iSubVer+nSubVer*(iSubHor-1);
%                     case 'Ve'
%                         paramIdxHor = iModel+nCbv*(iSubVer-1);
%                         paramIdxVer = 1:nParamLength(3)+nParamLength(3)*(iSubHor-1);
%                     case 'Vp'
%                         paramIdxHor = (1:nCbv)+nCbv*(iSubVer-1);
%                         paramIdxVer = (1:nParamLength(3))+nParamLength(3)*(iSubHor-1);
%                         paramIdxNoiseHor = (1:nCbv*nNoise)+nNoise*nCbv*(iSubVer-1);
%                         paramIdxNoiseVer = (1:nParamLength(3)*nNoise)+nNoise*nParamLength(3)*(iSubHor-1);
%                 end
                cHorAndVert = fixedParamMap & trueNoiseParam(:,:,subHorIdx)==lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{subHorIdx})(iSubHor) & ...
                    trueNoiseParam(:,:,subVerIdx)==lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{subVerIdx})(iSubVer);
                
                cNoiseCbvVec = NaN(nParamLength(cTIdx),nMixed*nNoise*nNoise);
                cTrueCbvVec = NaN(nParamLength(cTIdx),nMixed*nNoise*nNoise);
                for icTParam = 1:nParamLength(cTIdx)
                    cHorAndVertAndT = cHorAndVert & trueNoiseParam(:,:,cTIdx)==lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx})(icTParam);
                    cNoiseCbvVec(icTParam,:) = cAllParamMap(cHorAndVertAndT);
                    cTrueCbvVec(icTParam,:) = cParamTrue(cHorAndVertAndT);
                end
                
                cMeanParam = mean(cNoiseCbvVec,2);
                cStdParam = std(cNoiseCbvVec,[],2);
                cTrueParam = mean(cTrueCbvVec,2);
%                 cMeanParam = reshape(paramComp(iParam).meanMat(paramIdxHor,paramIdxVer,iModel),[nAxComp, 1]);
%                 cStdParam = reshape(paramComp(iParam).stdMat(paramIdxHor,paramIdxVer,iModel),[nAxComp, 1]);
% warning('REDUCED Standard Deviation for special plot'); stdDevRedFactor = 2;
%                 errorbar(cTrueParam(1:nParamLength(cTIdx)),cMeanParam,cStdParam/stdDevRedFactor,cStdParam/stdDevRedFactor,'.','Color',defaultPlotColors{mod(iModel,nPlotColor)+1});
                errorbar(cTrueParam(1:nParamLength(cTIdx)),cMeanParam,cStdParam,cStdParam,'.','Color',defaultPlotColors{mod(iModel,nPlotColor)+1});
                paramCompPlot(iParam).fitParamPart{iSubHor,iSubVer,iModel} = fitlm(cTrueParam(1:nParamLength(cTIdx)),cMeanParam);
                
                pLineHandle(iModel+1) = plot(cbvLim,polyval([paramCompPlot(iParam).fitParamPart{iSubHor,iSubVer,iModel}.Coefficients{2,1},paramCompPlot(iParam).fitParamPart{iSubHor,iSubVer,iModel}.Coefficients{1,1}],cbvLim),'-','Color',defaultPlotColors{mod(iModel,nPlotColor)+1});
                if nLegend<4
                    annotation('textbox',[(0.084+0.3*(iSubHor-1)), (0.97-0.3*iSubVer+0.1*(iModel-1)), 0.25, 0.04],'String',['y = ' num2str(paramCompPlot(iParam).fitParamPart{iSubHor,iSubVer,iModel}.Coefficients{2,1}) ...
                        ' x + ' num2str(paramCompPlot(iParam).fitParamPart{iSubHor,iSubVer,iModel}.Coefficients{1,1})],'FitBoxToText','on','Color',defaultPlotColors{mod(iModel,nPlotColor)+1});
                end
                hold off;
                if isfield(param2Plot(iParam),'manXLim')
                    xlim(param2Plot(iParam).manXLim);
                else
                    xlim(cbvLim);
                end
                ylim(cbvLim);
                title([lesionDroOptions.pkModel.displayNames{subHorIdx} ' = ' num2str(lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{subHorIdx})(iSubHor)) ' ' lesionDroOptions.pkModel.unitNames{subHorIdx}...
                    '; ' lesionDroOptions.pkModel.displayNames{subVerIdx} ' = ' num2str(lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{subVerIdx})(iSubVer)) ' ' lesionDroOptions.pkModel.unitNames{subVerIdx}]);
                
                paramCompPlot(iParam).axH{iSubHor,iSubVer}.LineWidth = 1.5;
                paramCompPlot(iParam).axH{iSubHor,iSubVer}.FontWeight = 'bold';
                axis square;
                if iSubVer == nSubVer
                    if isfield(param2Plot(iParam),'manXLabel')
                        xlabel(param2Plot(iParam).manXLabel);
                    else
                        if param2Plot(iParam).normParam
                            xlabel(['True r' lesionDroOptions.pkModel.displayNames{cTIdx} ' (-)']);
                        else
                            xlabel(['True ' lesionDroOptions.pkModel.displayNames{cTIdx} ' ' lesionDroOptions.pkModel.unitNames{cTIdx}]);
                        end
                    end
                else
                    paramCompPlot(iParam).axH{iSubHor,iSubVer}.XTickLabels = [];
                end
                if iSubHor == 1
                    if isfield(param2Plot(iParam),'manYLabel')
                        ylabel(param2Plot(iParam).manYLabel);
                    else
                        if param2Plot(iParam).normParam
                            ylabel(['Estimated r' lesionDroOptions.pkModel.displayNames{cTIdx} ' (-)']);
                        else
                            ylabel(['Estimated ' lesionDroOptions.pkModel.displayNames{cTIdx} ' ' lesionDroOptions.pkModel.unitNames{cTIdx}]);
                        end
                    end
                else
                    paramCompPlot(iParam).axH{iSubHor,iSubVer}.YTickLabels = [];
                end
            end
        end
        
        nCbv = nParamLength(cTIdx); % Vp in (-)
        % Overall figure
        
        cNoiseCbvVec = NaN(nParamLength(cTIdx),nSubHor*nSubVer*nMixed*nNoise*nNoise);
        cTrueCbvVec = NaN(nParamLength(cTIdx),nSubHor*nSubVer*nMixed*nNoise*nNoise);
        for icTParam = 1:nParamLength(cTIdx)
            cHorAndVertAndT = fixedParamMap & trueNoiseParam(:,:,cTIdx)==lesionDroOptions.pkModel.(lesionDroOptions.pkModel.paramNames{cTIdx})(icTParam);
            cNoiseCbvVec(icTParam,:) = cAllParamMap(cHorAndVertAndT);
            cTrueCbvVec(icTParam,:) = cParamTrue(cHorAndVertAndT);
        end
        
        avgCbv = mean(cNoiseCbvVec,2);
        stdCbv = std(cNoiseCbvVec,[],2);
        avgTrue = mean(cTrueCbvVec,2);
        cParamFit = paramComp(iParam).allMat(:,:,1,iModel);
        % Fit linear curve
        paramCompPlot(iParam).fitParam{iModel} = fitlm(cParamTrue(fixedParamMap),cParamFit(fixedParamMap));
        % Export linear fit
        figure(paramCompPlot(iParam).allFigH);hold on;
        %             plot(cParamTrue(:),cParamFit(:),'.');
        errorbar(avgTrue,avgCbv,stdCbv,stdCbv,'.','Color',defaultPlotColors{mod(iModel,nPlotColor)+1});
        
        if isfield(param2Plot(iParam),'manXLim')
            xlim(param2Plot(iParam).manXLim);
        else
            xlim(allCompLim);
        end
        ylim(allCompLim);
        linPValues = [paramCompPlot(iParam).fitParam{iModel}.Coefficients{2,1},paramCompPlot(iParam).fitParam{iModel}.Coefficients{1,1}];
        pLinError(iModel+1) = plot(linPlotLim, polyval(linPValues,linPlotLim),'-','Color',defaultPlotColors{mod(iModel,nPlotColor)+1},'Linewidth',2);
        
        if isfield(param2Plot(iParam),'manXLabel')
            xlabel(param2Plot(iParam).manXLabel);
        else
            if param2Plot(iParam).normParam
                xlabel(['True r' lesionDroOptions.pkModel.displayNames{cTIdx} ' (-)']);
            else
                xlabel(['True ' lesionDroOptions.pkModel.displayNames{cTIdx} ' ' lesionDroOptions.pkModel.unitNames{cTIdx}]);
            end
        end
        if isfield(param2Plot(iParam),'manYLabel')
            ylabel(param2Plot(iParam).manYLabel);
        else
            if param2Plot(iParam).normParam
                ylabel(['Estimated r' lesionDroOptions.pkModel.displayNames{cTIdx} ' (-)']);
            else
                ylabel(['Estimated ' lesionDroOptions.pkModel.displayNames{cTIdx} ' ' lesionDroOptions.pkModel.unitNames{cTIdx}]);
            end
        end
        cAxes = gca;
        cAxes.LineWidth = 1.5;
        cAxes.FontWeight = 'bold';
        %             title(paramComp(iParam).legend{iModel+1});
        %             annotation('textbox',[0.2 0.5 0.3 0.3],'String',['y = (' num2str(paramCompPlot(iParam).fitParam.Coefficients{2,1}) '\pm' num2str(paramCompPlot(iParam).fitParam.Coefficients{2,2}) ...
        %                 ')x + (' num2str(paramCompPlot(iParam).fitParam.Coefficients{1,1}) '\pm' num2str(paramCompPlot(iParam).fitParam.Coefficients{1,2}) ')'],'FitBoxToText','on');
    end
    legend(cAxes,pLinError,paramComp(iParam).legend);
    
    figure(paramCompPlot(iParam).allDetailFigH);
    legend(pLineHandle,paramComp(iParam).legend,'Location','SouthEast');
end
end