function figHandle = drodisplaydynamic(time,dynMaps,paramMap, pkModel,listNames)
% DRODISPLAYDYNAMIC Display dynamic curves of a DRO
%   figHandle = DRODISPLAYDYNAMIC(time,dynMaps,paramMap, pkModel,listNames)
%
% See also: DROCREATE, DROTILE
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-31

nParam = length(pkModel.paramNames);
nMaps2Compare = length(dynMaps);
[nT, nE, nParam1, nParam2] = size(dynMaps{1});

globalMax = -inf;
globalMin = inf;
for iMaps2Compare = 1:nMaps2Compare
    cMax = max(dynMaps{iMaps2Compare}(:));
    if cMax > globalMax; globalMax = cMax; end;
    cMin = min(dynMaps{iMaps2Compare}(:));
    if cMin > globalMin; globalMin = cMin; end;
end

if ~exist('listNames','var')
    legendNames = cell(1,nMaps2Compare*nE);
    for iMaps2Compare = 1:nMaps2Compare
        if nE == 1
            legendNames{iMaps2Compare} = num2str(iMaps2Compare);
        else
            for iE = 1:nE
                legendNames{iMaps2Compare} = ['Map: ' num2str(iMaps2Compare) ' Acq: ' num2str(iE)];
            end
        end
    end
else
    legendNames = cell(1,nMaps2Compare*nE);
    for iMaps2Compare = 1:nMaps2Compare
        if nE == 1
            legendNames{iMaps2Compare} = listNames{iMaps2Compare};
        else
            for iE = 1:nE
                legendNames{iMaps2Compare} = ['Map: ' listNames{iMaps2Compare} ' Acq: ' num2str(iE)];
            end
        end
    end
end

figHandle = figure;
figHandle.Position = [107         104        1411         845];
for iParam1 = 1:nParam1
    for iParam2 = 1:nParam2
        ax = subtightplot(nParam1,nParam2, nParam2*(iParam1-1)+iParam2);
        for iE = 1:nE
            for iMaps2Compare = 1:nMaps2Compare
                plot(time,dynMaps{iMaps2Compare}(:,iE,iParam1,iParam2));
                hold on;
            end
        end
        hold off;
        
        currentTitle = [];
        for iParam = 1:nParam
            currentTitle = [currentTitle pkModel.paramNames{iParam} '=' num2str(paramMap(iParam,iParam1,iParam2)) '  '];
        end
        ylim([cMin cMax]);
        xlim([time(1) time(end)]);
        if ~(iParam1 == nParam1 && iParam2 == 1)
            ax.XTickLabel = [];
            ax.YTickLabel = [];
        else
            if iE>1
            else
                legend(legendNames);
            end
        end
        t = title(currentTitle(1:end-2));
        t.Position = t.Position - [0 (cMax-cMin)/5 0];
    end
end

