function [meanParam, stdParam] = droparamsnoise(paramMap, nNoise, noiseIdx)
% DROPARAMNOISE Compute the mean and standard deviation of the noise
% dimensions (nNoise x nNoise squares) of a DRO
%   [meanParam, stdParam] = DROPARAMNOISE(paramMap, nNoise, noiseIdx)
%
% See also: DROCREATE, DROPATCH
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-31

paramLength = ones(1,8);
actualLength = size(paramMap);
nDimActual = length(actualLength);
paramLength(1:nDimActual) = actualLength;

redLength = paramLength;
redLength(noiseIdx(1)) = paramLength(noiseIdx(1))/nNoise;
redLength(noiseIdx(2)) = paramLength(noiseIdx(2))/nNoise;
if (round(redLength(noiseIdx(1)))~=redLength(noiseIdx(1)))||...
        (round(redLength(noiseIdx(2)))~=redLength(noiseIdx(2)))
    error('Number of noise data points is invalid.');
end
redIdxList = cell(1,8);
for iDim = 1:8
    redIdxList{iDim} = 1:redLength(iDim);
end

meanParam = NaN(redLength);
stdParam = NaN(redLength);
for iPNoise1=1:redLength(noiseIdx(1))
    iNoiseIdx1 = ((nNoise)*(iPNoise1-1)+1):nNoise*iPNoise1;
    cParamMap = sliceparamnoise(paramMap,noiseIdx(1),iNoiseIdx1);
    redIdxList{noiseIdx(1)} = iPNoise1;
    for iPNoise2 = 1:redLength(noiseIdx(2))
        iNoiseIdx2 = ((nNoise)*(iPNoise2-1)+1):nNoise*iPNoise2;
        ccParamMap = sliceparamnoise(cParamMap,noiseIdx(2),iNoiseIdx2);
        redIdxList{noiseIdx(2)} = iPNoise2;
        meanParam(redIdxList{1},redIdxList{2},redIdxList{3},redIdxList{4},redIdxList{5},redIdxList{6},redIdxList{7},redIdxList{8}) = ...
            (mean(mean(ccParamMap,noiseIdx(1)),noiseIdx(2)));
        stdParam(redIdxList{1},redIdxList{2},redIdxList{3},redIdxList{4},redIdxList{5},redIdxList{6},redIdxList{7},redIdxList{8}) = ...
            (std(std(ccParamMap,[],noiseIdx(1)),[],noiseIdx(2)));
    end
end
end

function slicedParam = sliceparamnoise(paramMap,noiseIdx,idxList)
switch noiseIdx
    case 1
        slicedParam = paramMap(idxList,:,:,:,:,:,:,:);
    case 2
        slicedParam = paramMap(:,idxList,:,:,:,:,:,:);
    case 3
        slicedParam = paramMap(:,:,idxList,:,:,:,:,:);
    case 4
        slicedParam = paramMap(:,:,:,idxList,:,:,:,:);
    case 5
        slicedParam = paramMap(:,:,:,:,idxList,:,:,:);
    case 6
        slicedParam = paramMap(:,:,:,:,:,idxList,:,:);
    case 7
        slicedParam = paramMap(:,:,:,:,:,:,idxList,:);
    case 8
        slicedParam = paramMap(:,:,:,:,:,:,:,idxList);
end
        
end