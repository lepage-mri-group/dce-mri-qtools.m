function [dataAIF, signalAIF, concAIF,aifMasks,phaseAif] = droaifpatch(time,aifModel,imgModel,ACQdata,tissueSignal,tissueConc,rowWidth,rowLoc,pveFactor,origAifMasks,phaseTiss)
% DROAIFPATCH Insert a AIF row patch in tissue DRO data at location
% specified by rowLoc and rowWidth
%
% [dataAIF, signalAIF, concAIF,aifMasks,phaseAif] = 
%   DROAIFPATCH(time,aifModel,imgParams,ACQdata, tissueSignal, tissueConc,
%       rowWidth,rowLoc,pveFactor,origAifMasks,phaseTiss) 
%
% See also: DROCREATE
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-30

%% Check input
rowDimConc = 2;
rowDimAcq = 3;

sizeAcqData = size(ACQdata);
sizeConc = size(tissueConc);
nNoise = sizeAcqData(rowDimAcq)/sizeConc(rowDimConc);
if nNoise~=imgModel.noiseParam.nNoise
    warning('Matrix noise dimension is inconcistent with DRO parameters. This can be the case if DROAIFPATCH was called more than once. Make sure that you started with the lower AIF patches. If not, check if sizeConc and sizeAcqData "row" dimensions matches what is expected here (2nd and 3rd).');
    nNoise =imgModel.noiseParam.nNoise;
end
if ~exist('rowLoc','var') || isempty(rowLoc)
    rowLoc = sizeConc(rowDimConc); % Default - Add patch at the end of DRO
end
if ~exist('pveFactor','var') || isempty(pveFactor)
    partialVolumeAif = false;
else
    partialVolumeAif = true;
end
if rowWidth>nNoise
    warning('This function was not designed for a AIF patch thickness greater than noise thickness. AIF thickness is reduced to noise thickness');
    rowWidth = nNoise;
end
if nargout > 4
    exportPhaseData = true;
    if ~exist('phaseTiss','var') || isempty(phaseTiss) %% Generate intial phase noise map
        [~, phaseTiss]= imgModel.noiseParam.FHandle(tissueSignal,imgModel.noiseParam);
    end
else
    exportPhaseData = false;
end
%% Generate arterial input function (AIF) signal
if ~isfield(aifModel,'AIFFit')
    aifModel.AIFFit = aifModel.FHandle(time,aifModel.pValues);
end
n0 = find(time>aifModel.pValues(1),1,'first');

[aifSignal, imgModel.bloodParam] = imgModel.FHandle(aifModel.AIFFit,n0,...
    imgModel.bloodParam,imgModel.seqParam);
if isfield(imgModel,'phase') && isfield(imgModel.phase,'FHandle') && ...
        isfield(imgModel.bloodParam.tracer,'enablePhase') && imgModel.bloodParam.tracer.enablePhase
    [aifPhase, ~] = imgModel.phase.FHandle(aifModel.AIFFit,n0,...
        imgModel.bloodParam,imgModel.seqParam);
    aifSignal = aifSignal.*exp(1j*aifPhase);
end
%% Add AIF concentration to DRO
repConc = sizeConc;
repConc(1) = 1;
repConc(rowDimConc) = 1;

sizeNoNoiseAif = sizeConc;
sizeNoNoiseAif(rowDimConc) = sizeNoNoiseAif(rowDimConc)+1;
concAIF = NaN(sizeNoNoiseAif);

concAIF(:,1:rowLoc,:,:,:,:,:,:) = tissueConc(:,1:rowLoc,:,:,:,:,:,:);
concAIF(:,rowLoc+1,:,:,:,:,:,:) = repmat(aifModel.AIFFit,repConc);
if rowLoc<sizeConc(rowDimConc)
    concAIF(:,(rowLoc+2):end,:,:,:,:,:,:) = tissueConc(:,(rowLoc+1):end,:,:,:,:,:,:);
end

%% Add AIF signal to DRO
repSignal = size(tissueSignal);
repSignal(1) = 1;
repSignal(rowDimAcq) = 1;

signalAIF = NaN([sizeNoNoiseAif(1) 1 sizeNoNoiseAif(2:end)]);

signalAIF(:,:,1:rowLoc,:,:,:,:,:) = tissueSignal(:,:,1:rowLoc,:,:,:,:,:);
if partialVolumeAif
    if rowLoc>1 % By default, it uses the upper signal row for PVE
        pveTissueSignal = tissueSignal(:,:,rowLoc,:,:,:,:,:);
    else
        pveTissueSignal = tissueSignal(:,:,rowLoc+1,:,:,:,:,:);
    end
    signalAIF(:,:,rowLoc+1,:,:,:,:,:) = pveFactor*repmat(aifSignal,repSignal)+ ...
        (1-pveFactor)*pveTissueSignal;
else
    signalAIF(:,:,rowLoc+1,:,:,:,:,:) = repmat(aifSignal,repSignal);
end
if rowLoc<sizeConc(rowDimConc)
    signalAIF(:,:,(rowLoc+2):end,:,:,:,:,:) = tissueSignal(:,:,(rowLoc+1):end,:,:,:,:,:);
end

%% Add AIF noisy acquired data to DRO
sizeAcqDataAif = sizeAcqData;
sizeAcqDataAif(rowDimAcq) = sizeAcqDataAif(rowDimAcq)+rowWidth;
dataAIF = NaN(sizeAcqDataAif);
dataAIF(:,:,1:nNoise*rowLoc,:,:,:,:,:) = ACQdata(:,:,1:nNoise*rowLoc,:,:,:,:,:);
dataAIF(:,:,(nNoise*rowLoc+1+rowWidth):end,:,:,:,:,:) = ACQdata(:,:,(nNoise*rowLoc+1):end,:,:,:,:,:);
if exportPhaseData
    [signalNoiseWidth phaseNoiseWidth]= imgModel.noiseParam.FHandle(signalAIF(:,:,rowLoc+1,:,:,:,:,:),imgModel.noiseParam);
    
    dataAIF(:,:,(nNoise*rowLoc)+(1:rowWidth),:,:,:,:,:) = signalNoiseWidth(:,:,(1:rowWidth),:,:,:,:,:);
    
    phaseAif = NaN(sizeAcqDataAif);
    
    phaseAif(:,:,1:nNoise*rowLoc,:,:,:,:,:) = phaseTiss(:,:,1:nNoise*rowLoc,:,:,:,:,:);
    phaseAif(:,:,(nNoise*rowLoc+1+rowWidth):end,:,:,:,:,:) = phaseTiss(:,:,(nNoise*rowLoc+1):end,:,:,:,:,:);
    phaseAif(:,:,(nNoise*rowLoc)+(1:rowWidth),:,:,:,:,:) = phaseNoiseWidth(:,:,(1:rowWidth),:,:,:,:,:);
else
    signalNoiseWidth = imgModel.noiseParam.FHandle(signalAIF(:,:,rowLoc+1,:,:,:,:,:),imgModel.noiseParam);
    dataAIF(:,:,(nNoise*rowLoc)+(1:rowWidth),:,:,:,:,:) = signalNoiseWidth(:,:,(1:rowWidth),:,:,:,:,:);
end
%% Mask of the tissue part of the DRO
if nargout > 3
    aifMasks.conc = zeros(sizeNoNoiseAif(2:end));
%     aifMasks.signal = zeros([sizeNoNoiseAif(1) 1 sizeNoNoiseAif(2:end)]);
    aifMasks.data = zeros(sizeAcqDataAif(3:end));
    
    if exist('origAifMasks','var') && ~isempty(origAifMasks)
        aifValue = max(origAifMasks.conc(:))+1;
        aifMasks.conc(1:rowLoc,:,:,:,:,:,:) = origAifMasks.conc(1:rowLoc,:,:,:,:,:,:);
        if rowLoc<sizeConc(rowDimConc)
            aifMasks.conc((rowLoc+2):end,:,:,:,:,:,:) = origAifMasks.conc((rowLoc+1):end,:,:,:,:,:,:);
        end
%         aifMasks.signal(:,:,1:rowLoc,:,:,:,:,:) = origAifMasks.signal(:,:,1:rowLoc,:,:,:,:,:);
%         if rowLoc<sizeConc(rowDimConc)
%             aifMasks.signal(:,:,(rowLoc+2):end,:,:,:,:,:) = origAifMasks.signal(:,:,(rowLoc+1):end,:,:,:,:,:);
%         end
        aifMasks.data(1:nNoise*rowLoc,:,:,:,:,:) = origAifMasks.data(1:nNoise*rowLoc,:,:,:,:,:);
        aifMasks.data((nNoise*rowLoc+1+rowWidth):end,:,:,:,:,:) = origAifMasks.data((nNoise*rowLoc+1):end,:,:,:,:,:);
    else
        aifValue = 1;
    end
    
    aifMasks.conc(rowLoc+1,:,:,:,:,:,:) = aifValue;
%     aifMasks.signal(:,:,rowLoc+1,:,:,:,:,:) = aifValue;
    aifMasks.data((nNoise*rowLoc)+(1:rowWidth),:,:,:,:,:) = aifValue;
end
end
