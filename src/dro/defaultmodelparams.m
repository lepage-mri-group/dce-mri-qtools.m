function [aifModel,pkModel,imgModel] = defaultmodelparams(time,t0,aifPeak,tissueParam,bloodParam,seqParam)
% DEFAULTMODELPARAMS Generates the default parameters for a MRI DCE SPGR
% digital reference object with a Horsfield AIF.
%
%   [aifModel,pkModel,imgModel] = DEFAULTMODELPARAMS(time,t0,aifPeak,tissueParam,bloodParam,seqParam)
%       Generates the DRO with a specified AIF peak concentration, tissue
%       relaxation parameters, blood relaxation parameters and sequence
%       parameters.
%
% See also: DROCREATE, DEFAULTDSCMODELPARAMS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-05-18

%% Default Horsfield AIF model
aifScaling = aifPeak/8.626; % mM
aifModel.FHandle = @aifhorsfield;
aifModel.Params.t0 = t0; % min
aifModel.Params.A = [10.06,0.33,0.37]; % mM
aifModel.Params.m = [16.02,1.17, 0.11];% 1/min
aifModel.Params.alpha = 5.26; % -
aifModel.Params.beta = 0.032; % min
aifModel.Params.tau = 0.129; % min
aifModel.Params.kN = 1; % (-)
aifModel.Params.k = aifScaling*3.0214;%9.1746*(droParam.aifModel.Params.beta*(droParam.aifModel.Params.alpha+1)+droParam.aifModel.Params.tau);
horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
aifModel.pValues = paramstruct2vec(aifModel.Params,horsfieldStructFieldOrder);
aifModel.units = 'mM';

%% PKModel
pkModel.hematocrit = 0.45;
pkModel.brainDensity = 1; % g/mL
if isfield(tissueParam,'FHandle')
    pkModel.FHandle = tissueParam.FHandle;
    pkModel.paramNames = tissueParam.paramNames;
    nParam = length(tissueParam.paramNames);
    for iParam = 1:nParam
        pkModel.(tissueParam.paramNames{iParam}) = 1/(pkModel.brainDensity/100)*tissueParam.pkm{iParam};
    end
else
    % Default phantom parameters
    pkModel.FHandle = @pkmetofts;
    pkModel.Ktrans = 1/(pkModel.brainDensity/100)*tissueParam.pkm{1}; % QIBA_v4. in mL/(100g min)
    pkModel.Ve = 1/(pkModel.brainDensity/100)*tissueParam.pkm{2}; % QIBA_v4. in mL/100g
    pkModel.Vp = 1/(pkModel.brainDensity/100)*tissueParam.pkm{3}; % QIBA_v4. mL/100g
    pkModel.paramNames = {'Ktrans','Ve','Vp'};
end

%% MRI signal
% Reference tissue
imgModel.FHandle = @signalmrispgr;
imgModel.tissueParam.S0 = 1e3; % QIBA4, Equilibrium magnetization. a.u
imgModel.tissueParam.R10 = 1/tissueParam.T10; % QIBA4, 1/ms 
imgModel.tissueParam.tracer.r1 = tissueParam.r1; % QIBA4, 1/(mM*s)
% Changes from QIBA_v4 - T2* relaxation 
imgModel.tissueParam.R20s = 1/tissueParam.T20s; % 1/ms 
imgModel.tissueParam.tracer.r2s = tissueParam.r2s; % Kjolby et al. 2006 (1/(mM*s)
% Blood
imgModel.bloodParam.S0 = 1e3; % QIBA_v4, Equilibrium magnetization. a.u
imgModel.bloodParam.R10 = 1/bloodParam.T10;  % QIBA_v4, 1/ms
% Changes from QIBA_v4 - T2* relaxation 
imgModel.bloodParam.R20s = 1/bloodParam.T20s; % 1/ms (Half white matter T20) Stanisz et al. 2005
imgModel.bloodParam.tracer.r1 = bloodParam.r1; % 1/(mM*s)
imgModel.bloodParam.tracer.r2s = bloodParam.r2s; % [(s mM)^-1]
if isfield(bloodParam,'r22s') && ~isempty(bloodParam.r22s)
    imgModel.bloodParam.tracer.r22s =  bloodParam.r22s; % [(s mM�)^-1]
    imgModel.bloodParam.tracer.enableQuadR2 = true;
else
    imgModel.bloodParam.tracer.enableQuadR2 = false;
end
if isfield(bloodParam,'xiM') && ~isempty(bloodParam.xiM)
    imgModel.bloodParam.tracer.xiM = bloodParam.xiM; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
    % imgModel.bloodParam.tracer.xiM = 3.20e-7; % (1/mM) = 320 ppm/M Gadobutrol, Diameter 13.5 mm, Parallel, Manganese solution (900 ms) (Korporaal, 2011)
    imgModel.bloodParam.tracer.enablePhase = true;
    imgModel.phase.FHandle = @signalmrispgrphase;
    if isfield(bloodParam,'vesselTheta') && ~isempty(bloodParam.vesselTheta)
        imgModel.bloodParam.theta = bloodParam.vesselTheta; % radians
    else
        imgModel.bloodParam.theta = 0; % radians
    end
else
    imgModel.bloodParam.tracer.enablePhase = false;
end
n0 = find(time>aifModel.Params.t0,1,'first')-1;
imgModel.bloodParam.tracer.framesBaseline = [1 n0];

% Changes from QIBA_v4 - DCE sequence parameters
imgModel.seqParam.te = seqParam.te;
imgModel.seqParam.nAcq = length(imgModel.seqParam.te);
imgModel.seqParam.tr = seqParam.tr; % ms
imgModel.seqParam.fa = seqParam.fa; % rad

imgModel.seqParam.B0 = seqParam.B0; % Tesla
imgModel.seqParam.voxelRes = seqParam.voxelRes; % mm
imgModel.seqParam.time = time;
imgModel.seqParam.t0 = aifModel.Params.t0;

% Changes from QIBA_v4 - Noise is added
imgModel.noiseParam.nNoise = seqParam.nNoise;
imgModel.noiseParam.seed = 700;
imgModel.noiseParam.repeatedPattern = false;
imgModel.noiseParam.FHandle = @noisemri;
imgModel.noiseParam.snr = seqParam.snr;
imgModel.noiseParam.standardDeviation = ...
    imgModel.tissueParam.S0/imgModel.noiseParam.snr;
end