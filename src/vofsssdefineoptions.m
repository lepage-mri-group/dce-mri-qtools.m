function vofOptions = vofsssdefineoptions(time,framesPre,nZ,vofType,cerebellumFov,gmMask,sssB0AngSlice,sssB0Thresh,te)
% VOFSSSDEFINEOPTIONS Set the chosen default options for the VOF 
% calculation
%  
%   vofOptions = VOFSSSDEFINEOPTIONS(time,framesPre,nZ,vofType,cerebellumFov,gmMask,sssB0AngSlice,sssB0Thresh)
%
% See also: AIFPVEDEFINEOPTIONS, GLOBALAIF, GLOBALVOF, AIFPVESIGNAL, AIFPVECONC
% Author: Benoit Bourassa-Moreau
% Creation date: 2021-01-25

%% Define options
if ~exist('cerebellumFov','var') || isempty(cerebellumFov)
    cerebellumFov = false;
end
if ~exist('sssB0AngSlice','var') || isempty(sssB0AngSlice)
    sssB0AngSlice = zeros(nZ,1);
end
if ~exist('sssB0Thresh','var') || isempty(sssB0Thresh)
    sssB0Thresh = 45/180*pi;
end
if ~exist('te','var') || isempty(te)
    te = 1.9; % ms
end

if ~exist('vofType','var') || isempty(vofType)
    vofOptions.conc.type = 'complex'; % Default cluster type
else
    vofOptions.conc.type = vofType;
end
if ~exist('gmMask','var') || isempty(gmMask)
    gmMask = [];
end
dt = time(2) - time(1);

% Typically first pass duration is 30 seconds, recirculation by 45 seconds
firstPassRecircDelay = 45/60; % min
minRecircPoints = 5;
recircEndIdx = find(time>(time(framesPre(2))+firstPassRecircDelay),1,'first')-1;
recircEndIdx = max(recircEndIdx,framesPre(2)+minRecircPoints);

% Refine range with average AIF to VOF delay for brain
vofAverageDelay = 7/60; % min
vofStartIdx = find(time>(time(framesPre(2))+vofAverageDelay),1,'first');
vofStartIdx = max(vofStartIdx,framesPre(2));

vofClusterRange = [vofStartIdx recircEndIdx];

%% VOF options
vofOptions.cluster.aif.nVoxelMax= 15; % maximum number of voxel in final AIF cluster
vofOptions.cluster.aif.nVoxelMin= 7; % minimum number of voxel in final AIF cluster
vofOptions.cluster.aif.diffPicco= 0; % Cluster selection criterion: Threshold between peak AUC or TTP criterion
vofOptions.cluster.aif.iaucRangeIdx = vofClusterRange;
vofOptions.cluster.time=time;
vofOptions.cluster.waitbar=1; % 0:off, 1:on
vofOptions.cluster.display=2;
vofOptions.cluster.aif.nSlice = round(nZ/2); % Displayed slice for debug plots

vofOptions.fit.init.t0 = [0, 4*dt];
vofOptions.fit.init.alpha = [2.5, 7];
vofOptions.fit.init.tau = [5/60, 10.6/60];
vofOptions.fit.display='off'; % 'notify' (default) | 'final' | 'iter' | 'off'

%%
switch vofType
    case 'magnitude' % basic
        magOptions = true;
        phaseOptions = false;
        splitOptions = false;
    case 'phase'
        magOptions = false;
        phaseOptions = true;
        splitOptions = false;
    case 'complex'
        magOptions = true;
        phaseOptions = true;
        splitOptions = false;
    case 'complex_inflow'
        magOptions = true;
        phaseOptions = true;
        splitOptions = false;
    case 'complex_split'
        magOptions = true;
        phaseOptions = true;
        splitOptions = true;
end

vofOptions.split.nKeptPerSlice = 3;
vofOptions.split.iaucRangeIdx = vofClusterRange;
vofOptions.split.cerebellumFov = cerebellumFov;
addMagSlices = 0;
if magOptions
    % Magnitude
    vofOptions.split.mag.inflowSlices = 20;
    vofOptions.split.mag.slabSlices = 5;
    vofOptions.split.mag.minMagSlices = 5;
    vofOptions.split.mag.minT1Blood = 1000; % ms T1 values decrease posterior to SSS caused by bone proximity. Exclude low blood T1 values
    addMagSlices = 0; % (OPT) Add a few more min slices for phase criterion if both phase and mag are required
end
if phaseOptions
    % Phase
    vofOptions.split.sssB0Ang = sssB0AngSlice;
    vofOptions.split.phase.B0AngThresh = sssB0Thresh;
    vofOptions.split.phase.nMinSlices = 5+addMagSlices;
    vofOptions.split.phase.nMaxSlices = nZ;
    vofOptions.split.phase.maxVoxelsPerSlice = 3;
    % Old phase slice limits
%     vofOptions.split.phase.nMinSlices = 3;
%     vofOptions.split.phase.nMaxSlices = 10;
%     vofOptions.split.phase.maxVoxelsPerSlice = 1;
    
    vofOptions.split.phase.gmMask = gmMask;
    vofOptions.split.framesPre = framesPre;
    vofOptions.split.phase.debug = false;
    
    % Maximum conc variations allowed during sequence gap
    allowedConcGapRate = [0.02 -0.1]; % [mM/min]
    B0 = 3;
    xiM = 3.209e-7;
    h1GyromagRatio = 2.675e8;
    larmorRad = h1GyromagRatio*B0;
    [bigGapDur,bigGapIdx ]= max(diff(time));
    if bigGapDur > 1 && bigGapDur > 5*min(diff(time))
        % Manage phase gap during sequence pause for blood sampling
        timeGap = (time(bigGapIdx+1)-time(bigGapIdx));
        limitGapPhase = allowedConcGapRate*((te/1e3)*larmorRad*xiM*(1-1/3)/2)*timeGap;
        vofOptions.split.phase.gapIdx = bigGapIdx;
        vofOptions.split.phase.limitGapPhase = limitGapPhase; % [Rad]
    else
        vofOptions.split.phase.gapIdx = [];
        vofOptions.split.phase.limitGapPhase = []; % [Rad]
    end
end

if splitOptions
% Split
end


end