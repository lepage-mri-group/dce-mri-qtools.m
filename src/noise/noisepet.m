function noiseSignal = noisepet(signal,noiseModel)
% NOISEPET Adds TOF PET noise to DRO time-signal matrix
%
%   noiseSignal = NOISEPET(signal,noiseModel) Gaussian noise is added
%   with a SD linearly proportional to the square root of true counts. This
%   is similar to the noise model used by (Wang et al., 2008) with a
%   sdSlope constant of 150 [sqrt(kcts/mL)].
%
%   Gaussian noise is a good representation for reconstructed PET data 
%   (RAMLA and FBP) (Teymurazyan et al., 2013). TOF PET SNR� is linearly 
%   proportional to PET true counts (Clementel et al., 2011) with a slope
%   of 8.1 [1/MCts] for a head phantom with RAMLA.This corresponds to
%   sdSlope = sqrt(1/(0.0081 * 0.2^3)) = 124 [sqrt(kcts/mL)]
%
% See also: NOISEMRI, NOISEMAPSDIMENSION 
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-10

%% Prepare noise matrix
[repSignal, timeNoise] = noisemapsdimension(signal,...
    noiseModel.nNoise,noiseModel.seed,1,noiseModel.repeatedPattern);

% Linear model between SNR^2 and Trues (Clementel et al., IEEE, 2011)

%% Compute noise
time = (cumsum(noiseModel.frameDurations) - noiseModel.frameDurations/2)/60;  % min
time = time - time(1);

nT = size(repSignal,1);
localSd = NaN(size(repSignal));
for iT=1:nT
    localSd(iT,:,:,:,:,:,:,:,:) = ...
        noiseModel.sdSlope*sqrt(repSignal(iT,:,:,:,:,:,:,:,:)*...
        exp(log(2)*time(iT)/noiseModel.tracerDecay)/noiseModel.frameDurations(iT)); % kBq/mL
end
noiseSignal =  repSignal + timeNoise.*localSd;

end