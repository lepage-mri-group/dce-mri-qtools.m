function [repSignal, varargout] = noisemapsdimension(signal,nNoise,seed,nMaps,repeatNoisePattern)
% NOISEMAPSDIMENSION Extend signal matrix dimension by nNoise^2 and prepare
% nMaps random number matrices.
%
%   [repSignal, varargout] = NOISEMAPSDIMENSION(signal,nNoise,seed,nMaps,repeatNoisePattern)
%
% See also: NOISEMRI, NOISEPET
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-10

%% Signal matrix size
paramLength = ones(1,8);
[nT, nAcq, paramLength(1), paramLength(2), paramLength(3), ...
    paramLength(4), paramLength(5), paramLength(6), paramLength(7), ...
    paramLength(8)] = size(signal);

%% Generate noise and signal maps
if ~isempty(seed)
    rng(seed,'twister');
else
    rng('shuffle','twister');
end
varargout = cell(1,nMaps);
for iMaps = 1:nMaps
    if repeatNoisePattern
        varargout{iMaps} = repmat(randn(nT,nAcq,nNoise,nNoise),...
            [1,1,paramLength]);
    else
        varargout{iMaps} = randn(nT,nAcq,nNoise*paramLength(1),...
            nNoise*paramLength(2),paramLength(3),paramLength(4),...
            paramLength(5),paramLength(6),paramLength(7),paramLength(8));
    end
end

repSignal = repelem(signal,...
    1, 1, nNoise, nNoise, 1, 1, 1, 1, 1, 1);