function [noiseMagnitude, noisePhase]= noisemri(signal,noiseModel)
% NOISEMRI Adds MRI noise to DRO time-signal matrix
%
%   noiseSignal = NOISEMRI(signal,noiseModel) Gaussian noise is added to
%   real and imaginary parts of the DRO signal. Input signal is real and 
%   output is magnitude of signal. Same noise definition as QIBA DCE-DR).
%
% See also: NOISEPET, NOISEMAPSDIMENSION
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-10

if isfield(noiseModel,'seed')
    seed = noiseModel.seed;
else
    seed = [];
end
%% Prepare noise matrix
[repSignal, timeNoiseReal, timeNoiseImag] = noisemapsdimension(signal,...
    noiseModel.nNoise,seed,2,noiseModel.repeatedPattern);

%% Compute noise
if nargout==1
    noiseMagnitude = ...
        sqrt((real(repSignal) + noiseModel.standardDeviation * timeNoiseReal).^2+...
        (imag(repSignal) + noiseModel.standardDeviation * timeNoiseImag).^2);
else
    cpxSignal = (real(repSignal) + noiseModel.standardDeviation * timeNoiseReal)+...
        1j*(imag(repSignal) + noiseModel.standardDeviation * timeNoiseImag);
    noiseMagnitude = abs(cpxSignal);
    noisePhase = angle(cpxSignal);    
end

end