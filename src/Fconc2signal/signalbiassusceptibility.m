function [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = signalbiassusceptibility(aifConc,gmConc,n0,bloodFractionReq,biasVesselAng,imgModel,showSim)
% SIGNALBIASSUSCEPTIBILITY compute arterial and surrounding tissue signal 
% with simulated infinite tube distant susceptibility effect. Both signal 
% are also combined with various blood fractions.
%
%   [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = SIGNALBIASSUSCEPTIBILITY(aifConc,gmConc,n0,bloodFractionReq,biasVesselAng,imgModel,showSim)
%
% See also: XIVOXELINFINITETUBE, SIGNALMRISPGR, SIGNALBIAST10, SIGNALBIASREACHSS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-03-09

%% Susceptibility simulations Based on Haacke et al. e.q. (26.6)
% General parameters
% Peak blood concentration 3-5 mM
[maxConc, maxConcIdx] = max(aifConc);
gamma = 2.675e8; % rad/sT
molarSusceptibility = imgModel.bloodParam.tracer.xiM; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)

% DCE sequence parameters
voxelRes = imgModel.seqParam.voxelRes; % mm
echoTime = imgModel.seqParam.te; % ms
B0 = imgModel.seqParam.B0; % Tesla
nVox = 128;
resWithinVoxel = voxelRes/nVox; % mm

degreeNormalisation = 1/pi*180;
% Vessel geometry
nAngulation = length(biasVesselAng);
nBloodFraction = length(bloodFractionReq);
approxVesselRadius = sqrt(bloodFractionReq*(voxelRes)^2/pi); % mm
angOfVesselTheta = repmat(biasVesselAng,[nBloodFraction ,1])/degreeNormalisation;

% vesselRadius = [0.451595534752706 0.466017278314334 0.433153691561221;...
%     0.583606821994862 0.587126941582748 0.560146797403379;...
%     1.01292301974918 0.997630201960648 0.994924806606750];
initVesselRadius = repmat(reshape(approxVesselRadius,[nBloodFraction,1]),[1 nAngulation]); % Manually adjusted for each vessel geometry (angulation and offsets)
initVesselRadius(repmat(reshape(bloodFractionReq,[nBloodFraction,1]),[1 nAngulation])==1) = voxelRes; % Radius approximation techinque has trouble reaching 100% blood fraction.
% Constant parameters
angOfVesselPhi = 0/degreeNormalisation;
dPhaseOrig = 1;
dPhasePeakConc = (maxConc*molarSusceptibility)*gamma*B0/2*(echoTime/1e3); % radians
dPhaseConc = (aifConc*molarSusceptibility)*gamma*B0/2*(echoTime/1e3); % radians

%% Susceptibility simulations
nPrep = 3; % Adjut vessel radius to get better blood fraction
bloodFractionActual = NaN(nBloodFraction,nAngulation);
isVoxel = cell(nBloodFraction,nAngulation);
isVessel = cell(nBloodFraction,nAngulation);
isTissue = cell(nBloodFraction,nAngulation);
nSimDim = cell(nBloodFraction,nAngulation);
voxelPhaseOffset = cell(nBloodFraction,nAngulation);
for iPrep = 1:nPrep
    if iPrep == 1
        correctedVesselRadius = initVesselRadius;
    else
        correctedVesselRadius = correctedVesselRadius.*sqrt(repmat(reshape(bloodFractionReq,[nBloodFraction,1]),[1 nAngulation])./bloodFractionActual);
    end
    for iAng = 1:nAngulation
        for iBf = 1:nBloodFraction
            currentVesselRadius = correctedVesselRadius(iBf,iAng);
            displayOffset = 2*[0 0 round(nVox/2 - currentVesselRadius/resWithinVoxel)];
            displayOffset(displayOffset<0) = 0;
            nSimDim{iBf,iAng} = nVox + displayOffset;
            simVoxRes = voxelRes/nVox*nSimDim{iBf,iAng};
            isVoxel{iBf,iAng} = false(nSimDim{iBf,iAng});
            isVoxel{iBf,iAng}((1:nVox)+displayOffset(1),(1:nVox)+displayOffset(2),(1:nVox)+displayOffset(3)) = true;
            
            vesselParam.radius = currentVesselRadius;
            vesselParam.theta = angOfVesselTheta(iBf,iAng);
            vesselParam.phi = angOfVesselPhi;
            [voxelPhaseOffset{iBf,iAng},isVessel{iBf,iAng},isTissue{iBf,iAng},bloodFractionActual(iBf,iAng),unitVector] = xivoxelinfinitetube(dPhaseOrig,vesselParam,isVoxel{iBf,iAng},simVoxRes,nSimDim{iBf,iAng});
        end
    end
end
%% Compute metrics with signal magnitude simulations
concSat = [0 0 1e12];
nT = length(aifConc);
nTSat = length(concSat);
bloodT10 = 1/imgModel.bloodParam.R10*ones(nAngulation,nBloodFraction);
satSignal = NaN(nAngulation,nTSat);
% Arterial
[aifSignalVec,imgModel.bloodParam] = imgModel.FHandle(aifConc,n0,...
    imgModel.bloodParam,imgModel.seqParam);
% Tissue
[gmSignalVec,imgModel.tissueParam] = imgModel.FHandle(gmConc,n0,...
    imgModel.tissueParam,imgModel.seqParam);
aifSignalAll = NaN(nBloodFraction,nAngulation,1,nT);
surTissuesSignal = NaN(nBloodFraction,nAngulation,1,nT);
artSignal = NaN(nBloodFraction,nAngulation,1,nT);
meanAllPhase = NaN(nBloodFraction,nAngulation);
percentDiff = NaN(nBloodFraction,nAngulation,1,nT);
for iAng = 1:nAngulation
    satSignal(iAng,:) = imgModel.FHandle(concSat,3,...
        imgModel.bloodParam,imgModel.seqParam);
    for iBf = 1:nBloodFraction
        disp(iBf);
        insidePhaseFraction = mean(voxelPhaseOffset{iBf,iAng}(isVessel{iBf,iAng}));
        aifSignalAll(iBf,iAng,1,:) = reshape(aifSignalVec.*exp(-1j*dPhaseConc*insidePhaseFraction),[1 1 1 nT]);
        gmPhaseOffsetList = voxelPhaseOffset{iBf,iAng}(isTissue{iBf,iAng});
        for iT = 1:nT
            gmSignalPhaseFrac = mean(exp(-1j*dPhaseConc(iT)*gmPhaseOffsetList));
            surTissuesSignal(iBf,iAng,1,iT) = gmSignalVec(iT)*gmSignalPhaseFrac;
        end
        relativeMagnitude = zeros([nSimDim{iBf,iAng}]);
        relativeMagnitude(isTissue{iBf,iAng}) = gmSignalVec(maxConcIdx);
        relativeMagnitude(isVessel{iBf,iAng}) = aifSignalVec(maxConcIdx);
        meanAllPhase(iBf,iAng) = sum(sum(sum(dPhasePeakConc*voxelPhaseOffset{iBf,iAng}.*relativeMagnitude)))./sum(sum(sum(relativeMagnitude)));
        
        if bloodFractionActual(iBf,iAng)==1
            artSignal(iBf,iAng,1,:) = aifSignalAll(iBf,iAng,1,:);
        else
            artSignal(iBf,iAng,1,:) = (bloodFractionActual(iBf,iAng)*aifSignalAll(iBf,iAng,1,:) + ...
                (1-bloodFractionActual(iBf,iAng))*surTissuesSignal(iBf,iAng,1,:));
        end
        percentDiff(iBf,iAng,1,:) = 100*abs(reshape(gmSignalVec,[1 1 1 nT])-surTissuesSignal(iBf,iAng,1,:))./abs(surTissuesSignal(iBf,iAng,1,:));
    end
end
gmSignal = repmat(reshape(gmSignalVec,[1 nT]),[nAngulation 1]);
aifSignal = permute(aifSignalAll(end,:,:,:),[2,4,1,3]);
%% Display
if exist('showSim','var') && isfield(showSim,'paramBfIdx') && isfield(showSim,'paramXiIdx') && isfield(showSim,'axTube') && isfield(showSim,'axPhase') && isfield(showSim,'axCpx') && isfield(showSim,'axSignal')
else
            figureSusceptibility = figure;
            figureSusceptibility.Position = [522   367   795   477];
            showSim.paramBfIdx = round(nBloodFraction/2);
            showSim.paramXiIdx = round(nAngulation/2);

            showSim.axTube = subplot(2,4,[1 2]);
            showSim.axPhase = subplot(2,4,3);
            showSim.axCpx = subplot(2,4,4);
            showSim.axSignal = subplot(2,4,[5 6]);
            showSim.axTube.Position = [0.082    0.50    0.36    0.43];
            showSim.axPhase.Position = [0.5322    0.5052    0.1566    0.42];
            showSim.axCpx.Position = [0.7609    0.5052    0.1566    0.42];
            showSim.axSignal.Position = [0.1300    0.1100    0.3628    0.30];
%             showSim.axTube = subplot(3,4,[1 2 5 6]);
%             showSim.axPhase = subplot(3,4,[3 7]);
%             showSim.axCpx = subplot(3,4,[4 8]);
%             showSim.axSignal = subplot(3,4,[9 10]);
end
iBf = showSim.paramBfIdx;
iAng = showSim.paramXiIdx;

if bloodFractionActual(iBf,iAng)<1
    normCpxSignal = 1./abs(artSignal(:,:,:,maxConcIdx));
    
    % figPveSim(iBf,iAng) = figure;
    % figPveSim(iBf,iAng).Position = [170   519   813   311];
    % ---- Subplot A. Susceptibility map ---
    axes(showSim.axTube);
    % Extract top and bottom surfaces
    voxelCoordinates = pixelfrommask(isVoxel{iBf,iAng});
    minVox = min(voxelCoordinates,[],1)-1;
    dimVox = max(voxelCoordinates,[],1)-minVox;
    xShownDim = (1:dimVox(1));
    yShownDim = (1:dimVox(2));
    zShownDim = (1:dimVox(3));
    shownPhaseDegOffset = dPhasePeakConc*degreeNormalisation*permute(voxelPhaseOffset{iBf,iAng}(xShownDim+minVox(1),yShownDim+minVox(2),zShownDim+minVox(3)),[1 2 3]);
    
    vesselCoordinates = pixelfrommask(isVessel{iBf,iAng});
    
    zSurfFirst = NaN(dimVox(1),dimVox(2));
    zSurfLast = NaN(dimVox(1),dimVox(2));
    for iPx = 1:length(vesselCoordinates(:,1))
        if isnan(zSurfFirst(vesselCoordinates(iPx,1)-minVox(1),vesselCoordinates(iPx,2)-minVox(2)))
            zSurfFirst(vesselCoordinates(iPx,1)-minVox(1),vesselCoordinates(iPx,2)-minVox(2)) = vesselCoordinates(iPx,3)-minVox(3);
        end
        zSurfLast(vesselCoordinates(iPx,1)-minVox(1),vesselCoordinates(iPx,2)-minVox(2)) = vesselCoordinates(iPx,3)-minVox(3);
    end
    % Extract junction surfaces
    xStartXPlane = [(1:dimVox(1));(1:dimVox(1))]';
    yStartXPlane = [zeros(dimVox(1),1) 20*eps*ones(dimVox(1),1)];
    zStartXPlane = NaN(dimVox(1),2);
    yStopXPlane = [zeros(dimVox(1),1) 20*eps*ones(dimVox(1),1)];
    zStopXPlane = NaN(dimVox(1),2);
    
    yStartYPlane = [(1:dimVox(2));(1:dimVox(2))]';
    xStartYPlane = [zeros(dimVox(1),1) 20*eps*ones(dimVox(1),1)];
    zStartYPlane = NaN(dimVox(1),2);
    xStopYPlane = [zeros(dimVox(1),1) 20*eps*ones(dimVox(1),1)];
    zStopYPlane = NaN(dimVox(1),2);
    for iX = 1:dimVox(1)
        startPlane = find(~isnan(zSurfFirst(iX,:)),1,'first');
        if ~isnan(zSurfLast(iX,startPlane))
            yStartXPlane(iX,:) = yStartXPlane(iX,:) + startPlane;
            zStartXPlane(iX,1) = zSurfFirst(iX,startPlane);
            zStartXPlane(iX,2) = zSurfLast(iX,startPlane);
        end
        stopPlane = find(~isnan(zSurfFirst(iX,:)),1,'last');
        if ~isnan(zSurfLast(iX,stopPlane))
            yStopXPlane(iX,:) = yStopXPlane(iX,:) + stopPlane;
            zStopXPlane(iX,1) = zSurfFirst(iX,stopPlane);
            zStopXPlane(iX,2) = zSurfLast(iX,stopPlane);
        end
    end
    for iY = 1:dimVox(2)
        startPlane = find(~isnan(zSurfFirst(:,iY)),1,'first');
        if ~isnan(zSurfLast(startPlane,iY))
            xStartYPlane(iY,:) = xStartYPlane(iY,:) + startPlane;
            zStartYPlane(iY,1) = zSurfFirst(startPlane,iY);
            zStartYPlane(iY,2) = zSurfLast(startPlane,iY);
        end
        stopPlane = find(~isnan(zSurfFirst(:,iY)),1,'last');
        if ~isnan(zSurfLast(stopPlane,iY))
            xStopYPlane(iY,:) = xStopYPlane(iY,:) + stopPlane;
            zStopYPlane(iY,1) = zSurfFirst(stopPlane,iY);
            zStopYPlane(iY,2) = zSurfLast(stopPlane,iY);
        end
    end
    % Prepare slices
    dimRes = voxelRes./dimVox;
    [XMatSlice,YMatSlice,ZMatSlice] = meshgrid(xShownDim*dimRes(1),yShownDim*dimRes(1),zShownDim*dimRes(1));
    
    % Plot figure
    surfColor = 0.3*[1 1 1];
    surfColorMat = repmat(reshape(surfColor,[1 1 3]),dimVox(1),dimVox(2));
    surfColorXMat = repmat(reshape(surfColor,[1 1 3]),dimVox(1),2);
    surfColorYMat = repmat(reshape(surfColor,[1 1 3]),dimVox(2),2);
    s1 = surf(XMatSlice(:,:,1),YMatSlice(:,:,1),zSurfFirst*dimRes(3),surfColorMat);
    hold on;
    s2 = surf(XMatSlice(:,:,1),YMatSlice(:,:,1),zSurfLast*dimRes(3),surfColorMat);
    s3 = surf(yStartXPlane*dimRes(2),xStartXPlane*dimRes(1),zStartXPlane*dimRes(3),surfColorXMat);
    s4 = surf(yStopXPlane*dimRes(2),xStartXPlane*dimRes(1),zStopXPlane*dimRes(3),surfColorXMat);
    s5 = surf(yStartYPlane*dimRes(2),xStartYPlane*dimRes(1),zStartYPlane*dimRes(3),surfColorYMat);
    s6 = surf(yStartYPlane*dimRes(2),xStopYPlane*dimRes(1),zStopYPlane*dimRes(3),surfColorYMat);
    
    sPhase = slice(XMatSlice,YMatSlice,ZMatSlice,shownPhaseDegOffset,[],yShownDim(round(dimVox(2)/2))*dimRes(2),yShownDim(round(dimVox(3)/2))*dimRes(3));
    for iSlice = 1:length(sPhase)
        sPhase(iSlice).EdgeColor = 'none';
    end
    plot3([0,dimVox(1)*dimRes(1)],dimVox(2)*dimRes(2)/2*[1 1],dimVox(3)*dimRes(3)/2*[1 1],'-k','LineWidth',3);
    % plot3(dimVox(1)*dimRes(1)/2*[1 1],[0,dimVox(2)*dimRes(2)],dimVox(3)*dimRes(3)/2*[1 1],'-k','LineWidth',2);
    % plot3(dimVox(1)*dimRes(1)/2*[1 1],dimVox(2)*dimRes(2)/2*[1 1],[0,dimVox(3)*dimRes(3)],'-k','LineWidth',2);
    colorH1 = colorbar;
    hold off;
    s1.EdgeColor = 'none';
    s2.EdgeColor = 'none';
    s3.EdgeColor = 'none';
    s4.EdgeColor = 'none';
    s5.EdgeColor = 'none';
    s6.EdgeColor = 'none';
    alphaSurf = 0.3;
    s1.FaceAlpha = alphaSurf;
    s2.FaceAlpha = alphaSurf;
    s3.FaceAlpha = alphaSurf;
    s4.FaceAlpha = alphaSurf;
    s5.FaceAlpha = alphaSurf;
    s6.FaceAlpha = alphaSurf;
    xlim([1 dimVox(1)]*dimRes(1));
    ylim([1 dimVox(2)]*dimRes(2));
    zlim([1 dimVox(3)]*dimRes(3));
    axis square;
    xlabel('X (mm)');
    ylabel('Y (mm)');
    zlabel('Z (mm)');
    ax1 = gca;
    ax1.Box = 'on';
    campos(ax1,[15.1369   13.9674    6.7390]);
    colorH1.Position = [0.9427    0.1961    0.0236    0.6109];
    % ---- Subplot B. Phase averages ----
    histogramBins = -60:5:60;
    meanVesselPhase = mean(dPhasePeakConc*voxelPhaseOffset{iBf,iAng}(isVessel{iBf,iAng}));
    meanTissPhase = mean(dPhasePeakConc*voxelPhaseOffset{iBf,iAng}(isTissue{iBf,iAng}));
    
    axes(showSim.axPhase);
    histogram(dPhasePeakConc*voxelPhaseOffset{iBf,iAng}(isTissue{iBf,iAng})*degreeNormalisation,histogramBins,'Normalization','probability','FaceColor',[1 1 1]*0.5,'EdgeColor','none');
    cVertLim = ylim;
    hold on;
    plot(meanTissPhase*[1 1+10*eps]*degreeNormalisation,cVertLim,'b-','Linewidth',1);
    plot(meanVesselPhase*[1 1+10*eps]*degreeNormalisation,cVertLim,'k-','Linewidth',1);
    plot(meanAllPhase(iBf,iAng)*[1 1+10*eps]*degreeNormalisation,cVertLim,'r-','Linewidth',1);
    hold off;
    xlim([min(histogramBins) max(histogramBins)]);
    xlabel('Phase offset (\circ)');
    ylabel('Normalized counts (-)');
    
    % ---- Subplot C. Complex signal ----
    axes(showSim.axCpx);
    plot([0 (1-bloodFractionActual(iBf,iAng))*gmSignal(iAng,maxConcIdx)]*normCpxSignal(iBf,iAng),[0 0],'g-'); hold on;
    if isreal(surTissuesSignal(iBf,iAng,:,maxConcIdx))
        plot([0 (1-bloodFractionActual(iBf,iAng))*surTissuesSignal(iBf,iAng,:,maxConcIdx)]*normCpxSignal(iBf,iAng),[0 0],'b-');
    else
        plot([0 (1-bloodFractionActual(iBf,iAng))*surTissuesSignal(iBf,iAng,:,maxConcIdx)]*normCpxSignal(iBf,iAng),'b-');
    end
    plot([(1-bloodFractionActual(iBf,iAng))*surTissuesSignal(iBf,iAng,:,maxConcIdx) (bloodFractionActual(iBf,iAng)*aifSignalAll(iBf,iAng,:,maxConcIdx)+(1-bloodFractionActual(iBf,iAng))*surTissuesSignal(iBf,iAng,:,maxConcIdx))]*normCpxSignal(iBf,iAng),'k-');
    plot([0 artSignal(iBf,iAng,:,maxConcIdx)]*normCpxSignal(iBf,iAng),'r-');
    hold off;
    xlabel('Real signal (a.u.)');
    ylabel('Imag. signal (a.u.)');
    
    % ---- Subplot D. Signal vs conc ----
    % gmOffset = 0;
    % aifOffset = 0;
    % gmNormalize = 1/(gmSignal(iAng,1));
    % plotOffset = 0;
    gmOffset = abs(gmSignal(iAng,1));
    aifOffset = abs(aifSignalAll(iBf,iAng,1,1));
    gmNormalize = 1/(gmOffset);
    plotOffset = 0;
    aifNormalize = gmNormalize/10;%1/(max(abs(aifSignalAll(iBf,iAng,1,:)))-aifOffset);
    correctedAif = abs((reshape(artSignal(iBf,iAng,1,:),[1 nT])-(1-bloodFractionActual(iBf,iAng))*gmSignal(iAng,:))/bloodFractionActual(iBf,iAng));
    axes(showSim.axSignal);
    plot(imgModel.seqParam.time,squeeze(abs(surTissuesSignal(iBf,iAng,1,:))-gmOffset)*gmNormalize+plotOffset,'.-b','MarkerSize',10);hold on;
    plot(imgModel.seqParam.time,squeeze(abs(gmSignal(iAng,:))-gmOffset)*gmNormalize+plotOffset,'-vg','MarkerSize',4);
    plot(imgModel.seqParam.time,squeeze(abs(aifSignalAll(iBf,iAng,1,:))-aifOffset)*aifNormalize+plotOffset,'-k','Linewidth',2,'MarkerSize',6);
    plot(imgModel.seqParam.time,(correctedAif-aifOffset)*aifNormalize+plotOffset,'-*r');hold off;
    legend('Surrounding','Remote','Blood (1/10)','PVCM (1/10)');
    ylabel('Rel. signal enhancement (-)');
    xlabel('Time (min)');
    xlim(imgModel.seqParam.t0+[-0.1 1]);
end
% hA.Position = [0.0700    0.1398    0.2315    0.7438];
% hB.Position = [0.3911    0.2000    0.1218    0.6000];
% hC.Position = [0.5830    0.2000    0.1170    0.6000];
% hD.Position = [0.7600    0.2000    0.1700    0.6000];