function [signal, tissueParam] = signalmrispgr(conc,preInjIdx,tissueParam,imgParam)
% SIGNALMRISPGR Computes MRI RF-spoiled Gradient-recalled echo steady-state
% signal from tissue concentration.
%
%   [signal, tissueParam] = SIGNALMRISPGR(conc,preInjIdx,tissueParam,imgParam)
%   Tissue signal curve is computed from the tissue concentration given
%   tracer parameters (relaxivity (r1,r2s)), tissue parameters (initial
%   relaxation rate (R10, R20) and initial signal (S0)) and sequence
%   parameters (te, tr, fa).
%
% See also: SIGNALMRISPGRPHASE, SIGNALPET
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-06

r1ms = tissueParam.tracer.r1/1e3; % from (s-1mM-1) to (ms-1mM-1)
if isfield(tissueParam.tracer,'r2s')
    r2sms = tissueParam.tracer.r2s/1e3; % from (s-1mM-1) to (ms-1mM-1)
else
    r2sms = 0;
end
if isfield(tissueParam.tracer,'r22s') && isfield(tissueParam.tracer,'enableQuadR2') && tissueParam.tracer.enableQuadR2  
    r22sms = tissueParam.tracer.r22s/1e3; % from (s-1mM-1) to (ms-1mM-1)
else
    r22sms = [];
end
if isfield(tissueParam,'R20s')
    R20s = tissueParam.R20s;
else
    R20s = 0;
end
[R1,R2s] = concrelaxivity(conc,r1ms,r2sms,r22sms,...
    tissueParam.R10,R20s); % ms

nE = length(imgParam.te);
nConc = length(conc);
if isrow(conc)
    signal = NaN(nE,nConc);
    for iE=1:nE
        mp = mrsignal( R1, R2s, imgParam.tr, imgParam.te(iE), imgParam.fa);
        if isfield(tissueParam,'S0') && ~isempty(tissueParam.S0) && ~isempty(preInjIdx) && preInjIdx < nConc
            signal(iE,:) = tissueParam.S0*mp/mean(mp(1:(preInjIdx-1)));
        else
            signal(iE,:) = mp;
        end
    end
else
    signal = NaN(nConc,nE);
    for iE=1:nE
        mp = mrsignal( R1, R2s, imgParam.tr, imgParam.te(iE), imgParam.fa);
        if isfield(tissueParam,'S0') && ~isempty(tissueParam.S0) && ~isempty(preInjIdx) && preInjIdx < nConc
            signal(:,iE) = tissueParam.S0*mp/mean(mp(1:(preInjIdx-1)));
        else
            signal(:,iE) = mp;
        end
    end
end

end

function [R1,R2s] = concrelaxivity(conc,r1,r2s,r22s,R10,R20s)
% conc : mM
% r1 : 1/(mM ms)
% r2s : 1/(mM ms)
% R10 : 1/ms
% R20s : 1/ms
R1 = r1*conc + R10; % 1/ms
if exist('r22s','var') && ~isempty(r22s) % Quadratic T2* relaxivity (blood)
    R2s = r2s*conc + r22s*conc.^2 + R20s; % 1/ms
else
    R2s = r2s*conc + R20s; % 1/ms
end
end

function mp = mrsignal( R1, R2, tr, te, fa)
% R1 : 1/ms
% R2 : 1/ms
% tr : ms
% te : ms
% fa : radians

mp = mptime(tr,te,fa,R1,R2);

end

function mzData = mztime(tr,fa,R1)
mzData = ((1-exp(-tr.*R1))./(1-exp(-tr.*R1).*cos(fa)));
end

function mpData = mptime(tr,te,fa,R1,R2)
mpData = mztime(tr,fa,R1).*sin(fa).*exp(-te.*R2);
end