function [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = signalbiast10(aifConc,gmConc,n0,bloodFractionReq,biasT10,imgModel)
% SIGNALBIAST10 compute arterial and gray matter signal with a reference
% blood T1 value and combine them to a measured signal with various blood 
% fractions. Returned bloodT10 matrix can be used as the biased blood T1 
% to investigate this error in concentration calculation.
%
%   [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = SIGNALBIAST10(
%       aifConc,gmConc,n0,bloodFractionReq,biasT10,imgModel)
%
% See also: SIGNALMRISPGR, SIGNALBIASSUSCEPTIBILITY, SIGNALBIASREACHSS
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-03-09

nBloodFraction = length(bloodFractionReq);
nT10 = length(biasT10);
concSat = [0 0 1e12];
nT = length(aifConc);
nTSat = length(concSat);
artSignal = NaN(nBloodFraction,nT10,1,nT);
aifSignal = NaN(nT10,nT);
aifSignalMagnitude = NaN(nT10,nT);
aifSignalPhase = zeros(nT10,nT);
gmSignal = NaN(nT10,nT);
satSignal = NaN(nT10,nTSat);
bloodT10 = NaN(nBloodFraction,nT10,1);
for iT10 = 1:nT10
    [aifSignalMagnitude(iT10,:), imgModel.bloodParam] = imgModel.FHandle(aifConc,n0,...
        imgModel.bloodParam,imgModel.seqParam);
    if isfield(imgModel,'phase') && isfield(imgModel.phase,'FHandle') && ...
            isfield(imgModel.bloodParam.tracer,'enablePhase') && imgModel.bloodParam.tracer.enablePhase
        [aifSignalPhase(iT10,:), imgModel.bloodParam] = imgModel.phase.FHandle(aifConc,n0,...
            imgModel.bloodParam,imgModel.seqParam);
        aifSignal(iT10,:) = aifSignalMagnitude(iT10,:).*exp(-1j*aifSignalPhase(iT10,:));
    else
        aifSignal(iT10,:) = aifSignalMagnitude(iT10,:);
    end
    satSignal(iT10,:) = imgModel.FHandle(concSat,3,...
        imgModel.bloodParam,imgModel.seqParam);
    % Tissue
    [gmSignal(iT10,:), imgModel.tissueParam] = imgModel.FHandle(gmConc,n0,...
        imgModel.tissueParam,imgModel.seqParam);
    for iBf = 1:nBloodFraction
        % Combined signal
        artSignal(iBf,iT10,1,:) = bloodFractionReq(iBf)*aifSignal(iT10,:) + (1-bloodFractionReq(iBf))*gmSignal(iT10,:);
    end
    bloodT10(:,iT10) = biasT10(iT10);
end
end