function [voxelPhaseOffset,isVessel,isTissue,bloodFraction,unitVector] = xivoxelinfinitetube(dPhase,vesselParam,isImgVoxel,simVoxelRes,simDims)
% XIVOXELINFINITETUBE Susceptibility simulations of an infinite tube
% (vesselParam) centered in a simulated voxel. Shifting the tube in the 
% imaging voxel is done with the isImgVoxel mask. 
% These simulations are based on Haacke et al. e.q. (26.6)
%
%   [voxelPhaseOffset,isVessel,isTissue,bloodFraction,unitVector] = XIVOXELINFINITETUBE(dPhase,vesselParam,isImgVoxel,simVoxelRes,simDims)
%
% See also: SIGNALBIASSUSCEPTIBILITY
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-03-07

%% Vessel orientation unit vector
vesselRadius = vesselParam.radius;
angOfVesselTheta = vesselParam.theta;
angOfVesselPhi = vesselParam.phi;

unitVector = [sin(angOfVesselTheta)*cos(angOfVesselPhi), sin(angOfVesselTheta)*sin(angOfVesselPhi), cos(angOfVesselTheta)];
if unitVector(1) ~= 0 && unitVector(2) ~= 0
    orthogUnitVector(2) = sqrt(unitVector(2)^2*unitVector(3)^2/(unitVector(3)^2*(unitVector(1)^2+unitVector(2)^2)+(unitVector(1)^2+unitVector(2)^2)^2));
    orthogUnitVector(1) = unitVector(1)/unitVector(2)*orthogUnitVector(2);
    orthogUnitVector(3) = -(unitVector(1)^2+unitVector(2)^2)/(unitVector(2)*unitVector(3))*orthogUnitVector(2);
elseif unitVector(1) ~= 0
    vecRatio = unitVector(3)/unitVector(1);
    orthogUnitVector = [-vecRatio/sqrt(vecRatio^2+1) 0 1/sqrt(vecRatio^2+1)];
elseif unitVector(2) ~= 0
    vecRatio = unitVector(3)/unitVector(2);
    orthogUnitVector = [0 -vecRatio/sqrt(vecRatio^2+1) 1/sqrt(vecRatio^2+1)];
else
    orthogUnitVector = [1 0 0];
end

%% Vessel radius
voxelCoordVectorX = linspace(-simVoxelRes(1)/2,simVoxelRes(1)/2,simDims(1));
voxelCoordVectorY = linspace(-simVoxelRes(2)/2,simVoxelRes(2)/2,simDims(2));
voxelCoordVectorZ = linspace(-simVoxelRes(3)/2,simVoxelRes(3)/2,simDims(3));

[voxelCoordGridX,voxelCoordGridY,voxelCoordGridZ] = ndgrid(voxelCoordVectorX,voxelCoordVectorY,voxelCoordVectorZ);

distanceAlongVessel = voxelCoordGridX*unitVector(1) + voxelCoordGridY*unitVector(2) + voxelCoordGridZ*unitVector(3);
voxelGridNorm = voxelCoordGridX.*voxelCoordGridX + voxelCoordGridY.*voxelCoordGridY + voxelCoordGridZ.*voxelCoordGridZ;
radiusFromVesselCentre = sqrt(voxelGridNorm - distanceAlongVessel.^2);

isVessel = (radiusFromVesselCentre<vesselRadius);

%% Vessel susceptibility
% theta = acos(distanceAlongVessel./sqrt(voxelGridNorm));

orthogResidualVecX = voxelCoordGridX - distanceAlongVessel*unitVector(1);
orthogResidualVecY = voxelCoordGridY - distanceAlongVessel*unitVector(2);
orthogResidualVecZ = voxelCoordGridZ - distanceAlongVessel*unitVector(3);
orthogResidualNorm = orthogResidualVecX.*orthogResidualVecX +orthogResidualVecY.*orthogResidualVecY +orthogResidualVecZ.*orthogResidualVecZ;
orthogResidualProj = orthogResidualVecX*orthogUnitVector(1) +orthogResidualVecY*orthogUnitVector(2) +orthogResidualVecZ*orthogUnitVector(3);

phi = acos(orthogResidualProj./sqrt(orthogResidualNorm));

voxelPhaseOffset = NaN(simDims);
voxelPhaseOffset(isVessel) = dPhase*(cos(angOfVesselTheta)^2-1/3);
voxelPhaseOffset(~isVessel) = dPhase*(sin(angOfVesselTheta)*vesselRadius./radiusFromVesselCentre(~isVessel)).^2.*cos(2*phi(~isVessel));

%% Final masks
isVessel = isVessel & isImgVoxel;
isTissue = (radiusFromVesselCentre>=vesselRadius) & isImgVoxel;
bloodFraction = sum(isVessel(:))/sum(isImgVoxel(:));

end