function [artSignal,aifSignal,gmSignal,satSignal,bloodT10,figSs] = signalbiasreachss(aifConc,gmConc,n0,bloodFractionReq,biasNTrSs,imgModel)
% SIGNALBIASREACHSS compute arterial and gray matter signal with blood
% that has not reached steady state.
%
%   [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = SIGNALBIASREACHSS(aifConc,gmConc,n0,bloodFractionReq,biasNTrSs,imgModel)
%
% See also: SIGNALMRISPGR
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-03-09

nBloodFraction = length(bloodFractionReq);
nSs = length(biasNTrSs);
concSat = [0 0 1e12];
nT = length(aifConc);
nTSat = length(concSat);
artSignal = NaN(nBloodFraction,nSs,1,nT);
aifSignalMagnitude = NaN(nSs,nT);
aifSignalPhase = NaN(nSs,nT);
aifSignal = NaN(nSs,nT);
gmSignal = NaN(nSs,nT);
satSignal = NaN(nSs,nTSat);
bloodT10 = 1/imgModel.bloodParam.R10*ones(nBloodFraction,nSs,1);

imgModel.bloodParam.nRep = max(biasNTrSs);
[aifSignalMaxTr, imgModel.bloodParam] = signalmrispgrreachss(aifConc,n0,...
    imgModel.bloodParam,imgModel.seqParam);
for iSs = 1:nSs
    % Arterial
    aifSignalMagnitude(iSs,:) = aifSignalMaxTr(biasNTrSs(iSs),:);
    satSignal(iSs,:) = imgModel.FHandle(concSat,3,...
        imgModel.bloodParam,imgModel.seqParam);
    if isfield(imgModel,'phase') && isfield(imgModel.phase,'FHandle') && ...
            isfield(imgModel.bloodParam.tracer,'enablePhase') && imgModel.bloodParam.tracer.enablePhase
        [aifSignalPhase(iSs,:), imgModel.bloodParam] = imgModel.phase.FHandle(aifConc,n0,...
            imgModel.bloodParam,imgModel.seqParam);
        aifSignal(iSs,:) = aifSignalMagnitude(iSs,:).*exp(-1j*aifSignalPhase(iSs,:));
    else
        aifSignal(iSs,:) = aifSignalMagnitude(iSs,:);
    end
    % Tissue
    [gmSignal(iSs,:), imgModel.tissueParam] = imgModel.FHandle(gmConc,n0,...
        imgModel.tissueParam,imgModel.seqParam);
    for iBf = 1:nBloodFraction
        % Combined signal
        artSignal(iBf,iSs,1,:) = bloodFractionReq(iBf)*aifSignal(iSs,:) + (1-bloodFractionReq(iBf))*gmSignal(iSs,:);
    end
end

%%
if nargout > 5
    iBf = ceil(nBloodFraction/2);
    ssColorOrder = 'rbg';
    [aifSignalAtSs, ~] = imgModel.FHandle(aifConc,n0,...
        imgModel.bloodParam,imgModel.seqParam);
    artSignalAtSs = bloodFractionReq(iBf)*aifSignalAtSs + (1-bloodFractionReq(iBf))*gmSignal(1,:);
    shiftArtSignal = 0;
    scaleArtSignal = 1/(max(aifSignalAtSs)-shiftArtSignal);
    allRepsModel = imgModel;
    allRepsModel.bloodParam.nRep = 300;
    allRepsModel.seqParam.B1Reps = ones(allRepsModel.bloodParam.nRep,1);
    allBiasNTrSs = biasNTrSs - min(biasNTrSs) + 1;
    [aifSignalAllTrs, allRepsModel.bloodParam] = signalmrispgrreachss(aifConc,n0,...
        allRepsModel.bloodParam,allRepsModel.seqParam);
    [~,nPeak] = max(aifConc);
    
    figSs = figure;
    figSs.Position = [ 610   385   807   475];
    subplot(2,3,1);
    p1 = plot(1:imgModel.bloodParam.nRep,scaleArtSignal*(aifSignalMaxTr(:,nPeak)-shiftArtSignal),'k-','LineWidth',1.5); hold on;
    p2 = plot(1:imgModel.bloodParam.nRep,scaleArtSignal*(aifSignalMaxTr(:,1)-shiftArtSignal),'k:','LineWidth',1.5);
    signalLim = ylim;
    for iSs = 1:nSs
        p3 = plot(biasNTrSs(iSs)+[0 10*eps],signalLim,[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '-']);
    end
    hold off;
    xlabel('Number of RF excitations');
    ylabel('Blood signal (a.u.)');
    
    subplot(2,3,2);
    legendNames = cell(1,length(biasNTrSs)+1);
    legendNames{1} = 'Steady-state';
    plot(imgModel.seqParam.time,scaleArtSignal*(aifSignalAtSs-shiftArtSignal),'-k','LineWidth',2); hold on;
    for iSs = 1:nSs
        plot(imgModel.seqParam.time,scaleArtSignal*(aifSignalMaxTr(biasNTrSs(iSs),:)-shiftArtSignal),[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '.-']);
        legendNames{iSs+1} = ['N_T_R = ' num2str(biasNTrSs(iSs))];
    end
    hold off;
    ylabel('Blood signal (a.u.)');
    xlabel('Time (min)');
    xlim(imgModel.seqParam.t0+[-0.1 1.1]);
    ylim([0 1.1]);
    
    subplot(2,3,3);
    plot(imgModel.seqParam.time,scaleArtSignal*(artSignalAtSs-shiftArtSignal),'-k','LineWidth',2);hold on;
    for iSs = 1:nSs
        plot(imgModel.seqParam.time,scaleArtSignal*(squeeze(abs(artSignal(iBf,iSs,1,:)))-shiftArtSignal),[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '.-']);
    end
    hold off;
    ylabel('Tissue signal (a.u.)');
    xlabel('Time (min)');
    xlim(imgModel.seqParam.t0+[-0.1 1.1]);
    % ylim([0.15 0.35]);
    
    % Without B1 slice profile
    subplot(2,3,4);
    p1 = plot(1:allRepsModel.bloodParam.nRep,scaleArtSignal*(aifSignalAllTrs(:,nPeak)-shiftArtSignal),'k-','LineWidth',1.5); hold on;
    p2 = plot(1:allRepsModel.bloodParam.nRep,scaleArtSignal*(aifSignalAllTrs(:,1)-shiftArtSignal),'k:','LineWidth',1.5);
    signalLim = ylim;
    for iSs = 1:nSs
        p3 = plot(biasNTrSs(iSs)+[0 10*eps],signalLim,[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '-']);
    end
    hold off;
    xlabel('Number of RF excitations');
    ylabel('Blood signal (a.u.)');
    legend('Peak AIF','Initial');
    
    subplot(2,3,5);
    legendNames = cell(1,length(biasNTrSs)+1);
    legendNames{1} = 'Steady-state';
    plot(allRepsModel.seqParam.time,scaleArtSignal*(aifSignalAtSs-shiftArtSignal),'-k','LineWidth',2); hold on;
    for iSs = 1:nSs
        plot(allRepsModel.seqParam.time,scaleArtSignal*(aifSignalAllTrs(biasNTrSs(iSs),:)-shiftArtSignal),[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '.-']);
        legendNames{iSs+1} = ['N_T_R = ' num2str(biasNTrSs(iSs))];
    end
    hold off;
    ylabel('Blood signal (a.u.)');
    xlabel('Time (min)');
    xlim(allRepsModel.seqParam.t0+[-0.1 1.1]);
    ylim([0 1.25]);
    
    subplot(2,3,6);
    plot(allRepsModel.seqParam.time,scaleArtSignal*(artSignalAtSs-shiftArtSignal),'-k','LineWidth',2);hold on;
    for iSs = 1:nSs
        plot(allRepsModel.seqParam.time,scaleArtSignal*(squeeze(abs(artSignal(iBf,iSs,1,:)))-shiftArtSignal),[ssColorOrder(mod(iSs,length(ssColorOrder))+1) '.-']);
    end
    hold off;
    ylabel('Tissue signal (a.u.)');
    xlabel('Time (min)');
    xlim(allRepsModel.seqParam.t0+[-0.1 1.1]);
    legend(legendNames,'Location','Best');
    % ylim([0.15 0.35]);
end

end