function [signalPhase, tissueParam] = signalmrispgrphase(conc,~,tissueParam,imgParam)
% SIGNALMRISPGRPHASE Computes MRI RF-spoiled Gradient-recalled echo steady-state
% signal phase from the concentration inside a vessel.
% Based on equation 2 of (Foottit et al., MRM,2010).
%
%   [signalPhase, tissueParam] = SIGNALMRISPGRPHASE(conc,[],tissueParam,imgParam)
%   Tissue signal curve is computed from the tissue concentration given
%   tracer parameters (molar susceptibility (xiM)), tissue initial phase 
%   (signalPhase0)) and orientation with B0 (theta), and imaging 
%   parameters (te, B0).
%
% See also: SIGNALMRISPGR, SIGNALPET
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-07-10

h1GyromagRatio = 2.675e8; % rad/(T s)
if isfield(imgParam,'B0')
    B0 = imgParam.B0;
else
    B0 = 3; %T
end
if isfield(tissueParam,'theta')
    theta = tissueParam.theta;
else
    theta = 0;% 0 radians = parallel with B0
end
if isfield(tissueParam,'signalPhase0')
    signalPhase0 = tissueParam.signalPhase0;
else
    signalPhase0 = 0;
end

xiM = tissueParam.tracer.xiM;

nE = length(imgParam.te);
nConc = length(conc);
if isrow(conc)
    signalPhase = NaN(nE,nConc);
    for iE=1:nE
        phaseChange = mrphase(conc,imgParam.te,xiM,B0,theta,h1GyromagRatio);
        signalPhase(iE,:) = phaseChange + signalPhase0;
    end
else
    signalPhase = NaN(nConc,nE);
    for iE=1:nE
        phaseChange = mrphase(conc,imgParam.te,xiM,B0,theta,h1GyromagRatio);
        signalPhase(:,iE) = phaseChange + signalPhase0;
    end
end
end

function phaseConc = mrphase(conc,te,xiM,B0,theta,gammaRad)
% conc [mM]
% te [ms]
% xiM [mM-1]
% B0 [T]
% theta [radians]
% gammaRad [rad/(T s)]

phaseConc = (te/1e3)*gammaRad/2*B0*xiM*conc*(cos(theta)^2-1/3);
end