function [signal, tissueParam]= signalpet(conc,~,tissueParam,~)
% SIGNALPET Computes PET signal from tissue concentration.
%
%   signal = SIGNALPET(conc,[],tissueParam,[])
%   Tissue signal curve (kBq/mL) is computed from the tissue concentration 
%   (kBq/mL or mM) given imaging parameter. Basic model, PET system
%   normalization and dynamic decay correction are expected to be accurate.
%
% See also SIGNALMRISPGR
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-06-06

switch tissueParam.units
    case 'mM' % Conversion of mmol/mL to kBq/mL
        avogadroNumber = 6.022140857e23; % atoms/moles
        decayRate = log(2)/(tissueParam.tracer.decay*60)/1e3; % 10^3/sec = kBq
        scalingFactor = decayRate*(avogadroNumber/1e3); % kBq/mmol
    case 'Bq/mL'
        scalingFactor = 1e-3;
    case 'kBq/mL'
        scalingFactor = 1;
    otherwise
        warning('Unknown PET tissue concentration units. [kBq/mL] unit conversion are required for noise calculation.');
end
signal = scalingFactor*conc; % kBq/mL
tissueParam.units = 'kBq/mL';
end