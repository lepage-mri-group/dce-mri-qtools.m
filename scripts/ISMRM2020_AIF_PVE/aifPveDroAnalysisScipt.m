% AIFPVEDROANALYSIS
%
% This script creates and analyze the AIF PVE digital reference object 
% (DRO) of the ISMRM 2020 abstract. It reproduces the figures 2 and 3 of the
% abstract.
%
% Author: Benoit Bourassa-Moreau
% Creation date: 2019-05-28

%% Create the DROinjDose = (0.05)*72; % mL = (mmol/kg * kg)*1 mL/mmol
injDose = (0.05)*72; % mL = (mmol/kg * kg)*1 mL/mmol
time = (0:2:660)/60;
t0 = 60.25/60; % min
aifPeak = 3; % mM
tissueParam.pkm{1} = [0 0.01 0.02 0.05 0.1 0.2]; % Ktrans in (1/min)
tissueParam.pkm{2} = [0.1,0.2, 0.5]; % Ve in (-)
tissueParam.pkm{3} = [0.001,0.005,0.01,0.02,0.05,0.1]; % Vp in (-)
tissueParam.T10 = 1000; % ms (about the white matter T10) QIBA
tissueParam.T20s = 35; % ms (Half white matter T20) Stanisz et al. 2005
tissueParam.r1 = 4.5; % QIBA4, 1/(mM*s)
tissueParam.r2s = 87; % Kjolby et al. 2006 (1/(mM*s))

bloodParam.T10 = 1440;  % QIBA_v4, 1/ms
bloodParam.T20s = 35; % ms (Half white matter T20) Stanisz et al. 2005
bloodParam.r1 = 4.5; % QIBA4, 1/(mM*s)
bloodParam.r2s = 0.4929; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
bloodParam.r22s =  2.6159; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
bloodParam.xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)

seqParam.te = 1.85; % ms
seqParam.tr = 4; % ms
seqParam.fa = 9.2/180*pi; % rad
% Changes from QIBA_v4 - Noise is added
seqParam.nNoise = 10;
seqParam.snr = 25;
seqParam.B0 = 3; % Tesla
seqParam.voxelRes = 2.3; % mm

vofParam.bloodParam.theta = 15/180*pi; % radians
vofParam.bloodParam.r2s = 0.4929; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
vofParam.bloodParam.r22s =  2.6159; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006

[droOptions.aifModel,droOptions.pkModel,droOptions.imgModel] = defaultmodelparams(time,t0,aifPeak,tissueParam,bloodParam,seqParam);

aifRows.idxLocation = [15 14];
aifRows.bloodFraction = [0.6 0.2];
[dataAIF, phaseAif, signalData, concData,aifMask,droParam,T10,trueDroParam] = aifpvedrocreate(time,droOptions,vofParam,aifRows);

% warning('Replace with manual folder selection');
% baseFolder = findphddirpath();
% processingFolder = fullfile(baseFolder,'5-Experiences\Phantom\8-Numerique\2019-11-12_AIFDRO_TestGitOpen');
processingFolder = uigetdir(pwd,'Select results folder');

figureFolder = mksubdirifneeded(processingFolder,'figures');
workspaceFolder = mksubdirifneeded(processingFolder,'workspaces');

droParam.aifModel.minXIdx = 1;
[cardiacOutput, auc1st] = aifcardiacoutput(droParam.aifModel,injDose);

%% Prepare matrix
n0 = find(time<droParam.aifModel.Params.t0,1,'last');
magPerm = permute(dataAIF,[3, 4, 2,1]);
phasePerm = permute(phaseAif,[3, 4, 2,1]);
T10BloodPerm = T10;
[nR,nC,nS,nT]=size(magPerm);
signal0 = mean(magPerm(:,:,:,1:n0),4);
b1MapPerm = ones(nR,nC,nS);
maskPermute = true(nR,nC,nS);

%% Compute basic Tissue concentration
[concPermute, outliers] = concmrispgrclosed(magPerm,signal0,...
    droParam.imgModel.tissueParam.tracer,T10BloodPerm,droParam.imgModel.seqParam,maskPermute,b1MapPerm);

%% Find basic bolus range
[dynPxList, ~] = dyn3dconversion(concPermute,maskPermute);
meanAllDyn = mean(dynPxList,1);
            use2ndDeriv = true; 
[~, rangeBolus, ~] = fitinitialenhancement(time,meanAllDyn,n0,[],[], use2ndDeriv); 
bolusRange = [rangeBolus(2) (3*rangeBolus(5)-2*rangeBolus(2))];
iauc = sum(concPermute(:,:,:,bolusRange(1):bolusRange(2)),4);


%% Semi-automatic mask
gmMask = maskPermute; % For a patient this should be the mask provided by SPM.

optionsSemiAuto.waitbar=1; % 0:off, 1:on
optionsSemiAuto.display=2;
optionsSemiAuto.aif.nSlice = 1; % Displayed slice for debug plots
optionsSemiAuto.aif.pArea= 0.7000; % AUC voxel rejection fraction
optionsSemiAuto.aif.pTTP= 0.45000; % TTP rejection fraction for vessels to tissue separation
manualMaskFile = fullfile(workspaceFolder,'manualDroMask.mat');

redoManualMasks = false;
if redoManualMasks || ~exist(manualMaskFile,'file')
    [venous.mask, arterial.mask, arterial.pxList, tissues.mask, tissues.pxList, venous.squareCentre, arterial.squareCentre] = ...
        semiautomasksvenarttiss(time,concPermute,iauc,maskPermute,gmMask,iauc,optionsSemiAuto);
    
    fprintf('--- DRO 20%% TEST ---\n');
    fprintf('--- Skip venous regions and limit arterial regions to 20%% ---\n');
    [~, arterial20p.mask, arterial20p.pxList, tissues20p.mask, tissues20p.pxList, ~, arterial20p.squareCentre] = ...
        semiautomasksvenarttiss(time,concPermute,iauc,maskPermute,gmMask,iauc,optionsSemiAuto);
    
    save(manualMaskFile,'venous','arterial','tissues','arterial20p','tissues20p');
else
    load(manualMaskFile);
end
%% DRO ideal masks
maskVof = aifMask.data == 1;

arterialTheo60p.mask = aifMask.data == 2;
arterialTheo20p.mask = aifMask.data == 3;
% Tissue voxels mask and list for PVC
[tissuesTheo20p.mask, ~,arterialTheo20p.pxList] = findsurroundingtissue(iauc,magPerm,maskPermute,arterialTheo20p.mask);
tissuesTheo20p.pxList = arterialTheo20p.pxList; tissuesTheo20p.pxList(:,1) = arterialTheo20p.pxList(:,1)-1;
[tissuesTheo60p.mask, ~,arterialTheo60p.pxList] = findsurroundingtissue(iauc,magPerm,maskPermute,arterialTheo60p.mask);
tissuesTheo60p.pxList = arterialTheo60p.pxList; tissuesTheo60p.pxList(:,1) = arterialTheo60p.pxList(:,1)-1;

artCalculationMask = arterial.mask | arterialTheo20p.mask | arterialTheo60p.mask;

%% Venous conc calculations
framesPre = [1 n0];
clusterType = 'bf';
droParam.imgModel.bloodParam.tracer.framesBaseline = framesPre;
firstPassDelay = 0.5; % min
pveEndIdx = find(time>4,1,'first')-1;
nZ = 1;
aifOptions = aifpvedefineoptions(time,framesPre,pveEndIdx,nZ,'basic',clusterType);

% optionsVofCluster.aif.nVoxelMax= 40; % maximum number of voxel in final AIF cluster
% optionsVofCluster.aif.nVoxelMin= 20; % minimum number of voxel in final AIF cluster
% optionsVofCluster.waitbar=1; % 0:off, 1:on
% optionsVofCluster.display=2;
% optionsVofCluster.aif.nSlice = 1; % Displayed slice for debug plots
% optionsVofCluster.aif.diffPicco= 0; % Cluster selection criterion: Threshold between Peak or TTP criterion
% 
% min1stPassPoints = 5;
% firstPassEndIdx = find(time>(time(framesPre(2))+firstPassDelay),1,'first');
% firstPassEndIdx = max(firstPassEndIdx,framesPre(2)+min1stPassPoints);
% optionsVofCluster.aif.cluster1stPassIdx = [framesPre(2) firstPassEndIdx];
% optionsVofCluster.time=time;

% Closed
optionsVofClosed = aifOptions.vof;
optionsVofClosed.conc.type = 'basic';
[venousClosed.fit,venousClosed.cluster, venousClosed.all, venousClosed.conc] = ...
    globalvof(time,[],[],T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,venous.mask,optionsVofClosed,concPermute);
% Open 
optionsVofOpen = aifOptions.vof;
optionsVofOpen.conc.type = 'open-form';
[venousOpen.fit,venousOpen.cluster, venousOpen.all, venousOpen.conc] = ...
    globalvof(time,magPerm,[],T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,venous.mask,optionsVofOpen,concPermute);

% CPX linear r2*
closedBloodRelax = droParam.imgModel.bloodParam.tracer;
closedBloodRelax.enableQuadR2 = false;
closedBloodRelax.r2s = 6.3; % Simonis
optionsVofR2 = aifOptions.vof;
[venousCpxLin.fit,venousCpxLin.cluster, venousCpxLin.all, venousCpxLin.conc] = ...
    globalvof(time,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    closedBloodRelax,droParam.imgModel.seqParam,venous.mask,optionsVofR2,concPermute);

% CPX quadratic r2*
[venousCpx.fit,venousCpx.cluster, venousCpx.all, venousCpx.conc] = ...
    globalvof(time,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,venous.mask,optionsVofR2,concPermute);

%% Arterial conc calculations
% Prepare parameters
bloodTracerParam = droParam.imgModel.bloodParam.tracer;
bloodTracerParam.enableBloodR2 = false;
bloodTracerParamOpen = droParam.imgModel.bloodParam.tracer;
bloodTracerParamOpen.enableBloodR2 = true;
aifCPvcOptions = aifpvedefineoptions(time,framesPre,pveEndIdx,nZ,'c-pvc',clusterType);
aifSPvcCOptions = aifpvedefineoptions(time,framesPre,pveEndIdx,nZ,'s-pvc-closed-form',clusterType);
aifSPvcOOptions = aifpvedefineoptions(time,framesPre,pveEndIdx,nZ,'s-pvc-open-form',clusterType);

% Mask auto all arterial
% Basic
[maskAutoAll.artClosed,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venousCpx,aifOptions);
% PVC-Conc
[maskAutoAll.artPveConc,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venousCpx,aifCPvcOptions);
% PVC-Signal Closed
[maskAutoAll.artPveClosed,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParam,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venousCpx,aifSPvcCOptions);
% PVC-Signal Open
[maskAutoAll.artPveOpen,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParamOpen,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venousCpx,aifSPvcOOptions);

% Mask auto 20% arterial
% Basic
[maskAuto20p.artClosed,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial20p,tissues20p,venousCpx,aifOptions);
% PVC-Conc
[maskAuto20p.artPveConc,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial20p,tissues20p,venousCpx,aifCPvcOptions);
% PVC-Signal Closed
[maskAuto20p.artPveClosed,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParam,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial20p,tissues20p,venousCpx,aifSPvcCOptions);
% PVC-Signal Open
[maskAuto20p.artPveOpen,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParamOpen,droParam.imgModel.seqParam,framesPre, ...
    venous, arterial20p,tissues20p,venousCpx,aifSPvcOOptions);


% Mask theo 60p
% Basic
[maskTheo60p.artClosed,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo60p, tissuesTheo60p,venousCpx,aifOptions);
% PVC-Conc
[maskTheo60p.artPveConc,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo60p, tissuesTheo60p,venousCpx,aifCPvcOptions);
% PVC-Signal Closed
[maskTheo60p.artPveClosed,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParam,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo60p, tissuesTheo60p,venousCpx,aifSPvcCOptions);
% PVC-Signal Open
[maskTheo60p.artPveOpen,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParamOpen,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo60p, tissuesTheo60p,venousCpx,aifSPvcOOptions);

% Mask auto 20% arterial
% Basic
[maskTheo20p.artClosed,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo20p,tissuesTheo20p,venousCpx,aifOptions);
% PVC-Conc
[maskTheo20p.artPveConc,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droParam.imgModel.bloodParam.tracer,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo20p,tissuesTheo20p,venousCpx,aifCPvcOptions);
% PVC-Signal Closed
[maskTheo20p.artPveClosed,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParam,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo20p,tissuesTheo20p,venousCpx,aifSPvcCOptions);
% PVC-Signal Open
[maskTheo20p.artPveOpen,~] = globalaif(...
    time,[],magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    bloodTracerParamOpen,droParam.imgModel.seqParam,framesPre, ...
    venous, arterialTheo20p,tissuesTheo20p,venousCpx,aifSPvcOOptions);

aifXLim = [0.9 2.0];
aifConcLim = [-0.5 4.0];
figure;
subplot(3,1,1);
plot(time,maskAutoAll.artPveConc.cluster.mean,time,maskAutoAll.artPveConc.fit.conc);
xlim(aifXLim); ylim(aifConcLim);
ylabel('AIF (mM)'); title('C-PVC');
subplot(3,1,2);
plot(time,maskAutoAll.artPveClosed.cluster.mean,time,maskAutoAll.artPveClosed.fit.conc);
xlim(aifXLim); ylim(aifConcLim);
ylabel('AIF (mM)'); title('S-PVC - Closed-form');
subplot(3,1,3);
plot(time,maskAutoAll.artPveOpen.cluster.mean,time,maskAutoAll.artPveOpen.fit.conc);
xlim(aifXLim); ylim(aifConcLim);
xlabel('Time (min)');
ylabel('AIF (mM)'); title('S-PVC - Open-form');
legend('Data','Fit');


%% FIT PKM eTofts-Kety
chosenAif = maskAutoAll.artPveClosed.cluster.mean;
[saturatedAifSignal, ~] = signalmrispgr(chosenAif,n0,droParam.imgModel.bloodParam,droParam.imgModel.seqParam);

[saturatedAif, ~] = concmrispgrclosed(saturatedAifSignal,mean(saturatedAifSignal(1:n0)),...
    droParam.imgModel.tissueParam.tracer,1/droParam.imgModel.bloodParam.R10,droParam.imgModel.seqParam,true,1);

figure;
plot(time,chosenAif,time,saturatedAif);
pkmFHandle = @pkmetofts;
x0 = [droParam.pkModel.Ktrans(3) droParam.pkModel.Ve(2) droParam.pkModel.Vp(3)];
lb = [droParam.pkModel.Ktrans(1) droParam.pkModel.Ve(1) droParam.pkModel.Vp(1)];
ub = [droParam.pkModel.Ktrans(end) droParam.pkModel.Ve(end) droParam.pkModel.Vp(end)]*2;
rb = false(1,3);
order = true(1,3);
fitOptions  = optimoptions('lsqcurvefit');
fitOptions.Display = 'Off';

[xMatClosed, errorInfoClosed] = fitpkm(time,maskAutoAll.artPveClosed.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);
[xMatBasic, errorInfoBasic] = fitpkm(time,maskAutoAll.artClosed.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);
[xMatAuc, errorInfoAuc] = fitpkm(time,maskAutoAll.artPveConc.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);
[xMatVof, errorInfoVof] = fitpkm(time,venousCpx.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);

%% Display DRO
droParam.pkModel.displayNames = {'K^T^r^a^n^s','v_e','v_p'};
droParam.pkModel.unitNames = {'(mL/100g min)','(mL/100g)','(mL/100g)'};
droTrueMaps = permute(trueDroParam,[2 3 1]);
[figDroMapSPveClosed, droSPveClosed.mean,droSPveClosed.bias,droSPveClosed.std, cLimMapOpen,cLimErrorOpen] = drodisplaymapserror(xMatClosed,droTrueMaps,droParam,aifMask.data);
[figDroMapCPve, droCPve.mean,droCPve.bias,droCPve.std,~,~] = drodisplaymapserror(xMatAuc,droTrueMaps,droParam,aifMask.data, cLimMapOpen,cLimErrorOpen);
[figDroMapCNoPve, droCNoPve.mean,droCNoPve.bias,droCNoPve.std,~,~] = drodisplaymapserror(xMatBasic,droTrueMaps,droParam,aifMask.data, cLimMapOpen,cLimErrorOpen);
[figDroMapVof, droVof.mean,droVof.bias,droVof.std,~,~] = drodisplaymapserror(xMatVof,droTrueMaps,droParam,aifMask.data, cLimMapOpen,cLimErrorOpen);

export_fig(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.png'),'-transparent','-m2');
export_fig(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.pdf'),'-transparent','-m2');
saveas(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.fig'),'fig');

export_fig(figDroMapCNoPve,fullfile(figureFolder, 'droMapsErrorCNoPve.png'),'-transparent','-m2');
export_fig(figDroMapCNoPve,fullfile(figureFolder, 'droMapsErrorCNoPve.pdf'),'-transparent','-m2');
saveas(figDroMapCNoPve,fullfile(figureFolder, 'droMapsErrorCNoPve.fig'),'fig');

export_fig(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.png'),'-transparent','-m2');
export_fig(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.pdf'),'-transparent','-m2');
saveas(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.fig'),'fig');

%%
droTrueMapsNoiseSize = repelem(droTrueMaps,10,10);
repAifMask = repmat(aifMask.data,[1 1 3]);
droTrueMapsNoiseSizeVessRows = NaN(size(repAifMask));
droTrueMapsNoiseSizeVessRows(repAifMask==0) = droTrueMapsNoiseSize(:);
colorVessel = {[236 112  22]./255, [ 52 148 186]./255, [119 147 60]./255, [149 55 53]./255,[128 100 162]./255};
figVarParam = figure;
% set(figVarParam,'Position',[323         195        1147         449]);
figVarParam.Position = [469    50   2*657   449];
nParam = 3;nVessel = 3;
pkmCompLinRsquared = NaN(nParam*nVessel,5);
for iParam=1:nParam
    subplot(1,3,iParam);
    cParamTrue = reshape(droTrueMapsNoiseSize(:,:,iParam),[],1);
    nParamSub = length(droParam.pkModel.(droParam.pkModel.paramNames{iParam}));
    cParamMean = NaN(3,nParamSub);
    cParamStd = NaN(3,nParamSub);
    cParamMap{1} = xMatVof(:,:,1,iParam);
    cParamMap{2} = xMatAuc(:,:,1,iParam);
    cParamMap{3} = xMatClosed(:,:,1,iParam);
    for iParamSub = 1:nParamSub
        cParamMask = droTrueMapsNoiseSizeVessRows(:,:,iParam)==droParam.pkModel.(droParam.pkModel.paramNames{iParam})(iParamSub);
        for iVessel=1:nVessel
            cParamMean(iVessel,iParamSub) = mean(cParamMap{iVessel}(cParamMask));
            cParamStd(iVessel,iParamSub) = std(cParamMap{iVessel}(cParamMask));
        end
    end
    minMaxCParam = droParam.pkModel.(droParam.pkModel.paramNames{iParam})([1 end]);
    cParamLim = mean(minMaxCParam)+1.3*diff(minMaxCParam)/2*[-1 1];
    lHandEq = plot(cParamLim,cParamLim,'k--','Linewidth',2);
    hold on;
    for iVessel=1:nVessel
        errorbar(droParam.pkModel.(droParam.pkModel.paramNames{iParam}),cParamMean(iVessel,:),cParamStd(iVessel,:),'.','Color',colorVessel{iVessel});
        cParamComputed = cParamMap{iVessel}(aifMask.data==0);
        [p,S] = polyfit(cParamTrue,cParamComputed,1);
        lHandVessel{iVessel} = plot(minMaxCParam,polyval(p,minMaxCParam),'-','Color',colorVessel{iVessel},'Linewidth',2);
        R = corrcoef(cParamTrue,cParamComputed);
        % Fit with errors
        mdl = fitlm(cParamTrue,cParamComputed);
        b = [mdl.Coefficients{'(Intercept)','Estimate'}, mdl.Coefficients{'x1','Estimate'}];
        bint = coefCI(mdl);
        H = [0 0; 0 1]; % Neglecting Intercept
        C = [0 1]'; % F-test that slope is One
        [pVal, F] = coefTest(mdl,H,C);
        rSquared = mdl.Rsquared.Ordinary;
        
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,1) = mean(bint(2,:));
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,2) = diff(bint(2,:))/2;
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,3) = mean(bint(1,:));
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,4) = diff(bint(1,:))/2;
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,5) = rSquared;
    end
    ax1 = gca; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
    
    xlim(cParamLim);
    ylim(cParamLim);
    ylabel([droParam.pkModel.displayNames{iParam} ' ' droParam.pkModel.unitNames{iParam}]);
    xlabel(['True ' droParam.pkModel.displayNames{iParam} ' ' droParam.pkModel.unitNames{iParam}]);
    axis square;
end
legend([lHandEq,lHandVessel{1},lHandVessel{2},lHandVessel{3}],'Line of equality','VOF','Contrast - Closed','Signal - Closed','Location','Best');

export_fig(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.png'),'-transparent','-m2');
export_fig(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.pdf'),'-transparent','-m2');
saveas(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.fig'),'fig');

%%
% Parameter stats
% noiseDimAcq = [3 4];
% [meanAcq, ~] = droparamsnoise(testCase.DRO(:,:,tissueRows,:), testCase.NNoise, noiseDimAcq);
figure; 
histLimBias = {[-20,2],[-25,5],[-10,5]};
histLimStd = {[0,2],[0,5],[0,1]};
for parameterIdx = 1:3
    subplot(3,2,2*parameterIdx-1);
    ktransHistList = linspace(histLimBias{parameterIdx}(1),histLimBias{parameterIdx}(2),16);
    histogram(reshape(droSPve.bias(:,:,parameterIdx),[],1),ktransHistList); hold on;
    histogram(reshape(droCPveClosed.bias(:,:,parameterIdx),[],1),ktransHistList);
    histogram(reshape(droCPveOpen.bias(:,:,parameterIdx),[],1),ktransHistList); hold off;
    subplot(3,2,2*parameterIdx);
    ktransHistList = linspace(histLimStd{parameterIdx}(1),histLimStd{parameterIdx}(2),16);
    histogram(reshape(droSPve.std(:,:,parameterIdx),[],1),ktransHistList); hold on;
    histogram(reshape(droCPveClosed.std(:,:,parameterIdx),[],1),ktransHistList);
    histogram(reshape(droCPveOpen.std(:,:,parameterIdx),[],1),ktransHistList); hold off;
    title(droParam.pkModel.displayNames{parameterIdx})
end
legend('Signal','Conc closed','Conc open');


%% DRO signal sample
displayedTimeIdx = 50;
clim = [500, 2100];
figDro = figure; 
figDro.Position = [92   104   560   636];

artMaskList = pixelfrommask(arterial.mask);
artMaskClusterList = pixelfrommask(maskAutoAll.artPveClosed.cluster.mask);
venMaskList = pixelfrommask(venous.mask);
venMaskClusterList = pixelfrommask(venousCpx.cluster.mask);

% imagesc(squeeze(dataAIF(displayedTimeIdx,1,:,:)),clim); colormap jet;
imagesc(iauc,[0 5]); colormap gray;
ax1 = gca;
[~,~,nX,nY] = size(dataAIF);
ax1.PlotBoxAspectRatio = [nY,nX,1];
ax1.XTick = (0:10:60)+0.5;
ytickRev = [(0:10:140), 141, 151 152:10:182, 183];
ax1.YTick = ytickRev+0.5;
ax1.XTickLabel = [];
ax1.YTickLabel = [];
ax1.LineWidth = 1.5;
hold on;
plot(artMaskList(:,2),artMaskList(:,1),'.g','Markersize',3);
plot(artMaskClusterList(:,2),artMaskClusterList(:,1),'or','Markersize',3);
plot(venMaskList(:,2),venMaskList(:,1),'.y','Markersize',3);
plot(venMaskClusterList(:,2),venMaskClusterList(:,1),'ob','Markersize',3);

export_fig(figDro,fullfile(figureFolder, 'droWMarkings.png'),'-transparent','-m2');
export_fig(figDro,fullfile(figureFolder, 'droWMarkings.pdf'),'-transparent','-m2');
saveas(figDro,fullfile(figureFolder, 'droWMarkings.fig'),'fig');

%% DRO AIF and VOF
shadedArea = false;
aifXLim = [0.9 2.0];
aifConcLim = [-0.5 4.0];
titlePosition = [0.5, 0.85, 0];
figAif = figure;
figAif.Position = [124    80   470   636];
gap = 0.02;
marg_h = 0.08;
marg_w = 0.08;
subtightplot(4,2,1,gap,marg_h,marg_w);
if shadedArea
    plot(time,droParam.aifModel.AIFFit,'k-s');
    hold on;
    plot(time,droParam.vofModel.AIFFit,'k.--','Markersize',20);
else
    plot(time,droParam.aifModel.AIFFit,'k-','Linewidth',2);
    hold on;
    plot(time,droParam.vofModel.AIFFit,'-.','Color',[1 1 1]*0.4,'Linewidth',2);
end
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
legend({'AIF','VOF'},'Position',[0.3163    0.8273    0.1404    0.0574]);
title('References', 'Units', 'normalized', 'Position', titlePosition);

plotColors = {[236 112  22]./255, [ 52 148 186]./255, [119 147 60]./255, [149 55 53]./255,[128 100 162]./255};
areaColors = { [243 169 114]./255,[128 193 219]./255, [169 209 142]./255, [217 150 148]./255,[179 162 199]./255};
errorbarOption.alpha = 0.5;
errorbarOption.error = 'std';
errorbarOption.line_width = 2;
errorbarOption.x_axis = time;
errorbarOption.handle = figAif;

if ~shadedArea
    aifXLimIdx(1) = find(time<aifXLim(1),1,'last');
    aifXLimIdx(2) = find(time>aifXLim(end),1,'first');
    conc2Display = false(size(time));
    conc2Display(aifXLimIdx(1):aifXLimIdx(2)) = true;
end

% VOF
subtightplot(4,2,3,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.vofModel.AIFFit,'k.--','Markersize',20); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pAll = plot_areaerrorbar(venousClosed.all.concList,errorbarOption);hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(venousClosed.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.vofModel.AIFFit(conc2Display),'k-','Color',[1 1 1]*0.4,'Linewidth',2); hold on;
    iPlot = 1;pAll = errorbar(time(conc2Display),venousClosed.all.mean(conc2Display),venousClosed.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 2;pCluster = errorbar(time(conc2Display),venousClosed.cluster.mean(conc2Display),venousClosed.cluster.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = []; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Closed', 'Units', 'normalized', 'Position', titlePosition);
legend([pRef,pAll,pCluster],{'Ref. VOF','All voxels','Cluster'},'Position',[0.2749    0.5891    0.1965    0.0833]); % ,'Position',[0.2749    0.1504    0.1965    0.0833]
subtightplot(4,2,5,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.vofModel.AIFFit,'k.--','Markersize',20); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pAll = plot_areaerrorbar(venousOpen.all.concList,errorbarOption);hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(venousOpen.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.vofModel.AIFFit(conc2Display),'k-','Color',[1 1 1]*0.4,'Linewidth',2); hold on;
    iPlot = 1;pAll = errorbar(time(conc2Display),venousOpen.all.mean(conc2Display),venousOpen.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 2;pCluster = errorbar(time(conc2Display),venousOpen.cluster.mean(conc2Display),venousOpen.cluster.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Open', 'Units', 'normalized', 'Position', titlePosition);
subtightplot(4,2,7,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.vofModel.AIFFit,'k.--','Markersize',20); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pAll = plot_areaerrorbar(venousCpx.all.concList,errorbarOption);hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(venousCpx.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.vofModel.AIFFit(conc2Display),'k-','Color',[1 1 1]*0.4,'Linewidth',2); hold on;
    iPlot = 1;pAll = errorbar(time(conc2Display),venousCpx.all.mean(conc2Display),venousCpx.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 2;pCluster = errorbar(time(conc2Display),venousCpx.cluster.mean(conc2Display),venousCpx.cluster.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Complex', 'Units', 'normalized', 'Position', titlePosition);
xlabel('Time (min)');

% AIF
subtightplot(4,2,2,gap,marg_h,marg_w);
hold off;
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p60p = plot_areaerrorbar(maskTheo60p.artClosed.all.concList,errorbarOption);hold on;
    iPlot = 5; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p20p = plot_areaerrorbar(maskTheo20p.artClosed.all.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artClosed.cluster.concList,errorbarOption); hold on;
    iPlot = 3; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artClosed.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;p60p = errorbar(time(conc2Display),maskTheo60p.artClosed.all.mean(conc2Display),maskTheo60p.artClosed.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 5;p20p = errorbar(time(conc2Display),maskTheo20p.artClosed.all.mean(conc2Display),maskTheo20p.artClosed.all.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster = errorbar(time(conc2Display),maskAutoAll.artClosed.all.mean(conc2Display),maskAutoAll.artClosed.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 3;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artClosed.all.mean(conc2Display),maskAuto20p.artClosed.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.YTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('No correction', 'Units', 'normalized', 'Position', titlePosition);
legend([pRef,p20p,p60p,pCluster,pCluster20p],{'Ref. AIF','Ideal 20%','Ideal 60%','Cluster all','Cluster 20%'},'Position',[0.6749    0.7817    0.2491    0.1093]); % ,'FontSize',8,'Position',[0.6942    0.1260    0.2491    0.1093]
subtightplot(4,2,4,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p60p = plot_areaerrorbar(maskTheo60p.artPveConc.all.concList,errorbarOption);hold on;
    iPlot = 3; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p20p = plot_areaerrorbar(maskTheo20p.artPveConc.all.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveConc.cluster.concList,errorbarOption); hold on;
    iPlot = 5; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveConc.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;p60p = errorbar(time(conc2Display),maskTheo60p.artPveConc.all.mean(conc2Display),maskTheo60p.artPveConc.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 5;p20p = errorbar(time(conc2Display),maskTheo20p.artPveConc.all.mean(conc2Display),maskTheo20p.artPveConc.all.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveConc.all.mean(conc2Display),maskAutoAll.artPveConc.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 3;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveConc.all.mean(conc2Display),maskAuto20p.artPveConc.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.YTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Conc. - Closed', 'Units', 'normalized', 'Position', titlePosition);
subtightplot(4,2,6,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p60p = plot_areaerrorbar(maskTheo60p.artPveClosed.all.concList,errorbarOption);hold on;
    iPlot = 3; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p20p = plot_areaerrorbar(maskTheo20p.artPveClosed.all.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveClosed.cluster.concList,errorbarOption); hold on;
    iPlot = 5; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveClosed.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;p60p = errorbar(time(conc2Display),maskTheo60p.artPveClosed.all.mean(conc2Display),maskTheo60p.artPveClosed.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 5;p20p = errorbar(time(conc2Display),maskTheo20p.artPveClosed.all.mean(conc2Display),maskTheo20p.artPveClosed.all.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveClosed.all.mean(conc2Display),maskAutoAll.artPveClosed.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 3;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveClosed.all.mean(conc2Display),maskAuto20p.artPveClosed.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.YTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Signal - Closed', 'Units', 'normalized', 'Position', titlePosition);
subtightplot(4,2,8,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p60p = plot_areaerrorbar(maskTheo60p.artPveOpen.all.concList,errorbarOption);hold on;
    iPlot = 3; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    p20p = plot_areaerrorbar(maskTheo20p.artPveOpen.all.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveOpen.cluster.concList,errorbarOption); hold on;
    iPlot = 5; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveOpen.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;p60p = errorbar(time(conc2Display),maskTheo60p.artPveOpen.all.mean(conc2Display),maskTheo60p.artPveOpen.all.std(conc2Display),...
        '.','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',12);
    iPlot = 5;p20p = errorbar(time(conc2Display),maskTheo20p.artPveOpen.all.mean(conc2Display),maskTheo20p.artPveOpen.all.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveOpen.all.mean(conc2Display),maskAutoAll.artPveOpen.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 3;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveOpen.all.mean(conc2Display),maskAuto20p.artPveOpen.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.YTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Signal - Open', 'Units', 'normalized', 'Position', titlePosition);
xlabel('Time (min)');


export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.png'),'-transparent','-m2');
% export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.pdf'),'-transparent','-m2');
print(figAif,'-dpdf',fullfile(figureFolder, 'aifVofCalculations.pdf'));
% export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.pdf'));
saveas(figAif,fullfile(figureFolder, 'aifVofCalculations.fig'),'fig');

%% NEW 1x5 Limited DRO AIF and VOF
shadedArea = false;
aifXLim = [0.9 2.0];
aifConcLim = [-0.5 4.0];
titlePosition = [0.5, 0.80, 0];
figAif = figure;
figAif.Position = [124    80   280   636];
gap = 0.02;
marg_h = 0.08;
marg_w = 0.12;

plotColors = {[236 112  22]./255, [ 52 148 186]./255, [119 147 60]./255, [149 55 53]./255,[128 100 162]./255};
areaColors = { [243 169 114]./255,[128 193 219]./255, [169 209 142]./255, [217 150 148]./255,[179 162 199]./255};
errorbarOption.alpha = 0.5;
errorbarOption.error = 'std';
errorbarOption.line_width = 2;
errorbarOption.x_axis = time;
errorbarOption.handle = figAif;

if ~shadedArea
    aifXLimIdx(1) = find(time<aifXLim(1),1,'last');
    aifXLimIdx(2) = find(time>aifXLim(end),1,'first');
    conc2Display = false(size(time));
    conc2Display(aifXLimIdx(1):aifXLimIdx(2)) = true;
end

% VOF
subtightplot(5,1,1,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.vofModel.AIFFit,'k.--','Markersize',20); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pClosed = plot_areaerrorbar(venousClosed.cluster.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pOpen = plot_areaerrorbar(venousOpen.cluster.concList,errorbarOption); hold on;
    iPlot = 3; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCpx = plot_areaerrorbar(venousCpx.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.vofModel.AIFFit(conc2Display),'k-','Color',[1 1 1]*0.4,'Linewidth',2); hold on;
    iPlot = 1;pClosed = errorbar(time(conc2Display),venousClosed.cluster.mean(conc2Display),venousClosed.cluster.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pOpen = errorbar(time(conc2Display),venousOpen.cluster.mean(conc2Display),venousOpen.cluster.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 3;pCpx = errorbar(time(conc2Display),venousCpx.cluster.mean(conc2Display),venousCpx.cluster.std(conc2Display),...
        's','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = []; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('VOF', 'Units', 'normalized', 'Position', titlePosition);
legend([pRef,pClosed,pOpen,pCpx],{'Ref. VOF','Closed','Open','Complex'}); % ,'Position',[0.2749    0.1504    0.1965    0.0833]
% ,'Position',[0.2749    0.5891    0.1965    0.0833]
% AIF
subtightplot(5,1,2,gap,marg_h,marg_w);
hold off;
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artClosed.cluster.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artClosed.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;pCluster = errorbar(time(conc2Display),maskAutoAll.artClosed.all.mean(conc2Display),maskAutoAll.artClosed.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artClosed.all.mean(conc2Display),maskAuto20p.artClosed.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('No correction', 'Units', 'normalized', 'Position', titlePosition);
legend([pRef,pCluster,pCluster20p],{'Ref. AIF','Cluster all','Cluster 20%'}); % ,'FontSize',8,'Position',[0.6942    0.1260    0.2491    0.1093]
% ,'Position',[0.6749    0.7817    0.2491    0.1093]
subtightplot(5,1,3,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveConc.cluster.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveConc.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveConc.all.mean(conc2Display),maskAutoAll.artPveConc.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveConc.all.mean(conc2Display),maskAuto20p.artPveConc.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Conc. - Closed', 'Units', 'normalized', 'Position', titlePosition);
subtightplot(5,1,4,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveClosed.cluster.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveClosed.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveClosed.all.mean(conc2Display),maskAutoAll.artPveClosed.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveClosed.all.mean(conc2Display),maskAuto20p.artPveClosed.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Signal - Closed', 'Units', 'normalized', 'Position', titlePosition);
subtightplot(5,1,5,gap,marg_h,marg_w);
if shadedArea
    pRef = plot(time,droParam.aifModel.AIFFit,'k-s'); hold on;
    iPlot = 1; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster = plot_areaerrorbar(maskAutoAll.artPveOpen.cluster.concList,errorbarOption); hold on;
    iPlot = 2; errorbarOption.color_line = plotColors{iPlot}; errorbarOption.color_area = areaColors{iPlot};
    pCluster20p = plot_areaerrorbar(maskAuto20p.artPveOpen.cluster.concList,errorbarOption); hold on;
else
    pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'k-','Linewidth',2); hold on;
    iPlot = 1;pCluster = errorbar(time(conc2Display),maskAutoAll.artPveOpen.all.mean(conc2Display),maskAutoAll.artPveOpen.all.std(conc2Display),...
        'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
    iPlot = 2;pCluster20p = errorbar(time(conc2Display),maskAuto20p.artPveOpen.all.mean(conc2Display),maskAuto20p.artPveOpen.all.std(conc2Display),...
        '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
        'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
end
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('Signal - Open', 'Units', 'normalized', 'Position', titlePosition);
xlabel('Time (min)');


export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.png'),'-transparent','-m2');
% export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.pdf'),'-transparent','-m2');
print(figAif,'-dpdf',fullfile(figureFolder, 'aifVofCalculations.pdf'));
% export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.pdf'));
saveas(figAif,fullfile(figureFolder, 'aifVofCalculations.fig'),'fig');
%%
[reviewedAifMask, ~] = reviewrectaifselection(iauc, time, maskAuto20p.artPveOpen.conc,bolusRange,arterial.squareCentre,maskAuto20p.artPveOpen.maskPve,arterial.pxList,tissues.mask,tissues.pxList,concPermute);
