% DROAIFPVEPRESENTATION
% This script creates and analyze the AIF PVE digital reference object 
% (DRO) of the AIF-PVCM ISMRM oral prensetation. It reproduces the figure
% on page 9 of the presentation.
%
% Author: Benoit Bourassa-Moreau
% Creation date: 2020-07-22

%% ---- 1. Create the DRO ----
injDose = (0.05)*72; % mL = (mmol/kg * kg)*1 mL/mmol
time = (0:2:660)/60;
t0 = 60.25/60; % min
aifPeak = 5; % mM
tissueParam.pkm{1} = [0 0.01 0.02 0.05 0.1 0.2]; % Ktrans in (1/min)
tissueParam.pkm{2} = [0.1,0.2, 0.5]; % Ve in (-)
tissueParam.pkm{3} = [0.001,0.005,0.01,0.02,0.05,0.1]; % Vp in (-)
tissueParam.T10 = 1000; % ms (about the white matter T10) QIBA
tissueParam.T20s = 35; % ms (Half white matter T20) Stanisz et al. 2005
tissueParam.r1 = 4.5; % QIBA4, 1/(mM*s)
tissueParam.r2s = 87; % Kjolby et al. 2006 (1/(mM*s))

bloodParam.T10 = 1440;  % QIBA_v4, 1/ms
bloodParam.T20s = 275/2; % ms (Half blood T20) Stanisz et al. 2005
bloodParam.r1 = 4.5; % QIBA4, 1/(mM*s)
bloodParam.r2s = 0; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
bloodParam.r22s =  0; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
bloodParam.xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)

seqParam.te = 1.85; % ms
seqParam.tr = 4; % ms
seqParam.fa = 9.2/180*pi; % rad
% Changes from QIBA_v4 - Noise is added
seqParam.nNoise = 10;
seqParam.snr = 25;
seqParam.B0 = 3; % Tesla
seqParam.voxelRes = 2.3; % mm

vofParam.bloodParam.theta = 15/180*pi; % radians
vofParam.bloodParam.r2s = 0.4929; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
vofParam.bloodParam.r22s =  2.6159; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006

[droOptions.aifModel,droOptions.pkModel,droOptions.imgModel] = defaultmodelparams(time,t0,aifPeak,tissueParam,bloodParam,seqParam);

aifRows.idxLocation = [15 14];
aifRows.bloodFraction = [0.2 0.12];
[dataAIF, phaseAif, signalData, concData,aifMask,droParam,T10,trueDroParam] = aifpvedrocreate(time,droOptions,vofParam,aifRows);

processingFolder = uigetdir(pwd,'Select results folder');

figureFolder = mksubdirifneeded(processingFolder,'figures');
workspaceFolder = mksubdirifneeded(processingFolder,'workspaces');

droParam.aifModel.minXIdx = 1;
[cardiacOutput, auc1st] = aifcardiacoutput(droParam.aifModel,injDose);

%% Prepare matrix
n0 = find(time<droParam.aifModel.Params.t0,1,'last');
magPerm = permute(dataAIF,[3, 4, 2,1]);
phasePerm = permute(phaseAif,[3, 4, 2,1]);
T10BloodPerm = T10;
[nR,nC,nS,nT]=size(magPerm);
signal0 = mean(magPerm(:,:,:,1:n0),4);
b1MapPerm = ones(nR,nC,nS);
maskPermute = true(nR,nC,nS);

%% Compute basic Tissue concentration
[concPermute, outliers] = concmrispgrclosed(magPerm,signal0,...
    droParam.imgModel.tissueParam.tracer,T10BloodPerm,droParam.imgModel.seqParam,maskPermute,b1MapPerm);

%% Find basic bolus range
[dynPxList, ~] = dyn3dconversion(concPermute,maskPermute);
meanAllDyn = mean(dynPxList,1);
            use2ndDeriv = true; 
[~, rangeBolus, ~] = fitinitialenhancement(time,meanAllDyn,n0,[],[], use2ndDeriv); 
bolusRange = [rangeBolus(2) (3*rangeBolus(5)-2*rangeBolus(2))];
iauc = sum(concPermute(:,:,:,bolusRange(1):bolusRange(2)),4);

%% ---- 2. Generate masks ----
% Semi-automatic mask
gmMask = maskPermute; % For a patient this should be the mask provided by SPM.

optionsSemiAuto.waitbar=1; % 0:off, 1:on
optionsSemiAuto.display=2;
optionsSemiAuto.aif.nSlice = 1; % Displayed slice for debug plots
optionsSemiAuto.aif.pArea= 0.7000; % AUC voxel rejection fraction
optionsSemiAuto.aif.pTTP= 0.45000; % TTP rejection fraction for vessels to tissue separation
manualMaskFile = fullfile(workspaceFolder,'manualDroMask.mat');

redoManualMasks = false;
if redoManualMasks || ~exist(manualMaskFile,'file')
    [venous.mask, arterial.mask, arterial.pxList, tissues.mask, tissues.pxList, venous.squareCentre, arterial.squareCentre] = ...
        semiautomasksvenarttiss(time,concPermute,iauc,maskPermute,gmMask,iauc,optionsSemiAuto);
    
    save(manualMaskFile,'venous','arterial','tissues');
else
    load(manualMaskFile);
end

% DRO ideal masks
maskVof = aifMask.data == 1;

arterialTheo60p.mask = aifMask.data == 2;
arterialTheo20p.mask = aifMask.data == 3;
% Tissue voxels mask and list for PVC
[tissuesTheo20p.mask, ~,arterialTheo20p.pxList] = findsurroundingtissue(iauc,magPerm,maskPermute,arterialTheo20p.mask);
tissuesTheo20p.pxList = arterialTheo20p.pxList; tissuesTheo20p.pxList(:,1) = arterialTheo20p.pxList(:,1)-1;
[tissuesTheo60p.mask, ~,arterialTheo60p.pxList] = findsurroundingtissue(iauc,magPerm,maskPermute,arterialTheo60p.mask);
tissuesTheo60p.pxList = arterialTheo60p.pxList; tissuesTheo60p.pxList(:,1) = arterialTheo60p.pxList(:,1)-1;

artCalculationMask = arterial.mask | arterialTheo20p.mask | arterialTheo60p.mask;

%% ---- 3. AIF-VOF calculation ----
% 3.a Complex-Ang-VOF and signal-based PVCM AIF
framesPre = [1 n0];
venous.sssB0AngSlice = vofParam.bloodParam.theta/pi*180; % defgrees

firstPassRecircDelay = 45/60; % min
pveEndIdx = find(time>(time(framesPre(2))+firstPassRecircDelay),1,'first')-1;
clusterType = 'iauc';
aifOptionsPveClosed = aifpvedefineoptions(time,framesPre,...
    pveEndIdx,size(magPerm,3),'s-pvc-closed-form',clusterType,'complex_split',gmMask);
aifOptionsPveClosed.vof.split.cerebellumFov = false;
aifOptionsPveClosed.vof.split.sssB0Ang = venous.sssB0AngSlice/180*pi;
% Modify default options for the single slice DRO
% Usually SSS extends for at least 5-10 slices and we keep 1 phase voxel per
% slice, thus about 8 voxels for phase
aifOptionsPveClosed.vof.split.phase.nMinSlices = 1;
aifOptionsPveClosed.vof.split.phase.maxVoxelsPerSlice = 8;
[artPveClosed,venCpx] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droOptions.imgModel.bloodParam.tracer,droOptions.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,[],aifOptionsPveClosed);

rangeIdxPve = aifOptionsPveClosed.aif.pve.rangeVofIAUC(1):aifOptionsPveClosed.aif.pve.rangeVofIAUC(2);
aucVof = trapz(time(rangeIdxPve),venCpx.cluster.mean(rangeIdxPve));
aucVofIdeal = trapz(time(rangeIdxPve),droParam.aifModel.AIFFit(rangeIdxPve));

% 3.b Concentration-based PVCM
aifOptionsPveConc = aifpvedefineoptions(time,framesPre,...
    pveEndIdx,size(magPerm,3),'c-pvc',clusterType,'complex_split',gmMask);
[artPveConc,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    droOptions.imgModel.bloodParam.tracer,droOptions.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venCpx,aifOptionsPveConc);

% 3.c Signal-based PVCM with fitted magnitude enhancement to include T2* effects
aifOptionsPveOpen = aifpvedefineoptions(time,framesPre,...
    pveEndIdx,size(magPerm,3),'s-pvc-open-form',clusterType,'complex_split',gmMask);
quadT2sTracer = droOptions.imgModel.bloodParam.tracer;
quadT2sTracer.r2s = vofParam.bloodParam.r2s; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
quadT2sTracer.r22s = vofParam.bloodParam.r22s; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
quadT2sTracer.enableQuadR2 = true;

[artPveOpen,~] = globalaif(...
    time,concPermute,magPerm,phasePerm,T10BloodPerm,b1MapPerm,...
    quadT2sTracer,droOptions.imgModel.seqParam,framesPre, ...
    venous, arterial, tissues,venCpx,aifOptionsPveOpen);

%% ---- 4. FIT PKM ----
% Extended Tofts-Kety
pkmFHandle = @pkmetofts;
x0 = [droParam.pkModel.Ktrans(3) droParam.pkModel.Ve(2) droParam.pkModel.Vp(3)];
lb = [droParam.pkModel.Ktrans(1) droParam.pkModel.Ve(1) droParam.pkModel.Vp(1)];
ub = [droParam.pkModel.Ktrans(end) droParam.pkModel.Ve(end) droParam.pkModel.Vp(end)]*2;
rb = false(1,3);
order = true(1,3);
fitOptions  = optimoptions('lsqcurvefit');
fitOptions.Display = 'Off';

[xMatClosed, errorInfoClosed] = fitpkm(time,artPveClosed.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);
[xMatAuc, errorInfoAuc] = fitpkm(time,artPveConc.cluster.mean,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);

[aifBat, ~] = batcheong(artPveClosed.cluster.mean, time,framesPre(2),0.25/60);
[vofBat, ~] = batcheong(venCpx.cluster.mean, time,framesPre(2),0.25/60);
delayCorrectedVof = interp1(time-(vofBat - aifBat),venCpx.cluster.mean,time,'linear','extrap');
[xMatVof, errorInfoVof] = fitpkm(time,delayCorrectedVof,concPermute,[],maskPermute,pkmFHandle,droParam.pkModel,x0,lb,ub,rb,fitOptions,order);

%% ---- 5. Display ----
%% 5.a DRO signal sample
figDro = figure; 
figDro.Position = [92   104   560   636];

artMaskList = pixelfrommask(arterial.mask);
artMaskClusterList = pixelfrommask(artPveClosed.cluster.mask);
venMaskList = pixelfrommask(venous.mask);
venMaskClusterList = pixelfrommask(venCpx.cluster.mask);

imagesc(iauc,[0 10]); colormap gray;
ax1 = gca;
[~,~,nX,nY] = size(dataAIF);
ax1.PlotBoxAspectRatio = [nY,nX,1];
ax1.XTick = (0:10:60)+0.5;
ytickRev = [(0:10:140), 141, 151 152:10:182, 183];
ax1.YTick = ytickRev+0.5;
ax1.XTickLabel = [];
ax1.YTickLabel = [];
ax1.LineWidth = 1.5;
hold on;
plot(artMaskList(:,2),artMaskList(:,1),'.g','Markersize',3);
plot(artMaskClusterList(:,2),artMaskClusterList(:,1),'or','Markersize',3);
plot(venMaskList(:,2),venMaskList(:,1),'.y','Markersize',3);
plot(venMaskClusterList(:,2),venMaskClusterList(:,1),'ob','Markersize',3);

%% Save file
export_fig(figDro,fullfile(figureFolder, 'droWMarkings.png'),'-transparent','-m2');
export_fig(figDro,fullfile(figureFolder, 'droWMarkings.pdf'),'-transparent','-m2');
saveas(figDro,fullfile(figureFolder, 'droWMarkings.fig'),'fig');

%% 5.b DRO AIF and VOF
aifXLim = [0.9 2.0];
aifConcLim = [-0.5 7.0];
titlePosition = [0.5, 0.85, 0];
figAif = figure;
figAif.Position = [124    80   280   604];
gap = 0.02;
marg_h = 0.08;
marg_w = 0.12;

plotColors = {[236 112  22]./255, [ 52 148 186]./255, [119 147 60]./255, [149 55 53]./255,[128 100 162]./255};
areaColors = { [243 169 114]./255,[128 193 219]./255, [169 209 142]./255, [217 150 148]./255,[179 162 199]./255};
errorbarOption.alpha = 0.5;
errorbarOption.error = 'std';
errorbarOption.line_width = 2;
errorbarOption.x_axis = time;
errorbarOption.handle = figAif;

    aifXLimIdx(1) = find(time<aifXLim(1),1,'last');
    aifXLimIdx(2) = find(time>aifXLim(end),1,'first');
    conc2Display = false(size(time));
    conc2Display(aifXLimIdx(1):aifXLimIdx(2)) = true;

% VOF
subtightplot(3,1,1,gap,marg_h,marg_w);
pRef = plot(time(conc2Display),droParam.vofModel.AIFFit(conc2Display),'-','Color',[1 1 1]*0.,'Linewidth',2); hold on;
pFit = plot(time(conc2Display),venCpx.fit.conc(conc2Display),'-','Color',plotColors{2},'Linewidth',1.5);
iPlot = 1;pAll = errorbar(time(conc2Display),venCpx.all.mean(conc2Display),venCpx.all.std(conc2Display),...
    'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
iPlot = 2;pCluster = errorbar(time(conc2Display),venCpx.cluster.mean(conc2Display),venCpx.cluster.std(conc2Display),...
    '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = []; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('VOF - Cpx.-fit', 'Units', 'normalized', 'Position', titlePosition);
legend([pRef,pAll,pCluster,pFit],{'Ref.','All','Cluster','Horsfield fit'}); 

% AIF
subtightplot(3,1,2,gap,marg_h,marg_w);
hold off;
pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'-','Color',[1 1 1]*0.,'Linewidth',2); hold on;
pFit = plot(time(conc2Display),artPveConc.fit.conc(conc2Display),'-','Color',plotColors{2},'Linewidth',1.5);
iPlot = 1;pAll = errorbar(time(conc2Display),artPveConc.all.mean(conc2Display),artPveConc.all.std(conc2Display),...
    'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
iPlot = 2;pCluster = errorbar(time(conc2Display),artPveConc.cluster.mean(conc2Display),artPveConc.cluster.std(conc2Display),...
    '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.XTickLabel = [];ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('AIF - Conc.', 'Units', 'normalized', 'Position', titlePosition);

subtightplot(3,1,3,gap,marg_h,marg_w);
pRef = plot(time(conc2Display),droParam.aifModel.AIFFit(conc2Display),'-','Color',[1 1 1]*0.,'Linewidth',2); hold on;
pFit = plot(time(conc2Display),artPveClosed.fit.conc(conc2Display),'-','Color',plotColors{2},'Linewidth',1.5);
iPlot = 1;pAll = errorbar(time(conc2Display),artPveClosed.all.mean(conc2Display),artPveClosed.all.std(conc2Display),...
    'v','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
iPlot = 2;pCluster = errorbar(time(conc2Display),artPveClosed.cluster.mean(conc2Display),artPveClosed.cluster.std(conc2Display),...
    '^','Color',plotColors{iPlot},'MarkerEdgeColor',plotColors{iPlot},...
    'MarkerFaceColor',plotColors{iPlot},'Markersize',4);
ylabel('C (mM)');
xlim(aifXLim); ylim(aifConcLim);
ax1 = gca; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
title('AIF - Signal', 'Units', 'normalized', 'Position', titlePosition);
xlabel('Time (min)');

%% Save file
export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.png'),'-transparent','-m2');
export_fig(figAif,fullfile(figureFolder, 'aifVofCalculations.pdf'),'-transparent','-m2');
saveas(figAif,fullfile(figureFolder, 'aifVofCalculations.fig'),'fig');

%% 5.c Display DRO PKM parameters
droParam.pkModel.displayNames = {'K^T^r^a^n^s','v_e','v_p'};
droParam.pkModel.unitNames = {'(mL/100g min)','(mL/100g)','(mL/100g)'};
droTrueMaps = permute(trueDroParam,[2 3 1]);
[figDroMapSPveClosed, droSPveClosed.mean,droSPveClosed.bias,droSPveClosed.std, cLimMapOpen,cLimErrorOpen] = drodisplaymapserror(xMatClosed,droTrueMaps,droParam,aifMask.data);
[figDroMapCPve, droCPve.mean,droCPve.bias,droCPve.std,~,~] = drodisplaymapserror(xMatAuc,droTrueMaps,droParam,aifMask.data, cLimMapOpen,cLimErrorOpen);
[figDroMapVof, droVof.mean,droVof.bias,droVof.std,~,~] = drodisplaymapserror(xMatVof,droTrueMaps,droParam,aifMask.data, cLimMapOpen,cLimErrorOpen);

%% Save files
export_fig(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.png'),'-transparent','-m2');
export_fig(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.pdf'),'-transparent','-m2');
saveas(figDroMapCPve,fullfile(figureFolder, 'droMapsErrorCPve.fig'),'fig');

export_fig(figDroMapVof,fullfile(figureFolder, 'droMapsErrorVof.png'),'-transparent','-m2');
export_fig(figDroMapVof,fullfile(figureFolder, 'droMapsErrorVof.pdf'),'-transparent','-m2');
saveas(figDroMapVof,fullfile(figureFolder, 'droMapsErrorVof.fig'),'fig');

export_fig(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.png'),'-transparent','-m2');
export_fig(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.pdf'),'-transparent','-m2');
saveas(figDroMapSPveClosed,fullfile(figureFolder, 'droMapsErrorSPveClosed.fig'),'fig');

%% 5.d Display PKM parameter differences from AIF choice
gap = 0.08;
marg_h = 0.08;
marg_w = 0.04;

markerType = '^v*';
droTrueMapsNoiseSize = repelem(droTrueMaps,10,10);
repAifMask = repmat(aifMask.data,[1 1 3]);
droTrueMapsNoiseSizeVessRows = NaN(size(repAifMask));
droTrueMapsNoiseSizeVessRows(repAifMask==0) = droTrueMapsNoiseSize(:);
colorVessel = {[236 112  22]./255, [ 52 148 186]./255, [119 147 60]./255, [149 55 53]./255,[128 100 162]./255};
figVarParam = figure;
% set(figVarParam,'Position',[323         195        1147         449]);
figVarParam.Position = [469    50   400 735];
nParam = 3;nVessel = 3;
pkmCompLinRsquared = NaN(nParam*nVessel,5);
for iParam=1:nParam
    subtightplot(nParam,1,iParam,gap,marg_h,marg_w);
    cParamTrue = reshape(droTrueMapsNoiseSize(:,:,iParam),[],1);
    nParamSub = length(droParam.pkModel.(droParam.pkModel.paramNames{iParam}));
    cParamMean = NaN(3,nParamSub);
    cParamStd = NaN(3,nParamSub);
    cParamMap{1} = xMatVof(:,:,1,iParam);
    cParamMap{2} = xMatAuc(:,:,1,iParam);
    cParamMap{3} = xMatClosed(:,:,1,iParam);
    for iParamSub = 1:nParamSub
        cParamMask = droTrueMapsNoiseSizeVessRows(:,:,iParam)==droParam.pkModel.(droParam.pkModel.paramNames{iParam})(iParamSub);
        for iVessel=1:nVessel
            cParamMean(iVessel,iParamSub) = mean(cParamMap{iVessel}(cParamMask));
            cParamStd(iVessel,iParamSub) = std(cParamMap{iVessel}(cParamMask));
        end
    end
    minMaxCParam = droParam.pkModel.(droParam.pkModel.paramNames{iParam})([1 end]);
    cParamLim = mean(minMaxCParam)+1.3*diff(minMaxCParam)/2*[-1 1];
    lHandEq = plot(cParamLim,cParamLim,'k--','Linewidth',2);
    hold on;
    for iVessel=1:nVessel
        errorbar(droParam.pkModel.(droParam.pkModel.paramNames{iParam}),cParamMean(iVessel,:),cParamStd(iVessel,:),markerType(iVessel),'Color',colorVessel{iVessel},'MarkerEdgeColor',plotColors{iVessel},'MarkerFaceColor',plotColors{iVessel},'MarkerSize',6,'Linewidth',1.5);
        cParamComputed = cParamMap{iVessel}(aifMask.data==0);
        [p,S] = polyfit(cParamTrue,cParamComputed,1);
        lHandVessel{iVessel} = plot(minMaxCParam,polyval(p,minMaxCParam),'-','Color',colorVessel{iVessel},'Linewidth',2);
        R = corrcoef(cParamTrue,cParamComputed);
        % Fit with errors
        mdl = fitlm(cParamTrue,cParamComputed);
        b = [mdl.Coefficients{'(Intercept)','Estimate'}, mdl.Coefficients{'x1','Estimate'}];
        bint = coefCI(mdl);
        H = [0 0; 0 1]; % Neglecting Intercept
        C = [0 1]'; % F-test that slope is One
        [pVal, F] = coefTest(mdl,H,C);
        rSquared = mdl.Rsquared.Ordinary;
        
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,1) = mean(bint(2,:));
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,2) = diff(bint(2,:))/2;
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,3) = mean(bint(1,:));
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,4) = diff(bint(1,:))/2;
        pkmCompLinRsquared(nVessel*(iParam-1)+iVessel,5) = rSquared;
    end
    ax1 = gca; ax1.FontName = 'Arial'; ax1.FontSize = 10;ax1.FontWeight = 'Bold';
    
    xlim(cParamLim);
    ylim(cParamLim);
    ylabel([droParam.pkModel.displayNames{iParam} ' ' droParam.pkModel.unitNames{iParam}]);
    xlabel(['True ' droParam.pkModel.displayNames{iParam} ' ' droParam.pkModel.unitNames{iParam}]);
    axis square;
end
legend([lHandEq,lHandVessel{1},lHandVessel{2},lHandVessel{3}],'Line of equality','VOF','AIF - Conc.','AIF - Signal','Location','Best');
%% Save files
export_fig(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.png'),'-transparent','-m2');
export_fig(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.pdf'),'-transparent','-m2');
saveas(figVarParam,fullfile(figureFolder, 'pkmParameterStatsComp.fig'),'fig');
