
projFigureFolder = 'E:\boub3207\Documents\Ecole\Doctorat\2-Articles\4-AIFPVE\Figures';
%% Parameters
refBloodT10 = 1932;% (ms)(Stanisz et al.)
refGmT10 = 1820; % ms (Stanisz et al.)
refGmT20 = 99/2; % (Half gray matter T20) Stanisz et al. 2005
seqFa = 9.2;
r1 = 3.2; % Gadovist 3T (Rohrer et al.)

% Caleb Roberts et al.
refBloodT10 = 1400;% (ms)
seqFa = 20; % 9.2
r1 = 3.3; % Omniscan 1.5T (Rohrer et al.)

%% Define bias variations
% Blood fractions
bloodFractionVal = [0.12 0.2 0.6];
nBf = length(bloodFractionVal);
% Sources of bias
biasTypes(1).name = 'susceptibility';
biasTypes(1).values = [90,60,30]; % Vessel angulation (degrees)
biasTypes(2).name = 'T10';
biasTypes(2).values = [1500 refBloodT10 2400]; % (ms)
% Arterial amd venous inflow
sequenceTr = 4; % ms
artAverageSpeed = 300; % mm/s
sssAverageSpeed = 150; % mm/s
artDistance = [50, 105, 140]; % mm [Willis-MCA-M3; ICA-MCA-M3; ICA-MCA-M4]
biasTypes(3).name = 'inflow';
biasTypes(3).values = round(artDistance/artAverageSpeed/(sequenceTr/1e3)); % nTR (-)
biasTypes(3).values = [15 30 60];%[100 200 300];
nBias = length(biasTypes);

nTissueTypes = 2; % AIF and GM
%% DRO Parameters
time = (0:2:660)/60;
injDose = (0.05)*72; % mL = (mmol/kg * kg)*1 mL/mmol

% AIF - Horsfield model
aifModel.FHandle = @aifhorsfield;
aifPeak = 5;
aifScaling = aifPeak/8.626; % mM
aifModel.Params.k = aifScaling*3.0214;%9.1746*(droParam.aifModel.Params.beta*(droParam.aifModel.Params.alpha+1)+droParam.aifModel.Params.tau);
aifModel.Params.t0 = 60.25/60;
aifModel.Params.A = [10.06,0.33,0.37]; % mM
aifModel.Params.m = [16.02,1.17, 0.11];% 1/min
aifModel.Params.alpha = 5.26; % -
aifModel.Params.beta = 0.032; % min
aifModel.Params.tau = 0.129; % min
aifModel.Params.kN = 1; % (-)
aifModel.units = 'mM';

% PKModel for GM tissues
pkModel.FHandle = @pkmetofts;
pkModel.hematocrit = 0.45;
pkModel.brainDensity = 1; % g/mL
% Default phantom parameters
pkModel.Ktrans = 1/(pkModel.brainDensity/100)*0.01; % QIBA_v4. in mL/(100g min)
pkModel.Ve = 1/(pkModel.brainDensity/100)*0.1; % QIBA_v4. in mL/100g
pkModel.Vp = 1/(pkModel.brainDensity/100)*0.001; % QIBA_v4. mL/100g
pkModel.paramNames = {'Ktrans','Ve','Vp'};

% MRI signal model
imgModel.FHandle = @signalmrispgr;

imgModel.tissueParam.S0 = 1e3; % QIBA4, Equilibrium magnetization. a.u
imgModel.tissueParam.R10 = 1/refGmT10; % QIBA4, 1/ms (about the white matter T10)
imgModel.tissueParam.tracer.r1 = r1; % QIBA4, 1/(mM*s)
% Changes from QIBA_v4 - T2* relaxation 
imgModel.tissueParam.R20s = 1/refGmT20; % 1/ms (Half white matter T20) Stanisz et al. 2005
imgModel.tissueParam.tracer.r2s = 87; % Kjolby et al. 2006 (1/(mM*s)

imgModel.bloodParam.S0 = 1e3; % QIBA_v4, Equilibrium magnetization. a.u
% Changes from QIBA_v4 - T2* relaxation 
imgModel.bloodParam.R20s = 0;%1/35; % 1/ms (Half white matter T20) Stanisz et al. 2005
imgModel.bloodParam.R10 = 1/refBloodT10;
imgModel.bloodParam.tracer.r1 = r1; % QIBA4, 1/(mM*s)
imgModel.bloodParam.tracer.r2s = 0;%0.4929; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
imgModel.bloodParam.tracer.r22s =  0;%2.6159; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006
imgModel.bloodParam.tracer.enableQuadR2 = true;
imgModel.bloodParam.tracer.xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
% imgModel.bloodParam.tracer.xiM = 3.20e-7; % (1/mM) = 320 ppm/M Gadobutrol, Diameter 13.5 mm, Parallel, Manganese solution (900 ms) (Korporaal, 2011)
imgModel.bloodParam.tracer.enablePhase = false;
imgModel.phase.FHandle = @signalmrispgrphase;

% DCE sequence parameters (Changes from QIBA_v4)
imgModel.seqParam.te = 1.85; % ms
imgModel.seqParam.nAcq = length(imgModel.seqParam.te);
imgModel.seqParam.tr = 4; % ms
imgModel.seqParam.fa = seqFa/180*pi; % rad
imgModel.seqParam.B0 = 3; % Tesla
imgModel.seqParam.voxelRes = 2.3; % mm
imgModel.seqParam.time = time;
imgModel.seqParam.t0 = aifModel.Params.t0;
% MRI noise model
imgModel.noiseParam.snr = 25;
imgModel.noiseParam.nNoise = 10;
imgModel.noiseParam.seed = 700;
imgModel.noiseParam.repeatedPattern = false;
imgModel.noiseParam.FHandle = @noisemri;
imgModel.noiseParam.standardDeviation = ...
    imgModel.bloodParam.S0/imgModel.noiseParam.snr;

%% Compute biased MRI signal and compute effect on AIF PVCM concentration
nT = length(time);
n0 = find(time>aifModel.Params.t0,1,'first')-1;
framesPre = [1 n0];
imgModel.bloodParam.tracer.framesPre = framesPre;
timePveStop = 1.8; % min
nTPveStop = find(time>timePveStop,1,'first')-1;
clusterType = 'iauc';
aifOptionsPveClosed = aifpvedefineoptions(time,framesPre,nTPveStop,1,'s-pvc-closed-form',clusterType);
aifOptionsPveClosed.aif.pve.reducedAifTime = 0;

for iBias = 1:nBias
    horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
    aifModel.pValues = paramstruct2vec(aifModel.Params,horsfieldStructFieldOrder);
    aifConc = aifModel.FHandle(time,aifModel.pValues);
    % Tissue
    cParamsValues = [pkModel.Ktrans, pkModel.Ve, pkModel.Vp];
    patientParam.hematocrit = pkModel.hematocrit;
    patientParam.brainDensity = pkModel.brainDensity; % g/mL
    gmConc = pkModel.FHandle(time,aifConc,patientParam,[],cParamsValues);
    % Compute signal with bias
    currentBiasType = biasTypes(iBias).name;
    nValBias = length(biasTypes(iBias).values);
    
    switch currentBiasType
        case 'susceptibility'
            [artSignal,aifSignal,gmSignal,satSignal,bloodT10,figPveSim] = signalbiassusceptibility(aifConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values,imgModel);
            iAng = 2; iBf = 2;
            geomStr = [num2str(biasTypes(iBias).values(iAng)) '_' num2str(bloodFractionVal(iBf))];
            saveas(figPveSim(iBf,iAng),fullfile(projFigureFolder, ['figSusceptibilitySimulations' geomStr '.fig']),'fig');
            export_fig(figPveSim(iBf,iAng),fullfile(projFigureFolder, ['figSusceptibilitySimulations' geomStr '.png']),'-transparent','-m2');
            % Remove susceptibility images before PDF
            subplotAxesHandle = findobj(figPveSim(iBf,iAng),'type','axes');
            nAxes = length(subplotAxesHandle); xAxesPos = NaN(nAxes,1);
            for iAxes = 1:nAxes; xAxesPos(iAxes) = subplotAxesHandle(iAxes).Position(1); end;
            [~,minAxesIdx] = min(xAxesPos);
            delete(subplotAxesHandle(minAxesIdx));
            print(figPveSim(iBf,iAng),'-dpdf',fullfile(projFigureFolder, ['figSusceptibilitySimulations' geomStr '.pdf']));
        case 'T10'
            [artSignal,aifSignal,gmSignal,satSignal,bloodT10] = signalbiast10(aifConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values,imgModel);
        case 'inflow'
            [artSignal,aifSignal,gmSignal,satSignal,bloodT10,figSs] = signalbiasreachss(aifConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values,imgModel);
            
            % Test pure inflow
            aifData = permute(aifSignal,[1 4 3 2]);
            aifData0 = mean(aifData(:,:,:,1:n0),4);
            inFlowT1 = refBloodT10*ones(3,1);%[400 800 1200]';
            [concDirectInflow, outliersPve] = concmrispgrclosed(aifData,aifData0,imgModel.bloodParam.tracer,inFlowT1,imgModel.seqParam,true(3,1),ones(3,1));
            
            figure; plot(time,squeeze(concDirectInflow));  hold on;
            plot(time,aifConc); hold off;
            legend(num2str(biasTypes(iBias).values(1)),num2str(biasTypes(iBias).values(2)),num2str(biasTypes(iBias).values(3)));
            
            saveas(figSs,fullfile(projFigureFolder, ['figSteadyStateSimulations'  '.fig']),'fig');
            export_fig(figSs,fullfile(projFigureFolder, ['figSteadyStateSimulations'  '.png']),'-transparent','-m2');
            print(figSs,'-dpdf',fullfile(projFigureFolder, ['figSteadyStateSimulations'  '.pdf']));
    end
    % Combined signal
    combSignal = NaN(nBf,nValBias,nTissueTypes,nT);
    combSignal(:,:,1,:) = artSignal;
    combSignal(:,:,2,:) = repmat(permute(gmSignal,[3 1 4 2]),[nBf,1,1,1]);
    % Compute conc
    venCpx.cluster.mean = aifConc;
    b1Map = ones(imgModel.noiseParam.nNoise,imgModel.noiseParam.nNoise,nTissueTypes);
    arterial.mask = false(imgModel.noiseParam.nNoise,imgModel.noiseParam.nNoise,nTissueTypes);
    arterial.mask(:,:,1) = true;
    tissues.mask = ~arterial.mask;
    arterial.pxList = pixelfrommask(arterial.mask);
    tissues.pxList = pixelfrommask(tissues.mask);
    nPx = size(arterial.pxList,1);
    
    signalPve(nBf,nValBias).mean = [];
    signalPve(nBf,nValBias).std = [];
    for iBf = 1:nBf
        for iValBias = 1:nValBias
            % Add noise
            acqData = permute(imgModel.noiseParam.FHandle(permute(combSignal(iBf,iValBias,:,:),[4,3,1,2]),imgModel.noiseParam),[3,4,2,1]);
            % Compute PVC-AIF
            arterial.T1blood = bloodT10(iBf,iValBias);
            [artPve(iBf,iValBias),~] = globalaif(...
                time,[],acqData,[],bloodT10,b1Map,...
                imgModel.bloodParam.tracer,imgModel.seqParam,framesPre,...
                [],arterial,tissues,venCpx,aifOptionsPveClosed);
            % Compute corrected signal with final blood fraction
            allCorrectedSignals = NaN(nPx,nT);
            for iPx = 1:nPx
                cBf = artPve(iBf,iValBias).factor(arterial.pxList(iPx,1),arterial.pxList(iPx,2),arterial.pxList(iPx,3));
                if cBf>0
                    allCorrectedSignals(iPx,:) = squeeze(acqData(arterial.pxList(iPx,1),arterial.pxList(iPx,2),arterial.pxList(iPx,3),:)/cBf - (1-cBf)/cBf*acqData(tissues.pxList(iPx,1),tissues.pxList(iPx,2),tissues.pxList(iPx,3),:));
                end
            end
            signalPve(iBf,iValBias).mean = mean(allCorrectedSignals,1);
            signalPve(iBf,iValBias).std = std(allCorrectedSignals,0,1);
        end
    end
    
    %% Display conc calculation
    gap = 0.01;
    marg_h = 0.08;
    marg_w = 0.07;
    
    aifLim = aifModel.Params.t0 + [-0.1 1];
    figAifPvcmSaturation = figure;
    figAifPvcmSaturation.Position = [609   346   665   520];
    for iValBias = 1:nValBias
        displayTime = time>aifLim(1) & time < aifLim(2);
        for iBf = 1:nBf
            ax3(iBf) = subtightplot(nValBias,nBf,nBf*(iValBias-1)+iBf,gap,marg_h,marg_w);
            plot(time(displayTime),aifConc(displayTime),'k-','Linewidth',2)
            hold on;
            errorbar(time(displayTime),squeeze(artPve(iBf,iValBias).all.mean(displayTime)),squeeze(artPve(iBf,iValBias).all.std(displayTime)),squeeze(artPve(iBf,iValBias).all.std(displayTime)),'r-');xlim(aifLim);
            
            plot(time(displayTime),artPve(iBf,iValBias).cluster.mean(displayTime),'b--','Linewidth',1);
            hold off;
            ylim([0 max(max(artPve(iBf,iValBias).cluster.mean)*1.1,aifPeak*1.3)]);
            if iBf == 1
                ylabel('Conc. (mM)');
            else
                ax3(iBf).YTickLabel = [];
            end
        end
        if iValBias==1
            for iBf = 1:nBf
                title(ax3(iBf),['BF ' num2str(bloodFractionVal(iBf)*100) '%']);
                ax3(iBf).XTickLabel = [];
            end
        elseif iValBias == nValBias
            for iBf = 1:nBf
                xlabel(ax3(iBf),'Time (min)');
            end
        else
            for iBf = 1:nBf
                ax3(iBf).XTickLabel = [];
            end
        end
    end
    legend(ax3(nBf),'True','Data','Cluster','Location','Best');
    print(figAifPvcmSaturation,'-dpdf',fullfile(projFigureFolder, ['aifConcBias_' currentBiasType '.pdf']));
    saveas(figAifPvcmSaturation,fullfile(projFigureFolder, ['aifConcBias_' currentBiasType '.fig']),'fig');
    export_fig(figAifPvcmSaturation,fullfile(projFigureFolder, ['aifConcBias_' currentBiasType '.png']),'-transparent','-m2');
    
    %% Old - DISplay signal
    
%         ax1 = subtightplot(nValBias,nBf,nBf*(iValBias-1) + 1,gap,marg_h,marg_w);
%         %         ax1 = subplot(nValBias,3,3*(iValBias-1) + 1);
%         plot(time,squeeze(combSignal(:,iValBias,1,:)),time,squeeze(gmSignal(1,iValBias,1,:))); xlim(aifLim);
%         ylabel('Signal (a.u.)');
%         
%         ax2 = subtightplot(nValBias,nBf,nBf*(iValBias-1) + 2,gap,marg_h,marg_w);
%         plot(time,aifSignal(iValBias,:),'-',time([1 end]),satSignal(iValBias,3)*[1 1],'k--',time([1 end]),maxSignalRatioFactor*satSignal(iValBias,3)*[1 1],'r--');
%         hold on;
%         for iBf = 1:nBf
%             errorbar(time,signalPve(iBf,iValBias).mean,signalPve(iBf,iValBias).std,signalPve(iBf,iValBias).std);
%         end
%         hold off;
%         xlim(aifLim);
end
 

