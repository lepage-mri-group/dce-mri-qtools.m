currentParams = [1 ...
    0.1 0.5 100 ...
    0.6 0.1 0.0775833485735807 ...
    8 0.0343708305978800 0.3 0.550909090909069];
currentParams(8) = 10;
currentParams(9) = 0.02;
currentParams(11) = 20;
currentParams(2) = 1e-10;
% currentParams(4) = 100;
currentParams(2) = 1e3;
currentParams(5) = 1;
currentParams(6) = 2;
currentParams(7) = 5;
%%
param.t0 = 1; % (min)
param.m = [NaN, 0.024, 0.002]*60; % (1/min)
param.A = [NaN, 3.99, 7]; % (kg/L)
% Recirculation delay
param.tau = 15/60; % (min)

% Subject 3 Aorta parameters
param.alpha = 3.05; % (-)
param.beta = 5/60; % (min)
param.m(1) = 0.053*60; % (1/min)
param.A(1) = 42.8; % (kg/L)

dose = 0.1; % mmol/kg
param.k = dose*75*(param.beta*(param.alpha+1)+param.tau); % 8.77*dose*(beta*(alpha+1)+tau)/kN; % (s mmol/kg)
figure(1); plot(redTime,redConc*1e3,redTime,aifhorsfield(redTime,param));

%%
[aifHors, bestParams, allParams, allResiduals, initValues,figHorsfield] =...
    fitmultihorsfield(redTime,t0,redConc*1e3,128,true,param);

figure(1); plot(redTime,redConc*1e3,redTime,aifhorsfield(redTime,bestParams));

%% Round 2
newStartingParam = [1 66.6692343702563 7.70774701148962 2.32027462731789 4.48770964838297 0.570309546020254 0.00100000886532964 7.99999898656650 0.0381576424895640 0.0800005466124474 8.81249999997885];
newParamStruct = paramvec2struct(newStartingParam,{'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'});
[aifHors, bestParams, allParams, allResiduals, initValues,figHorsfield] =...
    fitmultihorsfield(redTime,t0,redConc*1e3,256,true,newParamStruct);
figure(1); plot(redTime,redConc*1e3,redTime,aifhorsfield(redTime,bestParams));

%% Round 3
startingParamR3 = [1 75.1000435536873 6.82681677477981 1.85205371818894 5.53003797066961 0.613984336665013 0.00100000000002221 7.99999999999998 0.0391409095801694 0.0615128221720161 12.1738268307816];
