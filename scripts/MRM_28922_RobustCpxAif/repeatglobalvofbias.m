function [vofCalc] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerParam,vofConcOptions)

nTissueTypes = 2;
[nValBias,nMethods,~,nT] = size(venSignal);

% Combined signal
combSignal = NaN(nValBias,nMethods,nTissueTypes,nT);
combSignal(:,:,1,:) = venSignal;
combSignal(:,:,2,:) = repmat(permute(gmSignal,[3 1 4 2]),[nValBias,1,1,1]);

b1MapNoise = ones(imgModel.noiseParam.nNoise,imgModel.noiseParam.nNoise,nTissueTypes);
venousMask = false(imgModel.noiseParam.nNoise,imgModel.noiseParam.nNoise,nTissueTypes);
venousMask(:,:,1) = true;

if size(vofConcOptions,1)==nValBias
    calcOptionsChangeWithBias = true;
else
    calcOptionsChangeWithBias = false;
end
% Compute conc
vofCalc = cell(nValBias,nMethods);
bloodT1MapNoise = NaN(imgModel.noiseParam.nNoise,imgModel.noiseParam.nNoise,nTissueTypes);
for iMethods = 1:nMethods
    for iValBias = 1:nValBias
        if calcOptionsChangeWithBias
            cOptionBiasIdx = iValBias;
        else
            cOptionBiasIdx = 1;
        end
        % Add noise
        bloodT1MapNoise(:,:,1) = bloodT10(iValBias,iMethods);
        bloodT1MapNoise(:,:,2) = 1/imgModel.tissueParam.R10;
        if imgModel.noiseParam.nNoise == 1
            acqData = (combSignal(iValBias,iMethods,:,:));
            magData = abs(acqData);
            phaseData = -angle(acqData);
        else
            [acqMag, acqPhase] = imgModel.noiseParam.FHandle(permute(combSignal(iValBias,iMethods,:,:),[4,3,1,2]),imgModel.noiseParam);
            magData = permute(acqMag,[3,4,2,1]);
            phaseData = -permute(acqPhase,[3,4,2,1]);
        end
        % Compute VOF
        magData0 = mean(magData(:,:,:,framesPre(1):framesPre(2)),4);
        [venNoiseConc, ~] = concmrispgrclosed(magData,magData0,tracerParam{iMethods},bloodT1MapNoise,imgModel.seqParam,venousMask,b1MapNoise);
        
        if isfield(vofConcOptions{cOptionBiasIdx,iMethods},'split') && isfield(vofConcOptions{cOptionBiasIdx,iMethods}.split,'phase') && isfield(vofConcOptions{cOptionBiasIdx,iMethods}.split.phase,'gmMask')
            vofConcOptions{cOptionBiasIdx,iMethods}.split.phase.gmMask = false(size(bloodT1MapNoise));
            vofConcOptions{cOptionBiasIdx,iMethods}.split.phase.gmMask(:,:,2) = true;
        end
        [vofCalc{iValBias,iMethods}.fit,vofCalc{iValBias,iMethods}.cluster, vofCalc{iValBias,iMethods}.all, vofCalc{iValBias,iMethods}.conc] = ...
            globalvof(time,magData,phaseData,bloodT1MapNoise,b1MapNoise,...
            tracerParam{iMethods},imgModel.seqParam,venousMask,vofConcOptions{cOptionBiasIdx,iMethods},venNoiseConc);
    end
end

end