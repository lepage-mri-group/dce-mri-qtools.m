%% General parameters
% Output folders
projFigureFolder = uigetdir(pwd,'Select project folder to save images');
addNoise = false;

patientT10Approx = 1800;

% DRO Parameters
time = (0:2:660)/60;
injDose = (0.05)*72; % mL = (mmol/kg * kg)*1 mL/mmol
t0 = 60.25/60; % min

vofPeak = 5;
tissueParam.pkm{1} = 0.01; % Ktrans in (1/min)
tissueParam.pkm{2} = 0.1; % Ve in (-)
tissueParam.pkm{3} = 0.001; % Vp in (-)
tissueParam.T10 = 1820; % ms GM (Stanisz et al.)
tissueParam.T20s = 99/2; % (Half gray matter T20) Stanisz et al. 2005
tissueParam.r1 = 4.5; % QIBA4, 1/(mM*s) also similar to 4.46 measured by Shen et al. for gadobutrol at 3T
tissueParam.r2s = 87; % Kjolby et al. 2006 (1/(mM*s))

bloodParam.T10 = patientT10Approx;% (ms)
bloodParam.T20s = 275/2; % ms (Half blood T20) Stanisz et al. 2005
bloodParam.r1 = tissueParam.r1; % QIBA4, 1/(mM*s)
bloodParam.r2s = 0; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
bloodParam.xiM = 3.209e-7; % (1/mM) GD-DPTA, Diameter 6 mm, Parallel, 4.6 mL/sec (Van Osch, 2003)
bloodParam.vesselTheta = 0; % radians

bloodParamQuad.r2s = 0.4929; % [(s mM)^-1] Kjolby et al. 2006 (1/(mM*s)
bloodParamQuad.r22s =  2.6159; % [(s mM�)^-1]) @ 3T, Akbudak et al. 2004, reported by Kjolby et al. 2006

seqParam.te = 1.85; % ms
seqParam.tr = 4; % ms
seqParam.fa = 9.2/180*pi; % rad
seqParam.B0 = 3; % Tesla
seqParam.voxelRes = 2.3; % mm
% Changes from QIBA_v4 - Noise is added
if addNoise 
    seqParam.nNoise = 10;
    fileNameNoise = '_vxl_ss_eqWeight';
else
    seqParam.nNoise = 1;
    fileNameNoise = '_vxl_ss_noNoise_eqWeight';
end
seqParam.snr = 25;
[vofModel,pkModel,imgModel] = defaultmodelparams(time,t0,vofPeak,tissueParam,bloodParam,seqParam);

%% Recon parameters
angulationOffset = 0;%degrees
nTissueTypes = 2; % AIF and GM
aifLim = vofModel.Params.t0 + [-0.1 1];
nT = length(time);
framesPre = imgModel.bloodParam.tracer.framesBaseline;
n0 = framesPre(2);

firstPassRecircDelay = 45/60; % min
pveEndIdx = find(time>(time(framesPre(2))+firstPassRecircDelay),1,'first')-1;
clusterType = 'iauc';

sssB0AngSlice = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
sssB0AngMax = 50/180*pi;
noSssB0AngCorr = [];
sssB0Thresh = 30;
cerebellumFov = false;
gmMaskDefinedInBiasLoop = [];
vofMagOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'magnitude',cerebellumFov,[],noSssB0AngCorr,noSssB0AngCorr);
vofPhaseOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'phase',cerebellumFov,gmMaskDefinedInBiasLoop,noSssB0AngCorr,noSssB0AngCorr);
vofPhaseAngOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'phase',cerebellumFov,gmMaskDefinedInBiasLoop,sssB0AngSlice,sssB0AngMax);
vofCpxOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'complex_inflow',cerebellumFov,gmMaskDefinedInBiasLoop,noSssB0AngCorr,noSssB0AngCorr);
vofCpxAngOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'complex_inflow',cerebellumFov,gmMaskDefinedInBiasLoop,sssB0AngSlice,sssB0AngMax);
vofCpxSplitOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'complex_split',cerebellumFov,gmMaskDefinedInBiasLoop,noSssB0AngCorr,noSssB0AngCorr);
vofCpxSplitAngOptions = vofsssdefineoptions(time,framesPre,...
    nTissueTypes,'complex_split',cerebellumFov,gmMaskDefinedInBiasLoop,sssB0AngSlice,sssB0AngMax);

% Modify default options for the single slice DRO
% Usually SSS extends for at least 5-10 slices and we keep 1 phase voxel per
% slice, thus about 8 voxels for phase
vofMagOptions.split.mag.slabSlices = 0;
vofMagOptions.split.mag.minMagSlices = 1;
vofMagOptions.split.nKeptPerSlice = inf;

vofPhaseOptions.split.phase.nMinSlices = 1;
vofPhaseOptions.split.phase.nMaxSlices = nTissueTypes;
vofPhaseOptions.split.phase.maxVoxelsPerSlice = 8;
vofPhaseOptions.split.nKeptPerSlice = inf;

vofPhaseAngOptions.split.phase.nMinSlices = 1;
vofPhaseAngOptions.split.phase.nMaxSlices = nTissueTypes;
vofPhaseAngOptions.split.phase.maxVoxelsPerSlice = 8;
vofPhaseAngOptions.split.nKeptPerSlice = inf;

vofCpxOptions.split.mag.slabSlices = 0;
vofCpxOptions.split.mag.minMagSlices = 1;
vofCpxOptions.split.phase.nMinSlices = 1;
vofCpxOptions.split.phase.nMaxSlices = nTissueTypes;
vofCpxOptions.split.phase.maxVoxelsPerSlice = 8;
vofCpxOptions.split.nKeptPerSlice = inf;

vofCpxAngOptions.split.mag.slabSlices = 0;
vofCpxAngOptions.split.mag.minMagSlices = 1;
vofCpxAngOptions.split.phase.nMinSlices = 1;
vofCpxAngOptions.split.phase.nMaxSlices = nTissueTypes;
vofCpxAngOptions.split.phase.maxVoxelsPerSlice = 8;
vofCpxAngOptions.split.nKeptPerSlice = inf;

vofCpxSplitOptions.split.mag.slabSlices = 0;
vofCpxSplitOptions.split.mag.minMagSlices = 1;
vofCpxSplitOptions.split.phase.nMinSlices = 1;
vofCpxSplitOptions.split.phase.nMaxSlices = nTissueTypes;
vofCpxSplitOptions.split.phase.maxVoxelsPerSlice = 8;
vofCpxSplitOptions.split.nKeptPerSlice = inf;

vofCpxSplitAngOptions.split.mag.slabSlices = 0;
vofCpxSplitAngOptions.split.mag.minMagSlices = 1;
vofCpxSplitAngOptions.split.phase.nMinSlices = 1;
vofCpxSplitAngOptions.split.phase.nMaxSlices = nTissueTypes;
vofCpxSplitAngOptions.split.phase.maxVoxelsPerSlice = 8;
vofCpxSplitAngOptions.split.nKeptPerSlice = inf;

imgModelQuadRelax = imgModel;
imgModelQuadRelax.bloodParam.tracer.r2s = bloodParamQuad.r2s;
imgModelQuadRelax.bloodParam.tracer.r22s = bloodParamQuad.r22s;
imgModelQuadRelax.bloodParam.tracer.enableQuadR2 = true;
imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi;

% Measurement methods
methodNames = {'Mag.','Phase','Phase-ang.','Cpx','Cpx-ang.'}; % Vessel angulation (degrees)
% closed-form, no angulation, no r2*
vofDefaultOptions = cell(1,4);
tracerDefaultParam{1,1} = imgModel.bloodParam.tracer;
vofDefaultOptions{1,1} = vofMagOptions;
% phase, no angulation
tracerDefaultParam{1,2} = imgModelQuadRelax.bloodParam.tracer;
vofDefaultOptions{1,2} = vofPhaseOptions;
% complex, no angulation, r2*
tracerDefaultParam{1,3} = imgModelQuadRelax.bloodParam.tracer;
tracerDefaultParam{1,3}.inflowType = 'exp';
vofDefaultOptions{1,3} = vofCpxOptions;
% phase, angulation
tracerDefaultParam{1,4} = imgModelQuadRelax.bloodParam.tracer;
vofDefaultOptions{1,4} = vofPhaseAngOptions;
% complex new, angulation and r2*
tracerDefaultParam{1,5} = imgModelQuadRelax.bloodParam.tracer;
tracerDefaultParam{1,5}.inflowType = 'exp';
vofDefaultOptions{1,5} = vofCpxAngOptions;

% vofType = 'complex_split';
% aifOptionsPveClosed = aifpvedefineoptions(time,framesPre,pveEndIdx,1,'s-pvc-closed-form',clusterType,vofType);
% aifOptionsPveClosed.aif.pve.reducedAifTime = 0; % No delay simulated between AIF and VOF
% aifOptionsPveClosed.vof.cluster.aif.iaucRangeIdx = aifOptionsPveClosed.aif.cluster.aif.iaucRangeIdx;
% aifOptionsPveClosed.vof.split.iaucRangeIdx = aifOptionsPveClosed.vof.cluster.aif.iaucRangeIdx;
% vofOptions = aifOptionsPveClosed.vof;
% 
% angulationOffset = 0;%degrees
% imgModel.bloodParam.theta = angulationOffset/180*pi;
% vofOptions.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
% vofOptions.split.cerebellumFov = false;
% % Modify default options for the single slice DRO
% % Usually SSS extends for at least 5-10 slices and we keep 1 phase voxel per
% % slice, thus about 8 voxels for phase
% vofOptions.split.phase.nMinSlices = 1;
% vofOptions.split.phase.nMaxSlices = nTissueTypes;
% vofOptions.split.phase.maxVoxelsPerSlice = 8;
% 
% imgModelQuadRelax = imgModel;
% imgModelQuadRelax.bloodParam.tracer.r2s = bloodParamQuad.r2s;
% imgModelQuadRelax.bloodParam.tracer.r22s = bloodParamQuad.r22s;
% imgModelQuadRelax.bloodParam.tracer.enableQuadR2 = true;
% imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi;
% 
% aifOptionsPveClosedCpxTrad = aifpvedefineoptions(time,framesPre,pveEndIdx,1,'s-pvc-closed-form',clusterType,'complex');
% aifOptionsPveClosedCpxTrad.aif.pve.reducedAifTime = 0; % No delay simulated between AIF and VOF
% aifOptionsPveClosedCpxTrad.vof.cluster.aif.iaucRangeIdx = aifOptionsPveClosedCpxTrad.aif.cluster.aif.iaucRangeIdx;
% vofOptionsCpxTrad = aifOptionsPveClosedCpxTrad.vof;
% 
% % Measurement methods
% methodNames = {'Mag.','Phase','Phase-ang.','Cpx','Cpx-ang.'}; % Vessel angulation (degrees)
% % closed-form, no angulation, no r2*
% vofDefaultOptions = cell(1,4);
% tracerDefaultParam{1,1} = imgModel.bloodParam.tracer;
% vofDefaultOptions{1,1} = vofOptionsCpxTrad;
% vofDefaultOptions{1,1}.conc.type = 'basic';
% % phase, no angulation
% tracerDefaultParam{1,2} = imgModelQuadRelax.bloodParam.tracer;
% vofDefaultOptions{1,2} = vofOptions;
% vofDefaultOptions{1,2}.conc.type = 'phase';
% vofDefaultOptions{1,2}.conc.split.phase.B0AngThresh = 50/180*pi;
% % complex, no angulation, r2*
% tracerDefaultParam{1,3} = imgModelQuadRelax.bloodParam.tracer;
% vofDefaultOptions{1,3} = vofOptions;
% vofDefaultOptions{1,3}.conc.type = 'complex';
% % phase, angulation
% tracerDefaultParam{1,4} = imgModelQuadRelax.bloodParam.tracer;
% vofDefaultOptions{1,4} = vofOptions;
% vofDefaultOptions{1,4}.conc.type = 'phase-ang';
% vofDefaultOptions{1,4}.conc.split.phase.B0AngThresh = 50/180*pi;
% vofDefaultOptions{1,4}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
% % complex new, angulation and r2*
% tracerDefaultParam{1,5} = imgModelQuadRelax.bloodParam.tracer;
% vofDefaultOptions{1,5} = vofOptions;
% vofDefaultOptions{1,5}.conc.type = 'complex_split';
% vofDefaultOptions{1,5}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)

nMethods = size(vofDefaultOptions,2);

% Compute biased MRI signal and compute effect on AIF PVCM concentration
horsfieldStructFieldOrder = {'t0','A(1)','A(2)','A(3)','m(1)','m(2)','m(3)','alpha','beta','tau','k'};
vofModel.pValues = paramstruct2vec(vofModel.Params,horsfieldStructFieldOrder);
vofConc = vofModel.FHandle(time,vofModel.pValues);
% Tissue
cParamsValues = [pkModel.Ktrans, pkModel.Ve, pkModel.Vp];
patientParam.hematocrit = pkModel.hematocrit;
patientParam.brainDensity = pkModel.brainDensity; % g/mL
gmConc = pkModel.FHandle(time,vofConc,patientParam,[],cParamsValues);

%% Reference calculation
% bloodFractionVal = 1.0;
% angulationOffset = 15;%degrees
% bloodT1 = bloodParam.T10;
% 
% % No bias (BF: 1.0; Ang: 0 radians; BloodT1 = 1932 ms
% imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi; 
% [venSignal,vofSignal,gmSignal,satSignal,bloodT10] = signalbiast10(vofConc,gmConc,n0,bloodFractionVal,bloodT1,imgModelQuadRelax);
% venSignal = repmat(venSignal,[1 nMethods 1 1]);
% gmSignal = repmat(gmSignal,[nMethods 1]);
% bloodT10 = repmat(reshape(bloodT10,1,1),[1 nMethods 1]);
% 
% % Repeated conc calculation
% vofCurrentOptions = vofDefaultOptions;
% for iMethod = 1:nMethods
%     vofCurrentOptions{1,iMethod}.split.sssB0Ang = []; 
%     vofCurrentOptions{1,iMethod}.split.phase.debug = true;
% end
% vofCurrentOptions{1,nMethods-1}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
% vofCurrentOptions{1,nMethods}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
% [vofCalcRef] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

%% Bias free calculation
bloodFractionVal = 1.0;
angulationOffset = 0;%degrees
bloodT1 = bloodParam.T10;

% No bias (BF: 1.0; Ang: 0 radians; BloodT1 = 1932 ms
imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi; 
[venSignal,vofSignal,gmSignal,satSignal,bloodT10] = signalbiast10(vofConc,gmConc,n0,bloodFractionVal,bloodT1,imgModelQuadRelax);
venSignal = repmat(venSignal,[1 nMethods 1 1]);
gmSignal = repmat(gmSignal,[nMethods 1]);
bloodT10 = repmat(reshape(bloodT10,1,1),[1 nMethods 1]);

% Repeated conc calculation
vofCurrentOptions = vofDefaultOptions;
for iMethod = 1:nMethods
    vofCurrentOptions{1,iMethod}.split.sssB0Ang = []; 
    vofCurrentOptions{1,iMethod}.split.phase.debug = true;
end
vofCurrentOptions{1,nMethods-1}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofCurrentOptions{1,nMethods}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
[vofCalcNoBias] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

figure; 
for iMethod = 1:nMethods
        subplot(nMethods,1,iMethod);
        plot(time,vofConc,time,vofCalcNoBias{iMethod}.cluster.mean,time,vofCalcNoBias{iMethod}.all.mean,time,vofCalcNoBias{iMethod}.fit.conc)
        xlim(aifLim);
        ylim([0 6]);
end
legend('True','Cluster','All','Fit')

%% Bias 1. Blood fractions
iBias = 1;
bloodFractionVal = [0.8, 0.6];
angulationOffset = 0;%degrees
bloodT1 = bloodParam.T10;

biasTypes(iBias).name = 'BloodFraction';
biasTypes(iBias).values = bloodFractionVal;
biasTypes(iBias).noBiasValue = 1.0;
biasTypes(iBias).legendUnits = '';
biasTypes(iBias).vofRef = vofCalcNoBias;

nValBias = length(biasTypes(iBias).values);

% VEnous signal biased with a 5 degree misalignment and T2* blood relaxivity
imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi; 
[venSignal,vofSignal,gmSignal,satSignal,bloodT10] = signalbiassusceptibility(vofConc,gmConc,n0,biasTypes(iBias).values,angulationOffset,imgModelQuadRelax);
venSignal = repmat(venSignal,[1 nMethods 1 1]);
gmSignal = repmat(gmSignal,[nMethods 1]);
bloodT10 = repmat(reshape(bloodT10,nValBias,1),[1 nMethods 1]);

% Repeated conc calculation
vofCurrentOptions = vofDefaultOptions;
for iMethod = 1:nMethods
    vofCurrentOptions{1,iMethod}.split.sssB0Ang = []; 
    vofCurrentOptions{1,iMethod}.split.phase.debug = true;
end
vofCurrentOptions{1,nMethods-1}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofCurrentOptions{1,nMethods}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofOptions.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
[biasTypes(iBias).vofCalc] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

%% Bias 2. SSS to B0 angulation
% Define
iBias = 2;
bloodFractionVal = 1.0;
angulationOffset = [15,30,45];%,atan(sqrt(2))/pi*180];%degrees
bloodT1 = bloodParam.T10;

biasTypes(iBias).name = 'Angulation';
biasTypes(iBias).values = angulationOffset; % Vessel angulation (degrees)
biasTypes(iBias).noBiasValue = 0;
biasTypes(iBias).legendUnits = '\circ';
biasTypes(iBias).vofRef = vofCalcNoBias;

nValBias = length(biasTypes(iBias).values);
% Compute signal
figureSusceptibility = figure;
figureSusceptibility.Position = [522   367   795   477];
showSuccSim.paramBfIdx = 1;
showSuccSim.paramXiIdx = 2;
showSuccSim.axTube = subplot(2,4,[1 2]);
showSuccSim.axPhase = subplot(2,4,3);
showSuccSim.axCpx = subplot(2,4,4);
showSuccSim.axSignal = subplot(2,4,[5 6]);
showSuccSim.axConc = subplot(2,4,[7 8]);
showSuccSim.axTube.Position = [0.082    0.50    0.36    0.43];
showSuccSim.axPhase.Position = [0.5322    0.5052    0.1566    0.42];
showSuccSim.axCpx.Position = [0.7609    0.5052    0.1566    0.42];
showSuccSim.axSignal.Position = [0.1300    0.1100    0.3628    0.30];
showSuccSim.axConc.Position = [0.5422    0.1100    0.3628    0.30];

imgModelQuadRelax.bloodParam.theta = []; % Field not used in this model.
[venSignal,vofSignal,gmSignal,satSignal,bloodT10] = signalbiassusceptibility(vofConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values,imgModelQuadRelax,showSuccSim);
venSignal = repmat(reshape(venSignal,nValBias,1,1,nT),[1 nMethods 1 1]);
gmSignal = repmat(gmSignal(1,:),[nMethods 1]);
bloodT10 = repmat(reshape(bloodT10,nValBias,1),[1 nMethods 1]);

% Repeated conc calculation
vofCurrentOptions = cell(nValBias,nMethods);
for iBiasParam = 1:nValBias
    for iMethod = 1:nMethods
        vofCurrentOptions{iBiasParam,iMethod} = vofDefaultOptions{iMethod};
        vofCurrentOptions{iBiasParam,iMethod}.split.sssB0Ang = [];
        vofCurrentOptions{iBiasParam,iMethod}.split.phase.debug = true;
    end
    vofCurrentOptions{iBiasParam,nMethods-1}.split.sssB0Ang = angulationOffset(iBiasParam)*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
    vofCurrentOptions{iBiasParam,nMethods}.split.sssB0Ang = angulationOffset(iBiasParam)*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
end
[biasTypes(iBias).vofCalc] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

% Save figure
iShowParam = showSuccSim.paramXiIdx; iShowBf = showSuccSim.paramBfIdx;
geomStr = [num2str(biasTypes(iBias).values(iShowParam)) '_' num2str(bloodFractionVal(iShowBf))];
displayTime = time>aifLim(1) & time < aifLim(2);
axes(showSuccSim.axConc);
plot(time(displayTime),vofConc(displayTime),'k-','Linewidth',2)
hold on;
errorbar(time(displayTime),squeeze(biasTypes(iBias).vofCalc{iShowBf,iShowParam}.all.mean(displayTime)),squeeze(biasTypes(iBias).vofCalc{iShowBf,iShowParam}.all.std(displayTime)),squeeze(biasTypes(iBias).vofCalc{iShowBf,iShowParam}.all.std(displayTime)),'r-');xlim(aifLim);
plot(time(displayTime),biasTypes(iBias).vofCalc{iShowBf,iShowParam}.cluster.mean(displayTime),'b--','Linewidth',1);
hold off;
ylim([0 max(max(biasTypes(iBias).vofCalc{iShowBf,iShowParam}.cluster.mean)*1.1,vofPeak*1.3)]);
ylabel('Conc. (mM)');
xlabel(showSuccSim.axConc,'Time (min)');
legend(showSuccSim.axConc,'True','Data','Cluster','Location','Best');
xlabel(showSuccSim.axConc,'Time (min)');

saveas(figureSusceptibility,fullfile(projFigureFolder, ['figSusceptibilitySimulationsVof' geomStr fileNameNoise '.fig']),'fig');
export_fig(figureSusceptibility,fullfile(projFigureFolder, ['figSusceptibilitySimulationsVof' geomStr fileNameNoise '.png']),'-transparent','-m2');
% Remove susceptibility images before PDF
delete(showSuccSim.axTube);
print(figureSusceptibility,'-dpdf',fullfile(projFigureFolder, ['figSusceptibilitySimulationsVof' geomStr fileNameNoise '.pdf']));
%   
%% Bias 3. Blood T1
% Define
iBias = 3;
bloodFractionVal = 1.0;
angulationOffset = 0;%degrees
bloodT1 = [1400 2200];

biasTypes(iBias).name = 'BloodT1';
biasTypes(iBias).values = bloodT1; % Vessel angulation (degrees)
biasTypes(iBias).noBiasValue = bloodParam.T10;
biasTypes(iBias).legendUnits = ' ms';
biasTypes(iBias).vofRef = vofCalcNoBias;

nValBias = length(biasTypes(iBias).values);

% Compute signal
imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi;
[venSignal,vofSignal,gmSignal,satSignal,bloodT10] = signalbiast10(vofConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values,imgModelQuadRelax);
venSignal = repmat(reshape(venSignal,nValBias,1,1,nT),[1 nMethods 1 1]);
gmSignal = repmat(gmSignal(1,:),[nMethods 1]);
bloodT10 = repmat(reshape(bloodT10,nValBias,1),[1 nMethods 1]);

vofCurrentOptions = vofDefaultOptions;
for iMethod = 1:nMethods
    vofCurrentOptions{1,iMethod}.split.sssB0Ang = []; 
    vofCurrentOptions{1,iMethod}.split.phase.debug = true;
end
vofCurrentOptions{1,nMethods-1}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofCurrentOptions{1,nMethods}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofOptions.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
[biasTypes(iBias).vofCalc] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

% Manual conc average to compensate for magnitude excluded conc at low T1
vesselMask = false(seqParam.nNoise,seqParam.nNoise,2);
vesselMask(:,:,1) = true;
concNotNan = biasTypes(iBias).vofCalc{1,1}.conc.conc;
concNotNan(biasTypes(iBias).vofCalc{1,1}.conc.outliers==1) = NaN;
[allConcList,~] = dyn3dconversion(concNotNan,vesselMask,[]);
biasTypes(iBias).vofCalc{1,1}.all.mean = nanmean(allConcList,1);
biasTypes(iBias).vofCalc{1,1}.all.std = nanstd(allConcList,[],1);

%% Bias 4. Inflow
iBias = 4;
bloodFractionVal = 1.0;
% nRfSssVal = [270 170 70];
nRfSssVal = [270 170 120 70 50 30 10];
angulationOffset = 0;%degrees
bloodT1 = bloodParam.T10;

biasTypes(iBias).name = 'Inflow';
biasTypes(iBias).values = nRfSssVal;
biasTypes(iBias).noBiasValue = 999;
biasTypes(iBias).legendUnits = '';
biasTypes(iBias).vofRef = vofCalcNoBias;

nValBias = length(biasTypes(iBias).values);

sssMeanVelocity = 152e-3; % m/s
dcePdf.tr = 4e-3; % s
dcePdf.rfDur = 0.672e-3; % s
dcePdf.rfAmp = 7.045e-6; % T
dcePdf.slabDim = 2.3e-3*39; % m
dcePdf.grStr = 3.1e-3; % T/m
[dcePdf.b1AgainstRf,dcePdf.fovStartIdx,dcePdf.fovStopIdx,dcePdf.slabZ,dcePdf.slabB1] = getb1slabinflow(dcePdf.tr,dcePdf.rfDur,dcePdf.rfAmp,dcePdf.grStr,dcePdf.slabDim,sssMeanVelocity);
dceTheoEndIdx = find(dcePdf.b1AgainstRf(1:dcePdf.fovStopIdx)>1,1,'last');
prongatedDceB1AgainstRf = dcePdf.b1AgainstRf;
prongatedDceB1AgainstRf(dceTheoEndIdx:end) = 1;

% Add local T10
% nRF of VFA, smaller because TR is longer + offset from wider slab
vfaPdf.tr = 6.7e-3; % s
vfaPdf.rfDur = 0.851e-3; % s
vfaPdf.rfAmp = 13.411e-6; % T
vfaPdf.slabDim = 1.25e-3*96; % m
vfaPdf.grStr = 1.85e-3; % T/m
nRfSssValVfa = round(((nRfSssVal*dcePdf.tr)*sssMeanVelocity+(vfaPdf.slabDim-dcePdf.slabDim)/2)/sssMeanVelocity/vfaPdf.tr); 
[vfaPdf.b1AgainstRf,vfaPdf.fovStartIdx,vfaPdf.fovStopIdx,vfaPdf.slabZ,vfaPdf.slabB1] = getb1slabinflow(vfaPdf.tr,vfaPdf.rfDur,vfaPdf.rfAmp,vfaPdf.grStr,vfaPdf.slabDim,sssMeanVelocity);
vfaTheoEndIdx = find(vfaPdf.b1AgainstRf(1:vfaPdf.fovStopIdx)>1,1,'last');
prongatedVfaB1AgainstRf = vfaPdf.b1AgainstRf;
prongatedVfaB1AgainstRf(vfaTheoEndIdx:end) = 1;

vfaFa = [2.8 10 22.2]/180*pi;
vfaSeqParam.te = 3.1; % ms
vfaSeqParam.tr = 6.7; % ms
vfaSeqParam.B0 = 3; % Tesla
vfaSeqParam.voxelRes = 2.3; % mm
vfaSeqParam.B1Reps = prongatedVfaB1AgainstRf;

nFa = length(vfaFa);
nSs = length(nRfSssVal);
vfaSignal = NaN(nSs+1,1,1,nFa);
for iFa = 1:nFa
    vfaSeqParam.fa = vfaFa(iFa); % rad
    [signal, tissueParam] = signalmrispgrreachss(0,n0,imgModelQuadRelax.bloodParam,vfaSeqParam);
    for iSs = 1:nSs
        vfaSignal(iSs,1,1,iFa) = signal(nRfSssValVfa(iSs)+vfaPdf.fovStartIdx);
    end
    [signal, tissueParam] = signalmrispgr(0,n0,imgModelQuadRelax.bloodParam,vfaSeqParam);
    vfaSignal(nSs+1,1,1,iFa) = signal;
end
[t1MapVfa, ~, ~,~,fitErrorT1] = t1mapvfaqtools(vfaSignal,vfaFa/pi*180,vfaSeqParam.tr,true(nSs+1,1),'nonlin',ones(nSs+1,1),0,[],false);


% Venous signal biased with a 5 degree misalignment and T2* blood relaxivity
imgModelQuadRelax.bloodParam.theta = angulationOffset/180*pi; 
imgModelQuadRelax.seqParam.B1Reps = prongatedDceB1AgainstRf;
[venSignal,vofSignal,gmSignal,satSignal,bloodT10,figSs] = signalbiasreachss(vofConc,gmConc,n0,bloodFractionVal,biasTypes(iBias).values+dcePdf.fovStartIdx,imgModelQuadRelax);
venSignal = repmat(reshape(venSignal,nValBias,1,1,nT),[1 nMethods 1 1]);
gmSignal = repmat(gmSignal(1,:),[nMethods 1]);
T1isLocal = true;
if T1isLocal
    bloodT10 = repmat(reshape(t1MapVfa(1:nSs),nValBias,1),[1 nMethods 1]);
else
    bloodT10 = repmat(reshape(bloodT10(1:nSs),nValBias,1),[1 nMethods 1]);
end
% Repeated conc calculation
vofCurrentOptions = vofDefaultOptions;
for iMethod = 1:nMethods
    vofCurrentOptions{1,iMethod}.split.sssB0Ang = []; 
    vofCurrentOptions{1,iMethod}.split.phase.debug = true;
    vofCurrentOptions{1,iMethod}.split.mag.minT1Blood = min(t1MapVfa)-10;
end
vofCurrentOptions{1,nMethods-1}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofCurrentOptions{1,nMethods}.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
vofOptions.split.sssB0Ang = angulationOffset*ones(nTissueTypes,1)/180*pi; % Slice angulation (radians)
[biasTypes(iBias).vofCalc] = repeatglobalvofbias(time,vofConc,venSignal,gmSignal,framesPre,imgModel,bloodT10,tracerDefaultParam,vofCurrentOptions);

iMethod = 3;
NRFAll = NaN(nSs,1);
NRFAllSd = NaN(nSs,1);
for iSs = 1:nSs
    cRRf = biasTypes(iBias).vofCalc{iSs,iMethod}.conc.rRf(:,:,1);
    NRFAll(iSs,1) = mean(cRRf(:));
    NRFAllSd(iSs,1) = std(cRRf(:));
end
cRRf = biasTypes(iBias).vofRef{1,iMethod}.conc.rRf(:,:,1);
NRFAllRef = mean(cRRf(:));
NRFAllSdRef = std(cRRf(:));

% FAAll = NaN(nSs,1);
% FAAllSd = NaN(nSs,1);
% for iSs = 1:nSs
%     cFa = biasTypes(iBias).vofCalc{iSs,iMethod}.conc.fa(:,:,1);
%     FAAll(iSs,1) = mean(cFa(:));
%     FAAllSd(iSs,1) = std(cFa(:));
% end
% cRRf = biasTypes(iBias).vofRef{1,iMethod}.conc.fa(:,:,1);
% NFAAllRef = mean(cRRf(:));
% NFAAllSdRef = std(cRRf(:));
% 
% FAAll(iKeptInflow)/pi*180
% NFAAllRef/pi*180
% NFAAllSdRef/pi*180
%% Reduced inflow
iBias = 5;
biasTypes(iBias) = biasTypes(4);
biasTypes(iBias).name = 'ReducedInflow';
iKeptInflow = [1 4 6];
biasTypes(iBias).values = nRfSssVal(iKeptInflow);
biasTypes(iBias).vofCalc = biasTypes(4).vofCalc(iKeptInflow,:);

t1MapVfa(iKeptInflow)
% FAAll(iKeptInflow)/pi*180
% FAAllSd(iKeptInflow)/pi*180
NRFAll(iKeptInflow)
NRFAllSd(iKeptInflow)
%% Get NRF
NRFAll = NaN(7,5);
NRFAllSd = NaN(7,5);
for iBias = 1:5
    iMethod = 5;
    nSs = length(biasTypes(iBias).values);
    for iSs = 1:nSs
        cRRf = biasTypes(iBias).vofCalc{iSs,iMethod}.conc.rRf(:,:,1);
        NRFAll(iSs,iBias) = mean(cRRf(:));
        NRFAllSd(iSs,iBias) = std(cRRf(:));
    end
end
%% Compute global parameters
for iBias = 1:5
    [barAllParameter(iBias),barClusterParameter(iBias),figVofT10] = biasglobalparam(time,vofConc,biasTypes(iBias).vofRef,biasTypes(iBias).vofCalc,vofModel.Params.t0,methodNames,biasTypes(iBias).noBiasValue,biasTypes(iBias).values,biasTypes(iBias).legendUnits,aifLim);
    if size(biasTypes(iBias).vofCalc,1) == 3
        figVofT10.Position = [609   267   665   599];
    elseif size(biasTypes(iBias).vofCalc,1) == 2
        figVofT10.Position = [609   267   497   599];
    else
        figVofT10.Position = [30         265        1449   599];
    end
%     print(figVofT10,'-dpdf',fullfile(projFigureFolder, ['vofConcBias_' biasTypes(iBias).name fileNameNoise '.pdf']));
%     saveas(figVofT10,fullfile(projFigureFolder, ['vofConcBias_' biasTypes(iBias).name fileNameNoise '.fig']),'fig');
%     export_fig(figVofT10,fullfile(projFigureFolder, ['vofConcBias_' biasTypes(iBias).name fileNameNoise '.png']),'-transparent','-m2');
end
%% Slabs for inflowing blood

figInflowSlab = figure;
figInflowSlab.Position = [198   162   438   377];
pVfa = plot(vfaPdf.slabZ*1e3,vfaPdf.slabB1,'-g','Linewidth',2);
hold on;
plot(-vfaPdf.slabDim/2*[1 1+10*eps]*1e3,[0 2],':g','Linewidth',2);
plot(vfaPdf.slabDim/2*[1 1+10*eps]*1e3,[0 2],':g','Linewidth',2);
pDce = plot(dcePdf.slabZ*1e3,dcePdf.slabB1,'-r','Linewidth',2);
plot(-dcePdf.slabDim/2*[1 1+10*eps]*1e3,[0 2],':r','Linewidth',2);
plot(dcePdf.slabDim/2*[1 1+10*eps]*1e3,[0 2],':r','Linewidth',2);
for iKeptSs = 1:length(iKeptInflow)
    cRfPos = -dcePdf.slabDim/2 + nRfSssVal(iKeptInflow(iKeptSs))*dcePdf.tr*sssMeanVelocity;
    pRfPos = plot(cRfPos*[1 1+10*eps]*1e3,[0 2],':k','Linewidth',2);
end
pExtended = plot([36.48 125],[1 1],'--','Color',[1 1 1]*0.3,'Linewidth',2);
xlim([-100 125]);
ylim([0 1.5]);
xlabel('Foot-Head (mm)');
ylabel('B_1 (-)');
cAx = gca;
set(cAx,'FontName','Arial','FontSize',12,'FontWeight','Bold',  'LineWidth', 1);
legend([pVfa,pDce,pExtended,pRfPos],'VFA slab','DCE slab','Extended slab','N_p'); 

print(figInflowSlab,'-dpdf',fullfile(projFigureFolder, ['vofInflowSlabs' '.pdf']));
saveas(figInflowSlab,fullfile(projFigureFolder, ['vofInflowSlabs' '.fig']),'fig');
export_fig(figInflowSlab,fullfile(projFigureFolder, ['vofInflowSlabs' '.png']),'-transparent','-m2');

vfaPdf.slabDim/(vfaPdf.tr*sssMeanVelocity)
dcePdf.slabDim/(dcePdf.tr*sssMeanVelocity)

%% Display global parameters
nBias = length(biasTypes);
gap = 0.08;
marg_h = 0.08;
marg_w = 0.1;

ylimManual = {[-5 1],[-0.6 0.2]};

nSubs = 2;
figAllBias = figure;
figAllBias.Position = [238   209   668   526];
for iBias = 1:nBias
    nBars = size(barAllParameter(iBias).peakDiff,1);
    barsPlus = linspace(-0.47,0.47,nBars+2);
    xBars = repmat((1:nMethods)',[1 nBars]) + repmat(barsPlus(2:end-1) ,[nMethods, 1]);
    ax(iBias,1) = subtightplot(nBias,nSubs,nSubs*(iBias-1)+1,gap,marg_h,marg_w);
%     ax(iBias,1) = subtightplot(nSubs,nBias,iBias,gap,marg_h,marg_w);
    errorbar(xBars,barAllParameter(iBias).peakDiff',barAllParameter(iBias).peakDiffError','k.','Linewidth',1.5);
    hold on;
    plot([0,nMethods+1],[0,0],'k-');
    bar(barAllParameter(iBias).peakDiff','hist');
    hold off;
    ylabel('Peak diff. (mM)')
    ax(iBias,1).XTick = 1:nMethods;
    ax(iBias,1).XTickLabels = barAllParameter(iBias).paramXTick;
    xlim(0.5+[0, nMethods]);
    ylim(ylimManual{1});
    if iBias == nBias
        xlabel('Method');
    end
    %     legend(barAllParameter(iBias).paramLegend);
    ax(iBias,2) = subtightplot(nBias,nSubs,nSubs*(iBias-1)+2,gap,marg_h,marg_w);
%     ax(iBias,2) = subtightplot(nSubs,nBias,nBias+iBias,gap,marg_h,marg_w);
    errorbar(xBars,barAllParameter(iBias).tailDiff',barAllParameter(iBias).tailDiffError','k.','Linewidth',1.5);
    hold on;
    plot([0,nMethods+1],[0,0],'k-');
    barsHandle = bar(barAllParameter(iBias).tailDiff','hist');
    hold off;
    ylabel('Tail diff. (mM)')
    ax(iBias,2).XTick = 1:nMethods;
    ax(iBias,2).XTickLabels = barAllParameter(iBias).paramXTick;
    %     legend(barAllParameter(iBias).paramLegend);
%     ax(iBias,3) = subtightplot(nSubs,nBias,2*nBias+iBias,gap,marg_h,marg_w);
%     bar(barAllParameter(iBias).precision*100);
%     if iBias == 1
%         ylabel('Precision (%)')
%     end
%     ax(iBias,3).XTickLabels = barAllParameter(iBias).paramXTick;
    if iBias == nBias
        xlabel('Method');
    end
    xlim(0.5+[0, nMethods]);
    ylim(ylimManual{2});
    leg(iBias) = legend(barsHandle,barAllParameter(iBias).paramLegend);
end

%% Display cluster parameters
figClusterBias = figure;
figClusterBias.Position = [238   209   668   526];

for iBias = 1:nBias
    nBars = size(barClusterParameter(iBias).peakDiff,1);
    barsPlus = linspace(-0.47,0.47,nBars+2);
    xBars = repmat((1:nMethods)',[1 nBars]) + repmat(barsPlus(2:end-1) ,[nMethods, 1]);
    ax(iBias,1) = subtightplot(nBias,nSubs,nSubs*(iBias-1)+1,gap,marg_h,marg_w);
%     ax(iBias,1) = subtightplot(nSubs,nBias,iBias,gap,marg_h,marg_w);
    errorbar(xBars,barClusterParameter(iBias).peakDiff',barClusterParameter(iBias).peakDiffError','k.','Linewidth',1.5);
    hold on;
    plot([0,nMethods+1],[0,0],'k-');
    bar(barClusterParameter(iBias).peakDiff','hist');
    hold off;
    ylabel('Peak diff. (mM)')
    ax(iBias,1).XTick = 1:nMethods;
    ax(iBias,1).XTickLabels = barClusterParameter(iBias).paramXTick;
    xlim(0.5+[0, nMethods]);
    ylim(ylimManual{1});
    if iBias == nBias
        xlabel('Method');
    end
    %     legend(barClusterParameter(iBias).paramLegend);
    ax(iBias,2) = subtightplot(nBias,nSubs,nSubs*(iBias-1)+2,gap,marg_h,marg_w);
%     ax(iBias,2) = subtightplot(nSubs,nBias,nBias+iBias,gap,marg_h,marg_w);
    errorbar(xBars,barClusterParameter(iBias).tailDiff',barClusterParameter(iBias).tailDiffError','k.','Linewidth',1.5);
    hold on;
    plot([0,nMethods+1],[0,0],'k-');
    barsHandle = bar(barClusterParameter(iBias).tailDiff','hist');
    hold off;
    ylabel('Tail diff. (mM)')
    ax(iBias,2).XTick = 1:nMethods;
    ax(iBias,2).XTickLabels = barClusterParameter(iBias).paramXTick;
    %     legend(barClusterParameter(iBias).paramLegend);
%     ax(iBias,3) = subtightplot(nSubs,nBias,2*nBias+iBias,gap,marg_h,marg_w);
%     bar(barClusterParameter(iBias).precision*100);
%     if iBias == 1
%         ylabel('Precision (%)')
%     end
%     ax(iBias,3).XTickLabels = barClusterParameter(iBias).paramXTick;
    if iBias == nBias
        xlabel('Method');
    end
    xlim(0.5+[0, nMethods]);
    ylim(ylimManual{2});
    leg(iBias) = legend(barsHandle,barClusterParameter(iBias).paramLegend);
end
%%
print(figAllBias,'-dpdf',fullfile(projFigureFolder, ['vofConcBias_AccPrecAll' fileNameNoise '.pdf']));
saveas(figAllBias,fullfile(projFigureFolder, ['vofConcBias_AccPrecAll' fileNameNoise '.fig']),'fig');
export_fig(figAllBias,fullfile(projFigureFolder, ['vofConcBias_AccPrecAll' fileNameNoise '.png']),'-transparent','-m2');

print(figClusterBias,'-dpdf',fullfile(projFigureFolder, ['vofConcBias_AccPrecCluster' fileNameNoise '.pdf']));
saveas(figClusterBias,fullfile(projFigureFolder, ['vofConcBias_AccPrecCluster' fileNameNoise '.fig']),'fig');
export_fig(figClusterBias,fullfile(projFigureFolder, ['vofConcBias_AccPrecCluster' fileNameNoise '.png']),'-transparent','-m2');

%% Save workspace
% close all;
% save(fullfile('E:\boub3207\OneDrive - USherbrooke\LaboProjets\VOFCPX\Article\Workspaces',['simVofData' fileNameNoise '.mat']));