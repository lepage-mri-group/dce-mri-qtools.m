function [t1Map, fas, relNormr,t1MapLin, errorInfo] = t1mapvfaqtools(sortedImg,fas,tr,mask,method,varargin)
% 
% T1MAPVFA compute T1 quantitative data from variable flip angle images
% sortedImg : VFA data provided in vector (1 point data) or array 
%   (4D matrix (Nx,Ny,Nz,Nfa)). 
% fas : flip angles vector (degrees).
% tr : single repetition time (ms)
% mask : 2D or 3D logical mask (same size as sortedImg matrix minus the temporal dim).
% method : 'linear' or 'nonlin'
% b1Map :  2D or 3D logical mask (same size as sortedImg matrix minus the temporal dim).
%          Allows RF spoiling correction in 4th dimension
% debug : enable debug visualisation
% Author : Beno�t Bourassa-Moreau
% Date created : 2016-02-09
% Revision : 2018-05-30 - RF spoiling correction added in B1 map
% A function using parallel computing is also available
%% Data preparation
% Get dimension of data
[nX, nY, nZ, nFa] = size(sortedImg);

if tr<4
    warning('TR is smaller than 4. TR must be in ms, check input data.');
end

% Extract varargin
debug = 0;
outputFName = [];
medFilt = false;
if nargin==6
    b1Map = varargin{1};
elseif nargin==7
    b1Map = varargin{1};
    debug = varargin{2}; 
elseif nargin == 8
    b1Map = varargin{1};
    debug = varargin{2}; 
    outputFName = varargin{3};
elseif nargin == 9
    b1Map = varargin{1};
    debug = varargin{2}; 
    outputFName = varargin{3};
    medFilt = varargin{4};
else
    b1Map = ones(nX,nY,nZ,nFa);
end

% Spatially varying FA maps
fasRad = permute(fas(:)*pi/180,[2 3 4 1]);
% If B1 map is a single value. Repeat value for data size
if ((size(b1Map,1)==1)&&(size(b1Map,2)==1))&&(size(b1Map,3)==1) 
    % Single b1Map value
    fasRadSpatial = bsxfun(@times,b1Map*fasRad,ones(nX,nY,nZ));
elseif ((size(b1Map,1)==nX)&&(size(b1Map,2)==nY)&&(size(b1Map,3)==nZ)&&(ndims(b1Map)<4)) 
    % Same b1Map value for all FA
    fasRadSpatial = bsxfun(@times,fasRad,b1Map);
elseif ((size(b1Map,1)==nX)&&(size(b1Map,2)==nY))&&(size(b1Map,3)==nZ)&&(size(b1Map,4)==nFa) % B1 map and RF spoiling correction. b1Map value mapped for all FA
    % Same b1Map value for all FA
    fasRadSpatial = bsxfun(@times,fasRad,b1Map);
else
    warning('Unknown B1 map size!');
end
% Prepare fit data
% Flip angle ratio with B1 map correction
signalOverSinFa = sortedImg./sin(fasRadSpatial);
signalOverTanFa = sortedImg./tan(fasRadSpatial);

mask(any(b1Map==0,4)) = false; % Remove points with B1 map = 0 (flip angle = 0)

%% T1 map calculations
t1MapLin = zeros(nX,nY,nZ);
t1Map = zeros(nX,nY,nZ);
relNormr = zeros(nX,nY,nZ);
relNormrNl = zeros(nX,nY,nZ);

% Not for lsqcurvefit
% Options = optimset('lsqcurvefit');
% Options = optimset(Options,'TolFun',1e-8, 'TolX', 1e-8,'Display', 'Off');
Options = optimoptions('lsqcurvefit','Algorithm','trust-region-reflective','Display','Off');

signalToM0Factor = 30;
lb = [0 0];
ub = [10000 inf];

nFit = 20;

fit(1).label = 'linear';
fit(1).ydata = zeros(nX,nY,nZ,nFit);
if strcmpi(method,'nonlin')
    fit(2).label = 'nonlin';
    fit(2).xdata = linspace(min(fasRad),max(fasRad),nFit);
    fit(1).xdata = fit(2).xdata;
    
    fit(2).ydata = zeros(nX,nY,nZ,nFit);
else
    fit(1).xdata = linspace(min(signalOverTanFa(mask)),max(signalOverTanFa(mask)),nFit);
end

iPoint = 0; nPoints = sum(mask(:));
h = waitbar(iPoint/nPoints,'Voxelwise T1 value fitting...');
fun = @(x,fa)myfun(x,fa,tr);
errorInfo.outlier = false(nX,nY,nZ);
errorInfo.M0 = zeros(nX,nY,nZ);
for iX = 1:nX
    for iY = 1:nY
        for iZ = 1:nZ
            if mask(iX,iY,iZ)==1
                % Linear T1 map calculation
                iPoint = iPoint + 1;
                waitbar(iPoint/nPoints,h);
                x = squeeze(signalOverTanFa(iX,iY,iZ,:));
                y = squeeze(signalOverSinFa(iX,iY,iZ,:));
                [p,s]= polyfit(x,y,1);
                relNormr(iX,iY,iZ) = s.normr(1)/sqrt(sum(y.^2)); % percent of fit data
                t1MapLin(iX,iY,iZ) = -tr/log(p(1));
                if strcmpi(method,'nonlin')
                    sVsFa = squeeze(sortedImg(iX,iY,iZ,:))';
                    x0(1) = t1MapLin(iX,iY,iZ);
                    x0(2) = p(2)/(1-exp(-tr/x0(1)));
                    
                    % Verify linear inputs
                    [maxS, maxSIdx] = max(sVsFa);
                    if (~isreal(x0(1))) || (x0(1)>ub(1)) || (x0(1)<lb(1))
                        % MAR initialization. T1 from approx : max signal is at Ernst angle
                        x0(1) = -tr/log(cos(fasRadSpatial(iX,iY,iZ,maxSIdx)));
                        x0(1) = 1e3;
                        if ~isreal(x0(1))
                            x0(1) = 2e3; % Set it to a "normal" value
                        elseif x0(1)>ub(1)
                            x0(1) = ub(1);
                        elseif x0(1)<lb(1)
                            x0(1) = lb(1);
                        end
                    end
                    if (~isreal(x0(2))) || (x0(2)>2*signalToM0Factor*maxS) || (x0(2)<maxS)
                        % MAR initialization. M0 from max signal 
                        x0(2) = maxS*(1-cos(fasRadSpatial(iX,iY,iZ,maxSIdx))*exp(-tr/x0(1))) ...
                            /((1-exp(-tr/x0(1)))*sin(fasRadSpatial(iX,iY,iZ,maxSIdx)));
                        if (~isreal(x0(2))) || (x0(2)>2*signalToM0Factor*maxS) || (x0(2)<maxS)
                            x0(2) = signalToM0Factor*maxS;
                        end
                    end
                    lb(2) = maxS;
                    ub(2) = 10*x0(2);
                    % Compute least square
                    [fitParams,normr] = lsqcurvefit(fun,x0,squeeze(fasRadSpatial(iX,iY,iZ,:))',sVsFa,lb,ub,Options);
                    %                         fitParams = x0;
                    %                         normr = s.normr(1);
                    %                         %fprintf('Error for voxel (%d,%d,%d)\n',iX,iY,iZ);
                    %                         errorInfo.outlier(iX,iY,iZ) = true;
                    
                    if isempty(normr)
                        relNormrNl(iX,iY,iZ) = 0;
                    else
                        relNormrNl(iX,iY,iZ) = normr(1)/sqrt(sum(sVsFa.^2));  % percent of fit data
                    end
                    if isreal(fitParams(1))
                        t1Map(iX,iY,iZ) = fitParams(1);%-tr/log(fitParams(1));
                        errorInfo.M0(iX,iY,iZ) = fitParams(2);
                    elseif isreal(t1MapLin(iX,iY,iZ))
                        t1Map(iX,iY,iZ) = t1MapLin(iX,iY,iZ);
                        errorInfo.M0(iX,iY,iZ) = p(2)/(1-exp(-tr/t1MapLin(iX,iY,iZ)));
                    else
                        t1Map(iX,iY,iZ) = 0;
                        errorInfo.M0(iX,iY,iZ) = 0;
                        errorInfo.outlier(iX,iY,iZ) = true;
                    end
                else
                    t1Map(iX,iY,iZ) = t1MapLin(iX,iY,iZ);
                    errorInfo.M0(iX,iY,iZ) = p(2)/(1-exp(-tr/t1MapLin(iX,iY,iZ)));
                end
                
                if debug==1
                    if strcmpi(method,'nonlin')
                        fit(1).ydata(iX,iY,iZ,:) = myfun(x0,fit(1).xdata,tr);
                        fit(2).ydata(iX,iY,iZ,:) = myfun(fitParams,fit(1).xdata,tr);
                    else
                        fit(1).ydata(iX,iY,iZ,:) = polyval(p,fit(1).xdata);
                    end
                end
            end
        end
    end
end
close(h);

if debug==1
    fprintf('Left-Click on one point to check the fit.\n Middle-click to move through slices:\n   - left side of img, one slice down\n   - right side of img, one slide up\n CTRL-click or right-click when done\n');
    t1MapDebug = t1Map.*mask;
    t1MapDebug(isinf(t1Map)) = ub(1);
    t1MapDebug(isnan(t1Map)) = 0;
    t1MapDebug(imag(t1Map)~=0) = 0;
    if strcmpi(method,'nonlin')
        fit(2).param = t1MapDebug;
        fit(2).resnorm = relNormrNl;
        debugLabels = {'T_1 (s)','Signal (a.u.)','FA (rad)'};
        plotdebugfitdata(t1MapDebug,...
            sortedImg(:,:,:,:),fasRad, debugLabels,...
            fit);
    elseif strcmpi(method,'linear')
        fit(1).param = t1MapDebug;
        fit(1).resnorm = relNormr;
        debugLabels = {'T_1 (s)','Signal over sin(F.A.) (a.u)','Signal over tan(F.A.) (a.u.)'};
        plotdebugfitdata(t1MapDebug,...
            signalOverSinFa(:,:,:,:),signalOverTanFa(:,:,:,:), debugLabels,...
            fit);
    end
end

% Median filtering to improve map
if medFilt
    t1MapVfaFiltered = NaN(nX,nY,nZ);
    for iSlice = 1:nZ
        t1MapVfaFiltered(:,:,iSlice) = medfilt2(real(t1Map(:,:,iSlice)),[3 3]);
    end
    
    fitErrorFiltered = (imag(t1MapVfaFiltered)~=0 |t1MapVfaFiltered<0 | t1MapVfaFiltered>10000 | isnan(t1MapVfaFiltered) | isinf(t1MapVfaFiltered));
    
    t1MapVfaFiltered(fitErrorFiltered) = 0;
    
    t1Map = t1MapVfaFiltered;
    errorInfo.outlier = fitErrorFiltered;
end

% Save fit data
if ~isempty(outputFName)
    % Save mat file of openned dicom
    save(outputFName,'t1Map', 'fas', 'relNormr','t1MapLin', 'fitError');
end

end

% Old function used with weighting
% function F = myfunW(x,fa,tr,weightFit)
% % F = x(2)*(1-x(1))./(1-cos(fa)*x(1)).*sin(fa);
% F = weightFit.*(x(2)*(1-exp(-tr/x(1)))./(1-cos(fa)*exp(-tr/x(1))).*sin(fa));
% end
function F = myfun(x,fa,tr)
% F = x(2)*(1-x(1))./(1-cos(fa)*x(1)).*sin(fa);
F = (x(2)*(1-exp(-tr/x(1)))./(1-cos(fa)*exp(-tr/x(1))).*sin(fa));
end
