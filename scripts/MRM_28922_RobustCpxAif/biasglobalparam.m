function [barAllParameter,barClusterParameter,figAifPvcmSaturation] = biasglobalparam(time,aifConc,artPveNoBias,artPve,t0,methodNames,noBiasValue,biasValues,legendUnits,aifLim)
[nVarParams,nMethods] = size(artPve);
nLegend = length(biasValues);
%% Compute global parameters
[~,peakIdx] = max(aifConc);
processingTime = time> t0 & time < (t0 + 40/60);
nTProc = sum(processingTime);
tTailIdx = find(aifConc(peakIdx:end)<2,1,'first')+(peakIdx-1);
tailProcessingTime = time> time(tTailIdx) & time < (t0 + 40/60);
nTTailProc = sum(tailProcessingTime);
    
barAllParameter.peakDiff = NaN(nVarParams+1,nMethods);
barAllParameter.peakDiffError = NaN(nVarParams+1,nMethods);
barAllParameter.tailDiff = NaN(nVarParams+1,nMethods);
barAllParameter.tailDiffError = NaN(nVarParams+1,nMethods);
barAllParameter.rmsd = NaN(nVarParams+1,nMethods);
barAllParameter.rmsdError = NaN(nVarParams+1,nMethods);
barAllParameter.rmsdTail = NaN(nVarParams+1,nMethods);
barAllParameter.rmsdTailError = NaN(nVarParams+1,nMethods);
barAllParameter.accuracy = NaN(nVarParams+1,nMethods);
barAllParameter.accuracyError = NaN(nVarParams+1,nMethods);
barAllParameter.precision = NaN(nVarParams+1,nMethods);
barAllParameter.paramLegend = cell(1,nLegend+1);
barAllParameter.paramXTick = cell(1,nMethods);
% Prepare legend
if ~isempty(noBiasValue)
    if isnumeric(noBiasValue)
        barAllParameter.paramLegend{1} = ['No bias (' num2str(noBiasValue) legendUnits ')'];
    elseif ischar(noBiasValue)
        barAllParameter.paramLegend{1} = ['No bias (' noBiasValue legendUnits ')'];
    end
else
    barAllParameter.paramLegend{1} = 'No bias';
end
for iParam = 1:nLegend
    if isnumeric(biasValues)
        barAllParameter.paramLegend{iParam+1} = [num2str(biasValues(iParam)) legendUnits];
    elseif ischar(biasValues{iParam})
        barAllParameter.paramLegend{iParam+1} = [biasValues{iParam} legendUnits];
    else
        barAllParameter.paramLegend{iParam+1} = legendUnits;
    end
end

for iMethods = 1:nMethods
    barAllParameter.peakDiff(1,iMethods) = artPveNoBias{iMethods}.all.mean(peakIdx) - aifConc(peakIdx);
    barAllParameter.peakDiffError(1,iMethods) = artPveNoBias{iMethods}.all.std(peakIdx);
    
    barAllParameter.tailDiff(1,iMethods) = mean(artPveNoBias{iMethods}.all.mean(tailProcessingTime)) - mean(aifConc(tailProcessingTime));
    barAllParameter.tailDiffError(1,iMethods) = mean(artPveNoBias{iMethods}.all.std(tailProcessingTime));
    
    nPx = size(artPveNoBias{iMethods}.all.concList,1);
    
    repeatedRefConc = repmat(reshape(aifConc(processingTime),[1,nTProc]),[nPx,1]);
    allSquaredDiff = sqrt(sum((artPveNoBias{iMethods}.all.concList(:,processingTime)  - repeatedRefConc).^2/nTProc,2));
    barAllParameter.rmsd(1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barAllParameter.rmsdError(1,iMethods) = std(allSquaredDiff);
    
    repeatedRefTailConc = repmat(reshape(aifConc(tailProcessingTime),[1,nTTailProc]),[nPx,1]);
    allSquaredDiff = sqrt(sum((artPveNoBias{iMethods}.all.concList(:,tailProcessingTime)  - repeatedRefTailConc).^2/nTTailProc,2));
    barAllParameter.rmsdTail(1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barAllParameter.rmsdTailError(1,iMethods) = std(allSquaredDiff);
    
    allRelativeDiff = mean(abs(artPveNoBias{iMethods}.all.concList(:,processingTime)  - repeatedRefConc)./repeatedRefConc,2);
    barAllParameter.accuracy(1,iMethods) = mean(allRelativeDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barAllParameter.accuracyError(1,iMethods) = std(allRelativeDiff);
    barAllParameter.precision(1,iMethods) = mean(artPveNoBias{iMethods}.all.std(processingTime)./(aifConc(processingTime)));
end


for iMethods = 1:nMethods
    barAllParameter.paramXTick{iMethods} = methodNames{iMethods};
    for iParam = 1:nVarParams
        barAllParameter.peakDiff(iParam+1,iMethods) = artPve{iParam,iMethods}.all.mean(peakIdx) - aifConc(peakIdx);
        barAllParameter.peakDiffError(iParam+1,iMethods) = artPve{iParam,iMethods}.all.std(peakIdx);
        
        barAllParameter.tailDiff(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.all.mean(tailProcessingTime)) - mean(aifConc(tailProcessingTime));
        barAllParameter.tailDiffError(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.all.std(tailProcessingTime));
        
        nPx = size(artPve{iParam,iMethods}.all.concList,1);
        
        repeatedRefConc = repmat(reshape(aifConc(processingTime),[1,nTProc]),[nPx,1]);
        allSquaredDiff = sqrt(sum((artPve{iParam,iMethods}.all.concList(:,processingTime)  - repeatedRefConc).^2/nTProc,2));
        barAllParameter.rmsd(iParam+1,iMethods) = mean(allSquaredDiff);%mean(abs(artPve{iBf,iValBias}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barAllParameter.rmsdError(iParam+1,iMethods) = std(allSquaredDiff);
        
        repeatedRefTailConc = repmat(reshape(aifConc(tailProcessingTime),[1,nTTailProc]),[nPx,1]);
        allSquaredDiff = sqrt(sum((artPve{iParam,iMethods}.all.concList(:,tailProcessingTime)  - repeatedRefTailConc).^2/nTTailProc,2));
        barAllParameter.rmsdTail(iParam+1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barAllParameter.rmsdTailError(iParam+1,iMethods) = std(allSquaredDiff);
        
        allRelativeDiff = mean(abs(artPve{iParam,iMethods}.all.concList(:,processingTime)  - repeatedRefConc)./repeatedRefConc,2);
        barAllParameter.accuracy(iParam+1,iMethods) = mean(allRelativeDiff);%mean(abs(artPve{iBf,iValBias}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barAllParameter.accuracyError(iParam+1,iMethods) = std(allRelativeDiff);
        barAllParameter.precision(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.all.std(processingTime)./(aifConc(processingTime)));
    end
end
%% Compute clustered parameters

barClusterParameter.peakDiff = NaN(nVarParams+1,nMethods);
barClusterParameter.peakDiffError = NaN(nVarParams+1,nMethods);
barClusterParameter.tailDiff = NaN(nVarParams+1,nMethods);
barClusterParameter.tailDiffError = NaN(nVarParams+1,nMethods);
barClusterParameter.rmsd = NaN(nVarParams+1,nMethods);
barClusterParameter.rmsdError = NaN(nVarParams+1,nMethods);
barClusterParameter.accuracy = NaN(nVarParams+1,nMethods);
barClusterParameter.accuracyError = NaN(nVarParams+1,nMethods);
barClusterParameter.precision = NaN(nVarParams+1,nMethods);
barClusterParameter.paramLegend = cell(1,nMethods);
barClusterParameter.paramXTick = cell(1,nVarParams+1);

% Assign legend
barClusterParameter.paramLegend = barAllParameter.paramLegend;
barClusterParameter.paramXTick =  barAllParameter.paramXTick;

for iMethods = 1:nMethods
    barClusterParameter.peakDiff(1,iMethods) = artPveNoBias{iMethods}.cluster.mean(peakIdx) - aifConc(peakIdx);
    barClusterParameter.peakDiffError(1,iMethods) = artPveNoBias{iMethods}.cluster.std(peakIdx);
    
    barClusterParameter.tailDiff(1,iMethods) = mean(artPveNoBias{iMethods}.cluster.mean(tailProcessingTime)) - mean(aifConc(tailProcessingTime));
    barClusterParameter.tailDiffError(1,iMethods) = mean(artPveNoBias{iMethods}.cluster.std(tailProcessingTime));
    
    nPx = size(artPveNoBias{iMethods}.cluster.concList,1);
    
    repeatedRefConc = repmat(reshape(aifConc(processingTime),[1,nTProc]),[nPx,1]);
    allSquaredDiff = sqrt(sum((artPveNoBias{iMethods}.cluster.concList(:,processingTime)  - repeatedRefConc).^2/nTProc,2));
    barClusterParameter.rmsd(1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.cluster.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barClusterParameter.rmsdError(1,iMethods) = std(allSquaredDiff);
    
    repeatedRefTailConc = repmat(reshape(aifConc(tailProcessingTime),[1,nTTailProc]),[nPx,1]);
    allSquaredDiff = sqrt(sum((artPveNoBias{iMethods}.cluster.concList(:,tailProcessingTime)  - repeatedRefTailConc).^2/nTProc,2));
    barClusterParameter.rmsdTail(1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barClusterParameter.rmsdTailError(1,iMethods) = std(allSquaredDiff);
    
    allRelativeDiff = mean(abs(artPveNoBias{iMethods}.cluster.concList(:,processingTime)  - repeatedRefConc)./repeatedRefConc,2);
    barClusterParameter.accuracy(1,iMethods) = mean(allRelativeDiff);%mean(abs(artPveNoBias{iMethods}.cluster.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
    barClusterParameter.accuracyError(1,iMethods) = std(allRelativeDiff);
    barClusterParameter.precision(1,iMethods) = mean(artPveNoBias{iMethods}.cluster.std(processingTime)./(aifConc(processingTime)));
end


for iMethods = 1:nMethods
    barClusterParameter.paramXTick{iMethods} = methodNames{iMethods};
    for iParam = 1:nVarParams
        barClusterParameter.peakDiff(iParam+1,iMethods) = artPve{iParam,iMethods}.cluster.mean(peakIdx) - aifConc(peakIdx);
        barClusterParameter.peakDiffError(iParam+1,iMethods) = artPve{iParam,iMethods}.cluster.std(peakIdx);
        
        barClusterParameter.tailDiff(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.cluster.mean(tailProcessingTime)) - mean(aifConc(tailProcessingTime));
        barClusterParameter.tailDiffError(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.cluster.std(tailProcessingTime));
        
        nPx = size(artPve{iParam,iMethods}.cluster.concList,1);
        
        repeatedRefConc = repmat(reshape(aifConc(processingTime),[1,nTProc]),[nPx,1]);
        allSquaredDiff = sqrt(sum((artPve{iParam,iMethods}.cluster.concList(:,processingTime)  - repeatedRefConc).^2/nTProc,2));
        barClusterParameter.rmsd(iParam+1,iMethods) = mean(allSquaredDiff);%mean(abs(artPve{iBf,iValBias}.cluster.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barClusterParameter.rmsdError(iParam+1,iMethods) = std(allSquaredDiff);
        
        repeatedRefTailConc = repmat(reshape(aifConc(tailProcessingTime),[1,nTTailProc]),[nPx,1]);
        allSquaredDiff = sqrt(sum((artPve{iParam,iMethods}.cluster.concList(:,tailProcessingTime)  - repeatedRefTailConc).^2/nTProc,2));
        barClusterParameter.rmsdTail(iParam+1,iMethods) = mean(allSquaredDiff);%mean(abs(artPveNoBias{iMethods}.all.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barClusterParameter.rmsdTailError(iParam+1,iMethods) = std(allSquaredDiff);
        
        allRelativeDiff = mean(abs(artPve{iParam,iMethods}.cluster.concList(:,processingTime)  - repeatedRefConc)./repeatedRefConc,2);
        barClusterParameter.accuracy(iParam+1,iMethods) = mean(allRelativeDiff);%mean(abs(artPve{iBf,iValBias}.cluster.mean(processingTime) - aifConc(processingTime))./(aifConc(processingTime)));
        barClusterParameter.accuracyError(iParam+1,iMethods) = std(allRelativeDiff);
        barClusterParameter.precision(iParam+1,iMethods) = mean(artPve{iParam,iMethods}.cluster.std(processingTime)./(aifConc(processingTime)));
    end
end
%% Display conc calculation
gap = 0.01;
marg_h = 0.08;
marg_w = 0.07;
displayTime = time>aifLim(1) & time < (aifLim(2) + time(2)-time(1));
[aifPeak,~] = max(aifConc);

figAifPvcmSaturation = figure;
figAifPvcmSaturation.Position = [609   346   665   520];
for iMethods = 1:nMethods
    for iParam = 1:(nVarParams+1)
        ax3(iParam) = subtightplot(nMethods,nVarParams+1,(nVarParams+1)*(iMethods-1)+iParam,gap,marg_h,marg_w);
        if iParam == 1
            area(time(displayTime),squeeze(artPveNoBias{iMethods}.all.mean(displayTime)+2*artPveNoBias{iMethods}.all.std(displayTime)),'EdgeColor',[0 0 1],'FaceColor',0.7*[1 1 1]);
            hold on;
            area(time(displayTime),squeeze(artPveNoBias{iMethods}.all.mean(displayTime)-2*artPveNoBias{iMethods}.all.std(displayTime)),'EdgeColor',[0 0 1],'FaceColor',[1 1 1]);
%             plot(time(displayTime),squeeze(artPveNoBias{iMethods}.all.mean(displayTime)+2*artPveNoBias{iMethods}.all.std(displayTime)),'b-','Linewidth',2);
%             plot(time(displayTime),squeeze(artPveNoBias{iMethods}.all.mean(displayTime)-2*artPveNoBias{iMethods}.all.std(displayTime)),'b-','Linewidth',2);
%             errorbar(time(displayTime),squeeze(artPveNoBias{iMethods}.all.mean(displayTime)),squeeze(artPveNoBias{iMethods}.all.std(displayTime)),squeeze(artPveNoBias{iMethods}.all.std(displayTime)),'r-');xlim(aifLim);
%             plot(time(displayTime),artPveNoBias{iMethods}.cluster.mean(displayTime),'b--','Linewidth',1);
        else
            p2 = area(time(displayTime),squeeze(artPve{iParam-1,iMethods}.all.mean(displayTime)+2*artPve{iParam-1,iMethods}.all.std(displayTime)),'EdgeColor',[0 0 1],'FaceColor',0.7*[1 1 1]);
            hold on;
            area(time(displayTime),squeeze(artPve{iParam-1,iMethods}.all.mean(displayTime)-2*artPve{iParam-1,iMethods}.all.std(displayTime)),'EdgeColor',[0 0 1],'FaceColor',[1 1 1]);
%             plot(time(displayTime),squeeze(artPve{iParam-1,iMethods}.all.mean(displayTime)+2*artPve{iParam-1,iMethods}.all.std(displayTime)),'b-','Linewidth',2);
%             plot(time(displayTime),squeeze(artPve{iParam-1,iMethods}.all.mean(displayTime)-2*artPve{iParam-1,iMethods}.all.std(displayTime)),'b-','Linewidth',2);
%             errorbar(time(displayTime),squeeze(artPve{iParam-1,iMethods}.all.mean(displayTime)),squeeze(artPve{iParam-1,iMethods}.all.std(displayTime)),squeeze(artPve{iParam-1,iMethods}.all.std(displayTime)),'r-');xlim(aifLim);
%             plot(time(displayTime),artPve{iParam-1,iMethods}.cluster.mean(displayTime),'b--','Linewidth',1);
        end
        plot(aifLim,[0 0],'k-','Linewidth',1)
        plot(aifLim,aifPeak*1.3*[1 1],'k-','Linewidth',1)
        p1 = plot(time(displayTime),aifConc(displayTime),'k-','Linewidth',2);
        hold off;
        if iParam == 1
            ylabel('Conc. (mM)');
        else
            ax3(iParam).YTickLabel = [];
        end
        set(ax3(iParam),'FontName','Arial','FontSize',12,'FontWeight','Bold',  'LineWidth', 1);
        ylim([0 aifPeak*1.3]);
        xlim(aifLim);
    end
    if iMethods==1
        for iParam = 1:(nVarParams+1)
            title(ax3(iParam),barAllParameter.paramLegend{iParam});
            ax3(iParam).XTickLabel = [];
        end
    elseif iMethods == nMethods
        for iParam = 1:(nVarParams+1)
            xlabel(ax3(iParam),'Time (min)');
        end
    else
        for iParam = 1:(nVarParams+1)
            ax3(iParam).XTickLabel = [];
        end
    end
end
% legend([p1 p2],'True','Data','Location','Best');

