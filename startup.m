addpath(genpath(fileparts(mfilename('fullpath'))));

fprintf('Paths for dce-mri-qtools.m were added.\n');
fprintf('This toolbox offers command line based tools for quantiative DCE MRI.\n');
fprintf('Function usage can be found by looking into their UnitTests or with the example scripts in the ''tests'' subfolder.\n');