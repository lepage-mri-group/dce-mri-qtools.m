% Saving the figures produced in .fig and .png format

% Handle of the figures to save (the number of the figure is also its
% handle)
figures = [1 2 3 4];

% Filename for each figure
names = {'F_avant','F_apr�s','spectres_avant','spectres_apr�s'};

% Folder where to create a "R�sultats" folder to save the figure in
folderPrefix = 'K:\Docs\Doc\Projets\2015-06-18 - Syst�me champ large 3\2015-08-31 - Billes rouges_systeme repositionne\';

% Save the figure in .png and .fig
for i = 1:length(figures)
    export_fig(figures(i),[folderPrefix 'R�sultats\' names{i} '.png']);
    saveas(figures(i),[folderPrefix 'R�sultats\' names{i} '.fig'],'fig')
end
